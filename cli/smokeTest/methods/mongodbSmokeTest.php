<?php

/**
 * @后天添加不同的错误返回id
 */
function mongodbSmokeTest(
    string $mongoHost,
    array $uriOptions,
    array $driverOptions
) {
    $result = false;

    try {
        $mongoDriver = new \MongoDB\Client(
            $mongoHost,
            $uriOptions,
            $driverOptions
        );
        $result = true;
    } catch (MongoDB\Exception\InvalidArgumentException $excepion) {
        $result = false;
    } catch (MongoDB\Driver\Exception\InvalidArgumentException $excepion) {
        $result = false;
    } catch (MongoDB\Driver\Exception\RuntimeException $excepion) {
        $result = false;
    }
    return $result;
}
