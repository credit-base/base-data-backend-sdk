<?php

function mysqlSmokeTest($dsn, $user, $password)
{
    $result = false;

    try {
        $dbh = new PDO($dsn, $user, $password);
        $result = true;
    } catch (PDOException $e) {
        $result = false;
    }

    return $result;
}
