<?php

function httpSmokeTest(string $url)
{
    $client = new GuzzleHttp\Client([
        'timeout' => 1.0,
        'http_errors'=>false,
    ]);

    $statusCode = 0;

    try {
        $response = $client->get(
            $url,
            [
                'headers'=>[
                    'Accept-Encoding' => 'gzip'
                ]
            ]
        );

        $statusCode = $response->getStatusCode();
    } catch (GuzzleHttp\Exception\RequestException $e) {
        $statusCode = 500;
    }
    
    return $statusCode == 200;
}
