<?php

class MysqlApi implements ISmokeTest
{
    public function smokeTest() : bool
    {
        $host = \Marmot\Core::$container->get('database.host');
        $port = \Marmot\Core::$container->get('database.port');
        $dbname = \Marmot\Core::$container->get('database.dbname');
        $user = \Marmot\Core::$container->get('database.user');
        $password = \Marmot\Core::$container->get('database.password');

        return mysqlSmokeTest(
            $dsn = "mysql:host={$host};port={$port};dbname={$dbname}",
            $user,
            $password
        );
    }
}
