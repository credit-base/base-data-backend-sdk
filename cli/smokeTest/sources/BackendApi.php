<?php

class backendApi implements ISmokeTest
{
    public function smokeTest() : bool
    {
        return httpSmokeTest(\Marmot\Core::$container->get('services.backend.url'));
    }
}
