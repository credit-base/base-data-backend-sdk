<?php
include_once 'ISmokeTest.php';

include_once 'methods/httpSmokeTest.php';
include_once 'methods/fluentdSmokeTest.php';
include_once 'methods/mongodbSmokeTest.php';
include_once 'methods/mysqlSmokeTest.php';

$sources = include_once 'sources.php';

$result = 0;

foreach ($sources as $each) {
    $name = $each['name'];
    $file = $each['file'];

    include_once 'sources/'.$file.'.php';
    $smokeTest = new $file();
    $smokeResult = $smokeTest->smokeTest();

    if ($smokeResult) {
        runSucess($name);
    } else {
        runFail($name);
        $result = 1;
    }
}

exit($result);

