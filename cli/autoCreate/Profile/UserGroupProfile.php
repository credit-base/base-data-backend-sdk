<?php

/**
 * 支持rule: 
 * 1.cellphone: 手机
 * 2.qq: QQ号码
 * 3.email: 邮箱
 * 4.time: 时间
 * 5.数组: 状态码
 * 6.object: 对象
 * 7.string: 字符串
 */

return [
	'className' => 'UserGroup',
	'nameSpace' => 'UserGroup\Model',
	'comment' => '单位领域对象',
	'parameters' => [['key'=>'id','type'=>'int','rule'=>'int','default'=>0,'comment'=>'单位id'],
					 ['key'=>'name','type'=>'string','rule'=>'string','default'=>'','comment'=>'单位名称'],
					 /*['key'=>'cellphone','type'=>'string','rule'=>'cellphone','default'=>'','comment'=>'用户手机号'],
					 ['key'=>'qq','type'=>'string','rule'=>'qq','default'=>'','comment'=>'用户qq'],
					 ['key'=>'email','type'=>'string','rule'=>'email','default'=>'','comment'=>'用户邮箱'],*/
					 ['key'=>'createTime','type'=>'int','rule'=>'time','default'=>'Core::'.'$'."container->get('time')",'comment'=>'创建时间'],
					 ['key'=>'status','type'=>'int','rule'=>'int','default'=>0,'comment'=>'状态'],
					 //['key'=>'district','type'=>'Area\Model\Area','rule'=>'object','default'=>'','comment'=>'用户住址区']
					]
];
