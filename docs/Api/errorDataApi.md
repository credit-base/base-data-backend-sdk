# 失败数据接口文档

---

## 简介

本文档主要描述失败数据模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [登录](#登录)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用字典](./docs/Dictionary/common.md "通用字典")
    * [失败数据字典](./docs/Dictionary/errorData.md "失败数据字典")
    * [任务字典](./docs/Dictionary/task.md "任务字典")
    * [目录字典](./docs/Dictionary/template.md "目录字典")
* <a name="控件规范">控件规范</a>
* <a name="错误规范">错误规范</a>
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=task,template
2、fields[TYPE]请求参数
    2.1 fields[errorDatas]
    2.2 fields[tasks]
    2.3 fields[templates]
    2.4 fields[gbTemplates]
    2.5 fields[bjTemplates]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'errorDatas/1?fields[errorDatas]=category,template',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/errorDatas/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'errorDatas/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/errorDatas/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'errorDatas/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/errorDatas

1、检索条件
    1.1 filter[task] | 根据 任务id 搜索
    1.2 filter[category] | 根据 库类别 搜索
    1.3 filter[template] | 根据 目录id 搜索
2、排序
    2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
    2.2 sort=-id | -id 根据id倒序 | id 根据id正序
```

示例

```php
$response = $client->request('GET', 'errorDatas?filter[category]=1&sort=-updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

    {
        "meta": [],
        "data": {
            "type": "errorDatas",
            "id": "1",
            "attributes": {
                "category": 10,
                "itemsData": {
                    "ZTMC": "四川瑞煜建筑有限公司信阳分公司",
                    "XK_XDR_LB": "法人及非法人组织",
                    "TYSHXYDM": "91411500MA9F86JG96",
                    "XK_XDR_GSZC": "",
                    "XK_XDR_ZZJG": "91292",
                    "XK_XDR_SWDJ": "",
                    "XK_XDR_SYDW": "",
                    "XK_XDR_SHZZ": "MA9F86JG-9",
                    "XK_FRDB": "王灿国",
                    "XK_FR_ZJLX": "",
                    "XK_FR_ZJHM": "",
                    "XK_XDR_ZJLX": "",
                    "XK_XDR_ZJHM": "",
                    "XK_XKWS": "企业注册登记",
                    "XK_WSH": "411596000077514",
                    "XK_XKLB": "登记",
                    "XK_XKZS": "企业注册登记",
                    "XK_XKBH": "410000017210705A000092",
                    "XK_NR": "企业注册登记",
                    "XK_JDRQ": "20210715",
                    "XK_YXQZ": "2020-06-04",
                    "XK_YXQZI": "73050",
                    "XK_XKJG": "信阳市市场监督管理局专业分局",
                    "XK_XKJGDM": "11411500747429737F",
                    "XK_ZT": "1",
                    "XK_LYDW": "信阳市发展和改革委员会1",
                    "XK_LYDWDM": "11411500006067580U",
                    "BZ": "河南省高速公路联网管理中心",
                    "GQSJ": "20210816",
                    "ZTLB": "法人及非法人组织",
                    "GKFW": "社会公开",
                    "GXPL": "实时",
                    "XXFL": "行政许可",
                    "XXLB": "基础信息"
                },
                "errorType": 8,
                "errorReason": {
                    "XK_YXQZ": [
                        "8"
                    ],
                    "XK_YXQZI": [
                        "8"
                    ],
                    "XK_FR_ZJHM": [
                        "8"
                    ],
                    "XK_FR_ZJLX": [
                        "8"
                    ],
                    "XK_XDR_GSZC": [
                        "8"
                    ],
                    "XK_XDR_SWDJ": [
                        "8"
                    ],
                    "XK_XDR_SYDW": [
                        "8"
                    ],
                    "XK_XDR_ZJHM": [
                        "8"
                    ],
                    "XK_XDR_ZJLX": [
                        "8"
                    ]
                },
                "status": 0,
                "createTime": 1629871256,
                "updateTime": 1629871256,
                "statusTime": 0
            },
            "relationships": {
                "task": {
                    "data": {
                        "type": "tasks",
                        "id": "1"
                    }
                },
                "template": {
                    "data": {
                        "type": "templates",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8089/errorDatas/1"
            }
        },
        "included": [
            {
                "type": "tasks",
                "id": "1",
                "attributes": {
                    "pid": 0,
                    "total": 5,
                    "successNumber": 3,
                    "failureNumber": 2,
                    "sourceCategory": 10,
                    "sourceTemplate": 1,
                    "targetCategory": 10,
                    "targetTemplate": 1,
                    "targetRule": 1,
                    "scheduleTask": 1920,
                    "errorNumber": 0,
                    "fileName": "FAILURE_10_1_10_HZXKXX_1_10_1_2323fff.xlsx",
                    "status": -3,
                    "createTime": 1629871256,
                    "updateTime": 1629871256,
                    "statusTime": 1629871273
                },
                "relationships": {
                    "crew": {
                        "data": {
                            "type": "crews",
                            "id": "1"
                        }
                    },
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            },
            {
                "type": "templates",
                "id": "1",
                "attributes": {
                    "name": "行政许可信息",
                    "identify": "HZXKXX",
                    "subjectCategory": [
                        "1",
                        "3"
                    ],
                    "dimension": 1,
                    "exchangeFrequency": 1,
                    "infoClassify": "1",
                    "infoCategory": "1",
                    "description": "行政许可信息",
                    "category": 10,
                    "items": [
                        {
                            "name": "行政相对人名称",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "信用主体的法定名称",
                            "identify": "ZTMC",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人类别",
                            "type": "1",
                            "length": "16",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_LB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_1(统一社会信用代码)",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "TYSHXYDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_2 (工商注册号)",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_GSZC",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_3(组织机构代码)",
                            "type": "1",
                            "length": "9",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZZJG",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_4(税务登记号)",
                            "type": "1",
                            "length": "15",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SWDJ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_5(事业单位证书号)",
                            "type": "1",
                            "length": "12",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SYDW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_6(社会组织登记证号)",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SHZZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FRDB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人证件类型",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FR_ZJLX",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人证件号码",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FR_ZJHM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "证件类型",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZJLX",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "证件号码",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZJHM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政许可决定文书名称",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKWS",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政许可决定文书号",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_WSH",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可类别",
                            "type": "1",
                            "length": "256",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可证书名称",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKZS",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可编号",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKBH",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可内容",
                            "type": "1",
                            "length": "4000",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_NR",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可决定日期",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_JDRQ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "有效期自",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_YXQZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "有效期至",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_YXQZI",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可机关",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKJG",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可机关统一社会信用代码",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKJGDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "当前状态",
                            "type": "3",
                            "length": "1",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_ZT",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "数据来源单位",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_LYDW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "数据来源单位统一社会信用代码",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_LYDWDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "备注",
                            "type": "1",
                            "length": "4000",
                            "options": [],
                            "remarks": "",
                            "identify": "BZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "过期时间",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "GQSJ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "主体类别",
                            "type": "6",
                            "length": "50",
                            "options": [
                                "法人及非法人组织",
                                "个体工商户"
                            ],
                            "remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
                            "identify": "ZTLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "公开范围",
                            "type": "5",
                            "length": "20",
                            "options": [
                                "社会公开"
                            ],
                            "remarks": "支持单选",
                            "identify": "GKFW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "更新频率",
                            "type": "5",
                            "length": "20",
                            "options": [
                                "实时"
                            ],
                            "remarks": "支持单选",
                            "identify": "GXPL",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "信息分类",
                            "type": "5",
                            "length": "50",
                            "options": [
                                "行政许可"
                            ],
                            "remarks": "支持单选",
                            "identify": "XXFL",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "信息类别",
                            "type": "5",
                            "length": "50",
                            "options": [
                                "基础信息"
                            ],
                            "remarks": "信息性质类型，支持单选",
                            "identify": "XXLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        }
                    ],
                    "status": 0,
                    "createTime": 1626504022,
                    "updateTime": 1629172736,
                    "statusTime": 0
                },
                "relationships": {
                    "sourceUnit": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            }
        ]
    }

#### <a name="多条示例">多条示例</a>

    {
        "meta": {
            "count": 8,
            "links": {
                "first": 1,
                "last": 4,
                "prev": null,
                "next": 2
            }
        },
        "links": {
            "first": "127.0.0.1:8089/errorDatas/?include=task,templates&page[number]=1&page[size]=2",
            "last": "127.0.0.1:8089/errorDatas/?include=task,templates&page[number]=4&page[size]=2",
            "prev": null,
            "next": "127.0.0.1:8089/errorDatas/?include=task,templates&page[number]=2&page[size]=2"
        },
        "data": [
            {
                "type": "errorDatas",
                "id": "1",
                "attributes": {
                    "category": 10,
                    "itemsData": {
                        "ZTMC": "四川瑞煜建筑有限公司信阳分公司",
                        "XK_XDR_LB": "法人及非法人组织",
                        "TYSHXYDM": "91411500MA9F86JG96",
                        "XK_XDR_GSZC": "",
                        "XK_XDR_ZZJG": "91292",
                        "XK_XDR_SWDJ": "",
                        "XK_XDR_SYDW": "",
                        "XK_XDR_SHZZ": "MA9F86JG-9",
                        "XK_FRDB": "王灿国",
                        "XK_FR_ZJLX": "",
                        "XK_FR_ZJHM": "",
                        "XK_XDR_ZJLX": "",
                        "XK_XDR_ZJHM": "",
                        "XK_XKWS": "企业注册登记",
                        "XK_WSH": "411596000077514",
                        "XK_XKLB": "登记",
                        "XK_XKZS": "企业注册登记",
                        "XK_XKBH": "410000017210705A000092",
                        "XK_NR": "企业注册登记",
                        "XK_JDRQ": "20210715",
                        "XK_YXQZ": "2020-06-04",
                        "XK_YXQZI": "73050",
                        "XK_XKJG": "信阳市市场监督管理局专业分局",
                        "XK_XKJGDM": "11411500747429737F",
                        "XK_ZT": "1",
                        "XK_LYDW": "信阳市发展和改革委员会1",
                        "XK_LYDWDM": "11411500006067580U",
                        "BZ": "河南省高速公路联网管理中心",
                        "GQSJ": "20210816",
                        "ZTLB": "法人及非法人组织",
                        "GKFW": "社会公开",
                        "GXPL": "实时",
                        "XXFL": "行政许可",
                        "XXLB": "基础信息"
                    },
                    "errorType": 8,
                    "errorReason": {
                        "XK_YXQZ": [
                            "8"
                        ],
                        "XK_YXQZI": [
                            "8"
                        ],
                        "XK_FR_ZJHM": [
                            "8"
                        ],
                        "XK_FR_ZJLX": [
                            "8"
                        ],
                        "XK_XDR_GSZC": [
                            "8"
                        ],
                        "XK_XDR_SWDJ": [
                            "8"
                        ],
                        "XK_XDR_SYDW": [
                            "8"
                        ],
                        "XK_XDR_ZJHM": [
                            "8"
                        ],
                        "XK_XDR_ZJLX": [
                            "8"
                        ]
                    },
                    "status": 0,
                    "createTime": 1629871256,
                    "updateTime": 1629871256,
                    "statusTime": 0
                },
                "relationships": {
                    "task": {
                        "data": {
                            "type": "tasks",
                            "id": "1"
                        }
                    },
                    "template": {
                        "data": {
                            "type": "templates",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8089/errorDatas/1"
                }
            },
            {
                "type": "errorDatas",
                "id": "2",
                "attributes": {
                    "category": 10,
                    "itemsData": {
                        "ZTMC": "彭州市衡粤旅游发展有限公司",
                        "XK_XDR_LB": "法人及非法人组织",
                        "TYSHXYDM": "91410900MA46184E8Q",
                        "XK_XDR_GSZC": "",
                        "XK_XDR_ZZJG": "91291",
                        "XK_XDR_SWDJ": "237890014",
                        "XK_XDR_SYDW": "91410901M",
                        "XK_XDR_SHZZ": "MA46184E-7",
                        "XK_FRDB": "张晓苹",
                        "XK_FR_ZJLX": "身份证",
                        "XK_FR_ZJHM": "412825199408025678",
                        "XK_XDR_ZJLX": "身份证",
                        "XK_XDR_ZJHM": "412825199408025678",
                        "XK_XKWS": "高速公路联网管理中心交通行政许可决定",
                        "XK_WSH": "豫高速公路许 字第20210705000090号",
                        "XK_XKLB": "普通",
                        "XK_XKZS": "高速公路超限运输车辆通行证",
                        "XK_XKBH": "410000017210705A000091",
                        "XK_NR": "高速公路超限运输车辆通行证",
                        "XK_JDRQ": "20210715",
                        "XK_YXQZ": "20210715",
                        "XK_YXQZI": "20210808",
                        "XK_XKJG": "河南省高速公路联网管理中心",
                        "XK_XKJGDM": "12410000MB14527980",
                        "XK_ZT": "2",
                        "XK_LYDW": "河南省高速公路联网管理中心",
                        "XK_LYDWDM": "12410000MB14527980",
                        "BZ": "河南省高速公路联网管理中心",
                        "GQSJ": "20210816",
                        "ZTLB": "法人及非法人组织",
                        "GKFW": "社会公开",
                        "GXPL": "实时",
                        "XXFL": "行政许可",
                        "XXLB": "基础信息"
                    },
                    "errorType": 8,
                    "errorReason": {
                        "XK_XDR_GSZC": [
                            "8"
                        ]
                    },
                    "status": 0,
                    "createTime": 1629871256,
                    "updateTime": 1629871256,
                    "statusTime": 0
                },
                "relationships": {
                    "task": {
                        "data": {
                            "type": "tasks",
                            "id": "1"
                        }
                    },
                    "template": {
                        "data": {
                            "type": "templates",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8089/errorDatas/2"
                }
            }
        ],
        "included": [
            {
                "type": "tasks",
                "id": "1",
                "attributes": {
                    "pid": 0,
                    "total": 5,
                    "successNumber": 3,
                    "failureNumber": 2,
                    "sourceCategory": 10,
                    "sourceTemplate": 1,
                    "targetCategory": 10,
                    "targetTemplate": 1,
                    "targetRule": 1,
                    "scheduleTask": 1920,
                    "errorNumber": 0,
                    "fileName": "FAILURE_10_1_10_HZXKXX_1_10_1_2323fff.xlsx",
                    "status": -3,
                    "createTime": 1629871256,
                    "updateTime": 1629871256,
                    "statusTime": 1629871273
                },
                "relationships": {
                    "crew": {
                        "data": {
                            "type": "crews",
                            "id": "1"
                        }
                    },
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            },
            {
                "type": "templates",
                "id": "1",
                "attributes": {
                    "name": "行政许可信息",
                    "identify": "HZXKXX",
                    "subjectCategory": [
                        "1",
                        "3"
                    ],
                    "dimension": 1,
                    "exchangeFrequency": 1,
                    "infoClassify": "1",
                    "infoCategory": "1",
                    "description": "行政许可信息",
                    "category": 10,
                    "items": [
                        {
                            "name": "行政相对人名称",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "信用主体的法定名称",
                            "identify": "ZTMC",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人类别",
                            "type": "1",
                            "length": "16",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_LB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_1(统一社会信用代码)",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "TYSHXYDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_2 (工商注册号)",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_GSZC",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_3(组织机构代码)",
                            "type": "1",
                            "length": "9",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZZJG",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_4(税务登记号)",
                            "type": "1",
                            "length": "15",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SWDJ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_5(事业单位证书号)",
                            "type": "1",
                            "length": "12",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SYDW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政相对人代码_6(社会组织登记证号)",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_SHZZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人",
                            "type": "1",
                            "length": "50",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FRDB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人证件类型",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FR_ZJLX",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "法定代表人证件号码",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_FR_ZJHM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "证件类型",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZJLX",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "证件号码",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XDR_ZJHM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政许可决定文书名称",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKWS",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "行政许可决定文书号",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_WSH",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可类别",
                            "type": "1",
                            "length": "256",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可证书名称",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKZS",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可编号",
                            "type": "1",
                            "length": "64",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKBH",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可内容",
                            "type": "1",
                            "length": "4000",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_NR",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可决定日期",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_JDRQ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "有效期自",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_YXQZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "有效期至",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_YXQZI",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可机关",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKJG",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "许可机关统一社会信用代码",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_XKJGDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "当前状态",
                            "type": "3",
                            "length": "1",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_ZT",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "数据来源单位",
                            "type": "1",
                            "length": "200",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_LYDW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "数据来源单位统一社会信用代码",
                            "type": "1",
                            "length": "18",
                            "options": [],
                            "remarks": "",
                            "identify": "XK_LYDWDM",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "备注",
                            "type": "1",
                            "length": "4000",
                            "options": [],
                            "remarks": "",
                            "identify": "BZ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "过期时间",
                            "type": "2",
                            "length": "8",
                            "options": [],
                            "remarks": "",
                            "identify": "GQSJ",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "1",
                            "isNecessary": "1"
                        },
                        {
                            "name": "主体类别",
                            "type": "6",
                            "length": "50",
                            "options": [
                                "法人及非法人组织",
                                "个体工商户"
                            ],
                            "remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
                            "identify": "ZTLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "公开范围",
                            "type": "5",
                            "length": "20",
                            "options": [
                                "社会公开"
                            ],
                            "remarks": "支持单选",
                            "identify": "GKFW",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "更新频率",
                            "type": "5",
                            "length": "20",
                            "options": [
                                "实时"
                            ],
                            "remarks": "支持单选",
                            "identify": "GXPL",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "信息分类",
                            "type": "5",
                            "length": "50",
                            "options": [
                                "行政许可"
                            ],
                            "remarks": "支持单选",
                            "identify": "XXFL",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        },
                        {
                            "name": "信息类别",
                            "type": "5",
                            "length": "50",
                            "options": [
                                "基础信息"
                            ],
                            "remarks": "信息性质类型，支持单选",
                            "identify": "XXLB",
                            "isMasked": "0",
                            "maskRule": [],
                            "dimension": "2",
                            "isNecessary": "1"
                        }
                    ],
                    "status": 0,
                    "createTime": 1626504022,
                    "updateTime": 1629172736,
                    "statusTime": 0
                },
                "relationships": {
                    "sourceUnit": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            }
        ]
    }