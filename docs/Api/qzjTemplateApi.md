# 前置机资源目录接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [资源目录通用项目字典](../Dictionary/template.md "资源目录通用项目字典")
	* [前置机资源目录项目字典](../Dictionary/qzjTemplate.md "前置机资源目录项目字典")

*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [资源目录通用控件规范](../WidgetRule/template.md "资源目录通用控件规范")
	* [前置机资源目录控件规范](../WidgetRule/qzjTemplate.md "前置机资源目录控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    101=>array(
		'qzjTemplate' => 前置机资源目录已经存在
		'name' => 目录名称/信息项名称格式不正确
        'identify'=>目录标识/信息项标识格式不正确
        'subjectCategory=>主体类别格式不正确
        'dimension' => 公开范围格式不正确
        'infoClassify'=>信息分类格式不正确
        'infoCategory'=>信息类别格式不正确
        'description'=>目录描述格式不正确
        'exchangeFrequency'=>更新频率格式不正确
		'sourceUnit'=>来源委办局格式不正确
        'items'=>模板信息格式不正确
        'ZTMC' => 主体名称不存在
        'ZJHM' => 证件号码不存在
        'TYSHXYDM' => 统一社会信用代码不存在
        'type' => 信息项数据类型格式不正确
        'length' => 信息项数据长度格式不正确
        'options' => 信息项可选范围格式不正确
        'isNecessary' => 信息项是否必填格式不正确
        'isMasked' => 信息项是否脱敏格式不正确
        'maskRule' => 信息项脱敏规则格式不正确
        'remarks' => 信息项备注格式不正确
    )
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 行政许可                                      | 目录名称          |
| identify        | string     | 是            | XZXK                                         | 目录标识          |
| subjectCategory | array      | 是            | 1                                            | 主体类别(1 法人及非法人组织 2 自然人 3 个体工商户)        |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)  |
| exchangeFrequency| int       | 是            | 1                                            | 更新频率(1 实时 2 每日 3 每周 4 每月 5 每季度 6 每半年 7 每一年 8 每两年 9 每三年) |
| infoClassify    | int        | 是            | 1                                            | 信息分类(1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他) |
| infoCategory    | int        | 是            | 1                                            | 信息类别(1 前置机信息 2 守信信息 3 失信信息 4 其他信息)    |
| category        | int        | 是            | 1                                            | 目录类别(1 本级 2 国标 3 前置机 10 委办局 20 前置机委办局 21 前置机本级 22 前置机国标)    |
| description     | string     | 否            | 决定日期不能大于11个工作日                       | 目录描述     |
| items           | array      | 是            |                                              | 模板信息     |
| sourceUnit      | int        | 否            | 1                                            | 来源委办局    |
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### items
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 主体名称                                      | 信息项名称          |
| identify        | string     | 是            | ZTMC                                         | 数据标识          |
| type | int      | 是            | 1                                            | 数据类型(1 字符型 2 日期型 3 整数型 4 浮点型 5 枚举型 6 集合型)        |
| length       | int        | 是            | 1                                            | 数据长度  |
| options       | array        | 是            |                                           | 可选范围  |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)  |
| isNecessary| int        | 是            | 1                                            | 是否必填(0 否 1 是) |
| isMasked| int        | 是            | 1                                            | 是否脱敏(0 否 1 是) |
| maskRule    | array        | 是            | [1,2]                                           | 脱敏规则 |
| remarks     | string     | 否            |                        |   备注   |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[qzjTemplates]
		1.2 fields[userGroups]
		1.3 include=sourceUnit
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'qzjTemplates/1?fields[qzjTemplates]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/qzjTemplates/{id:\d+}

示例

	$response = $client->request('GET', 'qzjTemplates/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/qzjTemplates/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'qzjTemplates/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/bjSearchData

	1、检索条件
		1.1 filter[name] | 根据 目录名称 搜索
		1.2 filter[identify] | 根据 目录标识 搜索
		1.3 filter[dimension] | 根据 公开范围 搜索 | 社会公开 1 | 政务共享 2 | 授权查询 3
		1.4 filter[infoClassify] | 根据 信息分类 搜索
		1.5 filter[infoCategory] | 根据 信息类别 搜索
		1.6 filter[category] | 根据 目录类别 搜索 | 前置机委办局 20 | 前置机本级 21 | 前置机国标 22
		1.7 filter[sourceUnit] | 根据 来源委办局 搜索

	2、排序
		2.1 sort=-id | -id 根据id倒序 | id 根据id正序
		2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序

示例

	$response = $client->request('GET', 'qzjTemplates?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>

路由

	通过POST传参
	/qzjTemplates

示例

	$data = array(
		"data" => array(
			"type" => "qzjTemplates",
			"attributes" => array(
				"name" => '登记信息',    //目录名称
				"identify" => 'DJXX',    //目录标识
				"category" => 20,    //目录类别, 前置机委办局 20 | 前置机本级 21 | 前置机国标 22
				"subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
				"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
				"exchangeFrequency" => 1,    //更新频率
				"infoClassify" => 1,    //信息分类
				"infoCategory" => 1,    //信息类别
				"description" => "目录描述信息",    //目录描述
				"items" => array(
					array(
						"name" => '主体名称',    //信息项名称
						"identify" => 'ZTMC',    //数据标识
						"type" => 1,    //数据类型
						"length" => '200',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(),    //脱敏规则
						"remarks" => '信用主体名称',    //备注
					),
					array(
						"name" => '统一社会信用代码',    //信息项名称
						"identify" => 'TYSHXYDM',    //数据标识
						"type" => 1,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
						"remarks" => '信用主体代码',    //备注
					),
					array(
						"name" => '信息类别',    //信息项名称
						"identify" => 'XXLB',    //数据标识
						"type" => 5,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(
							"基础信息",
							"守信信息",
							"失信信息",
							"其他信息"
						),    //可选范围
						"dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
						"remarks" => '信息性质类型，支持单选',    //备注
					)
				)
			),
			"relationships" => array(
				"sourceUnit" => array( // 来源单位
					"data" => array(
						array("type" => "userGroups", "id" => 1)
					)
				)
			)
		)
	);

	$response = $client->request(
		'POST',
		'qzjTemplates',
		[
			'headers'=>['Content-Type' => 'application/vnd.api+json'],
			'json' => $data
		]
	);

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/qzjTemplates/{id:\d+}

示例

	$data = array(
		"data" => array(
			"type" => "qzjTemplates",
			"attributes" => array(
				"name" => '登记信息',    //目录名称
				"identify" => 'DJXX',    //目录标识
				"category" => 20,    //目录类别, 前置机委办局 20 | 前置机本级 21 | 前置机国标 22
				"subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
				"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
				"exchangeFrequency" => 1,    //更新频率
				"infoClassify" => 1,    //信息分类
				"infoCategory" => 1,    //信息类别
				"items" => array(
					array(
						"name" => '主体名称',    //信息项名称
						"identify" => 'ZTMC',    //数据标识
						"type" => 1,    //数据类型
						"length" => '200',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(),    //脱敏规则
						"remarks" => '信用主体名称',    //备注
					),
					array(
						"name" => '统一社会信用代码',    //信息项名称
						"identify" => 'TYSHXYDM',    //数据标识
						"type" => 1,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
						"remarks" => '信用主体代码',    //备注
					),
					array(
						"name" => '信息类别',    //信息项名称
						"identify" => 'XXLB',    //数据标识
						"type" => 5,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(
							"前置机信息",
							"守信信息",
							"失信信息",
							"其他信息"
						),    //可选范围
						"dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
						"remarks" => '信息性质类型，支持单选',    //备注
					)
				)
			),
			"relationships" => array(
				"sourceUnit" => array( // 来源单位
					"data" => array(
						array("type" => "userGroups", "id" => 1)
					)
				)
			)
		)
	);

	$response = $client->request(
		'PATCH',
		'qzjTemplates/1',
		[
			'headers'=>['Content-Type' => 'application/vnd.api+json'],
			'json' => $data
		]
	);

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "qzjTemplates",
			"id": "1",
			"attributes": {
				"name": "企业基本信息",
				"identify": "QYJBXX",
				"subjectCategory": [
					"1",
					"3"
				],
				"dimension": 1,
				"exchangeFrequency": 4,
				"infoClassify": "5",
				"infoCategory": "1",
				"description": "企业基本信息",
				"category": 20,
				"items": [
					{
						"name": "主体名称",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "ZTMC",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "统一社会信用代码",
						"type": "1",
						"length": "18",
						"options": [],
						"remarks": "",
						"identify": "TYSHXYDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "成立日期",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "CLRQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "核准日期",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "HZRQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "经营场所",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "JYCS",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "注册资本（万）",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "ZCZB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "营业期限自",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "YYQXQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "营业期限至",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "YYQXZ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "经营范围",
						"type": "1",
						"length": "2000",
						"options": [],
						"remarks": "",
						"identify": "JYFW",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "登记机关",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "DJJG",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "法定代表人",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "FDDBR",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "法人身份证号",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "FRZJHM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "登记状态",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "DJZT",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "企业类型",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "QYLX",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "企业类型代码",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "QYLXDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "行业门类",
						"type": "1",
						"length": "4",
						"options": [],
						"remarks": "",
						"identify": "HYML",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "行业代码",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "HYDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "所属区域",
						"type": "1",
						"length": "50",
						"options": [],
						"remarks": "",
						"identify": "SSQY",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "主体类别",
						"type": "6",
						"length": "50",
						"options": [
							"法人及非法人组织",
							"个体工商户"
						],
						"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
						"identify": "ZTLB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "公开范围",
						"type": "5",
						"length": "20",
						"options": [
							"社会公开"
						],
						"remarks": "支持单选",
						"identify": "GKFW",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "更新频率",
						"type": "5",
						"length": "20",
						"options": [
							"每月"
						],
						"remarks": "支持单选",
						"identify": "GXPL",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "信息分类",
						"type": "5",
						"length": "50",
						"options": [
							"其他"
						],
						"remarks": "支持单选",
						"identify": "XXFL",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "信息类别",
						"type": "5",
						"length": "50",
						"options": [
							"基础信息"
						],
						"remarks": "信息性质类型，支持单选",
						"identify": "XXLB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					}
				],
				"status": 0,
				"createTime": 1633766209,
				"updateTime": 1633766209,
				"statusTime": 0
			},
			"relationships": {
				"sourceUnit": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/qzjTemplates/1"
			}
		},
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 2,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "qzjTemplates",
				"id": "6",
				"attributes": {
					"name": "行政许可信息",
					"identify": "HZXKXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "行政许可信息",
					"category": 22,
					"items": [
						{
							"name": "行政相对人名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体的法定名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人类别",
							"type": "1",
							"length": "16",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_LB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_1(统一社会信用代码)",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_2 (工商注册号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_GSZC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_3(组织机构代码)",
							"type": "1",
							"length": "9",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZZJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_4(税务登记号)",
							"type": "1",
							"length": "15",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SWDJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_5(事业单位证书号)",
							"type": "1",
							"length": "12",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_6(社会组织登记证号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SHZZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_FRDB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKWS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_WSH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可类别",
							"type": "1",
							"length": "256",
							"options": [],
							"remarks": "",
							"identify": "XK_XKLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可证书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKZS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可编号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKBH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可内容",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "XK_NR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可决定日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_JDRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZI",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJGDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "当前状态",
							"type": "3",
							"length": "1",
							"options": [],
							"remarks": "",
							"identify": "XK_ZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDWDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "备注",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "BZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "过期时间",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "GQSJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"实时"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"行政许可"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1633766388,
					"updateTime": 1633766388,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/qzjTemplates/6"
				}
			},
			{
				"type": "qzjTemplates",
				"id": "2",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 22,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "HYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1633766269,
					"updateTime": 1633766269,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/qzjTemplates/2"
				}
			}
		],
		"included": [
			{
				"type": "userGroups",
				"id": "0",
				"attributes": {
					"name": "",
					"shortName": "",
					"status": 0,
					"createTime": 1633767279,
					"updateTime": 1633767279,
					"statusTime": 0
				}
			}
		]
	}