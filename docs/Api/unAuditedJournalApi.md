# 信用刊物审核管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [信用刊物项目字典](../Dictionary/journal.md "信用刊物项目字典")
	* [信用刊物审核项目字典](../Dictionary/unAuditedJournal.md "信用刊物审核项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [信用刊物控件规范](../WidgetRule/journal.md "信用刊物控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'title'=>标题格式不正确
		'source'=>来源格式不正确
		'cover'=>封面格式不正确
		'attachment'=>附件格式不正确
		'description'=>简介格式不正确
		'year'=>年份格式不正确
		'status'=>状态格式不正确
		'authImages'=>授权图片格式不正确
		'crewId'=>发布人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 诚信杂志第一期                                  | 信用刊物标题        |
| source          | string     | 是            | 中国政务网                                       | 信用刊物来源          |
| description     | string     | 是            | 信用刊物简介                                     | 信用刊物简介          |
| cover           | array      | 是            | array('name'=>'封面名称', 'identify'=>'封面地址.jpg') | 信用刊物封面      |
| attachment     | array       | 是            | array('name'=>'附件名称', 'identify'=>'附件地址.pdf') | 信用刊物附件      |
| authImages     | array       | 否            | array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg')) | 授权图片      |
| year            | int        | 是            | 2019                                            | 年份          |
| crew            | int        | 是            | 1                                            | 发布人           |
| publishUserGroup| int        | 是            | 1                                            | 发布委办局          |
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| applyStatus     | int        | 是            | 0                                            | 审核状态(默认 0待审核 2审核通过 -2 审核驳回)|
| rejectReason    | string     | 是            | 内容不合理                                     | 驳回原因          |
| operationType   | int        | 是            | 0                                            | 操作类型(1 添加 2 编辑 3 启用 4 禁用 )|
| applyInfoCategory| int        | 是            | 0                                           | 可以申请的信息分类(1 信用刊物 )|
| applyInfoType    | int        | 是            | 0                                           | 可以申请的最小信息分类(信用刊物类型)|
| applyCrew        | int        | 是            | Crew                                             | 审核人           |
| applyUserGroup   | int        | 是            | UserGroup                                           | 审核委办局          |
| relation         | int        | 是            | Crew                                           | 修改人          |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[unAuditedJournals]
		1.4 include=crew,publishUserGroup,applyUserGroup,applyCrew,relation
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'unAuditedJournals/1?fields[unAuditedJournals]=title',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedJournals/{id:\d+}

示例

	$response = $client->request('GET', 'unAuditedJournals/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedJournals/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'unAuditedJournals/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedJournals

	1、检索条件
	    1.1 filter[applyUserGroup] | 根据审核委办局搜索,委办局id
	    1.2 filter[applyInfoCategory] | 根据可以申请的信息分类搜索
	    1.3 filter[relation] | 根据关联信息搜索,此处为员工id
	    1.4 filter[title] | 根据信用刊物标题搜索
	    1.5 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 审核驳回 | 2 审核通过
	    1.6 filter[operationType] | 根据操作类型搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 审核状态 根据审核状态正序

示例

	$response = $client->request('GET', 'unAuditedJournals?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="重新编辑">重新编辑</a>
	
路由

	通过PATCH传参
	/unAuditedJournals/{id:\d+}/resubmit

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedJournals",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
				"authImages"=>array(
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg')
				),
				"description"=>"简介",
				"year"=>年份,
				"status"=>状态
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedJournals/1/resubmit',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/unAuditedJournals/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedJournals",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedJournals/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/unAuditedJournals/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedJournals",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedJournals/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "unAuditedJournals",
			"id": "18",
			"attributes": {
				"applyInfoCategory": 2,
				"applyInfoType": 1,
				"applyStatus": 2,
				"rejectReason": "",
				"operationType": 2,
				"journalId": 2,
				"title": "诚信中国年2021刊物",
				"source": "信用中国官网",
				"description": "诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介",
				"cover": {
					"name": "诚信中国年2021刊物",
					"identify": "诚信中国年2021刊物.jpg"
				},
				"attachment": {
					"name": "诚信中国年2021刊物",
					"identify": "诚信中国年2021刊物.pdf"
				},
				"authImages": [
					{
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.jpg"
					},
					{
						"name": "诚信中国年2021刊物1",
						"identify": "诚信中国年2021刊物1.jpg"
					}
				],
				"year": 2021,
				"status": 0,
				"createTime": 1621997535,
				"updateTime": 1621997553,
				"statusTime": 1621997553
			},
			"relationships": {
				"relation": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"applyUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"publishUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/unAuditedJournals/18"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 8,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "unAuditedJournals",
				"id": "15",
				"attributes": {
					"applyInfoCategory": 2,
					"applyInfoType": 1,
					"applyStatus": 2,
					"rejectReason": "审核驳回原因",
					"operationType": 1,
					"journalId": 0,
					"title": "信用刊物标题测试11111",
					"source": "来源测试11111",
					"description": "简介测试11111",
					"cover": {
						"name": "封面名称测试11111",
						"identify": "封面地址测试11111.jpg"
					},
					"attachment": {
						"name": "附件名称测试11111",
						"identify": "附件地址测试11111.pdf"
					},
					"authImages": [
						{
							"name": "name",
							"identify": "identify测试11111.jpg"
						},
						{
							"name": "name",
							"identify": "identify测试11111.jpg"
						},
						{
							"name": "name",
							"identify": "identify测试11111.jpg"
						}
					],
					"year": 1999,
					"status": 0,
					"createTime": 1621997069,
					"updateTime": 1621997357,
					"statusTime": 1621997357
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedJournals/15"
				}
			},
			{
				"type": "unAuditedJournals",
				"id": "16",
				"attributes": {
					"applyInfoCategory": 2,
					"applyInfoType": 1,
					"applyStatus": 0,
					"rejectReason": "",
					"operationType": 1,
					"journalId": 0,
					"title": "信用刊物标题1",
					"source": "来源1",
					"description": "简介1111111",
					"cover": {
						"name": "封面名称1",
						"identify": "封面地址1.jpg"
					},
					"attachment": {
						"name": "附件名称1",
						"identify": "附件地址1.pdf"
					},
					"authImages": [
						{
							"name": "name",
							"identify": "identify1.jpg"
						},
						{
							"name": "name",
							"identify": "identify1.jpg"
						},
						{
							"name": "name",
							"identify": "identify1.jpg"
						}
					],
					"year": 2020,
					"status": -2,
					"createTime": 1621997073,
					"updateTime": 1621997073,
					"statusTime": 0
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "0"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedJournals/16"
				}
			},
			{
				"type": "unAuditedJournals",
				"id": "18",
				"attributes": {
					"applyInfoCategory": 2,
					"applyInfoType": 1,
					"applyStatus": 2,
					"rejectReason": "",
					"operationType": 2,
					"journalId": 2,
					"title": "诚信中国年2021刊物",
					"source": "信用中国官网",
					"description": "诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介",
					"cover": {
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.jpg"
					},
					"attachment": {
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.pdf"
					},
					"authImages": [
						{
							"name": "诚信中国年2021刊物",
							"identify": "诚信中国年2021刊物.jpg"
						},
						{
							"name": "诚信中国年2021刊物1",
							"identify": "诚信中国年2021刊物1.jpg"
						}
					],
					"year": 2021,
					"status": 0,
					"createTime": 1621997535,
					"updateTime": 1621997553,
					"statusTime": 1621997553
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedJournals/18"
				}
			},
			{
				"type": "unAuditedJournals",
				"id": "19",
				"attributes": {
					"applyInfoCategory": 2,
					"applyInfoType": 1,
					"applyStatus": 2,
					"rejectReason": "",
					"operationType": 3,
					"journalId": "1",
					"title": "信用刊物标题",
					"source": "来源",
					"description": "简介",
					"cover": {
						"name": "封面名称",
						"identify": "封面地址.jpg"
					},
					"attachment": {
						"name": "附件名称",
						"identify": "附件地址.pdf"
					},
					"authImages": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"year": 2021,
					"status": -2,
					"createTime": 1621997599,
					"updateTime": 1621997627,
					"statusTime": 1621997627
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedJournals/19"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "crews",
				"id": "0",
				"attributes": {
					"realName": "",
					"cardId": "",
					"userName": "",
					"cellphone": "",
					"category": 4,
					"purview": [],
					"status": 0,
					"createTime": 1621998548,
					"updateTime": 1621998548,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "0"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			}
		]
	}