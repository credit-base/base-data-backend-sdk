# 信用随手拍管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [信用随手拍不同审核状态的数量统计](#信用随手拍不同审核状态的数量统计)	
	* [新增](#新增)
	* [删除](#删除)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
	* [信用随手拍项目字典](../Dictionary/creditPhotography.md "信用随手拍项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [信用随手拍控件规范](../WidgetRule/creditPhotography.md "信用随手拍控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'description'=>描述格式不正确
		'attachments'=>附件格式不正确
		'memberId'=>发布人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
	102=>array(
		'applyStatus'=>审核状态不能操作
		'status'=>状态不能操作
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| description     | string     | 是            | 信用随手拍描述                                   | 信用随手拍描述          |
| attachments     | array      | 是            | array(array('name'=>'附件名称', 'identify'=>'附件地址.jpg')) | 信用随手拍附件      |
| member          | int        | 是            | 1                                            | 发布人           |
| applyCrew       | int        | 是            | 1                                            | 审核人          |
| applyStatus     | int        | 是            | 0                                            | 审核状态(0待审核 -2已驳回 2 已通过)|
| rejectReason    | string     | 是            | 驳回原因                                      | 驳回原因          |
| status          | int        | 是            | 0                                            | 状态(0正常 -2删除)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[members]
	    1.3 fields[creditPhotography]
		1.4 include=applyCrew,member
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'creditPhotography/1?fields[creditPhotography]=description',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/creditPhotography/{id:\d+}

示例

	$response = $client->request('GET', 'creditPhotography/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/creditPhotography/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'creditPhotography/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/creditPhotography

	1、检索条件
	    1.1 filter[member] | 根据发布人搜索,前台用户id
	    1.2 filter[realName] | 根据前台用户真实姓名搜索
	    1.3 filter[status] | 根据状态搜索 | 0 正常 | -2 删除
	    1.3 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 已驳回 | 2 已通过,支持多个搜索,例 filter[applyStatus]=0,-2

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 状态 根据审核状态正序

示例

	$response = $client->request('GET', 'creditPhotography?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="信用随手拍不同审核状态的数量统计">信用随手拍不同审核状态的数量统计</a>

路由

	通过GET传参
	/statisticals/staticsCreditPhotography

	1、检索条件
	    1.1 filter[memberId] | 根据发布人搜索,前台用户id

示例

	$response = $client->request('GET', 'statisticals/staticsCreditPhotograph?filter[memberId]=2',['headers'=>['Content-' => 'application/vnd.api+json']]);

返回示例

	{
		"meta": [],
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": {
					"pendingTotal": "1",
					"approveTotal": "3",
					"rejectTotal": 0
				}
			},
			"links": {
				"self": "127.0.0.1:8089/statisticals/0"
			}
		}
	}	

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/creditPhotography

示例

	$data = array(
		'data' => array(
			"type"=>"creditPhotography",
			"attributes"=>array(
				"description"=>"描述",
				"attachments"=>array(
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg')
				)
			),
			"relationships"=>array(
				"member"=>array(
					"data"=>array(
						array("type"=>"members","id"=>1)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'creditPhotography',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="删除">删除</a>

路由

	通过DELETE传参
	/creditPhotography/{id:\d+}/delete

示例

	$response = $client->request('DELETE', 'creditPhotography/1/delete',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/creditPhotography/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"creditPhotography",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'creditPhotography/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/creditPhotography/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"creditPhotography",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'creditPhotography/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "creditPhotography",
			"id": "2",
			"attributes": {
				"description": "人无信不立,业无信不兴,国无信不宁",
				"attachments": [
					{
						"name": "name",
						"identify": "identify.jpg"
					},
					{
						"name": "name",
						"identify": "identify.jpg"
					},
					{
						"name": "name",
						"identify": "identify.jpg"
					}
				],
				"applyStatus": 2,
				"rejectReason": "",
				"status": 0,
				"createTime": 1622285937,
				"updateTime": 1622286017,
				"statusTime": 1622286017
			},
			"relationships": {
				"member": {
					"data": {
						"type": "members",
						"id": "2"
					}
				},
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/creditPhotography/2"
			}
		},
		"included": [
			{
				"type": "members",
				"id": "2",
				"attributes": {
					"userName": "测试用户名",
					"realName": "姓名",
					"cellphone": "18800000001",
					"email": "997809099@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"gender": 0,
					"status": 0,
					"createTime": 1621434329,
					"updateTime": 1621434329,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 5,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "creditPhotography",
				"id": "4",
				"attributes": {
					"description": "诚信是一种软实力,更是一种竞争力",
					"attachments": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"applyStatus": -2,
					"rejectReason": "数据重复",
					"status": 0,
					"createTime": 1622286210,
					"updateTime": 1622286244,
					"statusTime": 1622286244
				},
				"relationships": {
					"member": {
						"data": {
							"type": "members",
							"id": "4"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/creditPhotography/4"
				}
			},
			{
				"type": "creditPhotography",
				"id": "1",
				"attributes": {
					"description": "描述",
					"attachments": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"applyStatus": 0,
					"rejectReason": "",
					"status": -2,
					"createTime": 1622285898,
					"updateTime": 1622285977,
					"statusTime": 1622285977
				},
				"relationships": {
					"member": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "0"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/creditPhotography/1"
				}
			},
			{
				"type": "creditPhotography",
				"id": "5",
				"attributes": {
					"description": "诚信是一种软实力,更是一种竞争力",
					"attachments": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"applyStatus": 0,
					"rejectReason": "",
					"status": 0,
					"createTime": 1622286212,
					"updateTime": 1622286212,
					"statusTime": 0
				},
				"relationships": {
					"member": {
						"data": {
							"type": "members",
							"id": "4"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "0"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/creditPhotography/5"
				}
			},
			{
				"type": "creditPhotography",
				"id": "2",
				"attributes": {
					"description": "人无信不立,业无信不兴,国无信不宁",
					"attachments": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"applyStatus": 2,
					"rejectReason": "",
					"status": 0,
					"createTime": 1622285937,
					"updateTime": 1622286017,
					"statusTime": 1622286017
				},
				"relationships": {
					"member": {
						"data": {
							"type": "members",
							"id": "2"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/creditPhotography/2"
				}
			},
			{
				"type": "creditPhotography",
				"id": "3",
				"attributes": {
					"description": "诚信是一种软实力,更是一种竞争力",
					"attachments": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"applyStatus": 2,
					"rejectReason": "",
					"status": 0,
					"createTime": 1622285954,
					"updateTime": 1622286190,
					"statusTime": 1622286190
				},
				"relationships": {
					"member": {
						"data": {
							"type": "members",
							"id": "2"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/creditPhotography/3"
				}
			}
		],
		"included": [
			{
				"type": "members",
				"id": "4",
				"attributes": {
					"userName": "18800000001",
					"realName": "白玉",
					"cellphone": "18800000011",
					"email": "997934302@qq.com",
					"cardId": "412825199009094531",
					"contactAddress": "长安区",
					"gender": 2,
					"status": 0,
					"createTime": 1516174523,
					"updateTime": 1516174523,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "0",
				"attributes": {
					"realName": "",
					"cardId": "",
					"userName": "",
					"cellphone": "",
					"category": 4,
					"purview": [],
					"status": 0,
					"createTime": 1622286731,
					"updateTime": 1622286731,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "0"
						}
					}
				}
			},
			{
				"type": "members",
				"id": "2",
				"attributes": {
					"userName": "测试用户名",
					"realName": "姓名",
					"cellphone": "18800000001",
					"email": "997809099@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"gender": 0,
					"status": 0,
					"createTime": 1621434329,
					"updateTime": 1621434329,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}