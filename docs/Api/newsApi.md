# 新闻管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [移动](#移动)
	* [启用](#启用)
	* [禁用](#禁用)
	* [置顶](#置顶)
	* [取消置顶](#取消置顶)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [新闻项目字典](../Dictionary/news.md "新闻项目字典")
	* [新闻分类对应关系定义](../About/newsCategoryDefine.md "新闻分类对应关系定义")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [新闻控件规范](../WidgetRule/news.md "新闻控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'title'=>标题格式不正确
		'source'=>来源格式不正确
		'cover'=>封面格式不正确
		'attachments'=>附件格式不正确
		'content'=>内容格式不正确
		'status'=>状态格式不正确
		'stick'=>置顶状态格式不正确
		'newsType'=>新闻类型格式不正确
		'dimension'=>数据纬度格式不正确
		'homePageShowStatus'=>首页展示状态格式不正确
		'bannerStatus'=>轮播状态格式不正确
		'bannerImage'=>轮播图图片格式不正确
		'crewId'=>发布人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 中央应对新型冠状病毒感染肺炎疫情工作领导小组关于全面落实进一步保护关心爱护医务人员若干措施的通知 | 新闻标题        |
| source          | string     | 是            | 中国政务网                                       | 新闻来源          |
| content         | string     | 是            | 新闻内容                                         | 新闻内容          |
| description     | string     | 是            | 新闻描述                                         | 新闻描述          |
| parentCategory  | int        | 是            | 1                                               | 新闻父级分类      |
| category        | int        | 是            | 1                                               | 新闻分类         |
| newsType        | int        | 是            | 1                                               | 新闻类型         |
| dimension       | int        | 是            | 1                                               | 数据共享维度(1 社会公开 2 政务共享 3 授权查询)|
| cover           | array      | 否            | array('name'=>'封面名称', 'identify'=>'封面地址.jpg') | 新闻封面      |
| attachments     | array      | 否            | array(array('name'=>'附件名称', 'identify'=>'附件地址.doc')) | 新闻附件      |
| crew            | int        | 是            | 1                                            | 发布人           |
| publishUserGroup| int        | 是            | 1                                            | 发布委办局          |
| bannerStatus    | int        | 否            | 0                                            | 轮播状态(默认 0不轮播 2轮播)|
| bannerImage     | array      | 否            | array('name'=>'轮播图图片名称', 'identify'=>'轮播图图片地址.jpg') | 轮播图图片      |
| homePageShowStatus| int      | 否            | 0                                            | 首页展示状态(默认 0不展示 2展示)|
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| stick           | int        | 是            | 0                                            | 置顶状态(默认 0不置顶 2置顶)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[news]
		1.4 include=crew,publishUserGroup
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'news/1?fields[news]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/news/{id:\d+}

示例

	$response = $client->request('GET', 'news/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/news/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'news/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/news

	1、检索条件
	    1.1 filter[publishUserGroup] | 根据发布委办局搜索,委办局id
	    1.2 filter[parentCategory] | 根据新闻父级分类搜索
	    1.3 filter[category] | 根据新闻分类搜索
	    1.4 filter[newsType] | 根据新闻类型搜索
	    1.5 filter[dimension] | 根据数据共享纬度搜索 | 1 社会公开 2 政务共享
	    1.6 filter[title] | 根据新闻标题搜索
	    1.7 filter[status] | 根据状态搜索 | 0 启用 | -2 禁用
	    1.8 filter[stick] | 根据置顶状态搜索 | 0 不置顶 | 2 置顶
	    1.9 filter[bannerStatus] | 根据轮播状态搜索 | 0 不轮播 | 2 轮播
	    1.10 filter[homePageShowStatus] | 根据首页展示状态搜索 | 0 不展示 | 2 展示

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序
		2.3 sort=-stick | -stick 根据置顶状态倒序 | 置顶状态 根据置顶状态正序
		2.4 sort=-bannerStatus | -bannerStatus 根据轮播状态倒序 | 轮播状态 根据轮播状态正序
		2.5 sort=-homePageShowStatus | -homePageShowStatus 根据首页展示状态倒序 | 首页展示状态 根据首页展示状态正序

示例

	$response = $client->request('GET', 'news?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/news

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachments"=>array(
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify')
				),
				"content"=>"内容",
				"newsType"=>新闻类型,
				"dimension"=>数据共享维度,
				"status"=>状态,
				"stick"=>置顶状态,
				"bannerStatus"=>轮播状态,
				"bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
				"homePageShowStatus"=>首页展示状态,
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'news',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/news/{id:\d+}

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachments"=>array(
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify')
				),
				"content"=>"内容",
				"newsType"=>新闻类型,
				"dimension"=>数据共享维度,
				"status"=>状态,
				"stick"=>置顶状态,
				"bannerStatus"=>轮播状态,
				"bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
				"homePageShowStatus"=>首页展示状态,
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="移动">移动</a>
路由

	通过PATCH传参
	/news/{id:\d+}/move/{newsType:\d+}

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1/move/1',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="启用">启用</a>

路由

	通过PATCH传参
	/news/{id:\d+}/enable

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1/enable',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="禁用">禁用</a>

路由

	通过PATCH传参
	/news/{id:\d+}/disable

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1/disable',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="置顶">置顶</a>

路由

	通过PATCH传参
	/news/{id:\d+}/top

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1/top',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="取消置顶">取消置顶</a>

路由

	通过PATCH传参
	/news/{id:\d+}/cancelTop

示例

	$data = array(
		'data' => array(
			"type"=>"news",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'news/1/cancelTop',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "news",
			"id": "13",
			"attributes": {
				"title": "测试新闻标题123",
				"source": "测试新闻来源123",
				"cover": {
					"name": "测试封面名称123",
					"identify": "测试封面地址123.jpg"
				},
				"attachments": [
					{
						"name": "测试name13",
						"identify": "测试identify123.doc"
					}
				],
				"content": "测试内容123",
				"description": "测试内容123",
				"parentCategory": 0,
				"category": 1,
				"newsType": 1,
				"dimension": 2,
				"bannerImage": {
					"name": "测试轮播图图片名称123",
					"identify": "测试轮播图图片地址123.jpg"
				},
				"bannerStatus": 2,
				"homePageShowStatus": 2,
				"stick": 2,
				"status": 0,
				"createTime": 1620831782,
				"updateTime": 1620872174,
				"statusTime": 1620869215
			},
			"relationships": {
				"publishUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/news/13"
			}
		},
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 4,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "news",
				"id": "4",
				"attributes": {
					"title": "新闻标题测试",
					"source": "来源",
					"cover": [],
					"attachments": [
						{
							"name": "name",
							"identify": "identify.doc"
						}
					],
					"content": "内容",
					"description": "内容",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 1,
					"bannerImage": {
						"name": "轮播图图片名称",
						"identify": "轮播图图片地址.jpg"
					},
					"bannerStatus": 0,
					"homePageShowStatus": 0,
					"stick": 0,
					"status": 0,
					"createTime": 1620822056,
					"updateTime": 1620867697,
					"statusTime": 1620867697
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/news/4"
				}
			},
			{
				"type": "news",
				"id": "5",
				"attributes": {
					"title": "新闻标题测试",
					"source": "来源",
					"cover": [],
					"attachments": [
						{
							"name": "name",
							"identify": "identify.doc"
						}
					],
					"content": "内容",
					"description": "内容",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 1,
					"bannerImage": {
						"name": "轮播图图片名称",
						"identify": "轮播图图片地址.jpg"
					},
					"bannerStatus": 0,
					"homePageShowStatus": 0,
					"stick": 0,
					"status": 0,
					"createTime": 1620822185,
					"updateTime": 1620867966,
					"statusTime": 1620867966
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/news/5"
				}
			},
			{
				"type": "news",
				"id": "13",
				"attributes": {
					"title": "测试新闻标题123",
					"source": "测试新闻来源123",
					"cover": {
						"name": "测试封面名称123",
						"identify": "测试封面地址123.jpg"
					},
					"attachments": [
						{
							"name": "测试name13",
							"identify": "测试identify123.doc"
						}
					],
					"content": "测试内容123",
					"description": "测试内容123",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 2,
					"bannerImage": {
						"name": "测试轮播图图片名称123",
						"identify": "测试轮播图图片地址123.jpg"
					},
					"bannerStatus": 2,
					"homePageShowStatus": 2,
					"stick": 2,
					"status": 0,
					"createTime": 1620831782,
					"updateTime": 1620872174,
					"statusTime": 1620869215
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/news/13"
				}
			},
			{
				"type": "news",
				"id": "14",
				"attributes": {
					"title": "新闻标题测试",
					"source": "来源",
					"cover": [],
					"attachments": [
						{
							"name": "name",
							"identify": "identify.doc"
						}
					],
					"content": "内容",
					"description": "内容",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 1,
					"bannerImage": {
						"name": "轮播图图片名称",
						"identify": "轮播图图片地址.jpg"
					},
					"bannerStatus": 0,
					"homePageShowStatus": 0,
					"stick": 0,
					"status": 0,
					"createTime": 1620822402,
					"updateTime": 1620868252,
					"statusTime": 1620868252
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/news/14"
				}
			}
		],
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}