# 信用刊物管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [启用](#启用)
	* [禁用](#禁用)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [信用刊物项目字典](../Dictionary/journal.md "信用刊物项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [信用刊物控件规范](../WidgetRule/journal.md "信用刊物控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'title'=>标题格式不正确
		'source'=>来源格式不正确
		'cover'=>封面格式不正确
		'attachment'=>附件格式不正确
		'description'=>简介格式不正确
		'year'=>年份格式不正确
		'status'=>状态格式不正确
		'authImages'=>授权图片格式不正确
		'crewId'=>发布人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 诚信杂志第一期                                  | 信用刊物标题        |
| source          | string     | 是            | 中国政务网                                       | 信用刊物来源          |
| description     | string     | 是            | 信用刊物简介                                     | 信用刊物简介          |
| cover           | array      | 是            | array('name'=>'封面名称', 'identify'=>'封面地址.jpg') | 信用刊物封面      |
| attachment     | array       | 是            | array('name'=>'附件名称', 'identify'=>'附件地址.pdf') | 信用刊物附件      |
| authImages     | array       | 否            | array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg')) | 授权图片      |
| year            | int        | 是            | 2019                                            | 年份          |
| crew            | int        | 是            | 1                                            | 发布人           |
| publishUserGroup| int        | 是            | 1                                            | 发布委办局          |
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[journals]
		1.4 include=crew,publishUserGroup
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'journals/1?fields[journals]=title',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/journals/{id:\d+}

示例

	$response = $client->request('GET', 'journals/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/journals/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'journals/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/journals

	1、检索条件
	    1.1 filter[publishUserGroup] | 根据发布委办局搜索,委办局id
	    1.2 filter[title] | 根据信用刊物标题搜索
	    1.3 filter[status] | 根据状态搜索 | 0 启用 | -2 禁用
	    1.4 filter[year] | 根据发布年份搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序

示例

	$response = $client->request('GET', 'journals?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/journals

示例

	$data = array(
		'data' => array(
			"type"=>"journals",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
				"authImages"=>array(
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg')
				),
				"description"=>"简介",
				"year"=>年份,
				"status"=>状态
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'journals',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/journals/{id:\d+}

示例

	$data = array(
		'data' => array(
			"type"=>"journals",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
				"authImages"=>array(
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg')
				),
				"description"=>"简介",
				"year"=>年份,
				"status"=>状态
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'journals/1',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="启用">启用</a>

路由

	通过PATCH传参
	/journals/{id:\d+}/enable

示例

	$data = array(
		'data' => array(
			"type"=>"journals",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'journals/1/enable',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="禁用">禁用</a>

路由

	通过PATCH传参
	/journals/{id:\d+}/disable

示例

	$data = array(
		'data' => array(
			"type"=>"journals",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'journals/1/disable',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "journals",
			"id": "2",
			"attributes": {
				"title": "诚信中国年2021刊物",
				"source": "信用中国官网",
				"description": "诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介",
				"cover": {
					"name": "诚信中国年2021刊物",
					"identify": "诚信中国年2021刊物.jpg"
				},
				"attachment": {
					"name": "诚信中国年2021刊物",
					"identify": "诚信中国年2021刊物.pdf"
				},
				"authImages": [
					{
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.jpg"
					},
					{
						"name": "诚信中国年2021刊物1",
						"identify": "诚信中国年2021刊物1.jpg"
					}
				],
				"year": 2021,
				"status": 0,
				"createTime": 1621997069,
				"updateTime": 1621997553,
				"statusTime": 1621997357
			},
			"relationships": {
				"publishUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/journals/2"
			}
		},
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 2,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "journals",
				"id": "1",
				"attributes": {
					"title": "信用刊物标题",
					"source": "来源",
					"description": "简介",
					"cover": {
						"name": "封面名称",
						"identify": "封面地址.jpg"
					},
					"attachment": {
						"name": "附件名称",
						"identify": "附件地址.pdf"
					},
					"authImages": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"year": 2021,
					"status": -2,
					"createTime": 1621996436,
					"updateTime": 1621997649,
					"statusTime": 1621997649
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/journals/1"
				}
			},
			{
				"type": "journals",
				"id": "2",
				"attributes": {
					"title": "诚信中国年2021刊物",
					"source": "信用中国官网",
					"description": "诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介",
					"cover": {
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.jpg"
					},
					"attachment": {
						"name": "诚信中国年2021刊物",
						"identify": "诚信中国年2021刊物.pdf"
					},
					"authImages": [
						{
							"name": "诚信中国年2021刊物",
							"identify": "诚信中国年2021刊物.jpg"
						},
						{
							"name": "诚信中国年2021刊物1",
							"identify": "诚信中国年2021刊物1.jpg"
						}
					],
					"year": 2021,
					"status": 0,
					"createTime": 1621997069,
					"updateTime": 1621997553,
					"statusTime": 1621997357
				},
				"relationships": {
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/journals/2"
				}
			}
		],
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}