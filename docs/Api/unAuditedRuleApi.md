# 规则审核管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [撤销](#撤销)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [规则项目字典](../Dictionary/rule.md "规则项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [委办局资源目录项目字典](../Dictionary/wbjTemplate.md "委办局资源目录项目字典")
	* [本级资源目录项目字典](../Dictionary/bjTemplate.md "本级资源目录项目字典")
	* [国标资源目录项目字典](../Dictionary/gbTemplate.md "国标资源目录项目字典")
	* [规则审核项目字典](../Dictionary/unAuditedRule.md "规则审核项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [规则控件规范](../WidgetRule/rule.md "规则控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    101=>array(
        'rules'=>规则格式不正确
        'rulesName'=>规则名称不存在
        'completionRulesCount'=>补全数量不正确
        'completionRulesId' => 补全资源目录id格式不正确
        'completionRulesCategory' => 补全资源目录类型格式不正确
        'completionRulesBase'=>补全依据不正确
        'completionRulesItem'=>补全信息项不正确
        'completionRules'=>补全规则格式不正确
        'comparisonRulesCount'=>比对数量不正确
        'comparisonRulesId' => 比对资源目录ID格式不正确
        'comparisonRulesCategory' => 比对资源目录类型格式不正确
        'comparisonRulesBase'=>比对依据不正确
        'comparisonRulesItem'=>比对信息项不正确
        'comparisonRules'=>比对规则格式不正确
        'deDuplicationRules'=>去重规则格式不正确
        'deDuplicationRulesItems'=>去重信息项不正确
        'deDuplicationRulesResult' => 去重结果不正确
        'transformationRulesTransformationNotExist'=>转换资源目录信息项不存在
        'transformationRulesSourceNotExist'=>来源资源目录信息项不存在
        'transformationItemTypeNotExist'=>目标信息项类型不存在
        'typeCannotTransformation'=>类型不能转换
        'lengthCannotTransformation'=>长度不能转换
        'dimensionCannotTransformation'=>公开范围不能转换
        'isMaskedCannotTransformation'=>脱敏状态不能转换
        'maskRuleCannotTransformation'=>脱敏范围不能转换
        'zrrNotIncludeIdentify'=>补全依据需要包含身份证号信息项
        'completionRulesTransformationItemNotExist'=>补全规则目标资源目录模板待补全信息项不存在
        'completionRulesCompletionTemplateNotExist'=>补全规则补全信息项来源不存在
        'completionRulesCompletionTemplateItemNotExist'=>补全规则补全信息项不存在
        'comparisonRulesTransformationItemNotExist'=>比对规则目标资源目录模板待比对信息项不存在
        'comparisonRulesComparisonTemplateNotExist'=>比对规则比对信息项来源不存在
        'comparisonRulesBaseCannotName'=>比对项为企业名称,比对依据不能选择企业名称信息项
        'comparisonRulesBaseCannotIdentify'=>比对项为统一社会信用代码/身份证号,那比对依据不能选择统一社会信用代码/身份证号信息项
        'comparisonRulesComparisonTemplateItemNotExist'=>比对规则比对信息项不存在
        'deDuplicationRulesItemsNotExit'=>去重规则去重信息项不存在
        'transformationCategory'=>目标类型格式不正确
        'sourceCategory'=>来源类型格式不正确
        'rejectReason'=>驳回原因格式不正确
        'applyCrewId'=>审核人格式不正确
        'crewId'=>发布人格式不正确
        'transformationTemplateId'=>目标资源目录格式不正确
        'sourceTemplateId'=>来源资源目录格式不正确
        'applyCrewId'=>审核人格式不正确
		'requiredItemsMapping'=>存在未转换和补全的必填项
    ),
    100=>array(
        'crewId'=>发布人为空
        'userGroupId'=>来源委办局为空
        'sourceTemplateId'=>来源资源目录为空
        'transformationTemplateId'=>目标资源目录为空
    ),
    103=>array(
        'rule'=>规则数据已经存在
    ),
    102=>array(
        'status'=>状态不能操作
        'applyStatus'=>状态不能操作
    ),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| transformationTemplate | int | 是             | 1                                          | 目标资源目录      |
| sourceTemplate   | int       | 是             | 1                                          | 来源资源目录      |
| rules           | array      | 是             |                                             | 规则             |
| version         | int        | 是             | 1                                            | 版本号            |
| dataTotal       | int        | 是             | 1                                            | 已转换数据量       |
| crew            | int        | 是             | 1                                            | 发布人           |
| userGroup       | int        | 是             | 1                                            | 来源委办局          |
| type            | int        | 是             | 1                                            | 分类 (1 本级 2 国标)       |
| status          | int        | 是            | 0                                            | 状态(0正常 -2 删除)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| applyStatus     | int        | 是            | 0                                            | 审核状态(默认 0待审核 2审核通过 -2 审核驳回 -4 撤销)|
| rejectReason    | string     | 是            | 内容不合理                                     | 驳回原因          |
| operationType   | int        | 是            | 0                                            | 操作类型(1 添加 2 编辑 )|
| publishCrew     | int        | 是            | Crew                                         | 发布人           |
| applyCrew        | int        | 是            | Crew                                         | 审核人           |
| applyUserGroup   | int        | 是            | UserGroup                                    | 审核委办局          |
| relationId         | int        | 是          | RuleService                                         | 关联id          |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[unAuditedRules]
	    1.4 fields[bjTemplates]
	    1.5 fields[gbTemplates]
	    1.6 fields[templates]
	    1.7 fields[baseTemplates]
		1.7 include=publishCrew,userGroup,transformationTemplate,sourceTemplate,applyCrew,applyUserGroup
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'unAuditedRules/1?fields[unAuditedRules]=title',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedRules/{id:\d+}

示例

	$response = $client->request('GET', 'unAuditedRules/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedRules/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'unAuditedRules/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedRules

	1、检索条件
	    1.1 filter[transformationCategory] | 根据目标库类别搜索 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)
	    1.2 filter[transformationTemplate] | 根据目标资源目录id搜索
	    1.3 filter[userGroup] | 根据来源委办局搜索
	    1.4 filter[sourceTemplate] | 根据来源资源目录id搜索
	    1.5 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 审核驳回 | 2 审核通过 | -4 撤销
	    1.6 filter[operationType] | 根据操作类型搜索
	    1.7 filter[relationId] | 根据关联信息搜索,此处为规则id
	    1.8 filter[sourceCategory] | 根据来源类别搜索 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)
		1.9 filter[excludeTransformationCategory] | 排除目标类型

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 审核状态 根据审核状态正序

示例

	$response = $client->request('GET', 'unAuditedRules?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="重新编辑">重新编辑</a>
	
路由

	通过PATCH传参
	/unAuditedRules/{id:\d+}/resubmit

示例
				
	$data = array(
		'data' => array(
			"type"=>"rule",
			"attributes"=>array(
				"rules"=>array(
					'transformationRule' => array(
						"ZTMC" => "ZTMC",
						"XXLB" => "XXLB"
					),
					'completionRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
						'TYSHXYDM' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZJHM'),
						),
					),
					'comparisonRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
						'TYSHXYDM' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZJHM'),
						),
					),
					'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM"))
				),
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>2)
					)
				)
			)
		)
	);

	$response = $client->request(
					'PATCH',
					'unAuditedRules/1/resubmit',
					[
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
					]
				);

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/unAuditedRules/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedRules",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedRules/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/unAuditedRules/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedRules",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedRules/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="撤销">撤销</a>

路由

	通过PATCH传参
	/unAuditedRules/{id:\d+}/revoke

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedRules",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedRules/1/revoke',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "unAuditedRules",
			"id": "7",
			"attributes": {
				"applyStatus": 2,
				"rejectReason": "",
				"operationType": 1,
				"relationId": 3,
				"rules": {
					"transformationRule": {
						"ZTMC": "ZTMC",
						"XK_XDR_LB": "XK_XDR_LB",
						"TYSHXYDM": "TYSHXYDM",
						"XK_XDR_GSZC": "XK_XDR_GSZC",
						"XK_XDR_ZZJG": "XK_XDR_ZZJG",
						"XK_XDR_SWDJ": "XK_XDR_SWDJ",
						"XK_XDR_SYDW": "XK_XDR_SYDW",
						"XK_XDR_SHZZ": "XK_XDR_SHZZ",
						"XK_FRDB": "XK_FRDB",
						"XK_FR_ZJLX": "XK_FR_ZJLX",
						"XK_FR_ZJHM": "XK_FR_ZJHM",
						"XK_XDR_ZJLX": "XK_XDR_ZJLX",
						"XK_XDR_ZJHM": "XK_XDR_ZJHM",
						"XK_XKWS": "XK_XKWS",
						"XK_WSH": "XK_WSH",
						"XK_XKLB": "XK_XKLB",
						"XK_XKZS": "XK_XKZS",
						"XK_XKBH": "XK_XKBH",
						"XK_NR": "XK_NR",
						"XK_JDRQ": "XK_JDRQ",
						"XK_YXQZ": "XK_YXQZ",
						"XK_YXQZI": "XK_YXQZI",
						"XK_XKJG": "XK_XKJG",
						"XK_XKJGDM": "XK_XKJGDM",
						"XK_ZT": "XK_ZT",
						"XK_LYDW": "XK_LYDW",
						"XK_LYDWDM": "XK_LYDWDM",
						"BZ": "BZ"
					},
					"completionRule": {
						"ZTMC": [
							{
								"id": "2",
								"name": "公证员信息",
								"base": [
									"2"
								],
								"item": "ZTMC",
								"itemName": "主体名称"
							}
						]
					},
					"comparisonRule": {
						"ZTMC": [
							{
								"id": "2",
								"name": "公证员信息",
								"base": [
									"2"
								],
								"item": "ZTMC",
								"itemName": "主体名称"
							}
						]
					},
					"deDuplicationRule": {
						"result": "1",
						"items": []
					}
				},
				"version": 1631687786,
				"dataTotal": 0,
				"transformationCategory": 1,
				"sourceCategory": 10,
				"status": 0,
				"createTime": 1631687786,
				"updateTime": 1631689242,
				"statusTime": 1631689242
			},
			"relationships": {
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"publishCrew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"userGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"applyUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "0"
					}
				},
				"transformationTemplate": {
					"data": {
						"type": "bjTemplates",
						"id": "5"
					}
				},
				"sourceTemplate": {
					"data": {
						"type": "templates",
						"id": "1"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/unAuditedRules/7"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "userGroups",
				"id": "0",
				"attributes": {
					"name": "",
					"shortName": "",
					"status": 0,
					"createTime": 1631693802,
					"updateTime": 1631693802,
					"statusTime": 0
				}
			},
			{
				"type": "bjTemplates",
				"id": "5",
				"attributes": {
					"name": "行政许可信息",
					"identify": "XZXK",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "行政许可信息",
					"category": 1,
					"items": [
						{
							"name": "行政相对人名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体的法定名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人类别",
							"type": "1",
							"length": "16",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_LB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_1(统一社会信用代码)",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_3(工商登记码)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_GSZC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_2(组织机构代码)",
							"type": "1",
							"length": "9",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZZJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_4(税务登记号)",
							"type": "1",
							"length": "15",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SWDJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "事业单位证书号",
							"type": "1",
							"length": "12",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "社会组织登记证号",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SHZZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_FRDB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKWS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_WSH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可类别",
							"type": "1",
							"length": "256",
							"options": [],
							"remarks": "",
							"identify": "XK_XKLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可证书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKZS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可编号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKBH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可内容",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "XK_NR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可决定日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_JDRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZI",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJGDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "当前状态",
							"type": "3",
							"length": "1",
							"options": [],
							"remarks": "",
							"identify": "XK_ZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDWDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "备注",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "BZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "0"
						}
					],
					"status": 0,
					"createTime": 1626746174,
					"updateTime": 1626746174,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"gbTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "0"
						}
					}
				}
			},
			{
				"type": "templates",
				"id": "1",
				"attributes": {
					"name": "行政许可信息",
					"identify": "HZXKXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "行政许可信息",
					"category": 10,
					"items": [
						{
							"name": "行政相对人名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体的法定名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人类别",
							"type": "1",
							"length": "16",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_LB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_1(统一社会信用代码)",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_2 (工商注册号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_GSZC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_3(组织机构代码)",
							"type": "1",
							"length": "9",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZZJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_4(税务登记号)",
							"type": "1",
							"length": "15",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SWDJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_5(事业单位证书号)",
							"type": "1",
							"length": "12",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_6(社会组织登记证号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SHZZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_FRDB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKWS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_WSH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可类别",
							"type": "1",
							"length": "256",
							"options": [],
							"remarks": "",
							"identify": "XK_XKLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可证书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKZS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可编号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKBH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可内容",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "XK_NR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可决定日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_JDRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZI",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJGDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "当前状态",
							"type": "3",
							"length": "1",
							"options": [],
							"remarks": "",
							"identify": "XK_ZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDWDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "备注",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "BZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "过期时间",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "GQSJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"实时"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"行政许可"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1626504022,
					"updateTime": 1629172736,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "unAuditedRules",
				"id": "9",
				"attributes": {
					"applyStatus": 2,
					"rejectReason": "",
					"operationType": 1,
					"relationId": 4,
					"rules": {
						"transformationRule": {
							"ZTMC": "ZTMC",
							"XK_XDR_LB": "XK_XDR_LB",
							"TYSHXYDM": "TYSHXYDM",
							"XK_XDR_GSZC": "XK_XDR_GSZC",
							"XK_XDR_ZZJG": "XK_XDR_ZZJG",
							"XK_XDR_SWDJ": "XK_XDR_SWDJ",
							"XK_XDR_SYDW": "XK_XDR_SYDW",
							"XK_XDR_SHZZ": "XK_XDR_SHZZ",
							"XK_FRDB": "XK_FRDB",
							"XK_FR_ZJLX": "XK_FR_ZJLX",
							"XK_FR_ZJHM": "XK_FR_ZJHM",
							"XK_XDR_ZJLX": "XK_XDR_ZJLX",
							"XK_XDR_ZJHM": "XK_XDR_ZJHM",
							"XK_XKWS": "XK_XKWS",
							"XK_WSH": "XK_WSH",
							"XK_XKLB": "XK_XKLB",
							"XK_XKZS": "XK_XKZS",
							"XK_XKBH": "XK_XKBH",
							"XK_NR": "XK_NR",
							"XK_JDRQ": "XK_JDRQ",
							"XK_YXQZ": "XK_YXQZ",
							"XK_YXQZI": "XK_YXQZI",
							"XK_XKJG": "XK_XKJG",
							"XK_XKJGDM": "XK_XKJGDM",
							"XK_ZT": "XK_ZT",
							"XK_LYDW": "XK_LYDW",
							"XK_LYDWDM": "XK_LYDWDM",
							"BZ": "BZ"
						},
						"completionRule": {
							"ZTMC": [
								{
									"id": "2",
									"base": [
										"2"
									],
									"item": "ZTMC"
								}
							]
						},
						"comparisonRule": {
							"ZTMC": [
								{
									"id": "2",
									"base": [
										"2"
									],
									"item": "ZTMC"
								}
							]
						},
						"deDuplicationRule": {
							"result": "1",
							"items": []
						}
					},
					"version": 1631689116,
					"dataTotal": 0,
					"transformationCategory": 2,
					"sourceCategory": 10,
					"status": 0,
					"createTime": 1631689116,
					"updateTime": 1631689254,
					"statusTime": 1631689254
				},
				"relationships": {
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"publishCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					},
					"transformationTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "1"
						}
					},
					"sourceTemplate": {
						"data": {
							"type": "templates",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedRules/9"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "userGroups",
				"id": "0",
				"attributes": {
					"name": "",
					"shortName": "",
					"status": 0,
					"createTime": 1631693873,
					"updateTime": 1631693873,
					"statusTime": 0
				}
			},
			{
				"type": "gbTemplates",
				"id": "1",
				"attributes": {
					"name": "行政许可信息",
					"identify": "XZXK",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "行政许可信息",
					"category": 2,
					"items": [
						{
							"name": "行政相对人名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体的法定名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人类别",
							"type": "1",
							"length": "16",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_LB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_1(统一社会信用代码)",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_3(工商登记码)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_GSZC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_2(组织机构代码)",
							"type": "1",
							"length": "9",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZZJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_4(税务登记号)",
							"type": "1",
							"length": "15",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SWDJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "事业单位证书号",
							"type": "1",
							"length": "12",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "社会组织登记证号",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SHZZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_FRDB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKWS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_WSH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可类别",
							"type": "1",
							"length": "256",
							"options": [],
							"remarks": "",
							"identify": "XK_XKLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可证书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKZS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可编号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKBH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可内容",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "XK_NR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可决定日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_JDRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZI",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJGDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "当前状态",
							"type": "3",
							"length": "1",
							"options": [],
							"remarks": "",
							"identify": "XK_ZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDWDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "备注",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "BZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "0"
						}
					],
					"status": 0,
					"createTime": 1626503239,
					"updateTime": 1626503239,
					"statusTime": 0
				}
			},
			{
				"type": "templates",
				"id": "1",
				"attributes": {
					"name": "行政许可信息",
					"identify": "HZXKXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "行政许可信息",
					"category": 10,
					"items": [
						{
							"name": "行政相对人名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体的法定名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人类别",
							"type": "1",
							"length": "16",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_LB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_1(统一社会信用代码)",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_2 (工商注册号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_GSZC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_3(组织机构代码)",
							"type": "1",
							"length": "9",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZZJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_4(税务登记号)",
							"type": "1",
							"length": "15",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SWDJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_5(事业单位证书号)",
							"type": "1",
							"length": "12",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政相对人代码_6(社会组织登记证号)",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_SHZZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "XK_FRDB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_FR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件类型",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XDR_ZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKWS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行政许可决定文书号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_WSH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可类别",
							"type": "1",
							"length": "256",
							"options": [],
							"remarks": "",
							"identify": "XK_XKLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可证书名称",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKZS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可编号",
							"type": "1",
							"length": "64",
							"options": [],
							"remarks": "",
							"identify": "XK_XKBH",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可内容",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "XK_NR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可决定日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_JDRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "有效期至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "XK_YXQZI",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "许可机关统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_XKJGDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "当前状态",
							"type": "3",
							"length": "1",
							"options": [],
							"remarks": "",
							"identify": "XK_ZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "数据来源单位统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "XK_LYDWDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "备注",
							"type": "1",
							"length": "4000",
							"options": [],
							"remarks": "",
							"identify": "BZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "过期时间",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "GQSJ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"实时"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"行政许可"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1626504022,
					"updateTime": 1629172736,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					}
				}
			}
		]
	}