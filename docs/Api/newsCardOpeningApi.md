# 新闻管理简易接口

## 接口

* 单条
	* GET /news/{id:\d+} 
* 多条
	* GET /news/{ids:\d+,[\d,]+}
* 搜索
	* GET /news
		* 标题(模糊查询)
			* title
		* 状态(精确查询)
			* status
		* 发布委办局(精确查询)
			* publishUserGroup
		* 最后更新时间排序
			* updateTime
* 新增
	* POST /news 
	* 字段
		* 标题
			* 5-80位字符
			* 必填
			* title
		* 来源
			* 2-15位字符
			* 必填
			* source
		* 封面
			* jpg,png,jpeg,不大于2M
			* 必填,因文件服务器未部署好,所以此字段先为选填,服务器部署好改为必填
			* cover
		* 附件
			* pdf,不大于200M
			* 必填,因文件服务器未部署好,所以此字段先为选填,服务器部署好改为必填
			* attachment
		* 发布人
			* int
			* 必填
			* crew
* 启用
	* PATCH  /news/{id:\d+}/enable
* 禁用
	* PATCH  /news/{id:\d+}/disable

## 工作计划

### 20210524
* 新闻管理程序设计

### 20210525-20210526
* 新闻管理功能开发

### 20210527
* 新闻管理BDD测试

