# 接口错误返回说明

---

### 系统错误规范（1-99）

* [0](./docs/ErrorRule/common.md#ER-0000 "未定义错误")：未定义错误 
* [1](./docs/ErrorRule/common.md#ER-0001 "服务器运行错误")：服务器运行错误 
* [2](./docs/ErrorRule/common.md#ER-0002 "路由不存在")：路由不存在 
* [3](./docs/ErrorRule/common.md#ER-0003 "路由不支持方法")：路由不支持方法 
* [4](./docs/ErrorRule/common.md#ER-0004 "不支持的媒体协议")：不支持的媒体协议 
* [5](./docs/ErrorRule/common.md#ER-0005 "无法使用请求内容来响应")：无法使用请求内容来响应 
* [6](./docs/ErrorRule/common.md#ER-0006 "请求数据格式错误")：请求数据格式错误 
* [10](./docs/ErrorRule/common.md#ER-0010 "资源不存在")：资源不存在 
* [11](./docs/ErrorRule/common.md#ER-0011 "命令处理器不存在")：命令处理器不存在 
* [12](./docs/ErrorRule/common.md#ER-0012 "翻译器不存在")：翻译器不存在 
* [13](./docs/ErrorRule/common.md#ER-0013 "资源类型不存在")：资源类型不存在 
* [14](./docs/ErrorRule/common.md#ER-0014 "适配器不存在")：适配器不存在 

### 通用错误规范（100-500）

* [100](./docs/ErrorRule/common.md#ER-100 "数据为空")：数据为空 
* [101](./docs/ErrorRule/common.md#ER-101 "数据格式不正确")：数据格式不正确 
* [102](./docs/ErrorRule/common.md#ER-102 "资源不能操作")：资源不能操作 
* [103](./docs/ErrorRule/common.md#ER-103 "资源已存在")：资源已存在 
* [104](./docs/ErrorRule/common.md#ER-104 "数据不正确")：数据不正确 

### 人通用错误规范（501-1000）

* [501](./docs/ErrorRule/user.md#ER-501 "用户姓名格式不正确")：用户姓名格式不正确 
* [502](./docs/ErrorRule/user.md#ER-502 "手机号码格式不正确")：手机号码格式不正确 
* [503](./docs/ErrorRule/user.md#ER-503 "密码格式不正确")：密码格式不正确 
* [504](./docs/ErrorRule/user.md#ER-504 "身份证号码格式不正确")：身份证号码格式不正确 
* [506](./docs/ErrorRule/user.md#ER-506 "登录密码错误")：登录密码错误

### 员工错误规范（1001-1100）

* [1001](./docs/ErrorRule/crew.md#ER-1001 "DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP")：DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP 
