# 网站定制管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [获取首页定制数据](#获取首页定制数据)	
	* [新增](#新增)
	* [发布](#发布)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [网站定制项目字典](../Dictionary/websiteCustomize.md "网站定制项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [网站定制控件规范](../WidgetRule/websiteCustomize.md "网站定制控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    100=>array(
    	'crewId' => '发布人不存在',
    	'websiteCustomizeId' => '网站定制数据不存在'
    );
	101=>array(
		'content'=>内容格式不正确
		'content-keys' => 内容数组格式不正确
		'content-requiredKeys' => 内容必填项格式不正确
		'memorialStatus'=>内容背景置灰状态格式不正确
		'theme' => '页面主题标识格式不正确'
		'theme-color' => '页面主题标识-主题色格式不正确'
		'theme-image' => '页面主题标识-背景图格式不正确'
		'headerBarLeft' => '头部左侧位置标识页面主题标识格式不正确'
		'headerBarLeft-name' => '头部左侧位置标识页面主题标识名称格式不正确'
		'headerBarLeft-status' => '头部左侧位置标识页面主题标识状态格式不正确'
		'headerBarLeft-type' => '头部左侧位置标识页面主题标识类型格式不正确'
		'headerBarLeft-url' => '头部左侧位置标识页面主题标识链接格式不正确'
		'headerBarRight' => '头部右侧位置标识页面主题标识格式不正确'
		'headerBarRight-name' => '头部右侧位置标识页面主题标识名称格式不正确'
		'headerBarRight-status' => '头部右侧位置标识页面主题标识状态格式不正确'
		'headerBarRight-type' => '头部右侧位置标识页面主题标识类型格式不正确'
		'headerBarRight-url' => '头部右侧位置标识页面主题标识链接格式不正确'
		'headerBg' => '网站header背景格式不正确'
		'logo' => '网站logo格式不正确''
		'headerSearch' => '头部搜索栏位置标识格式不正确'
		'headerSearch-status' => '头部搜索栏位置标识状态格式不正确'
		'nav' => '头部导航位置标识格式不正确'
		'nav-count' => '头部导航位置标识数量超过限制'
		'nav-name' => '头部导航位置标识名称格式不正确'
		'nav-status' => '头部导航位置标识状态格式不正确'
		'nav-url' => '头部导航位置标识链接格式不正确'
		'nav-image' => '头部导航位置标识图片格式不正确'
		'centerDialog' => '中间弹框位置标识格式不正确'
		'centerDialog-status' => '中间弹框位置标识状态格式不正确'
		'centerDialog-url' => '中间弹框位置标识链接格式不正确'
		'centerDialog-image' => '中间弹框位置标识图片格式不正确'
		'animateWindow' => '飘窗位置标识格式不正确'
		'animateWindow-count' => '飘窗位置标识数量超出限制'
		'animateWindow-status' => '飘窗位置标识状态格式不正确'
		'animateWindow-image' => '飘窗位置标识图片格式不正确'
		'animateWindow-url' => '飘窗位置标识链接格式不正确'
		'leftFloatCard' => '左侧浮动卡片位置标识格式不正确'
		'leftFloatCard-count' => '左侧浮动卡片位置标识数量超出限制'
		'leftFloatCard-status' => '左侧浮动卡片位置标识状态格式不正确'
		'leftFloatCard-image' => '左侧浮动卡片位置标识图片格式不正确'
		'leftFloatCard-url' => '左侧浮动卡片位置标识链接格式不正确'
		'frameWindow' => '外链窗口格式不正确'
		'frameWindow-status' => '外链窗口状态格式不正确'
		'frameWindow-url' => '外链窗口链接格式不正确'
		'rightToolBar' => '右侧工具栏格式不正确'
		'rightToolBar-count' => '右侧工具栏数量超出限制'
		'rightToolBar-status' => '右侧工具栏状态格式不正确'
		'rightToolBar-name' => '右侧工具栏名称格式不正确'
		'rightToolBar-category' => '右侧工具栏分类格式不正确'
		'rightToolBar-images' => '右侧工具栏图片格式不正确'
		'rightToolBar-images-count' => '右侧工具栏图片数量超出限制'
		'rightToolBar-images-title' => '右侧工具栏图片标题格式不正确'
		'rightToolBar-images-image' => '右侧工具栏图片格式不正确'
		'rightToolBar-url' => '右侧工具栏链接格式不正确'
		'rightToolBar-description' => '右侧工具栏描述格式不正确'
		'specialColumn' => '专题专栏格式不正确'
		'specialColumn-count' => '专题专栏数量超出限制'
		'specialColumn-status' => '专题专栏状态格式不正确'
		'specialColumn-image' => '专题专栏图片格式不正确'
		'specialColumn-url' => '专题专栏链接格式不正确'
		'relatedLinks' => '相关链接格式不正确'
		'relatedLinks-count' => '相关链接数量超出限制'
		'relatedLinks-status' => '相关链接状态格式不正确'
		'relatedLinks-name' => '相关链接名称格式不正确'
		'relatedLinks-items' => '相关链接items格式不正确'
		'relatedLinks-items-status' => '相关链接items状态格式不正确'
		'relatedLinks-items-name' => '相关链接items名称格式不正确'
		'relatedLinks-items-url' => '相关链接items链接格式不正确'
		'footerBanner' => '底部轮播格式不正确'
		'footerBanner-count' => '底部轮播数量超出限制'
		'footerBanner-status' => '底部轮播状态格式不正确'
		'footerBanner-image' => '底部轮播图片格式不正确'
		'footerBanner-url' => '底部轮播链接格式不正确'
		'organizationGroup' => '组织列表格式不正确'
		'organizationGroup-count' => '组织列表数量超出限制'
		'organizationGroup-status' => '组织列表状态格式不正确'
		'organizationGroup-image' => '组织列表图片格式不正确'
		'organizationGroup-url' => '组织列表链接格式不正确'
		'silhouette' => '剪影格式不正确'
		'silhouette-status' => '剪影状态格式不正确'
		'silhouette-image' => '剪影图片格式不正确'
		'silhouette-description' => '剪影描述格式不正确'
		'partyAndGovernmentOrgans' => '党政机关格式不正确'
		'partyAndGovernmentOrgans-status' => '党政机关状态格式不正确'
		'partyAndGovernmentOrgans-image' => '党政机关图片格式不正确'
		'partyAndGovernmentOrgans-url' => '党政机关链接格式不正确'
		'governmentErrorCorrection' => '政府纠错格式不正确'
		'governmentErrorCorrection-status' => '政府纠错状态格式不正确'
		'governmentErrorCorrection-image' => '政府纠错图片格式不正确'
		'governmentErrorCorrection-url' => '政府纠错链接格式不正确'
		'footerNav' => '页脚导航格式不正确'
		'footerNav-status' => '页脚导航状态格式不正确'
		'footerNav-name' => '页脚导航名称格式不正确'
		'footerNav-url' => '页脚导航链接格式不正确'
		'footerTwo' => '页脚第二行内容呈现格式不正确'
		'footerTwo-status' => '页脚第二行内容呈现状态格式不正确'
		'footerTwo-name' => '页脚第二行内容呈现名称格式不正确'
		'footerTwo-url' => '页脚第二行内容呈现链接格式不正确'
		'footerTwo-description' => '页脚第二行内容呈现描述格式不正确'
		'footerThree' => '页脚第三行内容呈现格式不正确'
		'footerThree-status' => '页脚第三行内容呈现状态格式不正确'
		'footerThree-name' => '页脚第三行内容呈现名称格式不正确'
		'footerThree-url' => '页脚第三行内容呈现链接格式不正确'
		'footerThree-description' => '页脚第三行内容呈现描述格式不正确'
		'footerThree-type' => '页脚第三行内容呈现类型格式不正确'
		'footerThree-image' => '页脚第三行内容呈现图片格式不正确'
		'crewId'=>发布人格式不正确
		'category'=>网站定制分类格式不正确
		'status'=>状态格式不正确
	),
	102=>array(
		'status'=>状态不能操作
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| category        | int        | 是            | 0                                            | 定制分类(1 首页)  |
| content         | array      | 是            |                                              | 网站定制内容      |
| crew            | int        | 是            | 1                                            | 发布人           |
| version         | int        | 是            | 2108110987                                   | 版本号          |
| status          | int        | 是            | 0                                            | 状态(0未发布 2 发布)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[websiteCustomizes]
		1.3 include=crew
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'websiteCustomizes/1?fields[websiteCustomizes]=description',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/websiteCustomizes/{id:\d+}

示例

	$response = $client->request('GET', 'websiteCustomizes/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/websiteCustomizes/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'websiteCustomizes/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/websiteCustomizes

	1、检索条件
	    1.1 filter[crew] | 根据发布人搜索,员工id
	    1.2 filter[category] | 根据定制分类搜索 | 1 首页
	    1.3 filter[status] | 根据状态搜索 | 0 未发布 | 2 发布
	    1.4 filter[version] | 根据版本号搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序
		2.2 sort=-id | -id 根据id倒序 | 状态 根据id正序

示例

	$response = $client->request('GET', 'websiteCustomizes?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取首页定制数据">获取首页定制数据</a>

路由

	通过GET传参
	/websiteCustomizes/homePage

示例

	$response = $client->request('GET', 'websiteCustomizes/homePage',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/websiteCustomizes

示例

	$data = array(
		'data' => array(
			"type"=>"websiteCustomizes",
			"attributes"=>array(
				"category"=>1,
				"status"=> 2,
				"content"=>array(
					'memorialStatus' => 0,
					'theme' => 	array(
						'color' => '#41245',
						'image' => array(
							'name' => '图片名称',
							'identify' => '图片地址.png'
						)
					),
					'headerBarLeft' => array(
						array(
							'name' => '访问量统计',
							'status' => 0,
							'url' => '',
							'type' => 1
						),
					),
					'headerBarRight' => array(
						array(
							'name' => '字体',
							'status' => 0,
							'url' => '',
							'type' => 2
						),
						array(
							'name' => '无障碍阅读',
							'status' => 0,
							'url' => '',
							'type' => 3
						),
						array(
							'name' => '登录',
							'status' => 0,
							'url' => '/signIn',
							'type' => 4
						),
						array(
							'name' => '注册',
							'status' => 0,
							'url' => '/signUp',
							'type' => 5
						),
						array(
							'name' => '用户中心',
							'status' => 0,
							'url' => '/members',
							'type' => 6
						),
						array(
							'name' => '网站导航',
							'status' => 0,
							'url' => 'navigations/index',
							'type' => 0
						),
					),
					'headerBg' => array(
						'name' => 'headerBg名称',
						'identify' => 'headerBg地址.png'
					),
					'logo' => array(
						'name' => 'logo名称',
						'identify' => 'logo地址.png'
					),
					'headerSearch' => array(
						'creditInformation' => array(
							'status' => 0
						),
						'personalCreditInformation' => array(
							'status' => 0
						),
						'unifiedSocialCreditCode' => array(
							'status' => 0
						),
						'legalPerson' => array(
							'status' => 0
						),
						'newsTitle' => array(
							'status' => 0
						),
					),
					'nav' => array(
						array(
							'name' => '首页',
							'url' => '/index',
							'image' => array(
								'name' => '首页',
								'identify' => '首页.png'
							),
							'status' => 0
						),
						array(
							'name' => '组织架构',
							'url' => '/organizations/index',
							'image' => array(
								'name' => '组织架构',
								'identify' => '组织架构.png'
							),
							'status' => 0
						),
					),
					'centerDialog' => array(
						'status' =>0,
						'image' => array(
							'name'=> '中间弹框位置',
							'identify' => '中间弹框位置.jpg'
						),
						'url'=>''
					),
					'animateWindow' => array(
						array(
							'status' =>0,
							'image' => array(
								'name'=> '飘窗位置标识',
								'identify' => '飘窗位置标识.jpg'
							),
							'url'=>''
						),
					),
					'leftFloatCard' => array(
						array(
							'status' =>0,
							'image' => array(
								'name'=> '左侧浮动卡片位置标识',
								'identify' => '左侧浮动卡片位置标识.png'
							),
							'url'=>''
						),
					),
					'frameWindow' => array(
						'status' => 0,
						'url' => '/about'
					),
					'rightToolBar' => array(
						array(
							'name' => 'App',
							'category' => 1,
							'status' => 0,
							'images' => array(
								array(
									'title' => '安卓二维码',
									'image' => array(
										'name' => '安卓二维码',
										'identify' => '安卓二维码.jpg'
									)
								),
								array(
									'title' => 'IOS二维码',
									'image' => array(
										'name' => 'IOS二维码',
										'identify' => 'IOS二维码.jpg'
									)
								),
							),
						),
						array(
							'name' => '申诉',
							'category' => 2,
							'status' => 0,
							'url'=> '/appeals'
						),
						array(
							'name' => '联系电话',
							'category' => 3,
							'status' => 0,
							'description'=> '029-81563990'
						),
					),
					'specialColumn' => array(
						array(
							'status' => 0,
							'image' => array(
								'name'=> '专题专栏',
								'identify' => '专题专栏.jpg'
							),
							'url'=>''
						),
					),
					'relatedLinks' => array(
						array(
							'name' => '信用电子政务网站',
							'status' => 0,
							'items' => array(
								array(
									'name' => '国家发展和改革委',
									'url' => 'https://www.ndrc.gov.cn/',
									'status' => 0
								)
							)
						),
						array(
							'name' => '省区市信用网站',
							'status' => 0,
							'items' => array(
								array(
									'name' => '北京市',
									'url' => 'http://jxj.beijing.gov.cn/creditbj/',
									'status' => 0
								)
							)
						),
					),
					'footerBanner' => array(
						array(
							'status' =>0,
							'image' => array(
								'name'=> '底部轮播',
								'identify' => '底部轮播.jpg'
							),
							'url'=>''
						),
					),
					'organizationGroup' => array(
						array(
							'status' =>0,
							'image' => array(
								'name'=> '组织列表',
								'identify' => '组织列表.jpg'
							),
							'url'=>''
						),
					),
					'silhouette' => array(
						'image' => array(
							'name' => '剪影',
							'identify' => '剪影.png'
						),
						'description' => '剪影简介',
						'status'=>0
					),
					'partyAndGovernmentOrgans' => array(
						'status' => 0,
						'image' => array(
							'name' => '党政机关',
							'identify' => '党政机关.png'
						),
						'url' => 'http://bszs.conac.cn/sitename?method=show&id=56F0757F52F025CDE053022819AC2B19'
					),
					'governmentErrorCorrection' => array(
						'status' => 0,
						'image' => array(
							'name' => '政府纠错',
							'identify' => '政府纠错.png'
						),
						'url' => 'http://www.gov.cn/c125533/jbxxk.htm'
					),
					'footerNav' => array(
						array(
							'name' => '帮助中心',
							'url' => '/about',
							'status' => 0
						),
						array(
							'name' => '技术支持',
							'url' => '/about',
							'status' => 0
						),
						array(
							'name' => '关于我们',
							'url' => '/about',
							'status' => 0
						)
					),
					'footerTwo' => array(
						array(
							'name' => '主办单位',
							'description' => '发展和改革委员会',
							'url' => '',
							'status' => 0
						),
						array(
							'name' => '技术支持',
							'description' => '北京企信云信息科技有限公司',
							'url' => 'https://www.qixinyun.com/',
							'status' => 0
						),
					),
					'footerThree' => array(
						array(
							'name' => '备案号',
							'description' => 'ICP备案170000-1',
							'url' => 'https://beian.miit.gov.cn/#/Integrated/index',
							'type' => 7,
							'status' => 0
						),
						array(
							'image' => array(
								'name' => '公网安备',
								'identify' => '公网安备.png'
							),
							'name' => '公网安备',
							'description' => '公网安备',
							'url' => '',
							'type' => 8,
							'status' => 0
						),
						array(
							'name' => '网站标识码',
							'description' => '78907655671',
							'url' => '',
							'type' => 9,
							'status' => 0
						),
					)
				)
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>1)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'websiteCustomizes',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="发布">发布</a>

路由

	通过PATCH传参
	/websiteCustomizes/{id:\d+}/publish

示例

	$response = $client->request('PATCH', 'websiteCustomizes/1/publish',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "websiteCustomizes",
			"id": "1",
			"attributes": {
				"category": 1,
				"content": {
					"nav": [
						{
							"url": "/index",
							"name": "首页",
							"image": {
								"name": "首页",
								"identify": "首页.png"
							},
							"status": "0"
						},
						{
							"url": "/organizations/index",
							"name": "组织架构",
							"image": {
								"name": "组织架构",
								"identify": "组织架构.png"
							},
							"status": "0"
						}
					],
					"logo": {
						"name": "logo名称",
						"identify": "logo地址.png"
					},
					"theme": {
						"color": "#41245",
						"image": {
							"name": "图片名称",
							"identify": "图片地址.png"
						}
					},
					"headerBg": {
						"name": "headerBg名称",
						"identify": "headerBg地址.png"
					},
					"footerNav": [
						{
							"url": "/about",
							"name": "帮助中心",
							"status": "0"
						},
						{
							"url": "/about",
							"name": "技术支持",
							"status": "0"
						},
						{
							"url": "/about",
							"name": "关于我们",
							"status": "0"
						}
					],
					"footerTwo": [
						{
							"url": "",
							"name": "主办单位",
							"status": "0",
							"description": "发展和改革委员会"
						},
						{
							"url": "https://www.qixinyun.com/",
							"name": "技术支持",
							"status": "0",
							"description": "北京企信云信息科技有限公司"
						}
					],
					"silhouette": {
						"image": {
							"name": "剪影",
							"identify": "剪影.png"
						},
						"status": "0",
						"description": "剪影简介"
					},
					"footerThree": [
						{
							"url": "https://beian.miit.gov.cn/#/Integrated/index",
							"name": "备案号",
							"type": "7",
							"status": "0",
							"description": "ICP备案170000-1"
						},
						{
							"url": "",
							"name": "公网安备",
							"type": "8",
							"image": {
								"name": "公网安备",
								"identify": "公网安备.png"
							},
							"status": "0",
							"description": "公网安备"
						},
						{
							"url": "",
							"name": "网站标识码",
							"type": "9",
							"status": "0",
							"description": "78907655671"
						}
					],
					"frameWindow": {
						"url": "/about",
						"status": "0"
					},
					"centerDialog": {
						"url": "",
						"image": {
							"name": "中间弹框位置",
							"identify": "中间弹框位置.jpg"
						},
						"status": "0"
					},
					"footerBanner": [
						{
							"url": "",
							"image": {
								"name": "底部轮播",
								"identify": "底部轮播.jpg"
							},
							"status": "0"
						}
					],
					"headerSearch": {
						"newsTitle": {
							"status": "0"
						},
						"legalPerson": {
							"status": "0"
						},
						"creditInformation": {
							"status": "0"
						},
						"unifiedSocialCreditCode": {
							"status": "0"
						},
						"personalCreditInformation": {
							"status": "0"
						}
					},
					"relatedLinks": [
						{
							"name": "信用电子政务网站",
							"items": [
								{
									"url": "https://www.ndrc.gov.cn/",
									"name": "国家发展和改革委",
									"status": "0"
								}
							],
							"status": "0"
						},
						{
							"name": "省区市信用网站",
							"items": [
								{
									"url": "http://jxj.beijing.gov.cn/creditbj/",
									"name": "北京市",
									"status": "0"
								}
							],
							"status": "0"
						}
					],
					"rightToolBar": [
						{
							"name": "App",
							"images": [
								{
									"image": {
										"name": "安卓二维码",
										"identify": "安卓二维码.jpg"
									},
									"title": "安卓二维码"
								},
								{
									"image": {
										"name": "IOS二维码",
										"identify": "IOS二维码.jpg"
									},
									"title": "IOS二维码"
								}
							],
							"status": "0",
							"category": "1"
						},
						{
							"url": "/appeals",
							"name": "申诉",
							"status": "0",
							"category": "2"
						},
						{
							"name": "联系电话",
							"status": "0",
							"category": "3",
							"description": "029-81563990"
						}
					],
					"animateWindow": [
						{
							"url": "",
							"image": {
								"name": "飘窗位置标识",
								"identify": "飘窗位置标识.jpg"
							},
							"status": "0"
						}
					],
					"headerBarLeft": [
						{
							"url": "",
							"name": "访问量统计",
							"type": "1",
							"status": "0"
						}
					],
					"leftFloatCard": [
						{
							"url": "",
							"image": {
								"name": "左侧浮动卡片位置标识",
								"identify": "左侧浮动卡片位置标识.png"
							},
							"status": "0"
						}
					],
					"specialColumn": [
						{
							"url": "",
							"image": {
								"name": "专题专栏",
								"identify": "专题专栏.jpg"
							},
							"status": "0"
						}
					],
					"headerBarRight": [
						{
							"url": "",
							"name": "\b字体",
							"type": "2",
							"status": "0"
						},
						{
							"url": "",
							"name": "无障碍阅读",
							"type": "3",
							"status": "0"
						},
						{
							"url": "/signIn",
							"name": "登录",
							"type": "4",
							"status": "0"
						},
						{
							"url": "/signUp",
							"name": "注册",
							"type": "5",
							"status": "0"
						},
						{
							"url": "/members",
							"name": "用户中心",
							"type": "6",
							"status": "0"
						},
						{
							"url": "navigations/index",
							"name": "网站导航",
							"type": "0",
							"status": "0"
						}
					],
					"memorialStatus": "0",
					"organizationGroup": [
						{
							"url": "",
							"image": {
								"name": "组织列表",
								"identify": "组织列表.jpg"
							},
							"status": "0"
						}
					],
					"partyAndGovernmentOrgans": {
						"url": "http://bszs.conac.cn/sitename?method=show&amp;id=56F0757F52F025CDE053022819AC2B19",
						"image": {
							"name": "党政机关",
							"identify": "党政机关.png"
						},
						"status": "0"
					},
					"governmentErrorCorrection": {
						"url": "http://www.gov.cn/c125533/jbxxk.htm",
						"image": {
							"name": "政府纠错",
							"identify": "政府纠错.png"
						},
						"status": "0"
					}
				},
				"version": 2108116498,
				"status": 2,
				"createTime": 1628675909,
				"updateTime": 1628675909,
				"statusTime": 0
			},
			"relationships": {
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/websiteCustomizes/1"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 3,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "websiteCustomizes",
				"id": "1",
				"attributes": {
					"category": 1,
					"content": {
						"nav": [
							{
								"url": "/index",
								"name": "首页",
								"image": {
									"name": "首页",
									"identify": "首页.png"
								},
								"status": "0"
							},
							{
								"url": "/organizations/index",
								"name": "组织架构",
								"image": {
									"name": "组织架构",
									"identify": "组织架构.png"
								},
								"status": "0"
							}
						],
						"logo": {
							"name": "logo名称",
							"identify": "logo地址.png"
						},
						"theme": {
							"color": "#41245",
							"image": {
								"name": "图片名称",
								"identify": "图片地址.png"
							}
						},
						"headerBg": {
							"name": "headerBg名称",
							"identify": "headerBg地址.png"
						},
						"footerNav": [
							{
								"url": "/about",
								"name": "帮助中心",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "技术支持",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "关于我们",
								"status": "0"
							}
						],
						"footerTwo": [
							{
								"url": "",
								"name": "主办单位",
								"status": "0",
								"description": "发展和改革委员会"
							},
							{
								"url": "https://www.qixinyun.com/",
								"name": "技术支持",
								"status": "0",
								"description": "北京企信云信息科技有限公司"
							}
						],
						"silhouette": {
							"image": {
								"name": "剪影",
								"identify": "剪影.png"
							},
							"status": "0",
							"description": "剪影简介"
						},
						"footerThree": [
							{
								"url": "https://beian.miit.gov.cn/#/Integrated/index",
								"name": "备案号",
								"type": "7",
								"status": "0",
								"description": "ICP备案170000-1"
							},
							{
								"url": "",
								"name": "公网安备",
								"type": "8",
								"image": {
									"name": "公网安备",
									"identify": "公网安备.png"
								},
								"status": "0",
								"description": "公网安备"
							},
							{
								"url": "",
								"name": "网站标识码",
								"type": "9",
								"status": "0",
								"description": "78907655671"
							}
						],
						"frameWindow": {
							"url": "/about",
							"status": "0"
						},
						"centerDialog": {
							"url": "",
							"image": {
								"name": "中间弹框位置",
								"identify": "中间弹框位置.jpg"
							},
							"status": "0"
						},
						"footerBanner": [
							{
								"url": "",
								"image": {
									"name": "底部轮播",
									"identify": "底部轮播.jpg"
								},
								"status": "0"
							}
						],
						"headerSearch": {
							"newsTitle": {
								"status": "0"
							},
							"legalPerson": {
								"status": "0"
							},
							"creditInformation": {
								"status": "0"
							},
							"unifiedSocialCreditCode": {
								"status": "0"
							},
							"personalCreditInformation": {
								"status": "0"
							}
						},
						"relatedLinks": [
							{
								"name": "信用电子政务网站",
								"items": [
									{
										"url": "https://www.ndrc.gov.cn/",
										"name": "国家发展和改革委",
										"status": "0"
									}
								],
								"status": "0"
							},
							{
								"name": "省区市信用网站",
								"items": [
									{
										"url": "http://jxj.beijing.gov.cn/creditbj/",
										"name": "北京市",
										"status": "0"
									}
								],
								"status": "0"
							}
						],
						"rightToolBar": [
							{
								"name": "App",
								"images": [
									{
										"image": {
											"name": "安卓二维码",
											"identify": "安卓二维码.jpg"
										},
										"title": "安卓二维码"
									},
									{
										"image": {
											"name": "IOS二维码",
											"identify": "IOS二维码.jpg"
										},
										"title": "IOS二维码"
									}
								],
								"status": "0",
								"category": "1"
							},
							{
								"url": "/appeals",
								"name": "申诉",
								"status": "0",
								"category": "2"
							},
							{
								"name": "联系电话",
								"status": "0",
								"category": "3",
								"description": "029-81563990"
							}
						],
						"animateWindow": [
							{
								"url": "",
								"image": {
									"name": "飘窗位置标识",
									"identify": "飘窗位置标识.jpg"
								},
								"status": "0"
							}
						],
						"headerBarLeft": [
							{
								"url": "",
								"name": "访问量统计",
								"type": "1",
								"status": "0"
							}
						],
						"leftFloatCard": [
							{
								"url": "",
								"image": {
									"name": "左侧浮动卡片位置标识",
									"identify": "左侧浮动卡片位置标识.png"
								},
								"status": "0"
							}
						],
						"specialColumn": [
							{
								"url": "",
								"image": {
									"name": "专题专栏",
									"identify": "专题专栏.jpg"
								},
								"status": "0"
							}
						],
						"headerBarRight": [
							{
								"url": "",
								"name": "\b字体",
								"type": "2",
								"status": "0"
							},
							{
								"url": "",
								"name": "无障碍阅读",
								"type": "3",
								"status": "0"
							},
							{
								"url": "/signIn",
								"name": "登录",
								"type": "4",
								"status": "0"
							},
							{
								"url": "/signUp",
								"name": "注册",
								"type": "5",
								"status": "0"
							},
							{
								"url": "/members",
								"name": "用户中心",
								"type": "6",
								"status": "0"
							},
							{
								"url": "navigations/index",
								"name": "网站导航",
								"type": "0",
								"status": "0"
							}
						],
						"memorialStatus": "0",
						"organizationGroup": [
							{
								"url": "",
								"image": {
									"name": "组织列表",
									"identify": "组织列表.jpg"
								},
								"status": "0"
							}
						],
						"partyAndGovernmentOrgans": {
							"url": "http://bszs.conac.cn/sitename?method=show&amp;id=56F0757F52F025CDE053022819AC2B19",
							"image": {
								"name": "党政机关",
								"identify": "党政机关.png"
							},
							"status": "0"
						},
						"governmentErrorCorrection": {
							"url": "http://www.gov.cn/c125533/jbxxk.htm",
							"image": {
								"name": "政府纠错",
								"identify": "政府纠错.png"
							},
							"status": "0"
						}
					},
					"version": 2108116498,
					"status": 2,
					"createTime": 1628675909,
					"updateTime": 1628675909,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/websiteCustomizes/1"
				}
			},
			{
				"type": "websiteCustomizes",
				"id": "2",
				"attributes": {
					"category": 1,
					"content": {
						"nav": [
							{
								"url": "/index",
								"name": "首页",
								"image": {
									"name": "首页",
									"identify": "首页.png"
								},
								"status": "0"
							},
							{
								"url": "/organizations/index",
								"name": "组织架构",
								"image": {
									"name": "组织架构",
									"identify": "组织架构.png"
								},
								"status": "0"
							}
						],
						"logo": {
							"name": "logo名称",
							"identify": "logo地址.png"
						},
						"theme": {
							"color": "#41245",
							"image": {
								"name": "图片名称",
								"identify": "图片地址.png"
							}
						},
						"headerBg": {
							"name": "headerBg名称",
							"identify": "headerBg地址.png"
						},
						"footerNav": [
							{
								"url": "/about",
								"name": "帮助中心",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "技术支持",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "关于我们",
								"status": "0"
							}
						],
						"footerTwo": [
							{
								"url": "",
								"name": "主办单位",
								"status": "0",
								"description": "发展和改革委员会"
							},
							{
								"url": "https://www.qixinyun.com/",
								"name": "技术支持",
								"status": "0",
								"description": "北京企信云信息科技有限公司"
							}
						],
						"silhouette": {
							"image": {
								"name": "剪影",
								"identify": "剪影.png"
							},
							"status": "0",
							"description": "剪影简介"
						},
						"footerThree": [
							{
								"url": "https://beian.miit.gov.cn/#/Integrated/index",
								"name": "备案号",
								"type": "7",
								"status": "0",
								"description": "ICP备案170000-1"
							},
							{
								"url": "",
								"name": "公网安备",
								"type": "8",
								"image": {
									"name": "公网安备",
									"identify": "公网安备.png"
								},
								"status": "0",
								"description": "公网安备"
							},
							{
								"url": "",
								"name": "网站标识码",
								"type": "9",
								"status": "0",
								"description": "78907655671"
							}
						],
						"headerSearch": {
							"newsTitle": {
								"status": "0"
							},
							"legalPerson": {
								"status": "0"
							},
							"creditInformation": {
								"status": "0"
							},
							"unifiedSocialCreditCode": {
								"status": "0"
							},
							"personalCreditInformation": {
								"status": "0"
							}
						},
						"relatedLinks": [
							{
								"name": "信用电子政务网站",
								"items": [
									{
										"url": "https://www.ndrc.gov.cn/",
										"name": "国家发展和改革委",
										"status": "0"
									}
								],
								"status": "0"
							},
							{
								"name": "省区市信用网站",
								"items": [
									{
										"url": "http://jxj.beijing.gov.cn/creditbj/",
										"name": "北京市",
										"status": "0"
									}
								],
								"status": "0"
							}
						],
						"rightToolBar": [
							{
								"name": "App",
								"images": [
									{
										"image": {
											"name": "安卓二维码",
											"identify": "安卓二维码.jpg"
										},
										"title": "安卓二维码"
									},
									{
										"image": {
											"name": "IOS二维码",
											"identify": "IOS二维码.jpg"
										},
										"title": "IOS二维码"
									}
								],
								"status": "0",
								"category": "1"
							},
							{
								"url": "/appeals",
								"name": "申诉",
								"status": "0",
								"category": "2"
							},
							{
								"name": "联系电话",
								"status": "0",
								"category": "3",
								"description": "029-81563990"
							}
						],
						"headerBarLeft": [
							{
								"url": "",
								"name": "访问量统计",
								"type": "1",
								"status": "0"
							}
						],
						"headerBarRight": [
							{
								"url": "",
								"name": "\b字体",
								"type": "2",
								"status": "0"
							},
							{
								"url": "",
								"name": "无障碍阅读",
								"type": "3",
								"status": "0"
							},
							{
								"url": "/signIn",
								"name": "登录",
								"type": "4",
								"status": "0"
							},
							{
								"url": "/signUp",
								"name": "注册",
								"type": "5",
								"status": "0"
							},
							{
								"url": "/members",
								"name": "用户中心",
								"type": "6",
								"status": "0"
							},
							{
								"url": "navigations/index",
								"name": "网站导航",
								"type": "0",
								"status": "0"
							}
						],
						"memorialStatus": "0",
						"organizationGroup": [
							{
								"url": "",
								"image": {
									"name": "组织列表",
									"identify": "组织列表.jpg"
								},
								"status": "0"
							}
						],
						"partyAndGovernmentOrgans": {
							"url": "http://bszs.conac.cn/sitename?method=show&amp;id=56F0757F52F025CDE053022819AC2B19",
							"image": {
								"name": "党政机关",
								"identify": "党政机关.png"
							},
							"status": "0"
						},
						"governmentErrorCorrection": {
							"url": "http://www.gov.cn/c125533/jbxxk.htm",
							"image": {
								"name": "政府纠错",
								"identify": "政府纠错.png"
							},
							"status": "0"
						}
					},
					"version": 2108117471,
					"status": 0,
					"createTime": 1628676189,
					"updateTime": 1628676189,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/websiteCustomizes/2"
				}
			},
			{
				"type": "websiteCustomizes",
				"id": "3",
				"attributes": {
					"category": 1,
					"content": {
						"nav": [
							{
								"url": "/index",
								"name": "首页",
								"image": {
									"name": "首页",
									"identify": "首页.png"
								},
								"status": "0"
							},
							{
								"url": "/organizations/index",
								"name": "组织架构",
								"image": {
									"name": "组织架构",
									"identify": "组织架构.png"
								},
								"status": "0"
							}
						],
						"logo": {
							"name": "logo名称",
							"identify": "logo地址.png"
						},
						"theme": {
							"color": "#41245",
							"image": {
								"name": "图片名称",
								"identify": "图片地址.png"
							}
						},
						"headerBg": {
							"name": "headerBg名称",
							"identify": "headerBg地址.png"
						},
						"footerNav": [
							{
								"url": "/about",
								"name": "帮助中心",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "技术支持",
								"status": "0"
							},
							{
								"url": "/about",
								"name": "关于我们",
								"status": "0"
							}
						],
						"footerTwo": [
							{
								"url": "",
								"name": "主办单位",
								"status": "0",
								"description": "发展和改革委员会"
							},
							{
								"url": "https://www.qixinyun.com/",
								"name": "技术支持",
								"status": "0",
								"description": "北京企信云信息科技有限公司"
							}
						],
						"silhouette": {
							"image": {
								"name": "剪影",
								"identify": "剪影.png"
							},
							"status": "0",
							"description": "剪影简介"
						},
						"footerThree": [
							{
								"url": "https://beian.miit.gov.cn/#/Integrated/index",
								"name": "备案号",
								"type": "7",
								"status": "0",
								"description": "ICP备案170000-1"
							},
							{
								"url": "",
								"name": "公网安备",
								"type": "8",
								"image": {
									"name": "公网安备",
									"identify": "公网安备.png"
								},
								"status": "0",
								"description": "公网安备"
							},
							{
								"url": "",
								"name": "网站标识码",
								"type": "9",
								"status": "0",
								"description": "78907655671"
							}
						],
						"frameWindow": {
							"url": "/about",
							"status": "0"
						},
						"centerDialog": {
							"url": "",
							"image": {
								"name": "中间弹框位置",
								"identify": "中间弹框位置.jpg"
							},
							"status": "0"
						},
						"footerBanner": [
							{
								"url": "",
								"image": {
									"name": "底部轮播",
									"identify": "底部轮播.jpg"
								},
								"status": "0"
							}
						],
						"headerSearch": {
							"newsTitle": {
								"status": "0"
							},
							"legalPerson": {
								"status": "0"
							},
							"creditInformation": {
								"status": "0"
							},
							"unifiedSocialCreditCode": {
								"status": "0"
							},
							"personalCreditInformation": {
								"status": "0"
							}
						},
						"relatedLinks": [
							{
								"name": "信用电子政务网站",
								"items": [
									{
										"url": "https://www.ndrc.gov.cn/",
										"name": "国家发展和改革委",
										"status": "0"
									}
								],
								"status": "0"
							},
							{
								"name": "省区市信用网站",
								"items": [
									{
										"url": "http://jxj.beijing.gov.cn/creditbj/",
										"name": "北京市",
										"status": "0"
									}
								],
								"status": "0"
							}
						],
						"rightToolBar": [
							{
								"name": "App",
								"images": [
									{
										"image": {
											"name": "安卓二维码",
											"identify": "安卓二维码.jpg"
										},
										"title": "安卓二维码"
									},
									{
										"image": {
											"name": "IOS二维码",
											"identify": "IOS二维码.jpg"
										},
										"title": "IOS二维码"
									}
								],
								"status": "0",
								"category": "1"
							},
							{
								"url": "/appeals",
								"name": "申诉",
								"status": "0",
								"category": "2"
							},
							{
								"name": "联系电话",
								"status": "0",
								"category": "3",
								"description": "029-81563990"
							}
						],
						"animateWindow": [
							{
								"url": "",
								"image": {
									"name": "飘窗位置标识",
									"identify": "飘窗位置标识.jpg"
								},
								"status": "0"
							}
						],
						"headerBarLeft": [
							{
								"url": "",
								"name": "访问量统计",
								"type": "1",
								"status": "0"
							}
						],
						"leftFloatCard": [
							{
								"url": "",
								"image": {
									"name": "左侧浮动卡片位置标识",
									"identify": "左侧浮动卡片位置标识.png"
								},
								"status": "0"
							}
						],
						"specialColumn": [
							{
								"url": "",
								"image": {
									"name": "专题专栏",
									"identify": "专题专栏.jpg"
								},
								"status": "0"
							}
						],
						"headerBarRight": [
							{
								"url": "",
								"name": "\b字体",
								"type": "2",
								"status": "0"
							},
							{
								"url": "",
								"name": "无障碍阅读",
								"type": "3",
								"status": "0"
							},
							{
								"url": "/signIn",
								"name": "登录",
								"type": "4",
								"status": "0"
							},
							{
								"url": "/signUp",
								"name": "注册",
								"type": "5",
								"status": "0"
							},
							{
								"url": "/members",
								"name": "用户中心",
								"type": "6",
								"status": "0"
							},
							{
								"url": "navigations/index",
								"name": "网站导航",
								"type": "0",
								"status": "0"
							}
						],
						"memorialStatus": "1",
						"organizationGroup": [
							{
								"url": "",
								"image": {
									"name": "组织列表",
									"identify": "组织列表.jpg"
								},
								"status": "0"
							}
						],
						"partyAndGovernmentOrgans": {
							"url": "http://bszs.conac.cn/sitename?method=show&amp;id=56F0757F52F025CDE053022819AC2B19",
							"image": {
								"name": "党政机关",
								"identify": "党政机关.png"
							},
							"status": "0"
						},
						"governmentErrorCorrection": {
							"url": "http://www.gov.cn/c125533/jbxxk.htm",
							"image": {
								"name": "政府纠错",
								"identify": "政府纠错.png"
							},
							"status": "0"
						}
					},
					"version": 2108113563,
					"status": 2,
					"createTime": 1628676212,
					"updateTime": 1628676212,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/websiteCustomizes/3"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}