# 委办局接口文档

---

## 简介

本文档主要描述委办局模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用字典](./docs/Dictionary/common.md "通用字典")
    * [委办局字典](./docs/Dictionary/userGroup.md "委办局字典")
* <a name="控件规范">控件规范</a>
    * [通用控件规范](./docs/WidgetRule/common.md "通用控件规范")
* <a name="错误规范">错误规范</a>
    * [通用错误规范](./docs/ErrorRule/common.md "通用错误规范")
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

<table>
  <tr>
    <th><b>英文名称</b></th>
    <th><b>类型</b></th>
    <th><b>请求参数是否必填</b></th>
    <th><b>示例</b></th>
    <th><b>描述</b></th>
  </tr> 
  <tr>
    <td>name</td>
    <td>string</td>
    <td>非请求参数</td>
    <td>发展和改革委员会</td>
    <td>委办局名称</td>
  </tr>
  <tr>
    <td>shortName</td>
    <td>string</td>
    <td>非请求参数</td>
    <td>发改委</td>
    <td>委办局简称</td>
  </tr>
  <tr>
    <td>purview</td>
    <td>array</td>
    <td>否</td>
    <td>array('结构依据前端服务层及前端商定结果')</td>
    <td>权限范围</td>
  </tr>
</table>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=userGroup
2、fields[TYPE]请求参数
    2.1 fields[userGroups]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'userGroups/1?fields[userGroups]=name,updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/userGroups/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'userGroups/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/userGroups/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'userGroups/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/userGroups

1、检索条件
    1.1 filter[name] | 根据 委办局名称 搜索
2、排序
    2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
```

示例

```php
$response = $client->request('GET', 'userGroups?filter[name]=发改委&sort=-updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

```
{
    "meta": [],
    "data": {
        "type": "userGroups",
        "id": "1",
        "attributes": {
            "name": "发展和改革委员会",
            "shortName": "发改委",
            "status": 0,
            "createTime": 1531897790,
            "updateTime": 1531897790,
            "statusTime": 0
        },
        "links": {
            "self": "127.0.0.1:8080\/userGroups\/1"
        }
    }
}
```

#### <a name="多条示例">多条示例</a>

```
{
    "meta": {
        "count": 25,
        "links": {
            "first": 1,
            "last": 13,
            "prev": null,
            "next": 2
        }
    },
    "links": {
        "first": "127.0.0.1:8080\/userGroups?page[number]=1&page[size]=2",
        "last": "127.0.0.1:8080\/userGroups?page[number]=13&page[size]=2",
        "prev": null,
        "next": "127.0.0.1:8080\/userGroups?page[number]=2&page[size]=2"
    },
    "data": [
        {
            "type": "userGroups",
            "id": "1",
            "attributes": {
                "name": "发展和改革委员会",
                "shortName": "发改委",
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/userGroups\/1"
            }
        },
        {
            "type": "userGroups",
            "id": "2",
            "attributes": {
                "name": "市场监督管理局",
                "shortName": "市监局",
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/userGroups\/2"
            }
        }
    ]
}
```

