# 企业接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [统计企业关联数据数量](#统计企业关联数据数量)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [企业项目字典](../Dictionary/enterprise.md "企业项目字典")

## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是             | 企业名称                                     | 主体名称        |
| unifiedSocialCreditCode | string | 是         | 918909765432345678Q                         | 统一社会信用代码  |
| establishmentDate| int       | 是             | 20210801                                     | 成立日期        |
| approvalDate     | int       | 是             | 20210801                                     | 核准日期        |
| address          | string    | 是            | 陕西省西安市雁塔区长延堡街道                       | 住所/经营场所    |
| registrationCapital | string | 是            | 100                                           | 注册资本（万）    |
| businessTermStart | int      | 是            | 20210801                                     | 营业期限自        |
| businessTermTo    | int      | 是            | 20210801                                     | 营业期限至        |
| businessScope     | string    | 是           | 电子产品                                       | 经营范围         |
| registrationAuthority | string | 是          | 发改委                                         | 登记机关         |
| principal          | string   | 是           | 张萍                                           | 法定代表人/经营者 |
| principalCardId    | string   | 是           | 412825199009097654                             | 法人身份证号     |
| registrationStatus | string   | 是           | 存续(开业)                                       | 登记状态        |
| enterpriseTypeCode | string   | 是           | 100                                            | 企业类型代码     |
| enterpriseType     | string   | 是           | 内资企业                                         | 企业类型        |
| industryCategory   | string   | 是           | A                                               | 行业门类        |
| industryCode       | string   | 是           | 0111                                            | 行业代码        |
| administrativeArea | int      | 是           | 0111                                            | 所属区域        |
| status             | int        | 是            | 0                                            | 状态(0正常 -2 删除)|
| updateTime         | int        |               | 1535444931                                   | 更新时间         |
| creditTime         | int        |               | 1535444931                                   | 创建时间         |
| statusTime         | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[enterprises]
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'enterprises/1?fields[enterprises]=status',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/enterprises/{id:\d+}

示例

	$response = $client->request('GET', 'enterprises/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/enterprises/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'enterprises/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/enterprises

	1、检索条件
		1.1 filter[name] | 根据主体名称搜索
		1.2 filter[unifiedSocialCreditCode] | 根据统一社会信用代码搜索
		1.3 filter[principal] | 根据企业法人搜索 
		1.4 filter[identify] | 根据主体名称或统一社会信用代码搜索
	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序

示例

	$response = $client->request('GET', 'enterprises?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="统计企业关联数据数量">统计企业关联数据数量</a>

路由

	通过GET传参
	/statisticals/enterpriseRelationInformationCount

	1、检索条件
	    1.1 filter[unifiedSocialCreditCode] | 根据企业统一社会信用代码搜索
	    1.2 filter[dimension] | 根据公开范围搜索

示例

	$response = $client->request('GET', 'statisticals/enterpriseRelationInformationCount?filter[unifiedSocialCreditCode]=9127812999A2323X',['headers'=>['Content-' => 'application/vnd.api+json']]);

返回示例

	{
		"meta": [],
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": {
					"licensingTotal": "2",
					"penaltyTotal": "0",
					"incentiveTotal": "0",
					"punishTotal": "0"
				}
			},
			"links": {
				"self": "127.0.0.1:8089/statisticals/0"
			}
		}
	}

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "enterprises",
			"id": "4",
			"attributes": {
				"name": "宏远园林工程有限公司",
				"unifiedSocialCreditCode": "610527199403165500",
				"establishmentDate": 19980323,
				"approvalDate": 20180504,
				"address": "黑牛城道298号",
				"registrationCapital": "100",
				"businessTermStart": 19980323,
				"businessTermTo": 20180504,
				"businessScope": "园林绿化工程；园林绿化工程设计；花、苗、木（种子除外）生产销售、租、摆；室内外装饰；市政公用工程施工；园林机械设备、建材制品、五金交电化工（不含危险品）、日用杂品、装饰材料、百货零售兼批发；场地租赁；劳务服务（不含劳务派遣）；房屋租赁。（依法须经批准的项目，经相关部门批准后方可开展经营活动",
				"registrationAuthority": "市场和质量监督管理局",
				"principal": "王新华",
				"principalCardId": "610526199703036719",
				"registrationStatus": "存续（在营、开业、在册)",
				"enterpriseTypeCode": "150",
				"enterpriseType": "有限责任公司",
				"data": [],
				"industryCategory": "A",
				"industryCode": "11",
				"administrativeArea": 0,
				"status": 0,
				"createTime": 1516174523,
				"updateTime": 1516174523,
				"statusTime": 0
			},
			"links": {
				"self": "127.0.0.1:8089/enterprises/4"
			}
		}
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 4,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "enterprises",
				"id": "1",
				"attributes": {
					"name": "百瑞泰管业股份有限公司",
					"unifiedSocialCreditCode": "21120101600530263M",
					"establishmentDate": 20000110,
					"approvalDate": 20000110,
					"address": "南阳",
					"registrationCapital": "100",
					"businessTermStart": 20000110,
					"businessTermTo": 20300110,
					"businessScope": "装饰类",
					"registrationAuthority": "发展和改革委员会",
					"principal": "百泰",
					"principalCardId": "610526199703036713",
					"registrationStatus": "营业",
					"enterpriseTypeCode": "3661",
					"enterpriseType": "有限公司",
					"data": [],
					"industryCategory": "H",
					"industryCode": "2374",
					"administrativeArea": 0,
					"status": 0,
					"createTime": 1516174523,
					"updateTime": 1516174523,
					"statusTime": 0
				},
				"links": {
					"self": "127.0.0.1:8089/enterprises/1"
				}
			},
			{
				"type": "enterprises",
				"id": "2",
				"attributes": {
					"name": "德恒科技有限公司",
					"unifiedSocialCreditCode": "31120101600530264M",
					"establishmentDate": 20000101,
					"approvalDate": 20000109,
					"address": "南阳",
					"registrationCapital": "18",
					"businessTermStart": 20000109,
					"businessTermTo": 20300109,
					"businessScope": "科技类",
					"registrationAuthority": "发展和改革委员会",
					"principal": "赵楚",
					"principalCardId": "111525198503245845",
					"registrationStatus": "营业",
					"enterpriseTypeCode": "3661",
					"enterpriseType": "有限公司",
					"data": [],
					"industryCategory": "G",
					"industryCode": "2373",
					"administrativeArea": 0,
					"status": 0,
					"createTime": 1516174523,
					"updateTime": 1516174523,
					"statusTime": 0
				},
				"links": {
					"self": "127.0.0.1:8089/enterprises/2"
				}
			},
			{
				"type": "enterprises",
				"id": "3",
				"attributes": {
					"name": "青曲烟酒有限公司",
					"unifiedSocialCreditCode": "323434323234545001",
					"establishmentDate": 20200411,
					"approvalDate": 20200411,
					"address": "南阳",
					"registrationCapital": "100",
					"businessTermStart": 20200411,
					"businessTermTo": 20500411,
					"businessScope": "便利店",
					"registrationAuthority": "发展和改革委员会",
					"principal": "李而",
					"principalCardId": "610526199603036713",
					"registrationStatus": "登记状态",
					"enterpriseTypeCode": "3661",
					"enterpriseType": "有限公司",
					"data": [],
					"industryCategory": "A",
					"industryCode": "11",
					"administrativeArea": 0,
					"status": 0,
					"createTime": 1516174523,
					"updateTime": 1516174523,
					"statusTime": 0
				},
				"links": {
					"self": "127.0.0.1:8089/enterprises/3"
				}
			},
			{
				"type": "enterprises",
				"id": "4",
				"attributes": {
					"name": "宏远园林工程有限公司",
					"unifiedSocialCreditCode": "610527199403165500",
					"establishmentDate": 19980323,
					"approvalDate": 20180504,
					"address": "黑牛城道298号",
					"registrationCapital": "100",
					"businessTermStart": 19980323,
					"businessTermTo": 20180504,
					"businessScope": "园林绿化工程；园林绿化工程设计；花、苗、木（种子除外）生产销售、租、摆；室内外装饰；市政公用工程施工；园林机械设备、建材制品、五金交电化工（不含危险品）、日用杂品、装饰材料、百货零售兼批发；场地租赁；劳务服务（不含劳务派遣）；房屋租赁。（依法须经批准的项目，经相关部门批准后方可开展经营活动",
					"registrationAuthority": "市场和质量监督管理局",
					"principal": "王新华",
					"principalCardId": "610526199703036719",
					"registrationStatus": "存续（在营、开业、在册)",
					"enterpriseTypeCode": "150",
					"enterpriseType": "有限责任公司",
					"data": [],
					"industryCategory": "A",
					"industryCode": "11",
					"administrativeArea": 0,
					"status": 0,
					"createTime": 1516174523,
					"updateTime": 1516174523,
					"statusTime": 0
				},
				"links": {
					"self": "127.0.0.1:8089/enterprises/4"
				}
			}
		]
	}