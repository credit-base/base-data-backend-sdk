# 前台用户管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [注册](#注册)
	* [编辑](#编辑)
	* [登录](#登录)
	* [忘记密码](#忘记密码)
	* [修改密码](#修改密码)
	* [验证密保答案](#验证密保答案)
	* [启用](#启用)
	* [禁用](#禁用)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [前台用户控件规范](../WidgetRule/member.md "前台用户控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	10=>array(
		'userName'=>用户名不存在
	),
	101=>array(
		'userName'=>用户名格式不正确
		'email'=>邮箱格式不正确
		'contactAddress'=>联系地址格式不正确
		'securityQuestion'=>密保问题格式不正确
		'securityAnswer'=>密保答案格式不正确
		'gender'=>性别格式不正确
	),
	102=>array(
		'status'=>状态不能操作,
		'signInStatus'=>状态不能操作,用户被禁用
	),
	103=>array(
		'userName'=>用户名已经存在
		'cellphone'=>手机号已经存在
		'email'=>邮箱已经存在
	),
	104=>array(
		'securityAnswer'=>密保答案不正确
	),
	501=>姓名格式不正确
	502=>手机号码格式不正确
	503=>密码格式不正确
	504=>身份证号码格式不正确
	505=>array(
		'password'=>密码不正确
		'oldPassword'=>旧密码不正确
	)
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| userName        | string     | 是            | 张科                                         | 用户名           |
| realName        | string     | 是            | 张科                                         | 姓名             |
| cellphone       | string     | 是            | 18800000000                                  | 手机号           |
| cardId          | string     | 是            | 412825199009099876                           | 身份证号         |
| email           | string     | 是            | 997945678@qq.com                             | 邮箱            |
| contactAddress  | string     | 是            | 雁塔区长延堡街道                               | 联系地址          |
| securityQuestion| int        | 是            | 1                                           | 密保问题          |
| securityAnswer  | string     | 是            | 黑色                                         | 密保答案          |
| gender          | int        | 否            | 1                                           | 性别              |
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[members]
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'members/1?fields[members]=realName',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/members/{id:\d+}

示例

	$response = $client->request('GET', 'members/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/members/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'members/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/members

	1、检索条件
	    1.1 filter[realName] | 根据姓名搜索
	    1.2 filter[cellphone] | 根据手机号搜索
	    1.3 filter[status] | 根据状态搜索 | 0 启用 | -2 禁用

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序

示例

	$response = $client->request('GET', 'members?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="注册">注册</a>
	
路由

	通过POST传参
	/members

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"userName"=>"用户名",
				"realName"=>"姓名",
				"cardId"=>"身份证号",
				"cellphone"=>"手机号",
				"email"=>"邮箱",
				"contactAddress"=>"联系地址",
				"securityQuestion"=>密保问题,
				"securityAnswer"=>"密保答案",
				"password"=>"密码""
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'members',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/members/{id:\d+}

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"gender"=>性别
			),
		)
	);

	$response = $client->request(
	                'PATCH',
	                'members/1',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="登录">登录</a>
路由

	通过POST传参
	/members/signIn

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"userName"=>'用户名',
				"password"=>'Admin123$'
			),
		)
	);

	$response = $client->request(
	                'POST',
	                '/members/signIn',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="忘记密码">忘记密码</a>
路由

	通过PATCH传参
	/members/resetPassword

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"userName"=>用户名,
				"securityAnswer"=>密保答案,
				"password"=>密码
			),
		)
	);

	$response = $client->request(
	                'PATCH',
	                '/members/resetPassword',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="修改密码">修改密码</a>
路由

	通过PATCH传参
	/members/{id:\d+}/updatePassword

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"oldPassword"=>旧密码,
				"password"=>密码
			),
		)
	);

	$response = $client->request(
	                'PATCH',
	                '/members/1/updatePassword',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="验证密保答案">验证密保答案</a>
路由

	通过PATCH传参
	/members/{id:\d+}/validateSecurity

示例

	$data = array(
		'data' => array(
			"type"=>"members",
			"attributes"=>array(
				"securityAnswer"=>密保答案
			),
		)
	);

	$response = $client->request(
	                'PATCH',
	                '/members/1/validateSecurity',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="启用">启用</a>

路由

	通过PATCH传参
	/members/{id:\d+}/enable

示例

	$response = $client->request('PATCH', 'members/1/enable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="禁用">禁用</a>

路由

	通过PATCH传参
	/members/{id:\d+}/disable

示例

	$response = $client->request('PATCH', 'members/1/disable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "members",
			"id": "1",
			"attributes": {
				"userName": "用户名",
				"realName": "姓名",
				"cellphone": "手机号",
				"email": "邮箱",
				"cardId": "身份证号",
				"contactAddress": "联系地址",
				"gender": "性别",
				"securityQuestion": 1,
				"status": 0,
				"createTime": 1620831782,
				"updateTime": 1620872174,
				"statusTime": 1620869215
			}
			"links": {
				"self": "127.0.0.1:8089/news/13"
			}
		},
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "member",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "手机号",
					"email": "邮箱",
					"cardId": "身份证号",
					"contactAddress": "联系地址",
					"gender": "性别",
					"securityQuestion": 1,
					"status": 0,
					"createTime": 1620831782,
					"updateTime": 1620872174,
					"statusTime": 1620869215
				}
				"links": {
					"self": "127.0.0.1:8089/news/4"
				}
			},
		]
	}