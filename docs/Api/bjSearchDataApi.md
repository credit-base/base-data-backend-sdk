# 本级资源目录数据搜索接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [确认](#确认)
	* [封存](#封存)
	* [屏蔽](#屏蔽)
	* [新增](#新增)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [资源目录数据通用项目字典](../Dictionary/searchData.md "资源目录数据通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [委办局资源目录项目字典](../Dictionary/bjTemplate.md "委办局资源目录项目字典")
	* [本级资源目录数据项目字典](../Dictionary/bjSearchData.md "本级资源目录数据项目字典")

*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [资源目录数据通用控件规范](../WidgetRule/searchData.md "资源目录数据通用控件规范")
	* [本级资源目录数据控件规范](../WidgetRule/bjSearchData.md "本级资源目录数据控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    100=>array(
        'crew'=>发布员工为空
        'sourceUnit'=>来源委办局为空
        'template'=>资源目录为空
    ),
    103=>array(
        'hash'=>资源目录数据已经存在
    ),
    10=>array(
        'subjectCategory'=>主体类型不存在
    ),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| infoClassify    | int        | 是            | 1                                            | 信息分类(1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他) |
| infoCategory    | int        | 是            | 1                                            | 信息类别(1 基础信息 2 守信信息 3 失信信息 4 其他信息)    |
| crew            | int        | 是            | 1                                            | 发布人           |
| sourceUnit      | int        | 是            | 1                                            | 来源单位          |
| subjectCategory | int        | 是            | 1                                            | 主体类别(1 法人及非法人组织 2 自然人 3 个体工商户)        |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)               |
| name            | string     | 是            | 张文                                          | 主体名称          |
| identify        | string     | 是            | 412825199309094321                           | 主体标识          |
| expirationDate  | int        | 是            | 1618284835                                   | 有效期限          |
| status          | int        | 是            | 0                                            | 状态(0待确认 2 已确认 -2 屏蔽 -4 封存)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| itemsData       | int        | 是            | 1                                            | 资源目录数据      |
| itemsData       | int        | 是            | 1                                            | 资源目录数据      |
| description     | string     | 否            | 决定日期不能大于11个工作日                       | 待确认规则描述     |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[templates]
	    1.4 fields[bjItemsData]
		1.5 include = crew,sourceUnit,itemsData,template
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'bjSearchData/1?fields[bjSearchData]=name',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/bjSearchData/{id:\d+}

示例

	$response = $client->request('GET', 'bjSearchData/1',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/bjSearchData/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'bjSearchData/1,2,3',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/bjSearchData

	1、检索条件
	    1.1 filter[infoClassify] | 根据信息分类搜索 | 1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他
	    1.2 filter[infoCategory] | 根据信息类别搜索 | 1 基础信息 2 守信信息 3 失信信息 4 其他信息
	    1.3 filter[subjectCategory] | 根据主体类别搜索 | 1 法人及非法人组织 2 自然人 3 个体工商户
	    1.4 filter[dimension] | 根据公开范围搜索 | 1 社会公开 2 政务共享 3 授权查询
	    1.5 filter[status] | 根据状态搜索 | 0 待确认 2 已确认 -2 屏蔽 -4 封存
	    1.6 filter[expirationDate] | 根据有效期限搜索
	    1.7 filter[sourceUnit] | 根据来源单位搜索,来源单位id
	    1.8 filter[name] | 根据主体名称搜索
	    1.9 filter[identify] | 根据主体标识搜索
	    1.10 filter[template] | 根据资源目录搜索,委办局资源目录id

	2、排序
		2.1 sort=-id | -id 根据id倒序 | id 根据id正序
		2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序

示例

	$response = $client->request('GET', 'bjSearchData?sort=-id&page[number]=1&page[size]=20',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="确认">确认</a>

路由

	通过PATCH传参
	/bjSearchData/{id:\d+}/confirm

示例

	$response = $client->request('PATCH', 'bjSearchData/1/confirm',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="封存">封存</a>

路由

	通过PATCH传参
	/bjSearchData/{id:\d+}/delete

示例

	$response = $client->request('PATCH', 'bjSearchData/1/delete',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="屏蔽">屏蔽</a>

路由

	通过PATCH传参
	/bjSearchData/{id:\d+}/disable

示例

	$response = $client->request('PATCH', 'bjSearchData/1/disable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>

	$data = array(
		"data" => array(
			"type" => "bjSearchData",
			"attributes" => array(
				"subjectCategory" => 1,    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
				"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
				"expirationDate" => 1618285915,    //有效期限
			),
			"relationships" => array(
				"crew" => array( // 来源单位
					"data" => array(
						array("type" => "crews", "id" => 1)
					)
				),
				"sourceUnit" => array( // 来源单位
					"data" => array(
						array("type" => "userGroups", "id" => 1)
					)
				),
				"template" => array( // 委办局资源目录
					"data" => array(
						array("type" => "bjTemplates", "id" => 1)
					)
				),
				"itemsData" => array( //资源目录数据
					"data" => array(
						array(
							"type" => "itemsData", 
							"attributes" => array(
								"data" => array(
									"ZTMC" => "陕西传媒有限公司",
									"TYSHXYDM" => "921345679023456789"
								)
							)
						)
					)
				)
			)
		)
	);

```
$bjSearchData = new BjSearchData();

$bjSearchData->setCrew(new Crew(1));
$bjSearchData->setSourceUnit(new UserGroup(1));
$bjSearchData->setSubjectCategory(2);
$bjSearchData->setDimension(1);
$bjSearchData->setExpirationDate(1618285915);

$templateRepository = new BjTemplateRepository();
$bjTemplate = $templateRepository->fetchOne(4);

$bjSearchData->setTemplate($bjTemplate);

$data = array(
    "ZTMC" => "张文",
    "ZJHM" => "412819199409098765"
);

$bjItemsData = new BjItemsData();
$bjItemsData->setData($data);

$hash = md5(base64_encode(gzcompress(serialize($data))));

$bjSearchData->setItemsData($bjItemsData);
$bjSearchData->setHash($hash);

$bjSearchData->add();

```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "bjSearchData",
			"id": "1",
			"attributes": {
				"infoClassify": 1,
				"infoCategory": 1,
				"subjectCategory": 1,
				"dimension": 1,
				"name": "巨奥传媒有限公司",
				"identify": "4485508123572982",
				"expirationDate": 4102329600,
				"description": "",
				"status": -2,
				"frontEndProcessorStatus": 0,
				"createTime": 1624520615,
				"updateTime": 1624522223,
				"statusTime": 1624522223
			},
			"relationships": {
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"sourceUnit": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"itemsData": {
					"data": {
						"type": "bjItemsData",
						"id": "1"
					}
				},
				"template": {
					"data": {
						"type": "bjTemplates",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/bjSearchData/1"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "bjItemsData",
				"id": "1",
				"attributes": {
					"data": {
						"ZTMC": "巨奥传媒有限公司",
						"TYSHXYDM": "4485508123572982",
						"XXLB": "其他信息"
					}
				}
			},
			{
				"type": "bjTemplates",
				"id": "1",
				"attributes": {
					"name": "登记信息",
					"identify": "DJXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "目录描述信息",
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "信用主体代码",
							"identify": "TYSHXYDM",
							"isMasked": "1",
							"maskRule": [
								"3",
								"4"
							],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息",
								"守信信息",
								"失信信息",
								"其他信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "1",
							"maskRule": [
								"1",
								"2"
							],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1618284788,
					"updateTime": 1618284788,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"gbTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "1"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 4,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "bjSearchData",
				"id": "1",
				"attributes": {
					"infoClassify": 1,
					"infoCategory": 1,
					"subjectCategory": 1,
					"dimension": 1,
					"name": "巨奥传媒有限公司",
					"identify": "4485508123572982",
					"expirationDate": 4102329600,
					"description": "",
					"status": -2,
					"frontEndProcessorStatus": 0,
					"createTime": 1624520615,
					"updateTime": 1624522223,
					"statusTime": 1624522223
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "bjItemsData",
							"id": "1"
						}
					},
					"template": {
						"data": {
							"type": "bjTemplates",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/bjSearchData/1"
				}
			},
			{
				"type": "bjSearchData",
				"id": "10",
				"attributes": {
					"infoClassify": 1,
					"infoCategory": 1,
					"subjectCategory": 1,
					"dimension": 2,
					"name": "创联世纪传媒有限公司",
					"identify": "2450513052271635",
					"expirationDate": 4102329600,
					"description": "数据不符合确认规则",
					"status": -4,
					"frontEndProcessorStatus": 0,
					"createTime": 1624520677,
					"updateTime": 1624522387,
					"statusTime": 1624522387
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "bjItemsData",
							"id": "10"
						}
					},
					"template": {
						"data": {
							"type": "bjTemplates",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/bjSearchData/10"
				}
			},
			{
				"type": "bjSearchData",
				"id": "8",
				"attributes": {
					"infoClassify": 1,
					"infoCategory": 1,
					"subjectCategory": 1,
					"dimension": 2,
					"name": "鸿睿思博传媒有限公司",
					"identify": "5401573896282928",
					"expirationDate": 4102329600,
					"description": "数据不符合确认规则",
					"status": 0,
					"frontEndProcessorStatus": 0,
					"createTime": 1624520677,
					"updateTime": 1624520677,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "bjItemsData",
							"id": "8"
						}
					},
					"template": {
						"data": {
							"type": "bjTemplates",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/bjSearchData/8"
				}
			},
			{
				"type": "bjSearchData",
				"id": "2",
				"attributes": {
					"infoClassify": 1,
					"infoCategory": 1,
					"subjectCategory": 3,
					"dimension": 2,
					"name": "海创传媒有限公司",
					"identify": "5577369274085100",
					"expirationDate": 4102329600,
					"description": "",
					"status": 2,
					"frontEndProcessorStatus": 0,
					"createTime": 1624520615,
					"updateTime": 1624520615,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "bjItemsData",
							"id": "2"
						}
					},
					"template": {
						"data": {
							"type": "bjTemplates",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/bjSearchData/2"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "bjItemsData",
				"id": "1",
				"attributes": {
					"data": {
						"ZTMC": "巨奥传媒有限公司",
						"TYSHXYDM": "4485508123572982",
						"XXLB": "其他信息"
					}
				}
			},
			{
				"type": "bjTemplates",
				"id": "1",
				"attributes": {
					"name": "登记信息",
					"identify": "DJXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "目录描述信息",
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "信用主体代码",
							"identify": "TYSHXYDM",
							"isMasked": "1",
							"maskRule": [
								"3",
								"4"
							],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息",
								"守信信息",
								"失信信息",
								"其他信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "1",
							"maskRule": [
								"1",
								"2"
							],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1618284788,
					"updateTime": 1618284788,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"gbTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "bjItemsData",
				"id": "10",
				"attributes": {
					"data": {
						"ZTMC": "创联世纪传媒有限公司",
						"TYSHXYDM": "2450513052271635",
						"XXLB": "守信信息"
					}
				}
			},
			{
				"type": "bjItemsData",
				"id": "8",
				"attributes": {
					"data": {
						"ZTMC": "鸿睿思博传媒有限公司",
						"TYSHXYDM": "5401573896282928",
						"XXLB": "基础信息"
					}
				}
			},
			{
				"type": "bjItemsData",
				"id": "2",
				"attributes": {
					"data": {
						"ZTMC": "海创传媒有限公司",
						"TYSHXYDM": "5577369274085100",
						"XXLB": "失信信息"
					}
				}
			}
		]
	}