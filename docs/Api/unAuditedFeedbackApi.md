# 问题反馈审核管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新受理](#重新受理)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [互动类项目字典](../Dictionary/interaction.md "互动类项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
	* [受理委办局项目字典](../Dictionary/UserGroup.md "受理委办局项目字典")
	* [问题反馈项目字典](../Dictionary/feedback.md "问题反馈项目字典")
	* [问题反馈审核项目字典](../Dictionary/unAuditedFeedback.md "问题反馈审核项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [互动类通用控件规范](../WidgetRule/interaction.md "互动类通用控件规范")
	* [问题反馈控件规范](../WidgetRule/feedback.md "问题反馈控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'replyContent'=>回复内容格式不正确
		'replyImages'=>回复图片格式不正确
		'replyImagesCount'=>回复图片数量超出限制
		'admissibility'=>受理情况格式不正确
		'replyCrew'=>受理人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
	
## <a name="参数说明">参数说明</a>
     
### unAuditedFeedback

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 个人反馈                                       | 标题            |
| content         | string     | 是            | 反馈内容                                       | 内容            |
| member          | int        | 是            | 1                                            | 前台用户         |
| acceptUserGroup | int        | 是            | 1                                            | 受理委办局        |
| acceptStatus    | int        | 是            | 0                                            | 受理状态(0 待受理, 1 受理中, 2 受理完成)|
| reply           | int        | 是            | 1                                            | 回复信息        |
| status          | int        | 是            | 0                                            | 状态(0 正常, -2 撤销)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| applyStatus     | int        | 是            | 0                                            | 审核状态(默认 0待审核 2审核通过 -2 审核驳回)|
| rejectReason    | string     | 是            | 内容不合理                                     | 驳回原因          |
| operationType   | int        | 是            | 0                                            | 操作类型(1 添加 2 编辑 3 启用 4 禁用 5 置顶 6 取消置顶 7 移动 8 受理)|
| applyInfoCategory| int        | 是            | 0                                           | 可以申请的信息分类(6 互动类 )|
| applyInfoType    | int        | 是            | 0                                           | 可以申请的最小信息分类(问题反馈 1)|
| applyCrew        | int        | 是            | Crew                                             | 审核人           |
| applyUserGroup   | int        | 是            | UserGroup                                           | 受理委办局          |
| relation         | int        | 是            | Member                                           | 修改人          |

### reply

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| content         | string     | 是            | 回复内容                                       | 回复内容        |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 回复图片  |
| crew            | int        | 是            | 1                                            | 受理人         |
| admissibility   | int        | 是            | 0                                            | 受理情况(1 予以受理, 2 不予受理)|
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[members]
	    1.2 fields[userGroups]
	    1.3 fields[crews]
	    1.4 fields[replies]
	    1.5 fields[unAuditedFeedbacks]
		1.6 include = member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation

	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'unAuditedFeedbacks/1?fields[unAuditedFeedbacks]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedFeedbacks/{id:\d+}

示例

	$response = $client->request('GET', 'unAuditedFeedbacks/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedFeedbacks/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'unAuditedFeedbacks/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedFeedbacks

	1、检索条件
	    1.1 filter[applyUserGroup] | 根据受理委办局搜索,委办局id
	    1.2 filter[applyInfoCategory] | 根据可以申请的信息分类搜索
	    1.3 filter[applyInfoType] | 可以申请的最小信息分类,此处为互动类类型
	    1.4 filter[relation] | 根据关联信息搜索,此处为前台用户id
	    1.5 filter[title] | 根据标题搜索
	    1.6 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 审核驳回 | 2 审核通过
	    1.7 filter[operationType] | 根据操作类型搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 审核状态 根据审核状态正序

示例

	$response = $client->request('GET', 'unAuditedFeedbacks?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="重新受理">重新受理</a>
	
路由

	通过PATCH传参
	/unAuditedFeedbacks/{id:\d+}/resubmit

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedFeedbacks",
			"relationships"=>array(
				"reply"=>array(
					"data"=>array(
						array(
							"type"=>"replies",
							"attributes"=>array(
								"content"=>"回复内容",
								"images"=>array(
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg')
								),
								"admissibility"=>0
							),
							"relationships"=>array(
								"crew"=>array(
									"data"=>array(
										array("type"=>"crews","id"=>受理人id)
									)
								)
							),
						)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedFeedbacks/1/resubmit',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/unAuditedFeedbacks/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedFeedbacks",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedFeedbacks/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/unAuditedFeedbacks/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedFeedbacks",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedFeedbacks/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "unAuditedFeedbacks",
			"id": "118",
			"attributes": {
				"applyInfoCategory": 6,
				"applyInfoType": 1,
				"applyStatus": 2,
				"rejectReason": "",
				"operationType": 8,
				"feedbackId": "1",
				"title": "标题",
				"content": "内容",
				"acceptStatus": 0,
				"status": 0,
				"createTime": 1635576489,
				"updateTime": 1635576541,
				"statusTime": 1635576541
			},
			"relationships": {
				"relation": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "2"
					}
				},
				"applyUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"acceptUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"member": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"reply": {
					"data": {
						"type": "replies",
						"id": "14"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/unAuditedFeedbacks/118"
			}
		},
		"included": [
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "14",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635576489,
					"updateTime": 1635576489,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "unAuditedFeedbacks",
				"id": "118",
				"attributes": {
					"applyInfoCategory": 6,
					"applyInfoType": 1,
					"applyStatus": 2,
					"rejectReason": "",
					"operationType": 8,
					"feedbackId": "1",
					"title": "标题",
					"content": "内容",
					"acceptStatus": 0,
					"status": 0,
					"createTime": 1635576489,
					"updateTime": 1635576541,
					"statusTime": 1635576541
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "14"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedFeedbacks/118"
				}
			}
		],
		"included": [
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "14",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635576489,
					"updateTime": 1635576489,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}