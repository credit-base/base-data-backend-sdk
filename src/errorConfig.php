<?php
/**
 * 100 - 500 通用错误规范
 * 501 - 1000 人通用错误规范
 * 1001 - 1100 员工错误规范
 */

// 100 - 500 -- start
/**
 * 数据为空
 */
define('PARAMETER_IS_EMPTY', 100);
/**
 * 数据格式不正确
 */
define('PARAMETER_FORMAT_ERROR', 101);
/**
 * 资源不能操作
 */
define('RESOURCE_CAN_NOT_MODIFY', 102);
/**
 * 资源已存在
 */
define('RESOURCE_ALREADY_EXIST', 103);
/**
 * 数据不正确
 */
define('PARAMETER_ERROR', 104);
// 100 - 500 -- end

// 501 - 1000 -- start
/**
 * 用户姓名格式不正确
 */
define('USER_REAL_NAME_FORMAT_ERROR', 501);
/**
 * 手机号码格式不正确
 */
define('USER_CELLPHONE_FORMAT_ERROR', 502);
/**
 * 密码格式不正确
 */
define('USER_PASSWORD_FORMAT_ERROR', 503);
/**
 * 身份证号码格式不正确
 */
define('USER_CARDID_FORMAT_ERROR', 504);
/**
 * 密码不正确
 */
define('PASSWORD_INCORRECT', 505);

// 501 - 1000 -- end

// 1001 - 1100 -- start
/**
 * 该所属科室不属于该所属委办局
 */
define('DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP', 1001);

// 1001 - 1100 -- end
