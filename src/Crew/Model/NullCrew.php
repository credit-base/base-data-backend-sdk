<?php
namespace BaseSdk\Crew\Model;

use Marmot\Interfaces\INull;

class NullCrew extends Crew implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
