<?php
namespace BaseSdk\Crew\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Crew\Model\Crew;
use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

class CrewRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $crew = null) : Crew
    {
        return $this->translateToObject($expression, $crew);
    }

    protected function translateToObject(array $expression, $crew = null) : Crew
    {
        if (empty($expression)) {
            return NullCrew::getInstance();
        }

        unset($crew);
        $id = isset($expression['data']['id']) ? $expression['data']['id'] : 0;

        return new Crew($id);
    }

    public function objectToArray($crew, array $keys = array()) : array
    {
        unset($crew);
        unset($keys);
        return array();
    }
}
