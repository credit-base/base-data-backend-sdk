<?php
namespace BaseSdk\Rule\Translator;

use BaseSdk\Rule\Model\UnAuditedRuleService;
use BaseSdk\Rule\Model\NullUnAuditedRuleService;

use BaseSdk\Crew\Model\NullCrew;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class UnAuditedRuleServiceRestfulTranslator extends RuleServiceRestfulTranslator
{
    public function arrayToObject(array $expression, $unAuditedRuleService = null)
    {
        return $this->translateToObject($expression, $unAuditedRuleService);
    }

    protected function translateToObject(array $expression, $unAuditedRuleService = null)
    {
        if (empty($expression)) {
            return NullUnAuditedRuleService::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $ruleService = parent::translateToObject($expression, $unAuditedRuleService);

        $operationType = isset($attributes['operationType']) ? $attributes['operationType'] : 0;
        $relationId = isset($attributes['relationId']) ? $attributes['relationId'] : 0;
        $applyStatus = isset($attributes['applyStatus']) ? $attributes['applyStatus'] : 0;
        $rejectReason = isset($attributes['rejectReason']) ? $attributes['rejectReason'] : '';

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $applyCrew = NullCrew::getInstance();
        if (isset($relationships['applyCrew']['data'])) {
            $applyCrewArray = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $applyCrew = $this->getCrewRestfulTranslator()->arrayToObject($applyCrewArray);
        }

        $unAuditedRuleService = new UnAuditedRuleService(
            $ruleService->getId(),
            $ruleService->getSourceCategory(),
            $ruleService->getSourceTemplate(),
            $ruleService->getTransformationCategory(),
            $ruleService->getTransformationTemplate(),
            $ruleService->getRules(),
            $ruleService->getVersion(),
            $ruleService->getDataTotal(),
            $ruleService->getCrew(),
            $ruleService->getUserGroup(),
            $applyCrew,
            $operationType,
            $relationId,
            $applyStatus,
            $rejectReason,
            $ruleService->getStatus(),
            $ruleService->getStatusTime(),
            $ruleService->getCreateTime(),
            $ruleService->getUpdateTime()
        );

        return $unAuditedRuleService;
    }

    public function objectToArray($unAuditedRuleService, array $keys = array()) : array
    {
        if (!$unAuditedRuleService instanceof UnAuditedRuleService) {
            return [];
        }

        $ruleService = parent::objectToArray($unAuditedRuleService, $keys);
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'applyCrew',
                'rejectReason'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'unAuditedRules'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $unAuditedRuleService->getId();
        }

        $expression['data']['attributes'] = $ruleService['data']['attributes'];
        $expression['data']['relationships'] = isset($ruleService['data']['relationships']) ?
                                                $ruleService['data']['relationships'] :
                                                array();

        if (in_array('rejectReason', $keys)) {
            $expression['data']['attributes']['rejectReason'] = $unAuditedRuleService->getRejectReason();
        }

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = [
                [
                    "type" => "crews",
                    "id" => $unAuditedRuleService->getApplyCrew()->getId()
                ]
            ];
        }

        return $expression;
    }
}
