<?php
namespace BaseSdk\Rule\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\Rule\Model\RuleService;
use BaseSdk\Rule\Model\NullRuleService;

use BaseSdk\Template\Model\NullTemplate;
use BaseSdk\Template\Translator\TranslatorFactory;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RuleServiceRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }
    
    protected function getTemplateTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getTemplateRestfulTranslator(int $category) : IRestfulTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($category);
    }

    public function arrayToObject(array $expression, $ruleService = null)
    {
        return $this->translateToObject($expression, $ruleService);
    }

    protected function translateToObject(array $expression, $ruleService = null)
    {
        if (empty($expression)) {
            return NullRuleService::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $transformationCategory = isset($attributes['transformationCategory']) ?
                                    $attributes['transformationCategory'] :
                                    0;
        $sourceCategory = isset($attributes['sourceCategory']) ? $attributes['sourceCategory'] : 0;
        $rules = isset($attributes['rules']) ? $attributes['rules'] : array();
        $version = isset($attributes['version']) ? $attributes['version'] : 0;
        $dataTotal = isset($attributes['dataTotal']) ? $attributes['dataTotal'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $userGroup = NullUserGroup::getInstance();
        if (isset($relationships['userGroup']['data'])) {
            $userGroupArray = $this->changeArrayFormat($relationships['userGroup']['data']);
            $userGroup = $this->getUserGroupRestfulTranslator()->arrayToObject($userGroupArray);
        }
    
        $transformationTemplate = NullTemplate::getInstance();
        if (isset($relationships['transformationTemplate']['data'])) {
            $transformationTemplateArray = $this->changeArrayFormat($relationships['transformationTemplate']['data']);
            $transformationTemplate = $this->getTemplateRestfulTranslator($transformationCategory)->arrayToObject(
                $transformationTemplateArray
            );
        }

        $sourceTemplate = NullTemplate::getInstance();
        if (isset($relationships['sourceTemplate']['data'])) {
            $sourceTemplateArray = $this->changeArrayFormat($relationships['sourceTemplate']['data']);
            $sourceTemplate = $this->getTemplateRestfulTranslator($sourceCategory)->arrayToObject(
                $sourceTemplateArray
            );
        }

        $ruleService = new RuleService(
            $id,
            $sourceCategory,
            $sourceTemplate,
            $transformationCategory,
            $transformationTemplate,
            $rules,
            $version,
            $dataTotal,
            $crew,
            $userGroup,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        
        return $ruleService;
    }

    public function objectToArray($ruleService, array $keys = array()) : array
    {
        if (!$ruleService instanceof RuleService) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'sourceCategory',
                'sourceTemplate',
                'transformationCategory',
                'transformationTemplate',
                'rules',
                'crew',
                'userGroup',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'rules'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $ruleService->getId();
        }

        $attributes = array();
        if (in_array('sourceCategory', $keys)) {
            $attributes['sourceCategory'] = $ruleService->getSourceCategory();
        }
        if (in_array('transformationCategory', $keys)) {
            $attributes['transformationCategory'] = $ruleService->getTransformationCategory();
        }
        if (in_array('rules', $keys)) {
            $attributes['rules'] = $ruleService->getRules();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $ruleService->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $ruleService->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $ruleService->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $ruleService->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = [
                [
                    "type" => "crews",
                    "id" => $ruleService->getCrew()->getId()
                ]
            ];
        }
        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $ruleService->getUserGroup()->getId()
                ]
            ];
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['data']['relationships']['sourceTemplate']['data'] = [
                [
                    "type" => "sourceTemplates",
                    "id" => $ruleService->getSourceTemplate()->getId()
                ]
            ];
        }
        if (in_array('transformationTemplate', $keys)) {
            $expression['data']['relationships']['transformationTemplate']['data'] = [
                [
                    "type" => "transformationTemplates",
                    "id" => $ruleService->getTransformationTemplate()->getId()
                ]
            ];
        }

        return $expression;
    }
}
