<?php
namespace BaseSdk\Rule\Adapter\RuleService;

use BaseSdk\Rule\Model\RuleService;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IRuleServiceAdapter extends IFetchAbleRestfulAdapter
{
    public function add(RuleService $ruleService);

    public function edit(RuleService $ruleService, array $keys = array()) : bool;

    public function deletes(RuleService $ruleService, array $keys = array()) : bool;
}
