<?php
namespace BaseSdk\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Rule\Model\RuleService;
use BaseSdk\Rule\Model\NullRuleService;
use BaseSdk\Rule\Translator\RuleServiceRestfulTranslator;

class RuleServiceRestfulAdapter extends GuzzleAdapter implements IRuleServiceAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'RULE_SERVER_LIST'=>[
            'fields' => [],
            'include' => 'crew,userGroup,transformationTemplate,sourceTemplate'
        ],
        'RULE_SERVER_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'crew,userGroup,transformationTemplate,sourceTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new RuleServiceRestfulTranslator();
        $this->resource = 'rules';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullRuleService::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function add(RuleService $ruleService)
    {
        $data = $this->getTranslator()->objectToArray($ruleService);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $ruleService = $this->translateToObject();
            return $ruleService->getId();
        }

        return false;
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($ruleService, $keys);

        $this->patch(
            $this->getResource().'/'.$ruleService->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function deletes(RuleService $ruleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($ruleService, $keys);

        $this->patch(
            $this->getResource().'/'.$ruleService->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }
}
