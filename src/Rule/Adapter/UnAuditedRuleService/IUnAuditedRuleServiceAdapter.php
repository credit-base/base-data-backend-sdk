<?php
namespace BaseSdk\Rule\Adapter\UnAuditedRuleService;

use BaseSdk\Rule\Model\UnAuditedRuleService;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IUnAuditedRuleServiceAdapter extends IFetchAbleRestfulAdapter
{
    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;
}
