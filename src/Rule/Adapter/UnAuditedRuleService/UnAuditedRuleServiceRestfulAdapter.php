<?php
namespace BaseSdk\Rule\Adapter\UnAuditedRuleService;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Rule\Model\UnAuditedRuleService;
use BaseSdk\Rule\Model\NullUnAuditedRuleService;
use BaseSdk\Rule\Translator\UnAuditedRuleServiceRestfulTranslator;

class UnAuditedRuleServiceRestfulAdapter extends GuzzleAdapter implements IUnAuditedRuleServiceAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDITED_RULE_SERVER_LIST'=>[
            'fields' => [],
            'include' => 'publishCrew,userGroup,transformationTemplate,sourceTemplate,applyCrew,applyUserGroup'
        ],
        'UN_AUDITED_RULE_SERVER_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'publishCrew,userGroup,transformationTemplate,sourceTemplate,applyCrew,applyUserGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditedRuleServiceRestfulTranslator();
        $this->resource = 'unAuditedRules';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditedRuleService::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditedRuleService, $keys);

        $this->patch(
            $this->getResource().'/'.$unAuditedRuleService->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditedRuleService, $keys);

        $this->patch(
            $this->getResource().'/'.$unAuditedRuleService->getId(). '/approve',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditedRuleService, $keys);

        $this->patch(
            $this->getResource().'/'.$unAuditedRuleService->getId(). '/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditedRuleService, $keys);

        $this->patch(
            $this->getResource().'/'.$unAuditedRuleService->getId().'/revoke',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }
}
