<?php
namespace BaseSdk\Rule\Model;

use BaseSdk\Crew\Model\Crew;
use BaseSdk\UserGroup\Model\UserGroup;

use BaseSdk\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleService extends RuleService
{
    protected $id;

    protected $applyCrew;

    protected $operationType;

    protected $relationId;

    protected $applyStatus;

    protected $rejectReason;

    public function __construct(
        int $id = 0,
        int $sourceCategory = 0,
        Template $sourceTemplate = null,
        int $transformationCategory = 0,
        Template $transformationTemplate = null,
        array $rules = array(),
        int $version = 0,
        int $dataTotal = 0,
        Crew $crew = null,
        UserGroup $userGroup = null,
        Crew $applyCrew = null,
        int $operationType = 0,
        int $relationId = 0,
        int $applyStatus = 0,
        string $rejectReason = '',
        int $status = self::STATUS['NORMAL'],
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        parent::__construct(
            $id,
            $sourceCategory,
            $sourceTemplate,
            $transformationCategory,
            $transformationTemplate,
            $rules,
            $version,
            $dataTotal,
            $crew,
            $userGroup,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        $this->applyCrew = $applyCrew;
        $this->operationType = $operationType;
        $this->relationId = $relationId;
        $this->applyStatus = $applyStatus;
        $this->rejectReason = $rejectReason;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyCrew);
        unset($this->operationType);
        unset($this->relationId);
        unset($this->applyStatus);
        unset($this->rejectReason);
    }
    
    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function getOperationType() : int
    {
        return $this->operationType;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }
}
