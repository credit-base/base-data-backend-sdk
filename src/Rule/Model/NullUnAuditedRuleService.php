<?php
namespace BaseSdk\Rule\Model;

use Marmot\Interfaces\INull;

class NullUnAuditedRuleService extends UnAuditedRuleService implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
