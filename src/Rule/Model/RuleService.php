<?php
namespace BaseSdk\Rule\Model;

use BaseSdk\Crew\Model\Crew;

use BaseSdk\UserGroup\Model\UserGroup;

use BaseSdk\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleService
{
    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    protected $id;
    /**
     * @var Template $transformationTemplate 目标资源目录
     */
    protected $transformationTemplate;

    /**
     * @var int $transformationCategory 目标资源目录类型
     */
    protected $transformationCategory;

    /**
     * @var Template $sourceTemplate 来源资源目录
     */
    protected $sourceTemplate;

    /**
     * @var int $sourceCategory 来源资源目录类型
     */
    protected $sourceCategory;

    /**
     * @var array $rules 规则集合
     */
    protected $rules;

    /**
     * @var int $version 版本号
     */
    protected $version;

    /**
     * @var int $dataTotal 已转换数据量
     */
    protected $dataTotal;

    /**
     * @var Crew $crew 发布人
     */
    protected $crew;

    /**
     * @var UserGroup $userGroup 所属委办局
     */
    protected $userGroup;

    public function __construct(
        int $id = 0,
        int $sourceCategory = 0,
        Template $sourceTemplate = null,
        int $transformationCategory = 0,
        Template $transformationTemplate = null,
        array $rules = array(),
        int $version = 0,
        int $dataTotal = 0,
        Crew $crew = null,
        UserGroup $userGroup = null,
        int $status = self::STATUS['NORMAL'],
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->sourceCategory = $sourceCategory;
        $this->sourceTemplate = $sourceTemplate;
        $this->transformationCategory = $transformationCategory;
        $this->transformationTemplate = $transformationTemplate;
        $this->rules = $rules;
        $this->version = $version;
        $this->dataTotal = $dataTotal;
        $this->crew = $crew;
        $this->userGroup = $userGroup;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->transformationCategory);
        unset($this->transformationTemplate);
        unset($this->rules);
        unset($this->version);
        unset($this->dataTotal);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function getSourceTemplate() : Template
    {
        return $this->sourceTemplate;
    }

    public function getTransformationCategory() : int
    {
        return $this->transformationCategory;
    }

    public function getTransformationTemplate() : Template
    {
        return $this->transformationTemplate;
    }

    public function getRules() : array
    {
        return $this->rules;
    }

    public function getVersion() : int
    {
        return $this->version;
    }

    public function getDataTotal() : int
    {
        return $this->dataTotal;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
