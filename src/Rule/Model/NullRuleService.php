<?php
namespace BaseSdk\Rule\Model;

use Marmot\Interfaces\INull;

class NullRuleService extends RuleService implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }
}
