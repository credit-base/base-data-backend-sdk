<?php
namespace BaseSdk\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Rule\Model\UnAuditedRuleService;
use BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use BaseSdk\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter;
use BaseSdk\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceRestfulAdapter;

class UnAuditedRuleServiceRestfulRepository extends Repository implements IUnAuditedRuleServiceAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'UN_AUDITED_RULE_SERVER_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDITED_RULE_SERVER_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new UnAuditedRuleServiceRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IUnAuditedRuleServiceAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IUnAuditedRuleServiceAdapter
    {
        return new UnAuditedRuleServiceMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->resubmit($unAuditedRuleService, $keys);
    }
    
    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->approve($unAuditedRuleService, $keys);
    }

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->reject($unAuditedRuleService, $keys);
    }

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->revoke($unAuditedRuleService, $keys);
    }
}
