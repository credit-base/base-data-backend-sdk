<?php
namespace BaseSdk\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Rule\Model\RuleService;
use BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter;
use BaseSdk\Rule\Adapter\RuleService\RuleServiceMockAdapter;
use BaseSdk\Rule\Adapter\RuleService\RuleServiceRestfulAdapter;

class RuleServiceRestfulRepository extends Repository implements IRuleServiceAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'RULE_SERVER_LIST';
    const FETCH_ONE_MODEL_UN = 'RULE_SERVER_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new RuleServiceRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IRuleServiceAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IRuleServiceAdapter
    {
        return new RuleServiceMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function add(RuleService $ruleService)
    {
        return $this->getAdapter()->add($ruleService);
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($ruleService, $keys);
    }

    public function deletes(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->deletes($ruleService, $keys);
    }
}
