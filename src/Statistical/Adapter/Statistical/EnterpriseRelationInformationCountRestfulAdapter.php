<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use BaseSdk\Statistical\Model\Statistical;
use BaseSdk\Statistical\Model\NullStatistical;

class EnterpriseRelationInformationCountRestfulAdapter extends StatisticalRestfulAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        $this->get(
            $this->getResource() . '/enterpriseRelationInformationCount',
            array('filter'=>$filter)
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
