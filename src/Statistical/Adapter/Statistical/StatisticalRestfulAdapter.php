<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Statistical\Model\Statistical;
use BaseSdk\Statistical\Translator\StatisticalRestfulTranslator;

abstract class StatisticalRestfulAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    private $translator;

    private $resource;

    public function __construct()
    {
        parent::__construct(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
        $this->translator = new StatisticalRestfulTranslator();
        $this->resource = 'statisticals';
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }
}
