<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use BaseSdk\Statistical\Model\Statistical;

interface IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical;
}
