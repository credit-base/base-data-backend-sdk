<?php
namespace BaseSdk\Statistical\Model;

class Statistical
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var array $result 统计结果
     */
    private $result;

    public function __construct(int $id = 0, array $result = array())
    {
        $this->id = $id;
        $this->result = $result;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->result);
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getResult() : array
    {
        return $this->result;
    }
}
