<?php
namespace BaseSdk\Statistical\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Statistical\Model\Statistical;
use BaseSdk\Statistical\Model\NullStatistical;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

class StatisticalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $statistical = null)
    {
        return $this->translateToObject($expression, $statistical);
    }

    protected function translateToObject(array $expression, $statistical = null)
    {
        if (empty($expression)) {
            return NullStatistical::getInstance();
        }
       
        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $result = isset($attributes['result']) ? $attributes['result'] : '';

        $statistical = new Statistical($id, $result);
        
        return $statistical;
    }

    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);
        return array();
    }
}
