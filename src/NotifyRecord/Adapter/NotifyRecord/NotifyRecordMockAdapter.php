<?php
namespace BaseSdk\NotifyRecord\Adapter\NotifyRecord;

use BaseSdk\NotifyRecord\Utils\MockFactory;

class NotifyRecordMockAdapter implements INotifyRecordAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateNotifyRecord($id);
    }

    public function fetchList(array $ids) : array
    {
        $notifyRecordList = array();

        foreach ($ids as $id) {
            $notifyRecordList[] = MockFactory::generateNotifyRecord($id);
        }

        return $notifyRecordList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($number; $number<$size; $number++) {
            $ids[] = $number;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
