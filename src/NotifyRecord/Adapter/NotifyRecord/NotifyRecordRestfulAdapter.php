<?php
namespace BaseSdk\NotifyRecord\Adapter\NotifyRecord;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\NotifyRecord\Model\NullNotifyRecord;
use BaseSdk\NotifyRecord\Translator\NotifyRecordRestfulTranslator;

class NotifyRecordRestfulAdapter extends GuzzleAdapter implements INotifyRecordAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    const SCENARIOS = [
        'NOTIFY_RECORD_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'NOTIFY_RECORD_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    private $translator;
    
    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new NotifyRecordRestfulTranslator();
        $this->resource = 'notifyRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullNotifyRecord::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
