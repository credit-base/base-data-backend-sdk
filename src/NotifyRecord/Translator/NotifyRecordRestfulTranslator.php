<?php
namespace BaseSdk\NotifyRecord\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\NotifyRecord\Model\NotifyRecord;
use BaseSdk\NotifyRecord\Model\NullNotifyRecord;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

class NotifyRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $notifyRecord = null) : NotifyRecord
    {
        return $this->translateToObject($expression, $notifyRecord);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $notifyRecord = null) : NotifyRecord
    {
        if (empty($expression)) {
            return NullNotifyRecord::getInstance();
        }

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $hash = isset($attributes['hash']) ? $attributes['hash'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;

        $notifyRecord = new NotifyRecord(
            $id,
            $name,
            $hash,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $notifyRecord;
    }

    public function objectToArray($notifyRecord, array $keys = array()) : array
    {
        unset($notifyRecord);
        unset($keys);
        return array();
    }
}
