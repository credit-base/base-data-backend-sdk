<?php
namespace BaseSdk\NotifyRecord\Model;

use Marmot\Interfaces\INull;

class NullNotifyRecord extends NotifyRecord implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
