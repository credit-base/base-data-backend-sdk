<?php
namespace BaseSdk\NotifyRecord\Model;

class NotifyRecord
{
    const STATUS = [
        'PENDING' => 0,
        'SUCCESS'=>2
    ];

    protected $id;
    /**
     * @param string $name 文件名字
     */
    protected $name;
    /**
     * @param string $hash 文件hash
     */
    protected $hash;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;

    public function __construct(
        int $id = 0,
        string $name = '',
        string $hash = '',
        int $status = self::STATUS['PENDING'],
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->hash = $hash;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->hash);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
