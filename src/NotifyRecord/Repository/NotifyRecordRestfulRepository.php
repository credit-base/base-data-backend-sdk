<?php
namespace BaseSdk\NotifyRecord\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;
use BaseSdk\NotifyRecord\Adapter\NotifyRecord\NotifyRecordMockAdapter;
use BaseSdk\NotifyRecord\Adapter\NotifyRecord\NotifyRecordRestfulAdapter;

class NotifyRecordRestfulRepository extends Repository implements INotifyRecordAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'NOTIFY_RECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'NOTIFY_RECORD_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new NotifyRecordRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : INotifyRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : INotifyRecordAdapter
    {
        return new NotifyRecordMockAdapter();
    }

    public function scenario($scenario = self::FETCH_ONE_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
