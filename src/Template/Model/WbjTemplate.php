<?php
namespace BaseSdk\Template\Model;

use BaseSdk\UserGroup\Model\UserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjTemplate extends Template
{
    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    
    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $category = 0,
        array $subjectCategory = array(),
        int $dimension = 0,
        int $exchangeFrequency = 0,
        int $infoClassify = 0,
        int $infoCategory = 0,
        string $description = '',
        array $items = array(),
        UserGroup $sourceUnit = null,
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->sourceUnit = $sourceUnit;
        parent::__construct(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }
}
