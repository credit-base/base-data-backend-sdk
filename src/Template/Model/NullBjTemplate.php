<?php
namespace BaseSdk\Template\Model;

use Marmot\Interfaces\INull;

class NullBjTemplate extends BjTemplate implements INull
{

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
