<?php
namespace BaseSdk\Template\Model;

use BaseSdk\UserGroup\Model\UserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjTemplate extends Template
{
    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    /**
     * @var int $gbTemplate 国标目录
     */
    protected $gbTemplate;
    
    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $category = 0,
        array $subjectCategory = array(),
        int $dimension = 0,
        int $exchangeFrequency = 0,
        int $infoClassify = 0,
        int $infoCategory = 0,
        string $description = '',
        array $items = array(),
        UserGroup $sourceUnit = null,
        GbTemplate $gbTemplate = null,
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        parent::__construct(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        $this->sourceUnit = $sourceUnit;
        $this->gbTemplate = $gbTemplate;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->gbTemplate);
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function getGbTemplate() : gbTemplate
    {
        return $this->gbTemplate;
    }
}
