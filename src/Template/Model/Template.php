<?php
namespace BaseSdk\Template\Model;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
abstract class Template
{
    /**
     * 目录类别
     * @var CATEGORY['WBJ']  委办局 10
     * @var CATEGORY['BJ']  本级 1
     * @var CATEGORY['GB']  国标 2
     * @var CATEGORY['BASE']  基础 3
     * @var CATEGORY['QZJ_WBJ']  前置机委办局 20
     * @var CATEGORY['QZJ_BJ']  前置机本级 21
     * @var CATEGORY['QZJ_GB']  前置机国标 22
     */
    const CATEGORY = array(
        'WBJ' => 10,
        'BJ' => 1,
        'GB' => 2,
        'BASE' => 3,
        'QZJ_WBJ' => 20,
        'QZJ_BJ' => 21,
        'QZJ_GB' => 22,
    );
    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $name 目录名称
     */
    protected $name;
    /**
     * @var string $identify 目录标识
     */
    protected $identify;
    /**
     * @var int $category 模板类型
     */
    protected $category;
    /**
     * @var array $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $exchangeFrequency 更新频率
     */
    protected $exchangeFrequency;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var string $description 目录描述
     */
    protected $description;
    /**
     * @var array $items 模板信息
     */
    protected $items;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;
    
    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $category = 0,
        array $subjectCategory = array(),
        int $dimension = 0,
        int $exchangeFrequency = 0,
        int $infoClassify = 0,
        int $infoCategory = 0,
        string $description = '',
        array $items = array(),
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->identify = $identify;
        $this->category = $category;
        $this->subjectCategory = $subjectCategory;
        $this->dimension = $dimension;
        $this->exchangeFrequency = $exchangeFrequency;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
        $this->description = $description;
        $this->items = $items;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->category);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->exchangeFrequency);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->description);
        unset($this->items);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function getSubjectCategory() : array
    {
        return $this->subjectCategory;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function getExchangeFrequency() : int
    {
        return $this->exchangeFrequency;
    }

    public function getInfoClassify() : string
    {
        return $this->infoClassify;
    }

    public function getInfoCategory() : string
    {
        return $this->infoCategory;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
