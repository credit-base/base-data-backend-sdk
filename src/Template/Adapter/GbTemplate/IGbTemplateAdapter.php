<?php
namespace BaseSdk\Template\Adapter\GbTemplate;

use BaseSdk\Template\Model\GbTemplate;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IGbTemplateAdapter extends IFetchAbleRestfulAdapter
{
    public function add(GbTemplate $gbTemplate);

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool;
}
