<?php
namespace BaseSdk\Template\Adapter\GbTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Template\Model\GbTemplate;
use BaseSdk\Template\Model\NullGbTemplate;
use BaseSdk\Template\Translator\GbTemplateRestfulTranslator;

class GbTemplateRestfulAdapter extends GuzzleAdapter implements IGbTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    const SCENARIOS = [
        'GB_TEMPLATE_LIST'=>[
            'fields' => []
        ],
        'GB_TEMPLATE_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    private $translator;
    
    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new GbTemplateRestfulTranslator();
        $this->resource = 'gbTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    protected function getNullObject() : INull
    {
        return NullGbTemplate::getInstance();
    }

    public function add(GbTemplate $gbTemplate)
    {
        $data = $this->getTranslator()->objectToArray($gbTemplate);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $gbTemplate = $this->translateToObject();
            return $gbTemplate->getId();
        }

        return false;
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($gbTemplate, $keys);

        $this->patch(
            $this->getResource().'/'.$gbTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
