<?php
namespace BaseSdk\Template\Adapter\QzjTemplate;

use BaseSdk\Template\Model\QzjTemplate;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IQzjTemplateAdapter extends IFetchAbleRestfulAdapter
{
    public function add(QzjTemplate $qzjTemplate);

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool;
}
