<?php
namespace BaseSdk\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Template\Model\QzjTemplate;
use BaseSdk\Template\Model\NullQzjTemplate;
use BaseSdk\Template\Translator\QzjTemplateRestfulTranslator;

class QzjTemplateRestfulAdapter extends GuzzleAdapter implements IQzjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    const SCENARIOS = [
        'QZJ_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ],
        'QZJ_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ]
    ];
    
    private $translator;
    
    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new QzjTemplateRestfulTranslator();
        $this->resource = 'qzjTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getNullObject() : INull
    {
        return NullQzjTemplate::getInstance();
    }

    public function add(QzjTemplate $qzjTemplate)
    {
        $data = $this->getTranslator()->objectToArray($qzjTemplate);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $qzjTemplate = $this->translateToObject();
            return $qzjTemplate->getId();
        }

        return false;
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($qzjTemplate, $keys);

        $this->patch(
            $this->getResource().'/'.$qzjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
