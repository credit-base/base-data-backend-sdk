<?php
namespace BaseSdk\Template\Adapter\WbjTemplate;

use BaseSdk\Template\Model\WbjTemplate;
use BaseSdk\Template\Utils\WbjTemplateMockFactory;

class WbjTemplateMockAdapter implements IWbjTemplateAdapter
{
    public function fetchOne($id) : WbjTemplate
    {
        return WbjTemplateMockFactory::generateWbjTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $wbjTemplateList = array();

        foreach ($ids as $id) {
            $wbjTemplateList[$id] = WbjTemplateMockFactory::generateWbjTemplate($id);
        }

        return $wbjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(WbjTemplate $wbjTemplate)
    {
        unset($wbjTemplate);
        return true;
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        unset($wbjTemplate);
        unset($keys);
        return true;
    }
}
