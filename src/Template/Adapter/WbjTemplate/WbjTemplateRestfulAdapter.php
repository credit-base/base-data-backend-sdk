<?php
namespace BaseSdk\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Template\Model\WbjTemplate;
use BaseSdk\Template\Model\NullWbjTemplate;
use BaseSdk\Template\Translator\WbjTemplateRestfulTranslator;

class WbjTemplateRestfulAdapter extends GuzzleAdapter implements IWbjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    const SCENARIOS = [
        'WBJ_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ],
        'WBJ_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ]
    ];

    private $translator;
    
    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WbjTemplateRestfulTranslator();
        $this->resource = 'templates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getNullObject() : INull
    {
        return NullWbjTemplate::getInstance();
    }

    public function add(WbjTemplate $wbjTemplate)
    {
        $data = $this->getTranslator()->objectToArray($wbjTemplate);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $wbjTemplate = $this->translateToObject();
            return $wbjTemplate->getId();
        }

        return false;
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($wbjTemplate, $keys);

        $this->patch(
            $this->getResource().'/'.$wbjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
