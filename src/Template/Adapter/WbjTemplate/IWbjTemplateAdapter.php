<?php
namespace BaseSdk\Template\Adapter\WbjTemplate;

use BaseSdk\Template\Model\WbjTemplate;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IWbjTemplateAdapter extends IFetchAbleRestfulAdapter
{
    public function add(WbjTemplate $wbjTemplate);

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool;
}
