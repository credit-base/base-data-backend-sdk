<?php
namespace BaseSdk\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Template\Model\BaseTemplate;
use BaseSdk\Template\Model\NullBaseTemplate;
use BaseSdk\Template\Translator\BaseTemplateRestfulTranslator;

class BaseTemplateRestfulAdapter extends GuzzleAdapter implements IBaseTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    const SCENARIOS = [
        'BASE_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'BASE_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => ''
        ]
    ];
    
    private $translator;
    
    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new BaseTemplateRestfulTranslator();
        $this->resource = 'baseTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullBaseTemplate::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($baseTemplate, $keys);

        $this->patch(
            $this->getResource().'/'.$baseTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($baseTemplate);
            return true;
        }

        return false;
    }
}
