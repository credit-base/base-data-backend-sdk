<?php
namespace BaseSdk\Template\Adapter\BaseTemplate;

use BaseSdk\Template\Model\BaseTemplate;

use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IBaseTemplateAdapter extends IFetchAbleRestfulAdapter
{
    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool;
}
