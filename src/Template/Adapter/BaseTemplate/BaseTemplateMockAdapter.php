<?php
namespace BaseSdk\Template\Adapter\BaseTemplate;

use BaseSdk\Template\Model\BaseTemplate;
use BaseSdk\Template\Utils\BaseTemplateMockFactory;

class BaseTemplateMockAdapter implements IBaseTemplateAdapter
{
    public function fetchOne($id)
    {
        return BaseTemplateMockFactory::generateBaseTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $baseTemplateList = array();

        foreach ($ids as $id) {
            $baseTemplateList[$id] = BaseTemplateMockFactory::generateBaseTemplate($id);
        }

        return $baseTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($number);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        unset($baseTemplate);
        unset($keys);
        return true;
    }
}
