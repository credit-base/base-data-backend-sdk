<?php
namespace BaseSdk\Template\Adapter\BjTemplate;

use BaseSdk\Template\Model\BjTemplate;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IBjTemplateAdapter extends IFetchAbleRestfulAdapter
{
    public function add(BjTemplate $bjTemplate);

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool;
}
