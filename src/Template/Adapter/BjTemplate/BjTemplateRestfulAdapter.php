<?php
namespace BaseSdk\Template\Adapter\BjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Template\Model\BjTemplate;
use BaseSdk\Template\Model\NullBjTemplate;
use BaseSdk\Template\Translator\BjTemplateRestfulTranslator;

class BjTemplateRestfulAdapter extends GuzzleAdapter implements IBjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'BJ_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit,gbTemplate'
        ],
        'BJ_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit,gbTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new BjTemplateRestfulTranslator();
        $this->resource = 'bjTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullBjTemplate::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function add(BjTemplate $bjTemplate)
    {
        $data = $this->getTranslator()->objectToArray($bjTemplate);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $bjTemplate = $this->translateToObject();
            return $bjTemplate->getId();
        }

        return false;
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($bjTemplate, $keys);

        $this->patch(
            $this->getResource().'/'.$bjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
