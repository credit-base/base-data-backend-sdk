<?php
namespace BaseSdk\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Template\Model\BaseTemplate;
use BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use BaseSdk\Template\Adapter\BaseTemplate\BaseTemplateMockAdapter;
use BaseSdk\Template\Adapter\BaseTemplate\BaseTemplateRestfulAdapter;

class BaseTemplateRestfulRepository extends Repository implements IBaseTemplateAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'BASE_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'BASE_TEMPLATE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new BaseTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IBaseTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBaseTemplateAdapter
    {
        return new BaseTemplateMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($baseTemplate, $keys);
    }
}
