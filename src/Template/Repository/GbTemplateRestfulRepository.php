<?php
namespace BaseSdk\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Template\Model\GbTemplate;
use BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseSdk\Template\Adapter\GbTemplate\GbTemplateMockAdapter;
use BaseSdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter;

class GbTemplateRestfulRepository extends Repository implements IGbTemplateAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'GB_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'GB_TEMPLATE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IGbTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IGbTemplateAdapter
    {
        return new GbTemplateMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function add(GbTemplate $gbTemplate)
    {
        return $this->getAdapter()->add($gbTemplate);
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($gbTemplate, $keys);
    }
}
