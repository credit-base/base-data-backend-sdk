<?php
namespace BaseSdk\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Template\Model\BjTemplate;
use BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseSdk\Template\Adapter\BjTemplate\BjTemplateMockAdapter;
use BaseSdk\Template\Adapter\BjTemplate\BjTemplateRestfulAdapter;

class BjTemplateRestfulRepository extends Repository implements IBjTemplateAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'BJ_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'BJ_TEMPLATE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IBjTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBjTemplateAdapter
    {
        return new BjTemplateMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function add(BjTemplate $bjTemplate)
    {
        return $this->getAdapter()->add($bjTemplate);
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($bjTemplate, $keys);
    }
}
