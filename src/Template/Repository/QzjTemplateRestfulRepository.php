<?php
namespace BaseSdk\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Template\Model\QzjTemplate;
use BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use BaseSdk\Template\Adapter\QzjTemplate\QzjTemplateMockAdapter;
use BaseSdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter;

class QzjTemplateRestfulRepository extends Repository implements IQzjTemplateAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'QZJ_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'QZJ_TEMPLATE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new QzjTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IQzjTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IQzjTemplateAdapter
    {
        return new QzjTemplateMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function add(QzjTemplate $qzjTemplate)
    {
        return $this->getAdapter()->add($qzjTemplate);
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($qzjTemplate, $keys);
    }
}
