<?php
namespace BaseSdk\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Template\Model\WbjTemplate;
use BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseSdk\Template\Adapter\WbjTemplate\WbjTemplateMockAdapter;
use BaseSdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter;

class WbjTemplateRestfulRepository extends Repository implements IWbjTemplateAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'QZJ_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'QZJ_TEMPLATE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IWbjTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IWbjTemplateAdapter
    {
        return new WbjTemplateMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function add(WbjTemplate $wbjTemplate)
    {
        return $this->getAdapter()->add($wbjTemplate);
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($wbjTemplate, $keys);
    }
}
