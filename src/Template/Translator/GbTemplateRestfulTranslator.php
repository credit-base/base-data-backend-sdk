<?php
namespace BaseSdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\GbTemplate;
use BaseSdk\Template\Model\NullGbTemplate;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class GbTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $gbTemplate = null) : GbTemplate
    {
        return $this->translateToObject($expression, $gbTemplate);
    }

    protected function translateToObject(array $expression, $gbTemplate = null) : GbTemplate
    {
        if (empty($expression)) {
            return NullGbTemplate::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $items = isset($attributes['items']) ? $attributes['items'] : array();
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $exchangeFrequency = isset($attributes['exchangeFrequency']) ? $attributes['exchangeFrequency'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : array();
    
        $gbTemplate = new GbTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $gbTemplate;
    }

    public function objectToArray($gbTemplate, array $keys = array()) : array
    {
        if (!$gbTemplate instanceof GbTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $gbTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $gbTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $gbTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $gbTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $gbTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $gbTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $gbTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $gbTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $gbTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $gbTemplate->getItems();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $gbTemplate->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $gbTemplate->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $gbTemplate->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $gbTemplate->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
