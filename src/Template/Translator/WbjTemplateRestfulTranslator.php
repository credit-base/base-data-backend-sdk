<?php
namespace BaseSdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\WbjTemplate;
use BaseSdk\Template\Model\NullWbjTemplate;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WbjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator()
    {
        return new UserGroupRestfulTranslator();
    }

    public function arrayToObject(array $expression, $wbjTemplate = null) : WbjTemplate
    {
        return $this->translateToObject($expression, $wbjTemplate);
    }

    protected function translateToObject(array $expression, $wbjTemplate = null) : WbjTemplate
    {
        if (empty($expression)) {
            return NullWbjTemplate::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : array();
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $exchangeFrequency = isset($attributes['exchangeFrequency']) ? $attributes['exchangeFrequency'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $items = isset($attributes['items']) ? $attributes['items'] : array();
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $sourceUnit = NullUserGroup::getInstance();
        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnitArray = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $sourceUnit = $this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnitArray);
        }
    
        $wbjTemplate = new WbjTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $wbjTemplate;
    }

    public function objectToArray($wbjTemplate, array $keys = array()) : array
    {
        if (!$wbjTemplate instanceof WbjTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $wbjTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $wbjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $wbjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $wbjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $wbjTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $wbjTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $wbjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $wbjTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $wbjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $wbjTemplate->getItems();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $wbjTemplate->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $wbjTemplate->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $wbjTemplate->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $wbjTemplate->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $wbjTemplate->getSourceUnit()->getId()
                ]
            ];
        }

        return $expression;
    }
}
