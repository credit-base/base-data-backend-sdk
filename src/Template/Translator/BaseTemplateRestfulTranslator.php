<?php
namespace BaseSdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\BaseTemplate;
use BaseSdk\Template\Model\NullBaseTemplate;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class BaseTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $baseTemplate = null) : BaseTemplate
    {
        return $this->translateToObject($expression, $baseTemplate);
    }

    protected function translateToObject(array $expression, $baseTemplate = null) : BaseTemplate
    {
        if (empty($expression)) {
            return NullBaseTemplate::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $items = isset($attributes['items']) ? $attributes['items'] : array();
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : array();
        $exchangeFrequency = isset($attributes['exchangeFrequency']) ? $attributes['exchangeFrequency'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
    
        $baseTemplate = new BaseTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $baseTemplate;
    }

    public function objectToArray($baseTemplate, array $keys = array()) : array
    {
        if (!$baseTemplate instanceof BaseTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $baseTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $baseTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $baseTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $baseTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $baseTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $baseTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $baseTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $baseTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $baseTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $baseTemplate->getItems();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $baseTemplate->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $baseTemplate->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $baseTemplate->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $baseTemplate->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
