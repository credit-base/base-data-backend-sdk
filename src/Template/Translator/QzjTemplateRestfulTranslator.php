<?php
namespace BaseSdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\QzjTemplate;
use BaseSdk\Template\Model\NullQzjTemplate;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class QzjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator()
    {
        return new UserGroupRestfulTranslator();
    }

    public function arrayToObject(array $expression, $qzjTemplate = null) : QzjTemplate
    {
        return $this->translateToObject($expression, $qzjTemplate);
    }

    protected function translateToObject(array $expression, $qzjTemplate = null) : QzjTemplate
    {
        if (empty($expression)) {
            return NullQzjTemplate::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : array();
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $exchangeFrequency = isset($attributes['exchangeFrequency']) ? $attributes['exchangeFrequency'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $items = isset($attributes['items']) ? $attributes['items'] : array();
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $sourceUnit = NullUserGroup::getInstance();
        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnitArray = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $sourceUnit = $this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnitArray);
        }
    
        $qzjTemplate = new QzjTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $qzjTemplate;
    }

    public function objectToArray($qzjTemplate, array $keys = array()) : array
    {
        if (!$qzjTemplate instanceof QzjTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'category',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $qzjTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $qzjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $qzjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $qzjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $qzjTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $qzjTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $qzjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $qzjTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $qzjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $qzjTemplate->getItems();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $qzjTemplate->getCategory();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $qzjTemplate->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $qzjTemplate->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $qzjTemplate->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $qzjTemplate->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $qzjTemplate->getSourceUnit()->getId()
                ]
            ];
        }

        return $expression;
    }
}
