<?php
namespace BaseSdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\BjTemplate;
use BaseSdk\Template\Model\NullBjTemplate;

use BaseSdk\Template\Model\NullGbTemplate;

use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class BjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator()
    {
        return new UserGroupRestfulTranslator();
    }
    
    protected function getGbTemplateRestfulTranslator()
    {
        return new GbTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $bjTemplate = null) : BjTemplate
    {
        return $this->translateToObject($expression, $bjTemplate);
    }

    protected function translateToObject(array $expression, $bjTemplate = null) : BjTemplate
    {
        if (empty($expression)) {
            return NullBjTemplate::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $exchangeFrequency = isset($attributes['exchangeFrequency']) ? $attributes['exchangeFrequency'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : array();
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $items = isset($attributes['items']) ? $attributes['items'] : array();
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $sourceUnit = NullUserGroup::getInstance();
        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnitArray = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $sourceUnit = $this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnitArray);
        }
    
        $gbTemplate = NullGbTemplate::getInstance();
        if (isset($relationships['gbTemplate']['data'])) {
            $gbTemplateArray = $this->changeArrayFormat($relationships['gbTemplate']['data']);
            $gbTemplate = $this->getGbTemplateRestfulTranslator()->arrayToObject($gbTemplateArray);
        }

        $bjTemplate = new BjTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $gbTemplate,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $bjTemplate;
    }

    public function objectToArray($bjTemplate, array $keys = array()) : array
    {
        if (!$bjTemplate instanceof BjTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $bjTemplate->getId();
        }

        $attributes = array();
        if (in_array('name', $keys)) {
            $attributes['name'] = $bjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $bjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $bjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $bjTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $bjTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $bjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $bjTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $bjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $bjTemplate->getItems();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $bjTemplate->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $bjTemplate->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $bjTemplate->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $bjTemplate->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $bjTemplate->getSourceUnit()->getId()
                ]
            ];
        }
        if (in_array('gbTemplate', $keys)) {
            $expression['data']['relationships']['gbTemplate']['data'] = [
                [
                    "type" => "gbTemplates",
                    "id" => $bjTemplate->getGbTemplate()->getId()
                ]
            ];
        }

        return $expression;
    }
}
