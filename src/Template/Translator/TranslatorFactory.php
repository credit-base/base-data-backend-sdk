<?php
namespace BaseSdk\Template\Translator;

use BaseSdk\Template\Model\Template;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'BaseSdk\Template\Translator\BjTemplateRestfulTranslator',
        Template::CATEGORY['GB'] =>
        'BaseSdk\Template\Translator\GbTemplateRestfulTranslator',
        Template::CATEGORY['BASE'] =>
        'BaseSdk\Template\Translator\BaseTemplateRestfulTranslator',
        Template::CATEGORY['WBJ'] =>
        'BaseSdk\Template\Translator\WbjTemplateRestfulTranslator',
        Template::CATEGORY['QZJ_WBJ'] =>
        'BaseSdk\Template\Translator\QzjTemplateRestfulTranslator',
        Template::CATEGORY['QZJ_BJ'] =>
        'BaseSdk\Template\Translator\QzjTemplateRestfulTranslator',
        Template::CATEGORY['QZJ_GB'] =>
        'BaseSdk\Template\Translator\QzjTemplateRestfulTranslator',
    );

    public function getTranslator(int $category) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
