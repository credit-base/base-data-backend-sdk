<?php
namespace BaseSdk\Common\Repository;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Framework\Classes\Repository;

class NullRepository extends Repository implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    private function repositoryNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function getActualAdapter()
    {
        return $this->repositoryNotExist();
    }

    public function getMockAdapter()
    {
        return $this->repositoryNotExist();
    }

    public function fetchOne($id)
    {
        unset($id);

        return $this->repositoryNotExist();
    }

    public function fetchList(array $ids)
    {
        unset($ids);

        return $this->repositoryNotExist();
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);

        return $this->repositoryNotExist();
    }
}
