<?php
namespace BaseSdk\Common\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Classes\NullTranslator;

class NullRestfulTranslator extends NullTranslator implements IRestfulTranslator
{
    public function __construct()
    {
    }
    
    public function arrayToObjects(array $expression) : array
    {
        unset($expression);

        return array();
    }
}
