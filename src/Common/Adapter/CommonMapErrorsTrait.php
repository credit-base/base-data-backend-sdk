<?php
namespace BaseSdk\Common\Adapter;

use Marmot\Core;

trait CommonMapErrorsTrait
{
    abstract public function lastErrorId() : int;
    abstract public function lastErrorPointer();
    
    protected function mapErrors() : void
    {
        $id = $this->lastErrorId();
        $pointer = $this->lastErrorPointer();
        $source = array('pointer'=>$pointer);

        Core::setLastError($id, $source);
    }
}
