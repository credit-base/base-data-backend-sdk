<?php
namespace BaseSdk\Common\Adapter;

interface IFetchAbleRestfulAdapter
{
    public function fetchOne($id);

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array;
}
