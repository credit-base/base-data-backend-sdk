<?php
namespace BaseSdk\Enterprise\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use BaseSdk\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter;
use BaseSdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;

class EnterpriseRestfulRepository extends Repository implements IEnterpriseAdapter
{
    use FetchRestfulRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ENTERPRISE_LIST';
    const FETCH_ONE_MODEL_UN = 'ENTERPRISE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EnterpriseRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IEnterpriseAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEnterpriseAdapter
    {
        return new EnterpriseMockAdapter();
    }

    public function scenario($scenario = self::FETCH_ONE_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
