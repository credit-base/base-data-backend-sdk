<?php
namespace BaseSdk\Enterprise\Model;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class Enterprise
{
    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    protected $id;
    /**
     * @var string $name 主体名称
     */
    protected $name;
    /**
     * @var string $unifiedSocialCreditCode 统一社会信用代码
     */
    protected $unifiedSocialCreditCode;
    /**
     * @var int $establishmentDate 成立日期/注册日期
     */
    protected $establishmentDate;
    /**
     * @var int $approvalDate 核准日期
     */
    protected $approvalDate;
    /**
     * @var string $address 住所/经营场所
     */
    protected $address;
    /**
     * @var string $registrationCapital 注册资本（万）
     */
    protected $registrationCapital;
    /**
     * @var int $businessTermStart 营业期限自
     */
    protected $businessTermStart;

    /**
     * @var int $businessTermTo 营业期限至
     */
    protected $businessTermTo;

    /**
     * @var string $businessScope 经营范围
     */
    protected $businessScope;

    /**
     * @var string $registrationAuthority 登记机关
     */
    protected $registrationAuthority;

    /**
     * @var string $principal 法定代表人/经营者
     */
    protected $principal;

    /**
     * @var string $principalCardId 法人身份证号
     */
    protected $principalCardId;

    /**
     * @var string $registrationStatus 登记状态
     */
    protected $registrationStatus;

    /**
     * @var string $enterpriseTypeCode 企业类型代码
     */
    protected $enterpriseTypeCode;

    /**
     * @var string $enterpriseType 企业类型
     */
    protected $enterpriseType;

    /**
     * @var array $data 其他数据
     */
    protected $data;

    /**
     * @var string $industryCategory 行业门类
     */
    protected $industryCategory;

    /**
     * @var string $industryCode 行业代码
     */
    protected $industryCode;

    /**
     * @var string $administrativeArea 所属区域
     */
    protected $administrativeArea;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;

    public function __construct(
        int $id = 0,
        string $name = '',
        string $unifiedSocialCreditCode = '',
        int $establishmentDate = 0,
        int $approvalDate = 0,
        string $address = '',
        string $registrationCapital = '',
        int $businessTermStart = 0,
        int $businessTermTo = 0,
        string $businessScope = '',
        string $registrationAuthority = '',
        string $principal = '',
        string $principalCardId = '',
        string $registrationStatus = '',
        string $enterpriseTypeCode = '',
        string $enterpriseType = '',
        array $data = array(),
        string $industryCategory = '',
        string $industryCode = '',
        int $administrativeArea = 0,
        int $status = self::STATUS['NORMAL'],
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
        $this->establishmentDate = $establishmentDate;
        $this->approvalDate = $approvalDate;
        $this->address = $address;
        $this->registrationCapital = $registrationCapital;
        $this->businessTermStart = $businessTermStart;
        $this->businessTermTo = $businessTermTo;
        $this->businessScope = $businessScope;
        $this->registrationAuthority = $registrationAuthority;
        $this->principal = $principal;
        $this->principalCardId = $principalCardId;
        $this->registrationStatus = $registrationStatus;
        $this->enterpriseTypeCode = $enterpriseTypeCode;
        $this->enterpriseType = $enterpriseType;
        $this->data = $data;
        $this->industryCategory = $industryCategory;
        $this->industryCode = $industryCode;
        $this->administrativeArea = $administrativeArea;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->unifiedSocialCreditCode);
        unset($this->establishmentDate);
        unset($this->approvalDate);
        unset($this->address);
        unset($this->registrationCapital);
        unset($this->businessTermStart);
        unset($this->businessTermTo);
        unset($this->businessScope);
        unset($this->registrationAuthority);
        unset($this->principal);
        unset($this->principalCardId);
        unset($this->registrationStatus);
        unset($this->enterpriseTypeCode);
        unset($this->enterpriseType);
        unset($this->data);
        unset($this->industryCategory);
        unset($this->industryCode);
        unset($this->administrativeArea);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->unifiedSocialCreditCode;
    }

    public function getEstablishmentDate() : int
    {
        return $this->establishmentDate;
    }

    public function getApprovalDate() : int
    {
        return $this->approvalDate;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function getRegistrationCapital() : string
    {
        return $this->registrationCapital;
    }

    public function getBusinessTermStart() : int
    {
        return $this->businessTermStart;
    }

    public function getBusinessTermTo() : int
    {
        return $this->businessTermTo;
    }

    public function getBusinessScope() : string
    {
        return $this->businessScope;
    }

    public function getRegistrationAuthority() : string
    {
        return $this->registrationAuthority;
    }

    public function getPrincipal() : string
    {
        return $this->principal;
    }

    public function getPrincipalCardId() : string
    {
        return $this->principalCardId;
    }

    public function getRegistrationStatus() : string
    {
        return $this->registrationStatus;
    }

    public function getEnterpriseTypeCode() : string
    {
        return $this->enterpriseTypeCode;
    }

    public function getEnterpriseType() : string
    {
        return $this->enterpriseType;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getIndustryCategory() : string
    {
        return $this->industryCategory;
    }

    public function getIndustryCode() : string
    {
        return $this->industryCode;
    }

    public function getAdministrativeArea() : int
    {
        return $this->administrativeArea;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
