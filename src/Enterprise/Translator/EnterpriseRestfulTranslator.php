<?php
namespace BaseSdk\Enterprise\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Enterprise\Model\Enterprise;
use BaseSdk\Enterprise\Model\NullEnterprise;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

class EnterpriseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $enterprise = null) : Enterprise
    {
        return $this->translateToObject($expression, $enterprise);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $enterprise = null) : Enterprise
    {
        if (empty($expression)) {
            return NullEnterprise::getInstance();
        }

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $unifiedSocialCreditCode = isset($attributes['unifiedSocialCreditCode']) ?
            $attributes['unifiedSocialCreditCode'] :
            '';
        $establishmentDate = isset($attributes['establishmentDate']) ? $attributes['establishmentDate'] : 0;
        $approvalDate = isset($attributes['approvalDate']) ? $attributes['approvalDate'] : 0;
        $address = isset($attributes['address']) ? $attributes['address'] : '';
        $registrationCapital = isset($attributes['registrationCapital']) ? $attributes['registrationCapital'] : '';
        $businessTermStart = isset($attributes['businessTermStart']) ? $attributes['businessTermStart'] : 0;
        $businessTermTo = isset($attributes['businessTermTo']) ? $attributes['businessTermTo'] : 0;
        $businessScope = isset($attributes['businessScope']) ? $attributes['businessScope'] : '';
        $registrationAuthority = isset($attributes['registrationAuthority']) ?
                                $attributes['registrationAuthority'] :
                                '';
        $principal = isset($attributes['principal']) ? $attributes['principal'] : '';
        $principalCardId = isset($attributes['principalCardId']) ? $attributes['principalCardId'] : '';
        $registrationStatus = isset($attributes['registrationStatus']) ? $attributes['registrationStatus'] : '';
        $enterpriseTypeCode = isset($attributes['enterpriseTypeCode']) ? $attributes['enterpriseTypeCode'] : '';
        $enterpriseType = isset($attributes['enterpriseType']) ? $attributes['enterpriseType'] : '';
        $data = isset($attributes['data']) ? $attributes['data'] : array();
        $industryCategory = isset($attributes['industryCategory']) ? $attributes['industryCategory'] : '';
        $industryCode = isset($attributes['industryCode']) ? $attributes['industryCode'] : '';
        $administrativeArea = isset($attributes['administrativeArea']) ? $attributes['administrativeArea'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;

        $enterprise = new Enterprise(
            $id,
            $name,
            $unifiedSocialCreditCode,
            $establishmentDate,
            $approvalDate,
            $address,
            $registrationCapital,
            $businessTermStart,
            $businessTermTo,
            $businessScope,
            $registrationAuthority,
            $principal,
            $principalCardId,
            $registrationStatus,
            $enterpriseTypeCode,
            $enterpriseType,
            $data,
            $industryCategory,
            $industryCode,
            $administrativeArea,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $enterprise;
    }

    public function objectToArray($enterprise, array $keys = array()) : array
    {
        unset($enterprise);
        unset($keys);
        return array();
    }
}
