<?php
namespace BaseSdk\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\Enterprise\Model\NullEnterprise;
use BaseSdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class EnterpriseRestfulAdapter extends GuzzleAdapter implements IEnterpriseAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'ENTERPRISE_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'ENTERPRISE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new EnterpriseRestfulTranslator();
        $this->resource = 'enterprises';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullEnterprise::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
