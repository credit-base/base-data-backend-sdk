<?php
namespace BaseSdk\UserGroup\Model;

use Marmot\Interfaces\INull;

class NullUserGroup extends UserGroup implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
