<?php
namespace BaseSdk\UserGroup\Model;

class UserGroup
{
    protected $id;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
    }

    public function __destruct()
    {
        unset($this->id);
    }

    public function getId()
    {
        return $this->id;
    }
}
