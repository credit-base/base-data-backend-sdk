<?php
namespace BaseSdk\UserGroup\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\UserGroup\Model\UserGroup;
use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

class UserGroupRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $userGroup = null) : UserGroup
    {
        return $this->translateToObject($expression, $userGroup);
    }

    protected function translateToObject(array $expression, $userGroup = null) : UserGroup
    {
        if (empty($expression)) {
            return NullUserGroup::getInstance();
        }

        unset($userGroup);
        $id = isset($expression['data']['id']) ? $expression['data']['id'] : 0;

        return new UserGroup($id);
    }

    public function objectToArray($userGroup, array $keys = array()) : array
    {
        unset($userGroup);
        unset($keys);
        return array();
    }
}
