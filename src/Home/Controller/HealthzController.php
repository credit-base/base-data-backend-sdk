<?php
namespace Home\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

class HealthzController extends Controller
{
    use JsonApiTrait;

    public function healthz()
    {
        return true;
    }
}
