<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\ResourceCatalogData\Model\NullTask;
use BaseSdk\ResourceCatalogData\Model\GbSearchData;
use BaseSdk\ResourceCatalogData\Model\NullGbItemsData;
use BaseSdk\ResourceCatalogData\Model\NullGbSearchData;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Model\NullGbTemplate;
use BaseSdk\Template\Translator\GbTemplateRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class GbSearchDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    protected function getGbTemplateRestfulTranslator()
    {
        return new GbTemplateRestfulTranslator();
    }

    protected function getGbItemsDataRestfulTranslator()
    {
        return new GbItemsDataRestfulTranslator();
    }

    protected function getTaskRestfulTranslator()
    {
        return new TaskRestfulTranslator();
    }

    public function arrayToObject(array $expression, $gbSearchData = null) : GbSearchData
    {
        return $this->translateToObject($expression, $gbSearchData);
    }

    protected function translateToObject(array $expression, $gbSearchData = null) : GbSearchData
    {
        if (empty($expression)) {
            return NullGbSearchData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $expirationDate = isset($attributes['expirationDate']) ? $attributes['expirationDate'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : 0;
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $frontEndProcessorStatus = isset($attributes['frontEndProcessorStatus']) ?
                                    $attributes['frontEndProcessorStatus'] :
                                    0;
        $hash = isset($attributes['hash']) ? $attributes['hash'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $gbTemplate = NullGbTemplate::getInstance();
        if (isset($relationships['template']['data'])) {
            $gbTemplateArray = $this->changeArrayFormat($relationships['template']['data']);
            $gbTemplate = $this->getGbTemplateRestfulTranslator()->arrayToObject($gbTemplateArray);
        }

        $gbItemsData = NullGbItemsData::getInstance();
        if (isset($relationships['itemsData']['data'])) {
            $gbItemsDataArray = $this->changeArrayFormat($relationships['itemsData']['data']);
            $gbItemsData = $this->getGbItemsDataRestfulTranslator()->arrayToObject($gbItemsDataArray);
        }

        $task = NullTask::getInstance();
        if (isset($relationships['task']['data'])) {
            $taskArray = $this->changeArrayFormat($relationships['task']['data']);
            $task = $this->getTaskRestfulTranslator()->arrayToObject($taskArray);
        }

        $gbSearchData = new GbSearchData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $gbTemplate,
            $gbItemsData,
            $task,
            $description,
            $frontEndProcessorStatus,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $gbSearchData;
    }

    public function objectToArray($gbSearchData, array $keys = array()) : array
    {
        unset($gbSearchData);
        unset($keys);
        return array();
    }
}
