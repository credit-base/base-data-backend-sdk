<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\ResourceCatalogData\Model\NullTask;
use BaseSdk\ResourceCatalogData\Model\ErrorData;
use BaseSdk\ResourceCatalogData\Model\NullErrorData;
use BaseSdk\ResourceCatalogData\Model\NullErrorItemsData;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Model\NullTemplate;
use BaseSdk\Template\Translator\TranslatorFactory;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ErrorDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getTemplateTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getErrorTemplateRestfulTranslator(int $category) : IRestfulTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($category);
    }

    protected function getErrorItemsDataRestfulTranslator() : ErrorItemsDataRestfulTranslator
    {
        return new ErrorItemsDataRestfulTranslator();
    }

    protected function getTaskRestfulTranslator() : TaskRestfulTranslator
    {
        return new TaskRestfulTranslator();
    }

    public function arrayToObject(array $expression, $errorData = null) : ErrorData
    {
        return $this->translateToObject($expression, $errorData);
    }

    protected function translateToObject(array $expression, $errorData = null) : ErrorData
    {
        if (empty($expression)) {
            return NullErrorData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : 0;
        $expirationDate = isset($attributes['expirationDate']) ? $attributes['expirationDate'] : 0;
        $hash = isset($attributes['hash']) ? $attributes['hash'] : '';
        $category = isset($attributes['category']) ? $attributes['category'] : 0;
        $errorType = isset($attributes['errorType']) ? $attributes['errorType'] : 0;
        $errorReason = isset($attributes['errorReason']) ? $attributes['errorReason'] : array();
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $template = NullTemplate::getInstance();
        if (isset($relationships['template']['data'])) {
            $templateArray = $this->changeArrayFormat($relationships['template']['data']);
            $template = $this->getErrorTemplateRestfulTranslator($category)->arrayToObject($templateArray);
        }

        $errorItemsData = NullErrorItemsData::getInstance();
        if (isset($relationships['itemsData']['data'])) {
            $errorItemsDataArray = $this->changeArrayFormat($relationships['itemsData']['data']);
            $errorItemsData = $this->getErrorItemsDataRestfulTranslator()->arrayToObject($errorItemsDataArray);
        }

        $task = NullTask::getInstance();
        if (isset($relationships['task']['data'])) {
            $taskArray = $this->changeArrayFormat($relationships['task']['data']);
            $task = $this->getTaskRestfulTranslator()->arrayToObject($taskArray);
        }

        $errorData = new ErrorData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $template,
            $errorItemsData,
            $task,
            $category,
            $errorType,
            $errorReason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $errorData;
    }

    public function objectToArray($errorData, array $keys = array()) : array
    {
        unset($errorData);
        unset($keys);
        return array();
    }
}
