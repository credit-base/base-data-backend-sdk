<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\ResourceCatalogData\Model\NullTask;
use BaseSdk\ResourceCatalogData\Model\WbjSearchData;
use BaseSdk\ResourceCatalogData\Model\NullWbjItemsData;
use BaseSdk\ResourceCatalogData\Model\NullWbjSearchData;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Model\NullWbjTemplate;
use BaseSdk\Template\Translator\WbjTemplateRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WbjSearchDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    protected function getWbjTemplateRestfulTranslator()
    {
        return new WbjTemplateRestfulTranslator();
    }

    protected function getWbjItemsDataRestfulTranslator()
    {
        return new WbjItemsDataRestfulTranslator();
    }

    protected function getTaskRestfulTranslator()
    {
        return new TaskRestfulTranslator();
    }

    public function arrayToObject(array $expression, $wbjSearchData = null) : WbjSearchData
    {
        return $this->translateToObject($expression, $wbjSearchData);
    }

    protected function translateToObject(array $expression, $wbjSearchData = null) : WbjSearchData
    {
        if (empty($expression)) {
            return NullWbjSearchData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $hash = isset($attributes['hash']) ? $attributes['hash'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $expirationDate = isset($attributes['expirationDate']) ? $attributes['expirationDate'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $wbjTemplate = NullWbjTemplate::getInstance();
        if (isset($relationships['template']['data'])) {
            $wbjTemplateArray = $this->changeArrayFormat($relationships['template']['data']);
            $wbjTemplate = $this->getWbjTemplateRestfulTranslator()->arrayToObject($wbjTemplateArray);
        }

        $wbjItemsData = NullWbjItemsData::getInstance();
        if (isset($relationships['itemsData']['data'])) {
            $wbjItemsDataArray = $this->changeArrayFormat($relationships['itemsData']['data']);
            $wbjItemsData = $this->getWbjItemsDataRestfulTranslator()->arrayToObject($wbjItemsDataArray);
        }

        $task = NullTask::getInstance();
        if (isset($relationships['task']['data'])) {
            $taskArray = $this->changeArrayFormat($relationships['task']['data']);
            $task = $this->getTaskRestfulTranslator()->arrayToObject($taskArray);
        }

        $wbjSearchData = new WbjSearchData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $wbjTemplate,
            $wbjItemsData,
            $task,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $wbjSearchData;
    }

    public function objectToArray($wbjSearchData, array $keys = array()) : array
    {
        unset($wbjSearchData);
        unset($keys);
        return array();
    }
}
