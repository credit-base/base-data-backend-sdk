<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\ResourceCatalogData\Model\BjItemsData;
use BaseSdk\ResourceCatalogData\Model\NullBjItemsData;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $bjItemsData = null) : BjItemsData
    {
        return $this->translateToObject($expression, $bjItemsData);
    }

    protected function translateToObject(array $expression, $bjItemsData = null) : BjItemsData
    {
        if (empty($expression)) {
            return NullBjItemsData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $data = isset($attributes['data']) ? $attributes['data'] : array();
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $bjItemsData = new BjItemsData(
            $id,
            $data,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        
        return $bjItemsData;
    }

    public function objectToArray($bjItemsData, array $keys = array()) : array
    {
        unset($bjItemsData);
        unset($keys);
        return array();
    }
}
