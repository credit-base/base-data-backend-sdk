<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\ResourceCatalogData\Model\ErrorItemsData;
use BaseSdk\ResourceCatalogData\Model\NullErrorItemsData;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $errorItemsData = null) : ErrorItemsData
    {
        return $this->translateToObject($expression, $errorItemsData);
    }

    protected function translateToObject(array $expression, $errorItemsData = null) : ErrorItemsData
    {
        if (empty($expression)) {
            return NullErrorItemsData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $data = isset($attributes['data']) ? $attributes['data'] : array();
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $errorItemsData = new ErrorItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);

        return $errorItemsData;
    }

    public function objectToArray($errorItemsData, array $keys = array()) : array
    {
        unset($errorItemsData);
        unset($keys);
        return array();
    }
}
