<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\ResourceCatalogData\Model\Task;
use BaseSdk\ResourceCatalogData\Model\NullTask;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class TaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $task = null) : Task
    {
        return $this->translateToObject($expression, $task);
    }

    protected function translateToObject(array $expression, $task = null) : Task
    {
        if (empty($expression)) {
            return NullTask::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $pid = isset($attributes['pid']) ? $attributes['pid'] : 0;
        $total = isset($attributes['total']) ? $attributes['total'] : 0;
        $successNumber = isset($attributes['successNumber']) ? $attributes['successNumber'] : 0;
        $failureNumber = isset($attributes['failureNumber']) ? $attributes['failureNumber'] : 0;
        $sourceCategory = isset($attributes['sourceCategory']) ? $attributes['sourceCategory'] : 0;
        $sourceTemplate = isset($attributes['sourceTemplate']) ? $attributes['sourceTemplate'] : 0;
        $targetCategory = isset($attributes['targetCategory']) ? $attributes['targetCategory'] : 0;
        $targetTemplate = isset($attributes['targetTemplate']) ? $attributes['targetTemplate'] : 0;
        $targetRule = isset($attributes['targetRule']) ? $attributes['targetRule'] : 0;
        $scheduleTask = isset($attributes['scheduleTask']) ? $attributes['scheduleTask'] : 0;
        $errorNumber = isset($attributes['errorNumber']) ? $attributes['errorNumber'] : 0;
        $fileName = isset($attributes['fileName']) ? $attributes['fileName'] : '';
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $task = new Task(
            $id,
            $crew,
            $pid,
            $total,
            $successNumber,
            $failureNumber,
            $sourceCategory,
            $sourceTemplate,
            $targetCategory,
            $targetTemplate,
            $targetRule,
            $scheduleTask,
            $errorNumber,
            $fileName,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $task;
    }

    public function objectToArray($task, array $keys = array()) : array
    {
        unset($task);
        unset($keys);
        return array();
    }
}
