<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\ResourceCatalogData\Model\WbjItemsData;
use BaseSdk\ResourceCatalogData\Model\NullWbjItemsData;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $wbjItemsData = null) : WbjItemsData
    {
        return $this->translateToObject($expression, $wbjItemsData);
    }

    protected function translateToObject(array $expression, $wbjItemsData = null) : WbjItemsData
    {
        if (empty($expression)) {
            return NullWbjItemsData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $data = isset($attributes['data']) ? $attributes['data'] : array();
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $wbjItemsData = new WbjItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);

        return $wbjItemsData;
    }

    public function objectToArray($wbjItemsData, array $keys = array()) : array
    {
        unset($wbjItemsData);
        unset($keys);
        return array();
    }
}
