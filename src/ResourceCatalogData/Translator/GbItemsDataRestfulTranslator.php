<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\ResourceCatalogData\Model\GbItemsData;
use BaseSdk\ResourceCatalogData\Model\NullGbItemsData;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $gbItemsData = null) : GbItemsData
    {
        return $this->translateToObject($expression, $gbItemsData);
    }

    protected function translateToObject(array $expression, $gbItemsData = null) : GbItemsData
    {
        if (empty($expression)) {
            return NullGbItemsData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $data = isset($attributes['data']) ? $attributes['data'] : array();
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $gbItemsData = new GbItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);

        return $gbItemsData;
    }

    public function objectToArray($gbItemsData, array $keys = array()) : array
    {
        unset($gbItemsData);
        unset($keys);
        return array();
    }
}
