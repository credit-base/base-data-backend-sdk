<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\ResourceCatalogData\Model\NullTask;
use BaseSdk\ResourceCatalogData\Model\BjSearchData;
use BaseSdk\ResourceCatalogData\Model\NullBjItemsData;
use BaseSdk\ResourceCatalogData\Model\NullBjSearchData;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Model\NullBjTemplate;
use BaseSdk\Template\Translator\BjTemplateRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class BjSearchDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getBjTemplateRestfulTranslator() : BjTemplateRestfulTranslator
    {
        return new BjTemplateRestfulTranslator();
    }

    protected function getBjItemsDataRestfulTranslator() : BjItemsDataRestfulTranslator
    {
        return new BjItemsDataRestfulTranslator();
    }

    protected function getTaskRestfulTranslator() : TaskRestfulTranslator
    {
        return new TaskRestfulTranslator();
    }

    public function arrayToObject(array $expression, $bjSearchData = null) : BjSearchData
    {
        return $this->translateToObject($expression, $bjSearchData);
    }

    protected function translateToObject(array $expression, $bjSearchData = null) : BjSearchData
    {
        if (empty($expression)) {
            return NullBjSearchData::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $name = isset($attributes['name']) ? $attributes['name'] : '';
        $identify = isset($attributes['identify']) ? $attributes['identify'] : '';
        $infoClassify = isset($attributes['infoClassify']) ? $attributes['infoClassify'] : 0;
        $infoCategory = isset($attributes['infoCategory']) ? $attributes['infoCategory'] : 0;
        $subjectCategory = isset($attributes['subjectCategory']) ? $attributes['subjectCategory'] : 0;
        $dimension = isset($attributes['dimension']) ? $attributes['dimension'] : 0;
        $expirationDate = isset($attributes['expirationDate']) ? $attributes['expirationDate'] : 0;
        $hash = isset($attributes['hash']) ? $attributes['hash'] : '';
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $frontEndProcessorStatus = isset($attributes['frontEndProcessorStatus']) ?
                                    $attributes['frontEndProcessorStatus'] :
                                    0;
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $crew = NullCrew::getInstance();
        if (isset($relationships['crew']['data'])) {
            $crewArray = $this->changeArrayFormat($relationships['crew']['data']);
            $crew = $this->getCrewRestfulTranslator()->arrayToObject($crewArray);
        }

        $bjTemplate = NullBjTemplate::getInstance();
        if (isset($relationships['template']['data'])) {
            $bjTemplateArray = $this->changeArrayFormat($relationships['template']['data']);
            $bjTemplate = $this->getBjTemplateRestfulTranslator()->arrayToObject($bjTemplateArray);
        }

        $bjItemsData = NullBjItemsData::getInstance();
        if (isset($relationships['itemsData']['data'])) {
            $bjItemsDataArray = $this->changeArrayFormat($relationships['itemsData']['data']);
            $bjItemsData = $this->getBjItemsDataRestfulTranslator()->arrayToObject($bjItemsDataArray);
        }

        $task = NullTask::getInstance();
        if (isset($relationships['task']['data'])) {
            $taskArray = $this->changeArrayFormat($relationships['task']['data']);
            $task = $this->getTaskRestfulTranslator()->arrayToObject($taskArray);
        }

        $bjSearchData = new BjSearchData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $bjTemplate,
            $bjItemsData,
            $task,
            $description,
            $frontEndProcessorStatus,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $bjSearchData;
    }

    public function objectToArray($bjSearchData, array $keys = array()) : array
    {
        unset($bjSearchData);
        unset($keys);
        return array();
    }
}
