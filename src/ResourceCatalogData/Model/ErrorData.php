<?php
namespace BaseSdk\ResourceCatalogData\Model;

use BaseSdk\Template\Model\Template;

use BaseSdk\Crew\Model\Crew;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorData extends SearchData
{
    /**
     * @var Template $template 资源目录
     */
    protected $template;
    /**
     * @var ErrorItemsData $itemsData 资源目录数据
     */
    protected $itemsData;
     /**
     * @var int $category 资源目录类型
     */
    protected $category;
    
     /**
     * @var int $errorType 失败类型
     */
    protected $errorType;

     /**
     * @var array $errorReason 失败原因
     */
    protected $errorReason;

    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $infoClassify = 0,
        int $infoCategory = 0,
        Crew $crew = null,
        int $subjectCategory = 0,
        int $dimension = 0,
        int $expirationDate = self::EXPIRATION_DATE_DEFAULT,
        string $hash = '',
        Template $template = null,
        ErrorItemsData $itemsData = null,
        Task $task = null,
        int $category = 0,
        int $errorType = 0,
        array $errorReason = array(),
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        parent::__construct(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $task,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        $this->template = $template;
        $this->itemsData = $itemsData;
        $this->category = $category;
        $this->errorType = $errorType;
        $this->errorReason = $errorReason;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->template);
        unset($this->itemsData);
        unset($this->category);
        unset($this->errorType);
        unset($this->errorReason);
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function getItemsData() : ErrorItemsData
    {
        return $this->itemsData;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function getErrorType() : int
    {
        return $this->errorType;
    }

    public function getErrorReason() : array
    {
        return $this->errorReason;
    }
}
