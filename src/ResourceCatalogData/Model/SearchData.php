<?php
namespace BaseSdk\ResourceCatalogData\Model;

use BaseSdk\Crew\Model\Crew;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
abstract class SearchData
{
    const EXPIRATION_DATE_DEFAULT = '4102329600'; //20991231

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $name 主体名称
     */
    protected $name;
    /**
     * @var string $identify 主体标识
     */
    protected $identify;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var int $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $expirationDate 有效期限
     */
    protected $expirationDate;
    /**
     * @var string $hash 摘要hash
     */
    protected $hash;
    /**
     * @var Task $task 任务
     */
    protected $task;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;
    
    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $infoClassify = 0,
        int $infoCategory = 0,
        Crew $crew = null,
        int $subjectCategory = 0,
        int $dimension = 0,
        int $expirationDate = self::EXPIRATION_DATE_DEFAULT,
        string $hash = '',
        Task $task = null,
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->identify = $identify;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
        $this->crew = $crew;
        $this->subjectCategory = $subjectCategory;
        $this->dimension = $dimension;
        $this->expirationDate = $expirationDate;
        $this->hash = $hash;
        $this->task = $task;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->crew);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->expirationDate);
        unset($this->hash);
        unset($this->task);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function getSubjectCategory() : int
    {
        return $this->subjectCategory;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function getTask() : Task
    {
        return $this->task;
    }
    
    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
