<?php
namespace BaseSdk\ResourceCatalogData\Model;

use BaseSdk\Template\Model\GbTemplate;

use BaseSdk\Crew\Model\Crew;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchData extends SearchData
{
    const FRONT_END_PROCESSOR_STATUS = array(
        'NOT_IMPORT' => 0, //未导入前置机
        'IMPORT' => 2, //已导入前置机
    );

    /**
     * @var GbTemplate $template 资源目录
     */
    protected $template;
    /**
     * @var GbItemsData $itemsData 资源目录数据
     */
    protected $itemsData;
    /**
     * @var string $description 待确认规则描述
     */
    protected $description;
    /**
     * @var int $frontEndProcessorStatus 前置机状态
     */
    protected $frontEndProcessorStatus;

    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $infoClassify = 0,
        int $infoCategory = 0,
        Crew $crew = null,
        int $subjectCategory = 0,
        int $dimension = 0,
        int $expirationDate = self::EXPIRATION_DATE_DEFAULT,
        string $hash = '',
        GbTemplate $template = null,
        GbItemsData $itemsData = null,
        Task $task = null,
        string $description = '',
        int $frontEndProcessorStatus = self::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        parent::__construct(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $task,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        $this->description = $description;
        $this->frontEndProcessorStatus = $frontEndProcessorStatus;
        $this->template = $template;
        $this->itemsData = $itemsData;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->description);
        unset($this->frontEndProcessorStatus);
        unset($this->template);
        unset($this->itemsData);
    }

    public function getItemsData() : GbItemsData
    {
        return $this->itemsData;
    }

    public function getTemplate() : GbTemplate
    {
        return $this->template;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getFrontEndProcessorStatus() : int
    {
        return $this->frontEndProcessorStatus;
    }
}
