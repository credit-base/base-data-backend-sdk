<?php
namespace BaseSdk\ResourceCatalogData\Model;

use BaseSdk\Template\Model\WbjTemplate;

use BaseSdk\Crew\Model\Crew;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchData extends SearchData
{
    /**
     * @var WbjTemplate $template 资源目录
     */
    protected $template;
    /**
     * @var WbjItemsData $itemsData 资源目录数据
     */
    protected $itemsData;

    public function __construct(
        int $id = 0,
        string $name = '',
        string $identify = '',
        int $infoClassify = 0,
        int $infoCategory = 0,
        Crew $crew = null,
        int $subjectCategory = 0,
        int $dimension = 0,
        int $expirationDate = self::EXPIRATION_DATE_DEFAULT,
        string $hash = '',
        WbjTemplate $template = null,
        WbjItemsData $itemsData = null,
        Task $task = null,
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        parent::__construct(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $task,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );
        $this->template = $template;
        $this->itemsData = $itemsData;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->template);
        unset($this->itemsData);
    }

    public function getItemsData() : WbjItemsData
    {
        return $this->itemsData;
    }

    public function getTemplate() : WbjTemplate
    {
        return $this->template;
    }
}
