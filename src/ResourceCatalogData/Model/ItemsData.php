<?php
namespace BaseSdk\ResourceCatalogData\Model;

abstract class ItemsData
{
    private $id;

    private $data;

    public function __construct(
        int $id = 0,
        array $data = array(),
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->data = $data;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->data);
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
