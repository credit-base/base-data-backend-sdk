<?php
namespace BaseSdk\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

class NullErrorItemsData extends ErrorItemsData implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
