<?php
namespace BaseSdk\ResourceCatalogData\Model;

use BaseSdk\Template\Model\Template;

use BaseSdk\Crew\Model\Crew;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class Task
{
    /**
     * @var STATUS['DEFAULT']  进行中,默认 0
     * @var STATUS['SUCCESS']  成功 2
     * @var STATUS['FAILURE']  失败 -2
     * @var STATUS['FAILURE_FILE_DOWNLOAD']  失败文件下载成功 -3
     */
    const STATUS = array(
        'DEFAULT' => 0,
        'SUCCESS' => 2,
        'FAILURE' => -2,
        'FAILURE_FILE_DOWNLOAD' => -3
    );

    protected $id;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var int $pid 父id
     */
    protected $pid;
    /**
     * @var int $total 总数
     */
    protected $total;
    /**
     * @var int $successNumber 成功数
     */
    protected $successNumber;
    /**
     * @var int $failureNumber 失败数
     */
    protected $failureNumber;
    /**
     * @var int $sourceCategory 来源资源目录类型
     */
    protected $sourceCategory;
    /**
     * @var int $sourceTemplate 来源资源目录
     */
    protected $sourceTemplate;
    /**
     * @var int $targetCategory 目标资源目录类型
     */
    protected $targetCategory;
    /**
     * @var int $targetTemplate 目标资源目录
     */
    protected $targetTemplate;
    /**
     * @var int $targetRule 入目标库使用规则id
     */
    protected $targetRule;
    /**
     * @var int $scheduleTask 调度任务id
     */
    protected $scheduleTask;
    /**
     * @var int $errorNumber 错误编号
     */
    protected $errorNumber;
    /**
     * @var string $fileName 文件名
     */
    protected $fileName;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;

    public function __construct(
        int $id = 0,
        Crew $crew = null,
        int $pid = 0,
        int $total = 0,
        int $successNumber = 0,
        int $failureNumber = 0,
        int $sourceCategory = 0,
        int $sourceTemplate = 0,
        int $targetCategory = 0,
        int $targetTemplate = 0,
        int $targetRule = 0,
        int $scheduleTask = 0,
        int $errorNumber = 0,
        string $fileName = '',
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->crew = $crew;
        $this->pid = $pid;
        $this->total = $total;
        $this->successNumber = $successNumber;
        $this->failureNumber = $failureNumber;
        $this->sourceCategory = $sourceCategory;
        $this->sourceTemplate = $sourceTemplate;
        $this->targetCategory = $targetCategory;
        $this->targetTemplate = $targetTemplate;
        $this->targetRule = $targetRule;
        $this->scheduleTask = $scheduleTask;
        $this->errorNumber = $errorNumber;
        $this->fileName = $fileName;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->crew);
        unset($this->pid);
        unset($this->total);
        unset($this->successNumber);
        unset($this->failureNumber);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->targetCategory);
        unset($this->targetTemplate);
        unset($this->targetRule);
        unset($this->scheduleTask);
        unset($this->errorNumber);
        unset($this->fileName);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function getPid() : int
    {
        return $this->pid;
    }

    public function getTotal() : int
    {
        return $this->total;
    }

    public function getSuccessNumber() : int
    {
        return $this->successNumber;
    }

    public function getFailureNumber() : int
    {
        return $this->failureNumber;
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function getSourceTemplate() : int
    {
        return $this->sourceTemplate;
    }

    public function getTargetCategory() : int
    {
        return $this->targetCategory;
    }

    public function getTargetTemplate() : int
    {
        return $this->targetTemplate;
    }

    public function getTargetRule() : int
    {
        return $this->targetRule;
    }

    public function getScheduleTask() : int
    {
        return $this->scheduleTask;
    }

    public function getErrorNumber() : int
    {
        return $this->errorNumber;
    }

    public function getFileName() : string
    {
        return $this->fileName;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
