<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\ErrorData\ErrorDataMockAdapter;
use BaseSdk\ResourceCatalogData\Adapter\ErrorData\ErrorDataRestfulAdapter;

class ErrorDataRestfulRepository extends Repository implements IErrorDataAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'ERROR_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'ERROR_DATA_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ErrorDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IErrorDataAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IErrorDataAdapter
    {
        return new ErrorDataMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
