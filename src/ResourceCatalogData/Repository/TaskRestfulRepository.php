<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use BaseSdk\ResourceCatalogData\Adapter\Task\TaskMockAdapter;
use BaseSdk\ResourceCatalogData\Adapter\Task\TaskRestfulAdapter;

class TaskRestfulRepository extends Repository implements ITaskAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'TASK_LIST';
    const FETCH_ONE_MODEL_UN = 'TASK_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new TaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : ITaskAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITaskAdapter
    {
        return new TaskMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
