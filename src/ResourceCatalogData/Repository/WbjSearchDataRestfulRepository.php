<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData;
use BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataMockAdapter;
use BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter;

class WbjSearchDataRestfulRepository extends Repository implements IWbjSearchDataAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'WBJ_SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'WBJ_SEARCH_DATA_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjSearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IWbjSearchDataAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IWbjSearchDataAdapter
    {
        return new WbjSearchDataMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function disable(WbjSearchData $wbjSearchData) : bool
    {
        return $this->getAdapter()->disable($wbjSearchData);
    }
}
