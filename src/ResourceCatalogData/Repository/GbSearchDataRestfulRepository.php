<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\ResourceCatalogData\Model\GbSearchData;
use BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter;
use BaseSdk\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataRestfulAdapter;

class GbSearchDataRestfulRepository extends Repository implements IGbSearchDataAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'GB_SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'GB_SEARCH_DATA_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbSearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IGbSearchDataAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IGbSearchDataAdapter
    {
        return new GbSearchDataMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function confirm(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->confirm($gbSearchData);
    }
    
    public function deletes(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->deletes($gbSearchData);
    }
    
    public function disable(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->disable($gbSearchData);
    }
}
