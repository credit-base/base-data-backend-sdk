<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\ResourceCatalogData\Model\BjSearchData;
use BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter;
use BaseSdk\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataRestfulAdapter;

class BjSearchDataRestfulRepository extends Repository implements IBjSearchDataAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'BJ_SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'BJ_SEARCH_DATA_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjSearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IBjSearchDataAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBjSearchDataAdapter
    {
        return new BjSearchDataMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function confirm(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->confirm($bjSearchData);
    }
    
    public function deletes(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->deletes($bjSearchData);
    }
    
    public function disable(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->disable($bjSearchData);
    }
}
