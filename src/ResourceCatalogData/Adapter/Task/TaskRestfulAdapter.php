<?php
namespace BaseSdk\ResourceCatalogData\Adapter\Task;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\ResourceCatalogData\Model\NullTask;
use BaseSdk\ResourceCatalogData\Translator\TaskRestfulTranslator;

class TaskRestfulAdapter extends GuzzleAdapter implements ITaskAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'TASK_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'TASK_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new TaskRestfulTranslator();
        $this->resource = 'tasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullTask::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
