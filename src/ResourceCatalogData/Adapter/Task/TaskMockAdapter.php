<?php
namespace BaseSdk\ResourceCatalogData\Adapter\Task;

use BaseSdk\ResourceCatalogData\Utils\TaskMockFactory;

class TaskMockAdapter implements ITaskAdapter
{
    public function fetchOne($id)
    {
        return TaskMockFactory::generateTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $taskList = array();

        foreach ($ids as $id) {
            $taskList[$id] = TaskMockFactory::generateTask($id);
        }

        return $taskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}
