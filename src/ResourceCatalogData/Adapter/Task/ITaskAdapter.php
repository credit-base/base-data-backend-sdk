<?php
namespace BaseSdk\ResourceCatalogData\Adapter\Task;

use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface ITaskAdapter extends IFetchAbleRestfulAdapter
{
}
