<?php
namespace BaseSdk\ResourceCatalogData\Adapter\WbjSearchData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData;
use BaseSdk\ResourceCatalogData\Model\NullWbjSearchData;
use BaseSdk\ResourceCatalogData\Translator\WbjSearchDataRestfulTranslator;

class WbjSearchDataRestfulAdapter extends GuzzleAdapter implements IWbjSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'WBJ_SEARCH_DATA_LIST'=>[
            'fields' => [],
            'include' => 'crew,itemsData,template'
        ],
        'WBJ_SEARCH_DATA_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'crew,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WbjSearchDataRestfulTranslator();
        $this->resource = 'wbjSearchData';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullWbjSearchData::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function disable(WbjSearchData $wbjSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$wbjSearchData->getId().'/disable'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }
}
