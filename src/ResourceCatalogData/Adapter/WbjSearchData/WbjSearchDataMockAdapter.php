<?php
namespace BaseSdk\ResourceCatalogData\Adapter\WbjSearchData;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData;
use BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory;

class WbjSearchDataMockAdapter implements IWbjSearchDataAdapter
{
    public function fetchOne($id)
    {
        return WbjSearchDataMockFactory::generateWbjSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $wbjSearchDataList = array();

        foreach ($ids as $id) {
            $wbjSearchDataList[$id] = WbjSearchDataMockFactory::generateWbjSearchData($id);
        }

        return $wbjSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function disable(WbjSearchData $wbjSearchData) : bool
    {
        unset($wbjSearchData);
        return true;
    }
}
