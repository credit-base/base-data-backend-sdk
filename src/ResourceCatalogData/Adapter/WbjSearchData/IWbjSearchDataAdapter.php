<?php
namespace BaseSdk\ResourceCatalogData\Adapter\WbjSearchData;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IWbjSearchDataAdapter extends IFetchAbleRestfulAdapter
{
    public function disable(WbjSearchData $wbjSearchData) : bool;
}
