<?php
namespace BaseSdk\ResourceCatalogData\Adapter\ErrorData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\ResourceCatalogData\Model\NullErrorData;
use BaseSdk\ResourceCatalogData\Translator\ErrorDataRestfulTranslator;

class ErrorDataRestfulAdapter extends GuzzleAdapter implements IErrorDataAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'ERROR_DATA_LIST'=>[
            'fields'=>[],
            'include'=>'task,template'
        ],
        'ERROR_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'task,template'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new ErrorDataRestfulTranslator();
        $this->resource = 'errorDatas';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullErrorData::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
