<?php
namespace BaseSdk\ResourceCatalogData\Adapter\ErrorData;

use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IErrorDataAdapter extends IFetchAbleRestfulAdapter
{
}
