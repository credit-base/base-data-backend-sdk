<?php
namespace BaseSdk\ResourceCatalogData\Adapter\GbSearchData;

use BaseSdk\ResourceCatalogData\Model\GbSearchData;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IGbSearchDataAdapter extends IFetchAbleRestfulAdapter
{
    public function confirm(GbSearchData $gbSearchData) : bool;

    public function deletes(GbSearchData $gbSearchData) : bool;
    
    public function disable(GbSearchData $gbSearchData) : bool;
}
