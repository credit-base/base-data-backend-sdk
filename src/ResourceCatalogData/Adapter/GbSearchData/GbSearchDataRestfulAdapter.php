<?php
namespace BaseSdk\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\ResourceCatalogData\Model\GbSearchData;
use BaseSdk\ResourceCatalogData\Model\NullGbSearchData;
use BaseSdk\ResourceCatalogData\Translator\GbSearchDataRestfulTranslator;

class GbSearchDataRestfulAdapter extends GuzzleAdapter implements IGbSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'GB_SEARCH_DATA_LIST'=>[
            'fields' => [],
            'include' => 'crew,itemsData,template'
        ],
        'GB_SEARCH_DATA_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'crew,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new GbSearchDataRestfulTranslator();
        $this->resource = 'gbSearchData';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullGbSearchData::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function confirm(GbSearchData $gbSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$gbSearchData->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }

    public function deletes(GbSearchData $gbSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$gbSearchData->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }

    public function disable(GbSearchData $gbSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$gbSearchData->getId().'/disable'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }
}
