<?php
namespace BaseSdk\ResourceCatalogData\Adapter\BjSearchData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\ResourceCatalogData\Model\BjSearchData;
use BaseSdk\ResourceCatalogData\Model\NullBjSearchData;
use BaseSdk\ResourceCatalogData\Translator\BjSearchDataRestfulTranslator;

class BjSearchDataRestfulAdapter extends GuzzleAdapter implements IBjSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'BJ_SEARCH_DATA_LIST'=>[
            'fields' => [],
            'include' => 'crew,sourceUnit,itemsData,template'
        ],
        'BJ_SEARCH_DATA_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'crew,sourceUnit,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new BjSearchDataRestfulTranslator();
        $this->resource = 'bjSearchData';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullBjSearchData::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function confirm(BjSearchData $bjSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$bjSearchData->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }

    public function deletes(BjSearchData $bjSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$bjSearchData->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }

    public function disable(BjSearchData $bjSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$bjSearchData->getId().'/disable'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }
        return false;
    }
}
