<?php
namespace BaseSdk\ResourceCatalogData\Adapter\BjSearchData;

use BaseSdk\ResourceCatalogData\Model\BjSearchData;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IBjSearchDataAdapter extends IFetchAbleRestfulAdapter
{
    public function confirm(BjSearchData $bjSearchData) : bool;

    public function deletes(BjSearchData $bjSearchData) : bool;
    
    public function disable(BjSearchData $bjSearchData) : bool;
}
