<?php
namespace BaseSdk\WorkOrderTask\Model;

use Marmot\Interfaces\INull;

class NullParentTask extends ParentTask implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
