<?php
namespace BaseSdk\WorkOrderTask\Model;

use BaseSdk\Template\Model\Template;
use BaseSdk\UserGroup\Model\UserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTask
{
    /**
     * @var $id
     */
    protected $id;
    /**
     * @var ParentTask $parentTask 父任务
     */
    protected $parentTask;
    /**
     * @var Template $template 指派目录
     */
    protected $template;
    /**
     * @var UserGroup $assignObject 指派对象
     */
    protected $assignObject;
        /**
     * @var $isExistedTemplate 是否已存在目录
     */
    protected $isExistedTemplate;
    /**
     * @var $feedbackRecords 反馈记录
     */
    protected $feedbackRecords;
        /**
     * @var string $reason 撤销原因或终结原因
     */
    protected $reason;

    protected $status;

    protected $statusTime;

    protected $createTime;
    
    protected $updateTime;

    public function __construct(
        int $id = 0,
        ParentTask $parentTask = null,
        Template $template = null,
        UserGroup $assignObject = null,
        int $isExistedTemplate = 0,
        array $feedbackRecords = array(),
        string $reason = '',
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->parentTask = $parentTask;
        $this->template = $template;
        $this->assignObject = $assignObject;
        $this->isExistedTemplate = $isExistedTemplate;
        $this->feedbackRecords = $feedbackRecords;
        $this->reason = $reason;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->parentTask);
        unset($this->template);
        unset($this->assignObject);
        unset($this->isExistedTemplate);
        unset($this->feedbackRecords);
        unset($this->reason);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParentTask() : ParentTask
    {
        return $this->parentTask;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function getAssignObject() : UserGroup
    {
        return $this->assignObject;
    }

    public function getIsExistedTemplate() : int
    {
        return $this->isExistedTemplate;
    }

    public function getFeedbackRecords() : array
    {
        return $this->feedbackRecords;
    }
    
    public function getReason() : string
    {
        return $this->reason;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
