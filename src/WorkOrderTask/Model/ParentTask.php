<?php
namespace BaseSdk\WorkOrderTask\Model;

use BaseSdk\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ParentTask
{
    /**
     * @var TEMPLATE_TYPE['GB']  国标目录 1
     * @var TEMPLATE_TYPE['BJ']  本级目录 2
     */
    const TEMPLATE_TYPE = array(
        'GB' => 1,
        'BJ' => 2
    );

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $title 任务标题
     */
    protected $title;
    /**
     * @var string $description 任务描述
     */
    protected $description;
    /**
     * @var string $endTime 终结时间
     */
    protected $endTime;
    /**
     * @var array $attachment 依据附件
     */
    protected $attachment;
    /**
     * @var int $templateType 基础目录
     */
    protected $templateType;
    /**
     * @var Template $template 指派目录
     */
    protected $template;
    /**
     * @var array $assignObjects 指派对象
     */
    protected $assignObjects;
    /**
     * @var int $finishCount 已完结总数，任务状态为已撤销、已确认、已终结时，算作已完结
     */
    protected $finishCount;
    /**
     * @var string $reason 撤销原因
     */
    protected $reason;
    /**
     * @var int $ratio 父任务进度
     */
    protected $ratio;

    public function __construct(
        int $id = 0,
        string $title = '',
        string $description = '',
        string $endTime = '0000-00-00',
        array $attachment = array(),
        int $templateType = 0,
        Template $template = null,
        array $assignObjects = array(),
        int $finishCount = 0,
        int $ratio = 0,
        string $reason = '',
        int $status = 0,
        int $statusTime = 0,
        int $createTime = 0,
        int $updateTime = 0
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->endTime = $endTime;
        $this->attachment = $attachment;
        $this->templateType = $templateType;
        $this->template = $template;
        $this->assignObjects = $assignObjects;
        $this->finishCount = $finishCount;
        $this->ratio = $ratio;
        $this->reason = $reason;
        $this->status = $status;
        $this->statusTime = $statusTime;
        $this->createTime = $createTime;
        $this->updateTime = $updateTime;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->description);
        unset($this->endTime);
        unset($this->attachment);
        unset($this->templateType);
        unset($this->template);
        unset($this->assignObjects);
        unset($this->finishCount);
        unset($this->ratio);
        unset($this->reason);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getEndTime() : string
    {
        return $this->endTime;
    }

    public function getAttachment() : array
    {
        return $this->attachment;
    }

    public function getTemplateType() : int
    {
        return $this->templateType;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function getAssignObjects() : array
    {
        return $this->assignObjects;
    }

    public function getFinishCount() : int
    {
        return $this->finishCount;
    }

    public function getRatio() : int
    {
        return $this->ratio;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getStatusTime() : int
    {
        return $this->statusTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getUpdateTime() : int
    {
        return $this->updateTime;
    }
}
