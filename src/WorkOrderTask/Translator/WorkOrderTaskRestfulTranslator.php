<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask;
use BaseSdk\WorkOrderTask\Model\NullParentTask;
use BaseSdk\WorkOrderTask\Model\NullWorkOrderTask;
use BaseSdk\WorkOrderTask\Translator\TemplateTranslatorFactory;

use BaseSdk\Template\Model\NullTemplate;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WorkOrderTaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }
    
    protected function getParentTaskRestfulTranslator() : ParentTaskRestfulTranslator
    {
        return new ParentTaskRestfulTranslator();
    }

    protected function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return new TemplateTranslatorFactory();
    }

    protected function getTemplateRestfulTranslator(int $templateType) : IRestfulTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($templateType);
    }

    public function arrayToObject(array $expression, $workOrderTask = null) : WorkOrderTask
    {
        return $this->translateToObject($expression, $workOrderTask);
    }

    protected function translateToObject(array $expression, $workOrderTask = null) : WorkOrderTask
    {
        if (empty($expression)) {
            return NullWorkOrderTask::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $isExistedTemplate = isset($attributes['isExistedTemplate']) ? $attributes['isExistedTemplate'] : 0;
        $feedbackRecords = isset($attributes['feedbackRecords']) ? $attributes['feedbackRecords'] : array();
        $reason = isset($attributes['reason']) ? $attributes['reason'] : '';
        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $assignObject = NullUserGroup::getInstance();
        if (isset($relationships['assignObject']['data'])) {
            $assignObjectArray = $this->changeArrayFormat($relationships['assignObject']['data']);
            $assignObject = $this->getUserGroupRestfulTranslator()->arrayToObject($assignObjectArray);
        }

        $parentTask = NullParentTask::getInstance();
        if (isset($relationships['parentTask']['data'])) {
            $parentTaskArray = $this->changeArrayFormat($relationships['parentTask']['data']);
            $parentTask = $this->getParentTaskRestfulTranslator()->arrayToObject($parentTaskArray);
        }

        $template = NullTemplate::getInstance();
        if (isset($relationships['template']['data']) && !empty($parentTask->getTemplateType())) {
            $templateArray = $this->changeArrayFormat($relationships['template']['data']);
            $template = $this->getTemplateRestfulTranslator($parentTask->getTemplateType())->arrayToObject(
                $templateArray
            );
        }

        $workOrderTask = new WorkOrderTask(
            $id,
            $parentTask,
            $template,
            $assignObject,
            $isExistedTemplate,
            $feedbackRecords,
            $reason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $workOrderTask;
    }

    public function objectToArray($workOrderTask, array $keys = array()) : array
    {
        if (!$workOrderTask instanceof WorkOrderTask) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'feedbackRecords',
                'reason',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }
        
        $expression = array(
            'data' => array(
                'type' => 'workOrderTasks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $workOrderTask->getId();
        }

        $attributes = array();
        if (in_array('feedbackRecords', $keys)) {
            $attributes['feedbackRecords'] = $workOrderTask->getFeedbackRecords();
        }
        if (in_array('reason', $keys)) {
            $attributes['reason'] = $workOrderTask->getReason();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $workOrderTask->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $workOrderTask->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $workOrderTask->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $workOrderTask->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
