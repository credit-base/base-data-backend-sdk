<?php
namespace BaseSdk\WorkOrderTask\Translator;

use BaseSdk\WorkOrderTask\Model\ParentTask;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\NullRestfulTranslator;

class TemplateTranslatorFactory
{
    const MAPS = array(
        ParentTask::TEMPLATE_TYPE['BJ'] =>
        'BaseSdk\Template\Translator\BjTemplateRestfulTranslator',
        ParentTask::TEMPLATE_TYPE['GB'] =>
        'BaseSdk\Template\Translator\GbTemplateRestfulTranslator'
    );

    public function getTranslator(int $category) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
