<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use BaseSdk\Common\Translator\RestfulTranslatorTrait;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Model\NullParentTask;
use BaseSdk\WorkOrderTask\Translator\TemplateTranslatorFactory;

use BaseSdk\Template\Model\NullTemplate;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ParentTaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return new TemplateTranslatorFactory();
    }

    protected function getTemplateRestfulTranslator(int $templateType) : IRestfulTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($templateType);
    }

    public function arrayToObject(array $expression, $parentTask = null) : ParentTask
    {
        return $this->translateToObject($expression, $parentTask);
    }

    protected function translateToObject(array $expression, $parentTask = null) : ParentTask
    {
        if (empty($expression)) {
            return NullParentTask::getInstance();
        }

        $data = isset($expression['data']) ? $expression['data'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        $id = isset($data['id']) ? $data['id'] : 0;
        $title = isset($attributes['title']) ? $attributes['title'] : '';
        $description = isset($attributes['description']) ? $attributes['description'] : '';
        $endTime = isset($attributes['endTime']) ? $attributes['endTime'] : '';
        $attachment = isset($attributes['attachment']) ? $attributes['attachment'] : array();
        $templateType = isset($attributes['templateType']) ? $attributes['templateType'] : 0;
        $finishCount = isset($attributes['finishCount']) ? $attributes['finishCount'] : 0;
        $ratio = isset($attributes['ratio']) ? $attributes['ratio'] : 0;
        $reason = isset($attributes['reason']) ? $attributes['reason'] : '';

        $status = isset($attributes['status']) ? $attributes['status'] : 0;
        $updateTime = isset($attributes['updateTime']) ? $attributes['updateTime'] : 0;
        $createTime = isset($attributes['createTime']) ? $attributes['createTime'] : 0;
        $statusTime = isset($attributes['statusTime']) ? $attributes['statusTime'] : 0;

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        $template = NullTemplate::getInstance();
        if (isset($relationships['template']['data'])) {
            $templateArray = $this->changeArrayFormat($relationships['template']['data']);
            $template = $this->getTemplateRestfulTranslator($templateType)->arrayToObject(
                $templateArray
            );
        }

        $assignObjects = array();
        if (isset($relationships['assignObjects']['data'])) {
            foreach ($relationships['assignObjects']['data'] as $assignObject) {
                $assignObjectArray = $this->changeArrayFormat($assignObject);
                $assignObjects[] = $this->getUserGroupRestfulTranslator()->arrayToObject(
                    $assignObjectArray
                );
            }
        }

        $parentTask = new ParentTask(
            $id,
            $title,
            $description,
            $endTime,
            $attachment,
            $templateType,
            $template,
            $assignObjects,
            $finishCount,
            $ratio,
            $reason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $parentTask;
    }

    public function objectToArray($parentTask, array $keys = array()) : array
    {
        if (!$parentTask instanceof ParentTask) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'description',
                'endTime',
                'attachment',
                'templateType',
                'template',
                'assignObjects',
                'reason',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'parentTasks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $parentTask->getId();
        }

        $attributes = array();
        if (in_array('title', $keys)) {
            $attributes['title'] = $parentTask->getTitle();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $parentTask->getDescription();
        }
        if (in_array('endTime', $keys)) {
            $attributes['endTime'] = $parentTask->getEndTime();
        }
        if (in_array('attachment', $keys)) {
            $attributes['attachment'] = $parentTask->getAttachment();
        }
        if (in_array('templateType', $keys)) {
            $attributes['templateType'] = $parentTask->getTemplateType();
        }
        if (in_array('reason', $keys)) {
            $attributes['reason'] = $parentTask->getReason();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $parentTask->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $parentTask->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $parentTask->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $parentTask->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('template', $keys)) {
            $expression['data']['relationships']['template']['data'] = [
                [
                    "type" => "templates",
                    "id" => $parentTask->getTemplate()->getId()
                ]
            ];
        }

        if (in_array('assignObjects', $keys)) {
            $assignObjects = $this->getAssignObjectsArray($parentTask->getAssignObjects());
            $expression['data']['relationships']['assignObjects']['data'] = $assignObjects;
        }

        return $expression;
    }

    private function getAssignObjectsArray(array $assignObjects) : array
    {
        $assignObjectsArray = [];

        foreach ($assignObjects as $assignObject) {
            $assignObjectsArray[] = array(
                    'type' => 'userGroups',
                    'id' => $assignObject->getId()
                );
        }

        return $assignObjectsArray;
    }
}
