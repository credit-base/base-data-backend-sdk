<?php
namespace BaseSdk\WorkOrderTask\Adapter\WorkOrderTask;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IWorkOrderTaskAdapter extends IFetchAbleRestfulAdapter
{
    public function confirm(WorkOrderTask $workOrderTask) : bool;

    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool;

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool;

    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool;
}
