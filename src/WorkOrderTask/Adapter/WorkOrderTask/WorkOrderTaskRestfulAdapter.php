<?php
namespace BaseSdk\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask;
use BaseSdk\WorkOrderTask\Model\NullWorkOrderTask;
use BaseSdk\WorkOrderTask\Translator\WorkOrderTaskRestfulTranslator;

class WorkOrderTaskRestfulAdapter extends GuzzleAdapter implements IWorkOrderTaskAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'WORK_ORDER_TASK_LIST'=>[
            'fields' => [],
            'include' => 'parentTask,parentTask.assignObjects,assignObject,template'
        ],
        'WORK_ORDER_TASK_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'parentTask,parentTask.assignObjects,assignObject,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WorkOrderTaskRestfulTranslator();
        $this->resource = 'workOrderTasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullWorkOrderTask::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($workOrderTask, $keys);

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/feedback',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($workOrderTask, $keys);

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/end',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }

    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($workOrderTask, $keys);

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/revoke',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
