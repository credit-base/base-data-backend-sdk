<?php
namespace BaseSdk\WorkOrderTask\Adapter\ParentTask;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\Common\Adapter\IFetchAbleRestfulAdapter;

interface IParentTaskAdapter extends IFetchAbleRestfulAdapter
{
    public function add(ParentTask $parentTask);

    public function revoke(ParentTask $parentTask, array $keys = array()) : bool;
}
