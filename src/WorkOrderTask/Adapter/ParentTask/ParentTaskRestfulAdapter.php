<?php
namespace BaseSdk\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseSdk\Common\Adapter\CommonMapErrorsTrait;
use BaseSdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Model\NullParentTask;
use BaseSdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator;

class ParentTaskRestfulAdapter extends GuzzleAdapter implements IParentTaskAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'PARENT_TASK_LIST'=>[
            'fields' => [],
            'include' => 'assignObjects,template'
        ],
        'PARENT_TASK_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'assignObjects,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new ParentTaskRestfulTranslator();
        $this->resource = 'parentTasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullParentTask::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function add(ParentTask $parentTask)
    {
        $data = $this->getTranslator()->objectToArray($parentTask);

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $parentTask = $this->translateToObject();
            return $parentTask->getId();
        }

        return false;
    }

    public function revoke(ParentTask $parentTask, array $keys = array()) : bool
    {
        $data = $this->getTranslator()->objectToArray($parentTask, $keys);

        $this->patch(
            $this->getResource().'/'.$parentTask->getId().'/revoke',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject();
            return true;
        }

        return false;
    }
}
