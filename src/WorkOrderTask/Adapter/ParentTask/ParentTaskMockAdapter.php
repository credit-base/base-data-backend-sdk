<?php
namespace BaseSdk\WorkOrderTask\Adapter\ParentTask;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory;

class ParentTaskMockAdapter implements IParentTaskAdapter
{
    public function fetchOne($id) : ParentTask
    {
        return ParentTaskMockFactory::generateParentTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $parentTaskList = array();

        foreach ($ids as $id) {
            $parentTaskList[$id] = ParentTaskMockFactory::generateParentTask($id);
        }

        return $parentTaskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(ParentTask $parentTask)
    {
        unset($parentTask);
        return true;
    }

    public function revoke(ParentTask $parentTask, array $keys = array()) : bool
    {
        unset($parentTask);
        unset($keys);
        return true;
    }
}
