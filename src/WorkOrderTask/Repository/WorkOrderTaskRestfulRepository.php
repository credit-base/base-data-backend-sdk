<?php
namespace BaseSdk\WorkOrderTask\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask;
use BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter;
use BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

class WorkOrderTaskRestfulRepository extends Repository implements IWorkOrderTaskAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'WORK_ORDER_TASK_LIST';
    const FETCH_ONE_MODEL_UN = 'WORK_ORDER_TASK_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new WorkOrderTaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IWorkOrderTaskAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->confirm($workOrderTask);
    }
    
    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->end($workOrderTask, $keys);
    }

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->feedback($workOrderTask, $keys);
    }
    
    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->revoke($workOrderTask, $keys);
    }
}
