<?php
namespace BaseSdk\WorkOrderTask\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseSdk\Common\Repository\FetchRestfulRepositoryTrait;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseSdk\WorkOrderTask\Adapter\ParentTask\ParentTaskMockAdapter;
use BaseSdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

class ParentTaskRestfulRepository extends Repository implements IParentTaskAdapter
{
    use FetchRestfulRepositoryTrait;

    const LIST_MODEL_UN = 'PARENT_TASK_LIST';
    const FETCH_ONE_MODEL_UN = 'PARENT_TASK_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ParentTaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : IParentTaskAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IParentTaskAdapter
    {
        return new ParentTaskMockAdapter();
    }

    public function scenario($scenario = self::LIST_MODEL_UN)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function add(ParentTask $parentTask)
    {
        return $this->getAdapter()->add($parentTask);
    }

    public function revoke(ParentTask $parentTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->revoke($parentTask, $keys);
    }
}
