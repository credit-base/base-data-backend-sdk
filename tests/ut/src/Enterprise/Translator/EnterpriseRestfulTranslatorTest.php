<?php
namespace BaseSdk\Enterprise\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\Enterprise\Model\NullEnterprise;
use BaseSdk\Enterprise\Utils\EnterpriseRestfulUtils;

class EnterpriseRestfulTranslatorTest extends TestCase
{
    use EnterpriseRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriseRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $enterprise = \BaseSdk\Enterprise\Utils\MockFactory::generateEnterprise(1);

        $expression['data']['id'] = $enterprise->getId();
        $expression['data']['attributes']['name'] = $enterprise->getName();
        $expression['data']['attributes']['unifiedSocialCreditCode'] = $enterprise->getUnifiedSocialCreditCode();
        $expression['data']['attributes']['establishmentDate'] = $enterprise->getEstablishmentDate();
        $expression['data']['attributes']['approvalDate'] = $enterprise->getApprovalDate();
        $expression['data']['attributes']['address'] = $enterprise->getAddress();
        $expression['data']['attributes']['registrationCapital'] = $enterprise->getRegistrationCapital();
        $expression['data']['attributes']['businessTermStart'] = $enterprise->getBusinessTermStart();
        $expression['data']['attributes']['businessTermTo'] = $enterprise->getBusinessTermTo();
        $expression['data']['attributes']['businessScope'] = $enterprise->getBusinessScope();
        $expression['data']['attributes']['registrationAuthority'] = $enterprise->getRegistrationAuthority();
        $expression['data']['attributes']['principal'] = $enterprise->getPrincipal();
        $expression['data']['attributes']['principalCardId'] = $enterprise->getPrincipalCardId();
        $expression['data']['attributes']['registrationStatus'] = $enterprise->getRegistrationStatus();
        $expression['data']['attributes']['enterpriseTypeCode'] = $enterprise->getEnterpriseTypeCode();
        $expression['data']['attributes']['enterpriseType'] = $enterprise->getEnterpriseType();
        $expression['data']['attributes']['industryCategory'] = $enterprise->getIndustryCategory();
        $expression['data']['attributes']['industryCode'] = $enterprise->getIndustryCode();
        $expression['data']['attributes']['administrativeArea'] = $enterprise->getAdministrativeArea();

        $expression['data']['attributes']['createTime'] = $enterprise->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $enterprise->getUpdateTime();
        $expression['data']['attributes']['status'] = $enterprise->getStatus();
        $expression['data']['attributes']['statusTime'] = $enterprise->getStatusTime();

        $enterpriseObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Enterprise\Model\Enterprise', $enterpriseObject);
        $this->compareArrayAndObject($expression, $enterpriseObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $enterprise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Enterprise\Model\NullEnterprise', $enterprise);
    }

    public function testObjectToArray()
    {
        $enterprise = NullEnterprise::getInstance();

        $expression = $this->translator->objectToArray($enterprise);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
