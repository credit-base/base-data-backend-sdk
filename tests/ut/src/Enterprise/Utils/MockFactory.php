<?php
namespace BaseSdk\Enterprise\Utils;

use BaseSdk\Enterprise\Model\Enterprise;

class MockFactory
{
    public static function generateEnterprise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Enterprise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $unifiedSocialCreditCode = self::generateUnifiedSocialCreditCode($faker, $value);
        $establishmentDate = self::generateEstablishmentDate($faker, $value);
        $approvalDate = self::generateApprovalDate($faker, $value);
        $address = self::generateAddress($faker, $value);
        $registrationCapital = self::generateRegistrationCapital($faker, $value);
        $businessTermStart = self::generateBusinessTermStart($faker, $value);
        $businessTermTo = self::generateBusinessTermTo($faker, $value);
        $businessScope = self::generateBusinessScope($faker, $value);
        $registrationAuthority = self::generateRegistrationAuthority($faker, $value);
        $principal = self::generatePrincipal($faker, $value);
        $principalCardId = self::generatePrincipalCardId($faker, $value);
        $registrationStatus = self::generateRegistrationStatus($faker, $value);
        $enterpriseTypeCode = self::generateEnterpriseTypeCode($faker, $value);
        $enterpriseType = self::generateEnterpriseType($faker, $value);
        $data = self::generateData($faker, $value);
        $industryCategory = self::generateIndustryCategory($faker, $value);
        $industryCode = self::generateIndustryCode($faker, $value);
        $administrativeArea = self::generateAdministrativeArea($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();


        $enterprise = new Enterprise(
            $id,
            $name,
            $unifiedSocialCreditCode,
            $establishmentDate,
            $approvalDate,
            $address,
            $registrationCapital,
            $businessTermStart,
            $businessTermTo,
            $businessScope,
            $registrationAuthority,
            $principal,
            $principalCardId,
            $registrationStatus,
            $enterpriseTypeCode,
            $enterpriseType,
            $data,
            $industryCategory,
            $industryCode,
            $administrativeArea,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $enterprise;
    }

    private static function generateName($faker, $value) : string
    {
        return isset($value['name']) ? $value['name'] : $faker->company();
    }

    private static function generateUnifiedSocialCreditCode($faker, $value) : string
    {
        return isset($value['unifiedSocialCreditCode'])
            ? $value['unifiedSocialCreditCode']
            : $faker->bothify('##############????');
    }

    private static function generateEstablishmentDate($faker, $value) : int
    {
        return isset($value['establishmentDate']) ? $value['establishmentDate'] : $faker->date('ymd');
    }

    private static function generateApprovalDate($faker, $value) : int
    {
        return isset($value['approvalDate']) ? $value['approvalDate'] : $faker->date('ymd');
    }

    private static function generateAddress($faker, $value) : string
    {
        return isset($value['address']) ? $value['address'] : $faker->address();
    }

    private static function generateRegistrationCapital($faker, $value) : string
    {
        return isset($value['registrationCapital']) ? $value['registrationCapital'] : $faker->randomFloat();
    }

    private static function generateBusinessTermStart($faker, $value) : string
    {
        return isset($value['businessTermStart']) ? $value['businessTermStart'] : $faker->date('ymd');
    }

    private static function generateBusinessTermTo($faker, $value) : string
    {
        return isset($value['businessTermTo']) ? $value['businessTermTo'] : $faker->date('ymd');
    }

    private static function generateBusinessScope($faker, $value) : string
    {
        return isset($value['businessScope']) ? $value['businessScope'] : $faker->text(200);
    }

    private static function generateRegistrationAuthority($faker, $value) : string
    {
        return isset($value['registrationAuthority']) ? $value['registrationAuthority'] : $faker->company();
    }

    private static function generatePrincipal($faker, $value) : string
    {
        return isset($value['principal']) ? $value['principal'] : $faker->name();
    }

    private static function generatePrincipalCardId($faker, $value) : string
    {
        return isset($value['principalCardId']) ? $value['principalCardId'] : $faker->creditCardNumber();
    }

    private static function generateRegistrationStatus($faker, $value) : string
    {
        return isset($value['registrationStatus'])
        ? $value['registrationStatus']
        : $faker->randomElement(
            array('存续(在营、开业、在册)', '吊销，未注销', '吊 销，已注销', '注销', '撤销', '迁出')
        );
    }

    private static function generateEnterpriseTypeCode($faker, $value) : string
    {
        return isset($value['enterpriseTypeCode']) ? $value['enterpriseTypeCode'] : $faker->numberBetween(100, 1000);
    }

    private static function generateEnterpriseType($faker, $value) : string
    {
        return isset($value['enterpriseType'])
        ? $value['enterpriseType']
        : $faker->randomElement(array('个体工商户', '内资企业', '国有企业', '集体企业', '股份合作企业', '联营企业'));
    }

    private static function generateData($faker, $value) : array
    {
        return isset($value['data']) ? $value['data'] : $faker->words(3, false);
    }

    private static function generateIndustryCategory($faker, $value) : string
    {
        return isset($value['industryCategory'])
        ? $value['industryCategory']
        : $faker->randomElement(array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'));
    }
    
    private static function generateIndustryCode($faker, $value) : string
    {
        return isset($value['industryCode']) ? $value['industryCode'] : $faker->numberBetween(0000, 9999);
    }

    private static function generateAdministrativeArea($faker, $value) : int
    {
        return isset($value['administrativeArea']) ? $value['administrativeArea'] : $faker->numberBetween(0000, 9999);
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                Enterprise::STATUS
            );
    }
}
