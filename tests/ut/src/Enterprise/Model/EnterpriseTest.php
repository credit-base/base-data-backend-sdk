<?php
namespace BaseSdk\Enterprise\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseTest extends TestCase
{
    private $stub;

    private $enterprise;

    public function setUp()
    {
        $this->enterprise = \BaseSdk\Enterprise\Utils\MockFactory::generateEnterprise(1);
        $this->stub = new Enterprise(
            $this->enterprise->getId(),
            $this->enterprise->getName(),
            $this->enterprise->getUnifiedSocialCreditCode(),
            $this->enterprise->getEstablishmentDate(),
            $this->enterprise->getApprovalDate(),
            $this->enterprise->getAddress(),
            $this->enterprise->getRegistrationCapital(),
            $this->enterprise->getBusinessTermStart(),
            $this->enterprise->getBusinessTermTo(),
            $this->enterprise->getBusinessScope(),
            $this->enterprise->getRegistrationAuthority(),
            $this->enterprise->getPrincipal(),
            $this->enterprise->getPrincipalCardId(),
            $this->enterprise->getRegistrationStatus(),
            $this->enterprise->getEnterpriseTypeCode(),
            $this->enterprise->getEnterpriseType(),
            $this->enterprise->getData(),
            $this->enterprise->getIndustryCategory(),
            $this->enterprise->getIndustryCode(),
            $this->enterprise->getAdministrativeArea(),
            $this->enterprise->getStatus(),
            $this->enterprise->getStatusTime(),
            $this->enterprise->getCreateTime(),
            $this->enterprise->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->enterprise);
    }

    public function testGetId()
    {
        $this->assertEquals($this->enterprise->getId(), $this->stub->getId());
    }

    public function testGetName()
    {
        $this->assertEquals($this->enterprise->getName(), $this->stub->getName());
    }

    public function testGetUnifiedSocialCreditCode()
    {
        $this->assertEquals($this->enterprise->getUnifiedSocialCreditCode(), $this->stub->getUnifiedSocialCreditCode());
    }
    
    public function testGetEstablishmentDate()
    {
        $this->assertEquals($this->enterprise->getEstablishmentDate(), $this->stub->getEstablishmentDate());
    }
    
    public function testGetApprovalDate()
    {
        $this->assertEquals($this->enterprise->getApprovalDate(), $this->stub->getApprovalDate());
    }
    
    public function testGetAddress()
    {
        $this->assertEquals($this->enterprise->getAddress(), $this->stub->getAddress());
    }
    
    public function testGetRegistrationCapital()
    {
        $this->assertEquals($this->enterprise->getRegistrationCapital(), $this->stub->getRegistrationCapital());
    }
    
    public function testGetBusinessTermStart()
    {
        $this->assertEquals($this->enterprise->getBusinessTermStart(), $this->stub->getBusinessTermStart());
    }
    
    public function testGetBusinessTermTo()
    {
        $this->assertEquals($this->enterprise->getBusinessTermTo(), $this->stub->getBusinessTermTo());
    }
    
    public function testGetBusinessScope()
    {
        $this->assertEquals($this->enterprise->getBusinessScope(), $this->stub->getBusinessScope());
    }
    
    public function testGetRegistrationAuthority()
    {
        $this->assertEquals($this->enterprise->getRegistrationAuthority(), $this->stub->getRegistrationAuthority());
    }
    
    public function testGetPrincipal()
    {
        $this->assertEquals($this->enterprise->getPrincipal(), $this->stub->getPrincipal());
    }
    
    public function testGetPrincipalCardId()
    {
        $this->assertEquals($this->enterprise->getPrincipalCardId(), $this->stub->getPrincipalCardId());
    }
    
    public function testGetRegistrationStatus()
    {
        $this->assertEquals($this->enterprise->getRegistrationStatus(), $this->stub->getRegistrationStatus());
    }
    
    public function testGetEnterpriseTypeCode()
    {
        $this->assertEquals($this->enterprise->getEnterpriseTypeCode(), $this->stub->getEnterpriseTypeCode());
    }
    
    public function testGetEnterpriseType()
    {
        $this->assertEquals($this->enterprise->getEnterpriseType(), $this->stub->getEnterpriseType());
    }
    
    public function testGetData()
    {
        $this->assertEquals($this->enterprise->getData(), $this->stub->getData());
    }
    
    public function testGetIndustryCategory()
    {
        $this->assertEquals($this->enterprise->getIndustryCategory(), $this->stub->getIndustryCategory());
    }
    
    public function testGetIndustryCode()
    {
        $this->assertEquals($this->enterprise->getIndustryCode(), $this->stub->getIndustryCode());
    }
    
    public function testGetAdministrativeArea()
    {
        $this->assertEquals($this->enterprise->getAdministrativeArea(), $this->stub->getAdministrativeArea());
    }
    
    public function testGetStatus()
    {
        $this->assertEquals($this->enterprise->getStatus(), $this->stub->getStatus());
    }
    
    public function testGetStatusTime()
    {
        $this->assertEquals($this->enterprise->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->enterprise->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->enterprise->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
