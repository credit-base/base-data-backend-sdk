<?php
namespace BaseSdk\Enterprise\Adapter\Enterprise;

use PHPUnit\Framework\TestCase;

class EnterpriseRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockEnterpriseRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new EnterpriseRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIEnterpriseAdapter()
    {
        $adapter = new EnterpriseRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('enterprises', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'ENTERPRISE_LIST',
                EnterpriseRestfulAdapter::SCENARIOS['ENTERPRISE_LIST']
            ],
            [
                'ENTERPRISE_FETCH_ONE',
                EnterpriseRestfulAdapter::SCENARIOS['ENTERPRISE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Enterprise\Model\NullEnterprise',
            $this->adapter->getNullObject()
        );
    }
}
