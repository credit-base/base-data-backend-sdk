<?php
namespace BaseSdk\WorkOrderTask\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\WorkOrderTask\Model\ParentTask;

use BaseSdk\UserGroup\Model\UserGroup;

class ParentTaskMockFactory
{
    public static function generateParentTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ParentTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $title = self::generateTitle($faker, $value);
        $description = self::generateDescription($faker, $value);
        $endTime = self::generateEndTime($faker, $value);
        $attachment = self::generateAttachment($faker, $value);
        $template = self::generateTemplate($faker, $value);
        $templateType = $template->getCategory();
        $assignObjects = self::generateAssignObjects($faker, $value);
        $finishCount = isset($value['finishCount']) ? $value['finishCount'] : $faker->randomDigit();
        $ratio = isset($value['ratio']) ? $value['ratio'] : $faker->randomDigit();
        $reason = isset($value['reason']) ? $value['reason'] : $faker->title();
        $status = isset($value['status']) ? $value['status'] : $faker->randomDigit();
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $parentTask = new ParentTask(
            $id,
            $title,
            $description,
            $endTime,
            $attachment,
            $templateType,
            $template,
            $assignObjects,
            $finishCount,
            $ratio,
            $reason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $parentTask;
    }

    private static function generateTitle($faker, $value) : string
    {
        return isset($value['title']) ? $value['title'] : $faker->title();
    }

    private static function generateDescription($faker, $value) : string
    {
        return isset($value['description']) ? $value['description'] : $faker->title();
    }

    private static function generateEndTime($faker, $value) : string
    {
        return isset($value['endTime']) ? $value['endTime'] : $faker->date('Y-m-d', 'now');
    }

    private static function generateAttachment($faker, $value) : array
    {
        return isset($value['attachment']) ? $value['attachment'] : $faker->words(3, false);
    }

    private static function generateTemplate($faker, $value) : Template
    {
        return isset($value['template']) ?
            $value['template'] :
            \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
                $faker->randomDigit()
            );
    }

    private static function generateAssignObjects($faker, $value) : array
    {
        unset($faker);
        return isset($value['assignObjects']) ? $value['assignObjects'] : array(1=>new UserGroup(1));
    }
}
