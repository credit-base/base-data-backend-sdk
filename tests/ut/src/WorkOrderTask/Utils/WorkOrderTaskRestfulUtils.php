<?php
namespace BaseSdk\WorkOrderTask\Utils;

trait WorkOrderTaskRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $workOrderTask
    ) {
        $this->assertEquals($expectedArray['data']['id'], $workOrderTask->getId());
        if (isset($expectedArray['data']['attributes']['isExistedTemplate'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['isExistedTemplate'],
                $workOrderTask->getIsExistedTemplate()
            );
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['feedbackRecords'],
            $workOrderTask->getFeedbackRecords()
        );
        $this->assertEquals($expectedArray['data']['attributes']['reason'], $workOrderTask->getReason());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $workOrderTask->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $workOrderTask->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $workOrderTask->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $workOrderTask->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['parentTask']['data'][0]['type'],
                'parentTasks'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['parentTask']['data'][0]['id'],
                $unAuditedRuleService->getParentTask()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['assignObject']['data'][0]['type'],
                'userGroups'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['assignObject']['data'][0]['id'],
                $unAuditedRuleService->getAssignObject()->getId()
            );

            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $workOrderTask->getTemplate()->getId()
            );
        }
    }
}
