<?php
namespace BaseSdk\WorkOrderTask\Utils;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Model\WorkOrderTask;

use BaseSdk\UserGroup\Model\UserGroup;

class WorkOrderTaskMockFactory
{
    public static function generateWorkOrderTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WorkOrderTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $parentTask = self::generateParentTask($faker, $value);
        $template = $parentTask->getTemplate();
        $assignObject = self::generateAssignObject($faker, $value);
        $isExistedTemplate = isset($value['isExistedTemplate']) ? $value['isExistedTemplate'] : $faker->randomDigit();
        $feedbackRecords = self::generateFeedbackRecords($faker, $value);
        $reason = self::generateReason($faker, $value);
        $status = isset($value['status']) ? $value['status'] : $faker->randomDigit();
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $workOrderTask = new WorkOrderTask(
            $id,
            $parentTask,
            $template,
            $assignObject,
            $isExistedTemplate,
            $feedbackRecords,
            $reason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $workOrderTask;
    }

    private static function generateParentTask($faker, $value) : ParentTask
    {
        return isset($value['parentTask']) ?
            $value['parentTask'] :
            \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(
                $faker->randomDigit()
            );
    }

    private static function generateAssignObject($faker, $value) : UserGroup
    {
        return isset($value['assignObject']) ?
            $value['assignObject'] :
            \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(
                $faker->randomDigit()
            );
    }

    private static function generateFeedbackRecords($faker, $value) : array
    {
        return isset($value['feedbackRecords']) ? $value['feedbackRecords'] : array($faker->randomDigit());
    }

    private static function generateReason($faker, $value) : string
    {
        return isset($value['reason']) ? $value['reason'] : $faker->title();
    }
}
