<?php
namespace BaseSdk\WorkOrderTask\Utils;

trait ParentTaskRestfulUtils
{
    private function compareArrayAndObjectParentTask(
        array $expectedArray,
        $parentTask
    ) {
        $this->assertEquals($expectedArray['data']['id'], $parentTask->getId());
        $this->assertEquals($expectedArray['data']['attributes']['title'], $parentTask->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $parentTask->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['endTime'], $parentTask->getEndTime());
        $this->assertEquals($expectedArray['data']['attributes']['attachment'], $parentTask->getAttachment());
        $this->assertEquals($expectedArray['data']['attributes']['templateType'], $parentTask->getTemplateType());
        if (isset($expectedArray['data']['attributes']['finishCount'])) {
            $this->assertEquals($expectedArray['data']['attributes']['finishCount'], $parentTask->getFinishCount());
        }
        if (isset($expectedArray['data']['attributes']['ratio'])) {
            $this->assertEquals($expectedArray['data']['attributes']['ratio'], $parentTask->getRatio());
        }
        $this->assertEquals($expectedArray['data']['attributes']['reason'], $parentTask->getReason());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $parentTask->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $parentTask->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $parentTask->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $parentTask->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $parentTask->getTemplate()->getId()
            );

            $assignObjects = $expectedArray['data']['relationships']['assignObjects']['data'];

            foreach ($assignObjects as $assignObject) {
                $this->assertEquals(
                    $assignObject['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $assignObject['id'],
                    $parentTask->getAssignObjects()[$assignObject['id']]->getId()
                );
            }
        }
    }
}
