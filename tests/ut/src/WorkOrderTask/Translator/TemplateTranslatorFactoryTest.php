<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class TemplateTranslatorFactoryTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateTranslatorFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->translator);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullRestfulTranslator()
    {
        $translator = $this->translator->getTranslator(0);
            $this->assertInstanceOf(
                'BaseSdk\Common\Translator\NullRestfulTranslator',
                $translator
            );
    }

    public function testGetController()
    {
        foreach (TemplateTranslatorFactory::MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTranslator($key)
            );
        }
    }
}
