<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Utils\WorkOrderTaskRestfulUtils;

use BaseSdk\Template\Translator\BjTemplateRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskRestfulTranslatorTest extends TestCase
{
    use WorkOrderTaskRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WorkOrderTaskRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockWorkOrderTaskRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetParentTaskRestfulTranslator()
    {
        $translator = new MockWorkOrderTaskRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator',
            $translator->getParentTaskRestfulTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockWorkOrderTaskRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Translator\TemplateTranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = $this->getMockBuilder(MockWorkOrderTaskRestfulTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $restfulTranslator = new BjTemplateRestfulTranslator();
        $type = ParentTask::TEMPLATE_TYPE['BJ'];

        $translatorFactory = $this->prophesize(TemplateTranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($type))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($restfulTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateRestfulTranslator($type);
        $this->assertInstanceof('Marmot\Interfaces\IRestfulTranslator', $result);
        $this->assertEquals($restfulTranslator, $result);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $workOrderTask = \BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $expression['data']['id'] = $workOrderTask->getId();
        $expression['data']['attributes']['isExistedTemplate'] = $workOrderTask->getIsExistedTemplate();
        $expression['data']['attributes']['feedbackRecords'] = $workOrderTask->getFeedbackRecords();
        $expression['data']['attributes']['reason'] = $workOrderTask->getReason();
        $expression['data']['attributes']['createTime'] = $workOrderTask->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $workOrderTask->getUpdateTime();
        $expression['data']['attributes']['status'] = $workOrderTask->getStatus();
        $expression['data']['attributes']['statusTime'] = $workOrderTask->getStatusTime();

        $workOrderTaskObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskObject);
        $this->compareArrayAndObject($expression, $workOrderTaskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $workOrderTask = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\NullWorkOrderTask', $workOrderTask);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $workOrderTask = \BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $expression = $this->translator->objectToArray($workOrderTask);

        $this->compareArrayAndObject($expression, $workOrderTask);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'assignObject'=>['data'=>'mockAssignObject'],
            'parentTask'=>['data'=>'mockParentTask'],
            'template'=>['data'=>'mockTemplate']
        ];

        $translator = $this->getMockBuilder(MockWorkOrderTaskRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getUserGroupRestfulTranslator',
             'getParentTaskRestfulTranslator',
             'getTemplateRestfulTranslator',
         ])
        ->getMock();

        $assignObjectInfo = ['mockAssignObject'];
        $parentTaskInfo = ['mockParentTask'];
        $templateInfo = ['mockTemplate'];

        $parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
        $template = $parentTask->getTemplate();
        $assignObject = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(3))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['assignObject']['data']],
                 [$relationships['parentTask']['data']],
                 [$relationships['template']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $assignObjectInfo,
                $parentTaskInfo,
                $templateInfo
            ));
  
        $parentTaskRestfulTranslator = $this->prophesize(ParentTaskRestfulTranslator::class);
        $parentTaskRestfulTranslator->arrayToObject(Argument::exact($parentTaskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($parentTask);

        $assignObjectRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $assignObjectRestfulTranslator->arrayToObject(Argument::exact($assignObjectInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($assignObject);

        $templateRestfulTranslator = $this->prophesize(IRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getParentTaskRestfulTranslator')
            ->willReturn($parentTaskRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($assignObjectRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());

        //揭示
        $workOrderTask = $translator->arrayToObject($expression);

        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTask);
        $this->assertEquals($template, $workOrderTask->getTemplate());
        $this->assertEquals($parentTask, $workOrderTask->getParentTask());
        $this->assertEquals($assignObject, $workOrderTask->getAssignObject());
    }
}
