<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\WorkOrderTask\Model\ParentTask;
use BaseSdk\WorkOrderTask\Utils\ParentTaskRestfulUtils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Translator\TranslatorFactory;
use BaseSdk\Template\Translator\GbTemplateRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class ParentTaskRestfulTranslatorTest extends TestCase
{
    use ParentTaskRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ParentTaskRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockParentTaskRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockParentTaskRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Translator\TemplateTranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = $this->getMockBuilder(MockParentTaskRestfulTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $restfulTranslator = new GbTemplateRestfulTranslator();
        $type = ParentTask::TEMPLATE_TYPE['GB'];

        $translatorFactory = $this->prophesize(TemplateTranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($type))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($restfulTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateRestfulTranslator($type);
        $this->assertInstanceof('Marmot\Interfaces\IRestfulTranslator', $result);
        $this->assertEquals($restfulTranslator, $result);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);

        $expression['data']['id'] = $parentTask->getId();
        $expression['data']['attributes']['title'] = $parentTask->getTitle();
        $expression['data']['attributes']['description'] = $parentTask->getDescription();
        $expression['data']['attributes']['endTime'] = $parentTask->getEndTime();
        $expression['data']['attributes']['attachment'] = $parentTask->getAttachment();
        $expression['data']['attributes']['templateType'] = $parentTask->getTemplateType();
        $expression['data']['attributes']['finishCount'] = $parentTask->getFinishCount();
        $expression['data']['attributes']['ratio'] = $parentTask->getRatio();
        $expression['data']['attributes']['reason'] = $parentTask->getReason();
        $expression['data']['attributes']['createTime'] = $parentTask->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $parentTask->getUpdateTime();
        $expression['data']['attributes']['status'] = $parentTask->getStatus();
        $expression['data']['attributes']['statusTime'] = $parentTask->getStatusTime();

        $parentTaskObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\ParentTask', $parentTaskObject);
        $this->compareArrayAndObjectParentTask($expression, $parentTaskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $parentTask = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\NullParentTask', $parentTask);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);

        $expression = $this->translator->objectToArray($parentTask);

        $this->compareArrayAndObjectParentTask($expression, $parentTask);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'assignObjects'=>['data'=>array(0=>'mockAssignObjects')],
            'template'=>['data'=>'mockTemplate']
        ];

        $translator = $this->getMockBuilder(ParentTaskRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getTemplateRestfulTranslator',
             'getUserGroupRestfulTranslator'
         ])
        ->getMock();

        $assignObjectsInfo = ['mockAssignObjects'];
        $templateInfo = ['mockTemplate'];

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $userGroup = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['template']['data']],
                 [$relationships['assignObjects']['data'][0]]
             )
            ->will($this->onConsecutiveCalls(
                $templateInfo,
                $assignObjectsInfo
            ));

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($assignObjectsInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $templateRestfulTranslator = $this->prophesize(IRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());

        //揭示
        $parentTask = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\ParentTask', $parentTask);
        $this->assertEquals($gbTemplate, $parentTask->getTemplate());
        $this->assertEquals($userGroup, $parentTask->getAssignObjects()[0]);
    }
}
