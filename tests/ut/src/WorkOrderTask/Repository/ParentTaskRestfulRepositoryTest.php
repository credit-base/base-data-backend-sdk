<?php
namespace BaseSdk\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory;
use BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseSdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

class ParentTaskRestfulRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ParentTaskRestfulRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIParentTaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->repository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->repository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\ParentTaskMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(ParentTaskRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $parentTask = ParentTaskMockFactory::generateParentTask(1);
        
        $adapter = $this->prophesize(IParentTaskAdapter::class);
        $adapter->add(Argument::exact($parentTask))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($parentTask);
    }

    public function testRevoke()
    {
        $parentTask = ParentTaskMockFactory::generateParentTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IParentTaskAdapter::class);
        $adapter->revoke(Argument::exact($parentTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($parentTask, $keys);
    }
}
