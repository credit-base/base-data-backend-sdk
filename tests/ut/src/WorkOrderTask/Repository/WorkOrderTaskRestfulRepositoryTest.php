<?php
namespace BaseSdk\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory;
use BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

class WorkOrderTaskRestfulRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WorkOrderTaskRestfulRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIWorkOrderTaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->repository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->repository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testConfirm()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->confirm(Argument::exact($workOrderTask))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($workOrderTask);
    }

    public function testEnd()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->end(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->end($workOrderTask, $keys);
    }

    public function testFeedback()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->feedback(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->feedback($workOrderTask, $keys);
    }

    public function testRevoke()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->revoke(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($workOrderTask, $keys);
    }
}
