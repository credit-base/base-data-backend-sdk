<?php
namespace BaseSdk\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ParentTaskTest extends TestCase
{
    private $stub;

    private $parentTask;

    public function setUp()
    {
        $this->parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
        $this->stub = new ParentTask(
            $this->parentTask->getId(),
            $this->parentTask->getTitle(),
            $this->parentTask->getDescription(),
            $this->parentTask->getEndTime(),
            $this->parentTask->getAttachment(),
            $this->parentTask->getTemplateType(),
            $this->parentTask->getTemplate(),
            $this->parentTask->getAssignObjects(),
            $this->parentTask->getFinishCount(),
            $this->parentTask->getRatio(),
            $this->parentTask->getReason(),
            $this->parentTask->getStatus(),
            $this->parentTask->getStatusTime(),
            $this->parentTask->getCreateTime(),
            $this->parentTask->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->parentTask);
    }

    public function testGetId()
    {
        $this->assertEquals($this->parentTask->getId(), $this->stub->getId());
    }

    public function testGetTitle()
    {
        $this->assertEquals($this->parentTask->getTitle(), $this->stub->getTitle());
    }

    public function testGetDescription()
    {
        $this->assertEquals($this->parentTask->getDescription(), $this->stub->getDescription());
    }

    public function testGetEndTime()
    {
        $this->assertEquals($this->parentTask->getEndTime(), $this->stub->getEndTime());
    }

    public function testGetAttachment()
    {
        $this->assertEquals($this->parentTask->getAttachment(), $this->stub->getAttachment());
    }

    public function testGetTemplateType()
    {
        $this->assertEquals($this->parentTask->getTemplateType(), $this->stub->getTemplateType());
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->parentTask->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetAssignObjects()
    {
        $this->assertEquals($this->parentTask->getAssignObjects(), $this->stub->getAssignObjects());
    }

    public function testGetFinishCount()
    {
        $this->assertEquals($this->parentTask->getFinishCount(), $this->stub->getFinishCount());
    }

    public function testGetRatio()
    {
        $this->assertEquals($this->parentTask->getRatio(), $this->stub->getRatio());
    }

    public function testGetReason()
    {
        $this->assertEquals($this->parentTask->getReason(), $this->stub->getReason());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->parentTask->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->parentTask->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->parentTask->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->parentTask->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
