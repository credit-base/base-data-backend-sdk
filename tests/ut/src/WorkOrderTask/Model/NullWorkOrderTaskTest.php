<?php
namespace BaseSdk\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

class NullWorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = $this->getMockBuilder(NullWorkOrderTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    public function testExtendsWorkOrderTask()
    {
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\WorkOrderTask', $this->workOrderTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->workOrderTask);
    }
}
