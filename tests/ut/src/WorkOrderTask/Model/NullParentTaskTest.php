<?php
namespace BaseSdk\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

class NullParentTaskTest extends TestCase
{
    private $parentTask;

    public function setUp()
    {
        $this->parentTask = $this->getMockBuilder(NullParentTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    public function testExtendsParentTask()
    {
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\ParentTask', $this->parentTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->parentTask);
    }
}
