<?php
namespace BaseSdk\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskTest extends TestCase
{
    private $stub;

    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = \BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $this->stub = new WorkOrderTask(
            $this->workOrderTask->getId(),
            $this->workOrderTask->getParentTask(),
            $this->workOrderTask->getTemplate(),
            $this->workOrderTask->getAssignObject(),
            $this->workOrderTask->getIsExistedTemplate(),
            $this->workOrderTask->getFeedbackRecords(),
            $this->workOrderTask->getReason(),
            $this->workOrderTask->getStatus(),
            $this->workOrderTask->getStatusTime(),
            $this->workOrderTask->getCreateTime(),
            $this->workOrderTask->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->workOrderTask);
    }

    public function testGetId()
    {
        $this->assertEquals($this->workOrderTask->getId(), $this->stub->getId());
    }

    public function testGetParentTask()
    {
        $this->assertEquals($this->workOrderTask->getParentTask(), $this->stub->getParentTask());
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->workOrderTask->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetAssignObject()
    {
        $this->assertEquals($this->workOrderTask->getAssignObject(), $this->stub->getAssignObject());
    }

    public function testGetIsExistedTemplate()
    {
        $this->assertEquals($this->workOrderTask->getIsExistedTemplate(), $this->stub->getIsExistedTemplate());
    }

    public function testGetFeedbackRecords()
    {
        $this->assertEquals($this->workOrderTask->getFeedbackRecords(), $this->stub->getFeedbackRecords());
    }

    public function testGetReason()
    {
        $this->assertEquals($this->workOrderTask->getReason(), $this->stub->getReason());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->workOrderTask->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->workOrderTask->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->workOrderTask->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->workOrderTask->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
