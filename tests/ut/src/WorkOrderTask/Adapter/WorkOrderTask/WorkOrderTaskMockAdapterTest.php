<?php
namespace BaseSdk\WorkOrderTask\Adapter\WorkOrderTask;

use PHPUnit\Framework\TestCase;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new WorkOrderTaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testConfirm()
    {
        $this->assertTrue($this->adapter->confirm(new WorkOrderTask(), array()));
    }

    public function testFeedback()
    {
        $this->assertTrue($this->adapter->feedback(new WorkOrderTask(), array()));
    }

    public function testEnd()
    {
        $this->assertTrue($this->adapter->end(new WorkOrderTask(), array()));
    }

    public function testRevoke()
    {
        $this->assertTrue($this->adapter->revoke(new WorkOrderTask(), array()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Model\WorkOrderTask',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\WorkOrderTask\Model\WorkOrderTask',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\WorkOrderTask\Model\WorkOrderTask',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
