<?php
namespace BaseSdk\WorkOrderTask\Adapter\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockWorkOrderTaskRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new WorkOrderTaskRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIWorkOrderTaskAdapter()
    {
        $adapter = new WorkOrderTaskRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Translator\WorkOrderTaskRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('workOrderTasks', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'WORK_ORDER_TASK_LIST',
                WorkOrderTaskRestfulAdapter::SCENARIOS['WORK_ORDER_TASK_LIST']
            ],
            [
                'WORK_ORDER_TASK_FETCH_ONE',
                WorkOrderTaskRestfulAdapter::SCENARIOS['WORK_ORDER_TASK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Model\NullWorkOrderTask',
            $this->adapter->getNullObject()
        );
    }

    private function initWorkOrderTask(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockWorkOrderTaskRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $workOrderTask = \BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $data = array('data');

        $translator = $this->prophesize(IRestfulTranslator::class);
        $translator->objectToArray($workOrderTask, array())->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('workOrderTask');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $workOrderTask;
    }

    public function testFeedback()
    {
        $workOrderTask = $this->initWorkOrderTask(true);

        $result = $this->adapter->feedback($workOrderTask);
        $this->assertTrue($result);
    }

    public function testFeedbackFail()
    {
        $workOrderTask = $this->initWorkOrderTask(false);

        $result = $this->adapter->feedback($workOrderTask);
        $this->assertFalse($result);
    }

    public function testEnd()
    {
        $workOrderTask = $this->initWorkOrderTask(true);

        $result = $this->adapter->end($workOrderTask);
        $this->assertTrue($result);
    }

    public function testEndFail()
    {
        $workOrderTask = $this->initWorkOrderTask(false);

        $result = $this->adapter->end($workOrderTask);
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        $workOrderTask = $this->initWorkOrderTask(true);

        $result = $this->adapter->revoke($workOrderTask);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $workOrderTask = $this->initWorkOrderTask(false);

        $result = $this->adapter->revoke($workOrderTask);
        $this->assertFalse($result);
    }

    private function initConfirm(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockWorkOrderTaskRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $workOrderTask = \BaseSdk\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('workOrderTask');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $workOrderTask;
    }

    public function testConfirm()
    {
        $workOrderTask = $this->initConfirm(true);

        $result = $this->adapter->confirm($workOrderTask);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $workOrderTask = $this->initConfirm(false);

        $result = $this->adapter->confirm($workOrderTask);
        $this->assertFalse($result);
    }
}
