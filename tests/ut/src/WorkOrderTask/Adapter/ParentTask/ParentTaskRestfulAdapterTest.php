<?php
namespace BaseSdk\WorkOrderTask\Adapter\ParentTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ParentTaskRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockParentTaskRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new ParentTaskRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIParentTaskAdapter()
    {
        $adapter = new ParentTaskRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('parentTasks', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'PARENT_TASK_LIST',
                ParentTaskRestfulAdapter::SCENARIOS['PARENT_TASK_LIST']
            ],
            [
                'PARENT_TASK_FETCH_ONE',
                ParentTaskRestfulAdapter::SCENARIOS['PARENT_TASK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Model\NullParentTask',
            $this->adapter->getNullObject()
        );
    }

    private function initParentTask(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockParentTaskRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
        $data = array('data');

        $translator = $this->prophesize(IRestfulTranslator::class);
        $translator->objectToArray($parentTask, array())->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('parentTask');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $parentTask;
    }

    public function testRevoke()
    {
        $parentTask = $this->initParentTask(true);

        $result = $this->adapter->revoke($parentTask);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $parentTask = $this->initParentTask(false);

        $result = $this->adapter->revoke($parentTask);
        $this->assertFalse($result);
    }
    
    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockParentTaskRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $parentTask = \BaseSdk\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
        $data = array('data');

        $parentTranslator = $this->prophesize(IRestfulTranslator::class);
        $parentTranslator->objectToArray($parentTask)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($parentTranslator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($parentTask);
        }
        
        return $parentTask;
    }

    public function testAdd()
    {
        $parentTask = $this->initAdd(true);

        $result = $this->adapter->add($parentTask);
        $this->assertEquals($parentTask->getId(), $result);
    }

    public function testAddFail()
    {
        $parentTask = $this->initAdd(false);

        $result = $this->adapter->add($parentTask);
        $this->assertFalse($result);
    }
}
