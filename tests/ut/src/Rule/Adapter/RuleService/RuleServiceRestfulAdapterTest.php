<?php
namespace BaseSdk\Rule\Adapter\RuleService;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockRuleServiceRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new RuleServiceRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIRuleServiceAdapter()
    {
        $adapter = new RuleServiceRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Rule\Translator\RuleServiceRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('rules', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'RULE_SERVER_LIST',
                RuleServiceRestfulAdapter::SCENARIOS['RULE_SERVER_LIST']
            ],
            [
                'RULE_SERVER_FETCH_ONE',
                RuleServiceRestfulAdapter::SCENARIOS['RULE_SERVER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Model\NullRuleService',
            $this->adapter->getNullObject()
        );
    }

    private function initRuleService(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockRuleServiceRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $ruleService = \BaseSdk\Rule\Utils\MockFactory::generateRuleService(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('ruleService');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $ruleService;
    }

    public function testDeletes()
    {
        $ruleService = $this->initRuleService(true);

        $result = $this->adapter->deletes($ruleService);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $ruleService = $this->initRuleService(false);

        $result = $this->adapter->deletes($ruleService);
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $ruleService = $this->initRuleService(true);

        $result = $this->adapter->edit($ruleService);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        $ruleService = $this->initRuleService(false);

        $result = $this->adapter->edit($ruleService);
        $this->assertFalse($result);
    }
    
    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockRuleServiceRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $ruleService = \BaseSdk\Rule\Utils\MockFactory::generateRuleService(1);
        $data = array('data');

        $translator = $this->prophesize(IRestfulTranslator::class);
        $translator->objectToArray($ruleService)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($ruleService);
        }
        
        return $ruleService;
    }

    public function testAdd()
    {
        $ruleService = $this->initAdd(true);

        $result = $this->adapter->add($ruleService);
        $this->assertEquals($ruleService->getId(), $result);
    }

    public function testAddFail()
    {
        $ruleService = $this->initAdd(false);

        $result = $this->adapter->add($ruleService);
        $this->assertFalse($result);
    }
}
