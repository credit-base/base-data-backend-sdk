<?php
namespace BaseSdk\Rule\Adapter\UnAuditedRuleService;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockUnAuditedRuleServiceRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new UnAuditedRuleServiceRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIUnAuditedRuleServiceAdapter()
    {
        $adapter = new UnAuditedRuleServiceRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Rule\Translator\UnAuditedRuleServiceRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedRules', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDITED_RULE_SERVER_LIST',
                UnAuditedRuleServiceRestfulAdapter::SCENARIOS['UN_AUDITED_RULE_SERVER_LIST']
            ],
            [
                'UN_AUDITED_RULE_SERVER_FETCH_ONE',
                UnAuditedRuleServiceRestfulAdapter::SCENARIOS['UN_AUDITED_RULE_SERVER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Model\NullUnAuditedRuleService',
            $this->adapter->getNullObject()
        );
    }

    private function initUnAuditedRuleService(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockUnAuditedRuleServiceRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $unAuditedRuleService = \BaseSdk\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('unAuditedRuleService');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $unAuditedRuleService;
    }

    public function testResubmit()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(true);

        $result = $this->adapter->resubmit($unAuditedRuleService);
        $this->assertTrue($result);
    }

    public function testResubmitFail()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(false);

        $result = $this->adapter->resubmit($unAuditedRuleService);
        $this->assertFalse($result);
    }

    public function testApprove()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(true);

        $result = $this->adapter->approve($unAuditedRuleService);
        $this->assertTrue($result);
    }

    public function testApproveFail()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(false);

        $result = $this->adapter->approve($unAuditedRuleService);
        $this->assertFalse($result);
    }

    public function testReject()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(true);

        $result = $this->adapter->reject($unAuditedRuleService);
        $this->assertTrue($result);
    }

    public function testRejectFail()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(false);

        $result = $this->adapter->reject($unAuditedRuleService);
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(true);

        $result = $this->adapter->revoke($unAuditedRuleService);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $unAuditedRuleService = $this->initUnAuditedRuleService(false);

        $result = $this->adapter->revoke($unAuditedRuleService);
        $this->assertFalse($result);
    }
}
