<?php
namespace BaseSdk\Rule\Adapter\UnAuditedRuleService;

use PHPUnit\Framework\TestCase;

use BaseSdk\Rule\Model\UnAuditedRuleService;

class UnAuditedRuleServiceMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedRuleServiceMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testResubmit()
    {
        $this->assertTrue($this->adapter->resubmit(new UnAuditedRuleService(), array()));
    }

    public function testRevoke()
    {
        $this->assertTrue($this->adapter->revoke(new UnAuditedRuleService(), array()));
    }

    public function testApprove()
    {
        $this->assertTrue($this->adapter->approve(new UnAuditedRuleService(), array()));
    }

    public function testReject()
    {
        $this->assertTrue($this->adapter->reject(new UnAuditedRuleService(), array()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Model\UnAuditedRuleService',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Rule\Model\UnAuditedRuleService',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Rule\Model\UnAuditedRuleService',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
