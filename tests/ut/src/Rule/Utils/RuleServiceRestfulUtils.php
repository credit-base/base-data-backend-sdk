<?php
namespace BaseSdk\Rule\Utils;

trait RuleServiceRestfulUtils
{
    private function compareArrayAndObjectRuleService(
        array $expectedArray,
        $ruleService
    ) {
        $this->assertEquals($expectedArray['data']['id'], $ruleService->getId());
        $this->assertEquals(
            $expectedArray['data']['attributes']['transformationCategory'],
            $ruleService->getTransformationCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['sourceCategory'],
            $ruleService->getSourceCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['rules'], $ruleService->getRules());
        if (isset($expectedArray['data']['attributes']['version'])) {
            $this->assertEquals($expectedArray['data']['attributes']['version'], $ruleService->getVersion());
        }
        if (isset($expectedArray['data']['attributes']['dataTotal'])) {
            $this->assertEquals($expectedArray['data']['attributes']['dataTotal'], $ruleService->getDataTotal());
        }
        $this->assertEquals($expectedArray['data']['attributes']['status'], $ruleService->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $ruleService->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $ruleService->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $ruleService->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $ruleService->getCrew()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['userGroup']['data'][0]['type'],
                'userGroups'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['userGroup']['data'][0]['id'],
                $ruleService->getUserGroup()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['transformationTemplate']['data'][0]['id'],
                $ruleService->getTransformationTemplate()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceTemplate']['data'][0]['id'],
                $ruleService->getSourceTemplate()->getId()
            );
        }
    }

    private function compareArrayAndObjectUnAuditedRuleService(
        array $expectedArray,
        $unAuditedRuleService
    ) {
        $this->compareArrayAndObjectRuleService($expectedArray, $unAuditedRuleService);

        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['operationType'],
                $unAuditedRuleService->getOperationType()
            );
        }
        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['relationId'],
                $unAuditedRuleService->getRelationId()
            );
        }
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyStatus'],
                $unAuditedRuleService->getApplyStatus()
            );
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['rejectReason'],
            $unAuditedRuleService->getRejectReason()
        );

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['applyCrew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['applyCrew']['data'][0]['id'],
                $unAuditedRuleService->getCrew()->getId()
            );
        }
    }
}
