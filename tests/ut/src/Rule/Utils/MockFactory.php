<?php
namespace BaseSdk\Rule\Utils;

use BaseSdk\Template\Model\Template;

use BaseSdk\Rule\Model\RuleService;
use BaseSdk\Rule\Model\UnAuditedRuleService;

use BaseSdk\Crew\Model\Crew;

class MockFactory
{
    protected static function generateTransformationTemplate($faker, $value) : Template
    {
        return isset($value['transformationTemplate']) ?
            $value['transformationTemplate'] :
            \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
                $faker->randomDigit()
            );
    }

    protected static function generateSourceTemplate($faker, $value) : Template
    {
        return isset($value['sourceTemplate']) ?
            $value['sourceTemplate'] :
            \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
                $faker->randomDigit()
            );
    }

    protected static function generateRules($faker, $value) : array
    {
        unset($faker);
        $rules = array(
            'transformationRule' => array(
                "TYSHXYDM" => "TYSHXYDM",
                "ZTMC" => "ZTMC",
            ),
            'completionRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'comparisonRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM")),
        );
    
        return isset($value['rules']) ? $value['rules'] : $rules;
    }

    protected static function generateCrew($faker, $value) : Crew
    {
        return isset($value['crew']) ?
        $value['crew'] :
        \BaseSdk\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                RuleService::STATUS
            );
    }

    public static function generateRuleService(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : RuleService {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $sourceTemplate = self::generateSourceTemplate($faker, $value);
        $sourceCategory = $sourceTemplate->getCategory();
        $transformationTemplate =self::generateTransformationTemplate($faker, $value);
        $transformationCategory = $transformationTemplate->getCategory();
        $rules = self::generateRules($faker, $value);
        $version = $faker->unixTime();
        $dataTotal = $faker->randomDigit();
        $crew = self::generateCrew($faker, $value);
        $userGroup = $sourceTemplate->getSourceUnit();
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $ruleService = new RuleService(
            $id,
            $sourceCategory,
            $sourceTemplate,
            $transformationCategory,
            $transformationTemplate,
            $rules,
            $version,
            $dataTotal,
            $crew,
            $userGroup,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $ruleService;
    }

    public static function generateUnAuditedRuleService(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedRuleService {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $sourceTemplate = self::generateSourceTemplate($faker, $value);
        $sourceCategory = $sourceTemplate->getCategory();
        $transformationTemplate =self::generateTransformationTemplate($faker, $value);
        $transformationCategory = $transformationTemplate->getCategory();
        $rules = self::generateRules($faker, $value);
        $version = $faker->unixTime();
        $dataTotal = $faker->randomDigit();
        $crew = $applyCrew = self::generateCrew($faker, $value);
        $userGroup = $sourceTemplate->getSourceUnit();
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomDigit();
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomDigit();
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->title();
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $unAuditRuleService = new UnAuditedRuleService(
            $id,
            $sourceCategory,
            $sourceTemplate,
            $transformationCategory,
            $transformationTemplate,
            $rules,
            $version,
            $dataTotal,
            $crew,
            $userGroup,
            $applyCrew,
            $operationType,
            $id,
            $applyStatus,
            $rejectReason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $unAuditRuleService;
    }
}
