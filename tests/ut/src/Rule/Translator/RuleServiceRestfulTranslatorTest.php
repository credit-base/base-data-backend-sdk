<?php
namespace BaseSdk\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Rule\Utils\RuleServiceRestfulUtils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Translator\TranslatorFactory;
use BaseSdk\Template\Translator\WbjTemplateRestfulTranslator;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceRestfulTranslatorTest extends TestCase
{
    use RuleServiceRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleServiceRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockRuleServiceRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockRuleServiceRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockRuleServiceRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\TranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = $this->getMockBuilder(MockRuleServiceRestfulTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $restfulTranslator = new WbjTemplateRestfulTranslator();
        $category = Template::CATEGORY['WBJ'];

        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($restfulTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateRestfulTranslator($category);
        $this->assertInstanceof('Marmot\Interfaces\IRestfulTranslator', $result);
        $this->assertEquals($restfulTranslator, $result);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $ruleService = \BaseSdk\Rule\Utils\MockFactory::generateRuleService(1);

        $expression['data']['id'] = $ruleService->getId();
        $expression['data']['attributes']['transformationCategory'] = $ruleService->getTransformationCategory();
        $expression['data']['attributes']['sourceCategory'] = $ruleService->getSourceCategory();
        $expression['data']['attributes']['rules'] = $ruleService->getRules();
        $expression['data']['attributes']['version'] = $ruleService->getVersion();
        $expression['data']['attributes']['dataTotal'] = $ruleService->getDataTotal();
        $expression['data']['attributes']['createTime'] = $ruleService->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $ruleService->getUpdateTime();
        $expression['data']['attributes']['status'] = $ruleService->getStatus();
        $expression['data']['attributes']['statusTime'] = $ruleService->getStatusTime();

        $ruleServiceObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $ruleServiceObject);
        $this->compareArrayAndObjectRuleService($expression, $ruleServiceObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $ruleService = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\NullRuleService', $ruleService);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $ruleService = \BaseSdk\Rule\Utils\MockFactory::generateRuleService(1);

        $expression = $this->translator->objectToArray($ruleService);

        $this->compareArrayAndObjectRuleService($expression, $ruleService);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'userGroup'=>['data'=>'mockUserGroup'],
            'transformationTemplate'=>['data'=>'mockTransformationTemplate'],
            'sourceTemplate'=>['data'=>'mockSourceTemplate']
        ];

        $translator = $this->getMockBuilder(RuleServiceRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getTemplateRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $userGroupInfo = ['mockUserGroup'];
        $transformationTemplateInfo = ['mockTransformationTemplate'];
        $sourceTemplateInfo = ['mockSourceTemplate'];

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);
        $userGroup = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['userGroup']['data']],
                 [$relationships['transformationTemplate']['data']],
                 [$relationships['sourceTemplate']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $userGroupInfo,
                $transformationTemplateInfo,
                $sourceTemplateInfo
            ));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $templateRestfulTranslator = $this->prophesize(IRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($transformationTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);
        $templateRestfulTranslator->arrayToObject(Argument::exact($sourceTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplate);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(2))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());

        //揭示
        $ruleService = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $ruleService);
        $this->assertEquals($gbTemplate, $ruleService->getTransformationTemplate());
        $this->assertEquals($wbjTemplate, $ruleService->getSourceTemplate());
        $this->assertEquals($crew, $ruleService->getCrew());
        $this->assertEquals($userGroup, $ruleService->getUserGroup());
    }
}
