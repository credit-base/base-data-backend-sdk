<?php
namespace BaseSdk\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Rule\Utils\RuleServiceRestfulUtils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Translator\TranslatorFactory;
use BaseSdk\Template\Translator\WbjTemplateRestfulTranslator;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class UnAuditedRuleServiceRestfulTranslatorTest extends TestCase
{
    use RuleServiceRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedRuleServiceRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testCorrectInstanceExtendsRuleServiceRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Translator\RuleServiceRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditedRuleService = \BaseSdk\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression['data']['id'] = $unAuditedRuleService->getId();
        $expression['data']['attributes']['transformationCategory'] =
            $unAuditedRuleService->getTransformationCategory();
        $expression['data']['attributes']['sourceCategory'] = $unAuditedRuleService->getSourceCategory();
        $expression['data']['attributes']['rules'] = $unAuditedRuleService->getRules();
        $expression['data']['attributes']['version'] = $unAuditedRuleService->getVersion();
        $expression['data']['attributes']['dataTotal'] = $unAuditedRuleService->getDataTotal();
        $expression['data']['attributes']['operationType'] = $unAuditedRuleService->getOperationType();
        $expression['data']['attributes']['relationId'] = $unAuditedRuleService->getRelationId();
        $expression['data']['attributes']['applyStatus'] = $unAuditedRuleService->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditedRuleService->getRejectReason();
        $expression['data']['attributes']['createTime'] = $unAuditedRuleService->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditedRuleService->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditedRuleService->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditedRuleService->getStatusTime();

        $unAuditedRuleServiceObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\UnAuditedRuleService', $unAuditedRuleServiceObject);
        $this->compareArrayAndObjectUnAuditedRuleService($expression, $unAuditedRuleServiceObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditedRuleService = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\NullUnAuditedRuleService', $unAuditedRuleService);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $unAuditedRuleService = \BaseSdk\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression = $this->translator->objectToArray($unAuditedRuleService);

        $this->compareArrayAndObjectUnAuditedRuleService($expression, $unAuditedRuleService);
    }

    public function testObjectToArrayKeys()
    {
        $keys = array('rejectReason');
        $unAuditedRuleService = \BaseSdk\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression = $this->translator->objectToArray($unAuditedRuleService, $keys);

        $this->assertEquals(
            $expression['data']['attributes']['rejectReason'],
            $unAuditedRuleService->getRejectReason()
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'userGroup'=>['data'=>'mockUserGroup'],
            'transformationTemplate'=>['data'=>'mockTransformationTemplate'],
            'sourceTemplate'=>['data'=>'mockSourceTemplate'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
        ];

        $translator = $this->getMockBuilder(UnAuditedRuleServiceRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getTemplateRestfulTranslator',
                    ])->getMock();

        $crewInfo = ['mockCrew'];
        $applyCrewInfo = ['mockApplyCrew'];
        $userGroupInfo = ['mockUserGroup'];
        $transformationTemplateInfo = ['mockTransformationTemplate'];
        $sourceTemplateInfo = ['mockSourceTemplate'];

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);
        $userGroup = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with($expression['included'], $expression['data']['relationships'])
            ->willReturn($relationships);

        $translator->expects($this->exactly(5))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['userGroup']['data']],
                 [$relationships['transformationTemplate']['data']],
                 [$relationships['sourceTemplate']['data']],
                 [$relationships['applyCrew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $userGroupInfo,
                $transformationTemplateInfo,
                $sourceTemplateInfo,
                $applyCrewInfo
            ));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))->shouldBeCalledTimes(1)->willReturn($crew);
        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $templateRestfulTranslator = $this->prophesize(IRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($transformationTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);
        $templateRestfulTranslator->arrayToObject(Argument::exact($sourceTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplate);
        //绑定
        $translator->expects($this->exactly(2))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(2))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());

        //揭示
        $unAuditedRuleService = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Rule\Model\UnAuditedRuleService', $unAuditedRuleService);
        $this->assertEquals($gbTemplate, $unAuditedRuleService->getTransformationTemplate());
        $this->assertEquals($wbjTemplate, $unAuditedRuleService->getSourceTemplate());
        $this->assertEquals($crew, $unAuditedRuleService->getCrew());
        $this->assertEquals($crew, $unAuditedRuleService->getApplyCrew());
        $this->assertEquals($userGroup, $unAuditedRuleService->getUserGroup());
    }
}
