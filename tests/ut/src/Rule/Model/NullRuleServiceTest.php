<?php
namespace BaseSdk\Rule\Model;

use PHPUnit\Framework\TestCase;

class NullRuleServiceTest extends TestCase
{
    private $ruleService;

    public function setUp()
    {
        $this->ruleService = $this->getMockBuilder(NullRuleService::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->ruleService);
    }

    public function testExtendsRuleService()
    {
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $this->ruleService);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->ruleService);
    }
}
