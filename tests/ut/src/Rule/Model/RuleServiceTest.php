<?php
namespace BaseSdk\Rule\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceTest extends TestCase
{
    private $stub;

    private $ruleService;

    public function setUp()
    {
        $this->ruleService = \BaseSdk\Rule\Utils\MockFactory::generateRuleService(1);
        $this->stub = new RuleService(
            $this->ruleService->getId(),
            $this->ruleService->getSourceCategory(),
            $this->ruleService->getSourceTemplate(),
            $this->ruleService->getTransformationCategory(),
            $this->ruleService->getTransformationTemplate(),
            $this->ruleService->getRules(),
            $this->ruleService->getVersion(),
            $this->ruleService->getDataTotal(),
            $this->ruleService->getCrew(),
            $this->ruleService->getUserGroup(),
            $this->ruleService->getStatus(),
            $this->ruleService->getStatusTime(),
            $this->ruleService->getCreateTime(),
            $this->ruleService->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->ruleService);
    }

    public function testGetId()
    {
        $this->assertEquals($this->ruleService->getId(), $this->stub->getId());
    }

    public function testGetSourceCategory()
    {
        $this->assertEquals($this->ruleService->getSourceCategory(), $this->stub->getSourceCategory());
    }

    public function testGetSourceTemplate()
    {
        $this->assertEquals($this->ruleService->getSourceTemplate(), $this->stub->getSourceTemplate());
    }

    public function testGetTransformationCategory()
    {
        $this->assertEquals(
            $this->ruleService->getTransformationCategory(),
            $this->stub->getTransformationCategory()
        );
    }

    public function testGetTransformationTemplate()
    {
        $this->assertEquals(
            $this->ruleService->getTransformationTemplate(),
            $this->stub->getTransformationTemplate()
        );
    }

    public function testGetRules()
    {
        $this->assertEquals($this->ruleService->getRules(), $this->stub->getRules());
    }

    public function testGetVersion()
    {
        $this->assertEquals($this->ruleService->getVersion(), $this->stub->getVersion());
    }

    public function testGetDataTotal()
    {
        $this->assertEquals($this->ruleService->getDataTotal(), $this->stub->getDataTotal());
    }

    public function testGetCrew()
    {
        $this->assertEquals($this->ruleService->getCrew(), $this->stub->getCrew());
    }

    public function testGetUserGroup()
    {
        $this->assertEquals($this->ruleService->getUserGroup(), $this->stub->getUserGroup());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->ruleService->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->ruleService->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->ruleService->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->ruleService->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
