<?php
namespace BaseSdk\Rule\Model;

use PHPUnit\Framework\TestCase;

class NullUnAuditedRuleServiceTest extends TestCase
{
    private $unAuditedRuleService;

    public function setUp()
    {
        $this->unAuditedRuleService = $this->getMockBuilder(NullUnAuditedRuleService::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditedRuleService);
    }

    public function testExtendsUnAuditedRuleService()
    {
        $this->assertInstanceof('BaseSdk\Rule\Model\UnAuditedRuleService', $this->unAuditedRuleService);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->unAuditedRuleService);
    }
}
