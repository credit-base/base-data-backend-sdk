<?php
namespace BaseSdk\Rule\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceTest extends TestCase
{
    private $stub;

    private $unAuditedRuleService;

    public function setUp()
    {
        $this->unAuditedRuleService = \BaseSdk\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);
        $this->stub = new UnAuditedRuleService(
            $this->unAuditedRuleService->getId(),
            $this->unAuditedRuleService->getSourceCategory(),
            $this->unAuditedRuleService->getSourceTemplate(),
            $this->unAuditedRuleService->getTransformationCategory(),
            $this->unAuditedRuleService->getTransformationTemplate(),
            $this->unAuditedRuleService->getRules(),
            $this->unAuditedRuleService->getVersion(),
            $this->unAuditedRuleService->getDataTotal(),
            $this->unAuditedRuleService->getCrew(),
            $this->unAuditedRuleService->getUserGroup(),
            $this->unAuditedRuleService->getApplyCrew(),
            $this->unAuditedRuleService->getOperationType(),
            $this->unAuditedRuleService->getRelationId(),
            $this->unAuditedRuleService->getApplyStatus(),
            $this->unAuditedRuleService->getRejectReason(),
            $this->unAuditedRuleService->getStatus(),
            $this->unAuditedRuleService->getStatusTime(),
            $this->unAuditedRuleService->getCreateTime(),
            $this->unAuditedRuleService->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->unAuditedRuleService);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $this->unAuditedRuleService);
    }

    public function testGetApplyCrew()
    {
        $this->assertEquals($this->unAuditedRuleService->getApplyCrew(), $this->stub->getApplyCrew());
    }

    public function testGetOperationType()
    {
        $this->assertEquals($this->unAuditedRuleService->getOperationType(), $this->stub->getOperationType());
    }

    public function testGetRelationId()
    {
        $this->assertEquals($this->unAuditedRuleService->getRelationId(), $this->stub->getRelationId());
    }

    public function testGetApplyStatus()
    {
        $this->assertEquals($this->unAuditedRuleService->getApplyStatus(), $this->stub->getApplyStatus());
    }

    public function testGetRejectReason()
    {
        $this->assertEquals($this->unAuditedRuleService->getRejectReason(), $this->stub->getRejectReason());
    }
}
