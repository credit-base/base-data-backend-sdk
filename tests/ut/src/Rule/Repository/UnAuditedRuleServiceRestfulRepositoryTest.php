<?php
namespace BaseSdk\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Rule\Utils\MockFactory;
use BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use BaseSdk\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceRestfulAdapter;

class UnAuditedRuleServiceRestfulRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedRuleServiceRestfulRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIUnAuditedRuleServiceAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->repository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->repository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UnAuditedRuleServiceRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testResubmit()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->resubmit(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->resubmit($unAuditedRuleService, $keys);
    }

    public function testApprove()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->approve(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->approve($unAuditedRuleService, $keys);
    }

    public function testReject()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->reject(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->reject($unAuditedRuleService, $keys);
    }

    public function testRevoke()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->revoke(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($unAuditedRuleService, $keys);
    }
}
