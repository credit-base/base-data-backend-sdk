<?php
namespace BaseSdk\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Rule\Utils\MockFactory;
use BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter;
use BaseSdk\Rule\Adapter\RuleService\RuleServiceRestfulAdapter;

class RuleServiceRestfulRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(RuleServiceRestfulRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIRuleServiceAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->repository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\RuleServiceRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->repository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseSdk\Rule\Adapter\RuleService\RuleServiceMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(RuleServiceRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $ruleService = MockFactory::generateRuleService(1);
        
        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->add(Argument::exact($ruleService))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($ruleService);
    }

    public function testEdit()
    {
        $ruleService = MockFactory::generateRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->edit(Argument::exact($ruleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($ruleService, $keys);
    }

    public function testDeletes()
    {
        $ruleService = MockFactory::generateRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->deletes(Argument::exact($ruleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($ruleService, $keys);
    }
}
