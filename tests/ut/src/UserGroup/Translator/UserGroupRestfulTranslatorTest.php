<?php
namespace BaseSdk\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\UserGroup\Model\NullUserGroup;
use BaseSdk\UserGroup\Utils\UserGroupRestfulUtils;

class UserGroupRestfulTranslatorTest extends TestCase
{
    use UserGroupRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $userGroup = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $expression['data']['id'] = $userGroup->getId();

        $userGroupObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\UserGroup\Model\UserGroup', $userGroupObject);
        $this->compareArrayAndObject($expression, $userGroupObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $userGroup = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\UserGroup\Model\NullUserGroup', $userGroup);
    }

    public function testObjectToArray()
    {
        $userGroup = NullUserGroup::getInstance();

        $expression = $this->translator->objectToArray($userGroup);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
