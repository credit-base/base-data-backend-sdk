<?php
namespace BaseSdk\UserGroup\Model;

use PHPUnit\Framework\TestCase;

class NullUserGroupTest extends TestCase
{
    private $userGroup;

    public function setUp()
    {
        $this->userGroup = $this->getMockBuilder(NullUserGroup::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('BaseSdk\UserGroup\Model\UserGroup', $this->userGroup);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->userGroup);
    }
}
