<?php
namespace BaseSdk\UserGroup\Model;

use PHPUnit\Framework\TestCase;

class UserGroupTest extends TestCase
{
    private $stub;

    private $userGroup;

    public function setUp()
    {
        $this->userGroup = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $this->stub = new UserGroup(
            $this->userGroup->getId()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->userGroup);
    }

    public function testGetId()
    {
        $this->assertEquals($this->userGroup->getId(), $this->stub->getId());
    }
}
