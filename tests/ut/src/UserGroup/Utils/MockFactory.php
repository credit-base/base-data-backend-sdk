<?php
namespace BaseSdk\UserGroup\Utils;

use BaseSdk\UserGroup\Model\UserGroup;

class MockFactory
{
    public static function generateUserGroup(
        int $id = 0
    ) : UserGroup {
        $userGroup = new UserGroup($id);
        return $userGroup;
    }
}
