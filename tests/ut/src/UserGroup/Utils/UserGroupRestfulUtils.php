<?php
namespace BaseSdk\UserGroup\Utils;

trait UserGroupRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $userGroup
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $userGroup->getId());
        }
    }
}
