<?php
namespace BaseSdk\Statistical\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\Statistical\Model\NullStatistical;
use BaseSdk\Statistical\Utils\StatisticalRestfulUtils;

class StatisticalRestfulTranslatorTest extends TestCase
{
    use StatisticalRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new StatisticalRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $statistical = \BaseSdk\Statistical\Utils\MockFactory::generateStatistical(1);

        $expression['data']['id'] = $statistical->getId();
        $expression['data']['attributes']['result'] = $statistical->getResult();

        $statisticalObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Statistical\Model\Statistical', $statisticalObject);
        $this->compareArrayAndObject($expression, $statisticalObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $statistical = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Statistical\Model\NullStatistical', $statistical);
    }

    public function testObjectToArray()
    {
        $statistical = NullStatistical::getInstance();

        $expression = $this->translator->objectToArray($statistical);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
