<?php
namespace BaseSdk\Statistical\Model;

use PHPUnit\Framework\TestCase;

class StatisticalTest extends TestCase
{
    private $stub;

    private $statistical;

    public function setUp()
    {
        $this->statistical = \BaseSdk\Statistical\Utils\MockFactory::generateStatistical(1);

        $this->stub = new Statistical(
            $this->statistical->getId(),
            $this->statistical->getResult()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->statistical);
    }

    public function testGetId()
    {
        $this->assertEquals($this->statistical->getId(), $this->stub->getId());
    }

    public function testGetName()
    {
        $this->assertEquals($this->statistical->getResult(), $this->stub->getResult());
    }
}
