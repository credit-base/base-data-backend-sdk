<?php
namespace BaseSdk\Statistical\Model;

use PHPUnit\Framework\TestCase;

class NullStatisticalTest extends TestCase
{
    private $nullStatistical;

    public function setUp()
    {
        $this->nullStatistical = NullStatistical::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullStatistical);
    }

    public function testExtendsStatistical()
    {
        $this->assertInstanceof('BaseSdk\Statistical\Model\Statistical', $this->nullStatistical);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->nullStatistical);
    }
}
