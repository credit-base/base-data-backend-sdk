<?php
namespace BaseSdk\Statistical\Utils;

trait StatisticalRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $statistical
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $statistical->getId());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['result'],
            $statistical->getResult()
        );
    }
}
