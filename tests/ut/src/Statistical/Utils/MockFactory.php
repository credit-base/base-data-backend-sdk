<?php
namespace BaseSdk\Statistical\Utils;

use BaseSdk\Statistical\Model\Statistical;

class MockFactory
{
    public static function generateStatistical(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Statistical {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $result = self::generateResult($faker, $value);

        $statistical = new Statistical($id, $result);

        return $statistical;
    }

    private static function generateResult($faker, $value) : array
    {
        return  isset($value['result']) ? $value['result'] : $faker->words(3, false);
    }
}
