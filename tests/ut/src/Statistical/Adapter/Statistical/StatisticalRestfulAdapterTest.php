<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use PHPUnit\Framework\TestCase;

class StatisticalRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockStatisticalRestfulAdapter::class)
                                ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = $this->getMockBuilder(StatisticalRestfulAdapter::class)
                                ->getMockForAbstractClass();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIStatisticalAdapter()
    {
        $adapter = $this->getMockBuilder(StatisticalRestfulAdapter::class)
                                ->getMockForAbstractClass();

        $this->assertInstanceOf(
            'BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Statistical\Translator\StatisticalRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('statisticals', $this->adapter->getResource());
    }
}
