<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use PHPUnit\Framework\TestCase;

use BaseSdk\Statistical\Model\NullStatistical;

class EnterpriseRelationInformationCountRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new EnterpriseRelationInformationCountRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsStatisticalRestfulAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Statistical\Adapter\Statistical\StatisticalRestfulAdapter',
            $this->adapter
        );
    }

    public function testAnalysePublic()
    {
        $filter = array('filter');
        $resources = 'resources';
        $statistical = \BaseSdk\Statistical\Utils\MockFactory::generateStatistical(1);

        $adapter = $this->getMockBuilder(EnterpriseRelationInformationCountRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn($resources);
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(true);
        $adapter->expects($this->exactly(1))->method('translateToObject')->willReturn($statistical);
             
        $result = $adapter->analyse($filter);
        $this->assertEquals($statistical, $result);
    }

    public function testAnalysePublicFail()
    {
        $filter = array('filter');
        $resources = 'resources';
        $nullStatistical = NullStatistical::getInstance();

        $adapter = $this->getMockBuilder(EnterpriseRelationInformationCountRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn($resources);
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(false);
        
        $result = $adapter->analyse($filter);
        $this->assertEquals($nullStatistical, $result);
    }
}
