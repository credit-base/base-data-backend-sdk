<?php
namespace BaseSdk\Template\Utils;

trait WbjTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $wbjTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $wbjTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $wbjTemplate->getIdentify());
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $wbjTemplate->getCategory());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $wbjTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $wbjTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $wbjTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $wbjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $wbjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $wbjTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $wbjTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $wbjTemplate->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $wbjTemplate->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $wbjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $wbjTemplate->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['type'],
                'userGroups'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['id'],
                $wbjTemplate->getSourceUnit()->getId()
            );
        }
    }
}
