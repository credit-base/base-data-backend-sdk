<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Model\BjTemplate;
use BaseSdk\Template\Model\GbTemplate;
use BaseSdk\UserGroup\Model\UserGroup;

class BjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateBjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $identify = self::generateIdentify($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $description = self::generateDescription($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $exchangeFrequency = self::generateExchangeFrequency($faker, $value);
        $sourceUnit = self::generateSourceUnit($faker, $value);
        $gbTemplate = self::generateGbTemplate($faker, $value);
        $status = self::generateStatus($faker, $value);
        $items = self::generateItems($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $bjTemplate = new BjTemplate(
            $id,
            $name,
            $identify,
            Template::CATEGORY['BJ'],
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $gbTemplate,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $bjTemplate;
    }

    private static function generateSourceUnit($faker, $value) : UserGroup
    {
        return isset($value['sourceUnit']) ?
                    $value['sourceUnit'] :
                    \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
    }

    private static function generateGbTemplate($faker, $value) : GbTemplate
    {
        return isset($value['gbTemplate']) ?
                    $value['gbTemplate'] :
                    GbTemplateMockFactory::generateGbTemplate($faker->randomDigit());
    }
}
