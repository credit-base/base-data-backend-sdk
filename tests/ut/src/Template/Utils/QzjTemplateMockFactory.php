<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\QzjTemplate;
use BaseSdk\UserGroup\Model\UserGroup;

class QzjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateQzjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : QzjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $items = self::generateItems($faker, $value);
        $identify = self::generateIdentify($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $description = self::generateDescription($faker, $value);
        $sourceUnit = self::generateSourceUnit($faker, $value);
        $exchangeFrequency = self::generateExchangeFrequency($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $category = self::generateCategory($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $qzjTemplate = new QzjTemplate(
            $id,
            $name,
            $identify,
            $category,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $qzjTemplate;
    }

    private static function generateSourceUnit($faker, $value) : UserGroup
    {
        return isset($value['sourceUnit']) ?
                    $value['sourceUnit'] :
                    \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
    }

    private static function generateCategory($faker, $value) : int
    {
        return isset($value['category']) ? $value['category'] : $faker->randomDigit();
    }
}
