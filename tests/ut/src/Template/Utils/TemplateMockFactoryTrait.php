<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\Template;

trait TemplateMockFactoryTrait
{
    protected static function generateName($faker, $value) : string
    {
        return isset($value['name']) ? $value['name'] : $faker->word;
    }

    protected static function generateIdentify($faker, $value) : string
    {
        return isset($value['identify']) ? $value['identify'] : $faker->regexify('[A-Z_]{1,100}');
    }

    protected static function generateSubjectCategory($faker, $value) : array
    {
        return isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            $faker->randomElements(array(1,2,3), 2);
    }

    protected static function generateDimension($faker, $value) : int
    {
        return isset($value['dimension']) ? $value['dimension'] : $faker->randomElement(array(1,2,3));
    }

    protected static function generateExchangeFrequency($faker, $value) : int
    {
        return isset($value['exchangeFrequency']) ? $value['exchangeFrequency'] : $faker->randomDigit();
    }

    protected static function generateInfoClassify($faker, $value) : int
    {
        return isset($value['infoClassify']) ? $value['infoClassify'] : $faker->randomDigit();
    }

    protected static function generateInfoCategory($faker, $value) : int
    {
        return isset($value['infoCategory']) ? $value['infoCategory'] : $faker->randomDigit();
    }

    protected static function generateDescription($faker, $value) : string
    {
        return isset($value['description']) ? $value['description'] : $faker->sentence();
    }

    protected static function generateItems($faker, $value) : array
    {
        return isset($value['items']) ?
            $value['items'] :
            $faker->randomElements(array('1','2','3'), 2);
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ? $value['status'] : $faker->randomDigit();
    }
}
