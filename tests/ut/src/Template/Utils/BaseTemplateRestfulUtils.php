<?php
namespace BaseSdk\Template\Utils;

trait BaseTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $baseTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $baseTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $baseTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $baseTemplate->getIdentify());
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $baseTemplate->getCategory());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $baseTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $baseTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $baseTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $baseTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $baseTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $baseTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $baseTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $baseTemplate->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $baseTemplate->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $baseTemplate->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $baseTemplate->getUpdateTime());
    }
}
