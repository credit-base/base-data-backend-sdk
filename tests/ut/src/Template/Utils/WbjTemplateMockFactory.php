<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Model\WbjTemplate;

use BaseSdk\UserGroup\Model\UserGroup;

class WbjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateWbjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $identify = self::generateIdentify($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $exchangeFrequency = self::generateExchangeFrequency($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $description = self::generateDescription($faker, $value);
        $items = self::generateItems($faker, $value);
        $sourceUnit = self::generateSourceUnit($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $wbjTemplate = new WbjTemplate(
            $id,
            $name,
            $identify,
            Template::CATEGORY['WBJ'],
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $wbjTemplate;
    }

    private static function generateSourceUnit($faker, $value) : UserGroup
    {
        return isset($value['sourceUnit']) ?
                    $value['sourceUnit'] :
                    \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
    }
}
