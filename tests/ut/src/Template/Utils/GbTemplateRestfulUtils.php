<?php
namespace BaseSdk\Template\Utils;

trait GbTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $gbTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $gbTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $gbTemplate->getIdentify());
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $gbTemplate->getCategory());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $gbTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $gbTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $gbTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $gbTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $gbTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $gbTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $gbTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $gbTemplate->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $gbTemplate->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $gbTemplate->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $gbTemplate->getUpdateTime());
    }
}
