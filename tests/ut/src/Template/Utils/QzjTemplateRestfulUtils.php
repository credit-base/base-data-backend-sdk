<?php
namespace BaseSdk\Template\Utils;

trait QzjTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $qzjTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $qzjTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $qzjTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $qzjTemplate->getIdentify());
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $qzjTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $qzjTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $qzjTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $qzjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $qzjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $qzjTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['category'], $qzjTemplate->getCategory());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $qzjTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $qzjTemplate->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $qzjTemplate->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $qzjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $qzjTemplate->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['type'],
                'userGroups'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['id'],
                $qzjTemplate->getSourceUnit()->getId()
            );
        }
    }
}
