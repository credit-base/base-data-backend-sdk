<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Model\GbTemplate;

class GbTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateGbTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $identify = self::generateIdentify($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $exchangeFrequency = self::generateExchangeFrequency($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $description = self::generateDescription($faker, $value);
        $items = self::generateItems($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $gbTemplate = new GbTemplate(
            $id,
            $name,
            $identify,
            Template::CATEGORY['GB'],
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $gbTemplate;
    }
}
