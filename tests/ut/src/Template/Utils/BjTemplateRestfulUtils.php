<?php
namespace BaseSdk\Template\Utils;

trait BjTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $bjTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $bjTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $bjTemplate->getIdentify());
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $bjTemplate->getCategory());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $bjTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $bjTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $bjTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $bjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $bjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $bjTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $bjTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $bjTemplate->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $bjTemplate->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $bjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $bjTemplate->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['type'],
                'userGroups'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['sourceUnit']['data'][0]['id'],
                $bjTemplate->getSourceUnit()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['gbTemplate']['data'][0]['type'],
                'gbTemplates'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['gbTemplate']['data'][0]['id'],
                $bjTemplate->getGbTemplate()->getId()
            );
        }
    }
}
