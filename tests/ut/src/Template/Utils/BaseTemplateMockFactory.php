<?php
namespace BaseSdk\Template\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Model\BaseTemplate;

class BaseTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateBaseTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BaseTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $identify = self::generateIdentify($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $description = self::generateDescription($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $exchangeFrequency = self::generateExchangeFrequency($faker, $value);
        $items = self::generateItems($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        $baseTemplate = new BaseTemplate(
            $id,
            $name,
            $identify,
            Template::CATEGORY['BASE'],
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $baseTemplate;
    }
}
