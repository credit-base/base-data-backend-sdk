<?php
namespace BaseSdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\WbjTemplateRestfulUtils;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class WbjTemplateRestfulTranslatorTest extends TestCase
{
    use WbjTemplateRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new WbjTemplateRestfulTranslator();
        $this->childTranslator = new class extends WbjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $expression['data']['id'] = $wbjTemplate->getId();
        $expression['data']['attributes']['name'] = $wbjTemplate->getName();
        $expression['data']['attributes']['identify'] = $wbjTemplate->getIdentify();
        $expression['data']['attributes']['category'] = $wbjTemplate->getCategory();
        $expression['data']['attributes']['subjectCategory'] = $wbjTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $wbjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $wbjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $wbjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $wbjTemplate->getInfoCategory();
        $expression['data']['attributes']['description'] = $wbjTemplate->getDescription();
        $expression['data']['attributes']['items'] = $wbjTemplate->getItems();
        $expression['data']['attributes']['createTime'] = $wbjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $wbjTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $wbjTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $wbjTemplate->getStatusTime();

        $wbjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\WbjTemplate', $wbjTemplateObject);
        $this->compareArrayAndObject($expression, $wbjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $wbjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\NullWbjTemplate', $wbjTemplate);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $expression = $this->translator->objectToArray($wbjTemplate);

        $this->compareArrayAndObject($expression, $wbjTemplate);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $translator = $this->getMockBuilder(WbjTemplateRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $sourceUnitInfo = ['mockSourceUnit'];

        $sourceUnit = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->once())
            ->method('changeArrayFormat')
            ->with($relationships['sourceUnit']['data'])
            ->willReturn($sourceUnitInfo);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        //揭示
        $wbjTemplate = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\WbjTemplate', $wbjTemplate);
        $this->assertEquals($sourceUnit, $wbjTemplate->getSourceUnit());
    }
}
