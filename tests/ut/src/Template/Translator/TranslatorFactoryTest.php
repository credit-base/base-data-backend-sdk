<?php
namespace BaseSdk\Template\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class TranslatorFactoryTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new TranslatorFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->translator);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullRestfulTranslator()
    {
        $translator = $this->translator->getTranslator(0);
            $this->assertInstanceOf(
                'BaseSdk\Common\Translator\NullRestfulTranslator',
                $translator
            );
    }

    public function testGetController()
    {
        foreach (TranslatorFactory::MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTranslator($key)
            );
        }
    }
}
