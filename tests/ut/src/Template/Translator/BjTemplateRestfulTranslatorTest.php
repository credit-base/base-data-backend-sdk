<?php
namespace BaseSdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\BjTemplateRestfulUtils;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class BjTemplateRestfulTranslatorTest extends TestCase
{
    use BjTemplateRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new BjTemplateRestfulTranslator();
        $this->childTranslator = new class extends BjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getGbTemplateRestfulTranslator() : GbTemplateRestfulTranslator
            {
                return parent::getGbTemplateRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetGbTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\GbTemplateRestfulTranslator',
            $this->childTranslator->getGbTemplateRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);

        $expression['data']['id'] = $bjTemplate->getId();
        $expression['data']['attributes']['name'] = $bjTemplate->getName();
        $expression['data']['attributes']['identify'] = $bjTemplate->getIdentify();
        $expression['data']['attributes']['category'] = $bjTemplate->getCategory();
        $expression['data']['attributes']['subjectCategory'] = $bjTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $bjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $bjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $bjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $bjTemplate->getInfoCategory();
        $expression['data']['attributes']['description'] = $bjTemplate->getDescription();
        $expression['data']['attributes']['items'] = $bjTemplate->getItems();
        $expression['data']['attributes']['createTime'] = $bjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $bjTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $bjTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $bjTemplate->getStatusTime();

        $bjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\BjTemplate', $bjTemplateObject);
        $this->compareArrayAndObject($expression, $bjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $bjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\NullBjTemplate', $bjTemplate);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);

        $expression = $this->translator->objectToArray($bjTemplate);

        $this->compareArrayAndObject($expression, $bjTemplate);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'gbTemplate'=>['data'=>'mockGbTemplate'],
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $translator = $this->getMockBuilder(BjTemplateRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getGbTemplateRestfulTranslator',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $gbTemplateInfo = ['mockGbTemplate'];
        $sourceUnitInfo = ['mockSourceUnit'];

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $sourceUnit = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 gbTemplate, sourceUnit
        //依次返回 $gbTemplateInfo 和 $sourceUnitInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['sourceUnit']['data']],
                 [$relationships['gbTemplate']['data']]
             )
            ->will($this->onConsecutiveCalls($sourceUnitInfo, $gbTemplateInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $gbTemplateRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);
        $gbTemplateRestfulTranslator->arrayToObject(Argument::exact($gbTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getGbTemplateRestfulTranslator')
            ->willReturn($gbTemplateRestfulTranslator->reveal());

        //揭示
        $bjTemplate = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\BjTemplate', $bjTemplate);
        $this->assertEquals($gbTemplate, $bjTemplate->getGbTemplate());
        $this->assertEquals($sourceUnit, $bjTemplate->getSourceUnit());
    }
}
