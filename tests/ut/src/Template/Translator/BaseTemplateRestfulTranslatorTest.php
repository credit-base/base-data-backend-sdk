<?php
namespace BaseSdk\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\BaseTemplateRestfulUtils;

class BaseTemplateRestfulTranslatorTest extends TestCase
{
    use BaseTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BaseTemplateRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $baseTemplate = \BaseSdk\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);

        $expression['data']['id'] = $baseTemplate->getId();
        $expression['data']['attributes']['name'] = $baseTemplate->getName();
        $expression['data']['attributes']['identify'] = $baseTemplate->getIdentify();
        $expression['data']['attributes']['category'] = $baseTemplate->getCategory();
        $expression['data']['attributes']['subjectCategory'] = $baseTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $baseTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $baseTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $baseTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $baseTemplate->getInfoCategory();
        $expression['data']['attributes']['description'] = $baseTemplate->getDescription();
        $expression['data']['attributes']['items'] = $baseTemplate->getItems();
        $expression['data']['attributes']['createTime'] = $baseTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $baseTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $baseTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $baseTemplate->getStatusTime();

        $baseTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\BaseTemplate', $baseTemplateObject);
        $this->compareArrayAndObject($expression, $baseTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $baseTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\NullBaseTemplate', $baseTemplate);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $baseTemplate = \BaseSdk\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);

        $expression = $this->translator->objectToArray($baseTemplate);

        $this->compareArrayAndObject($expression, $baseTemplate);
    }
}
