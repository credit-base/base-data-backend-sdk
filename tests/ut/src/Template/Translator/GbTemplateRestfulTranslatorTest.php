<?php
namespace BaseSdk\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\GbTemplateRestfulUtils;

class GbTemplateRestfulTranslatorTest extends TestCase
{
    use GbTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbTemplateRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);

        $expression['data']['id'] = $gbTemplate->getId();
        $expression['data']['attributes']['name'] = $gbTemplate->getName();
        $expression['data']['attributes']['identify'] = $gbTemplate->getIdentify();
        $expression['data']['attributes']['category'] = $gbTemplate->getCategory();
        $expression['data']['attributes']['subjectCategory'] = $gbTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $gbTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $gbTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $gbTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $gbTemplate->getInfoCategory();
        $expression['data']['attributes']['description'] = $gbTemplate->getDescription();
        $expression['data']['attributes']['items'] = $gbTemplate->getItems();
        $expression['data']['attributes']['createTime'] = $gbTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $gbTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $gbTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $gbTemplate->getStatusTime();

        $gbTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\GbTemplate', $gbTemplateObject);
        $this->compareArrayAndObject($expression, $gbTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $gbTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\NullGbTemplate', $gbTemplate);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);

        $expression = $this->translator->objectToArray($gbTemplate);

        $this->compareArrayAndObject($expression, $gbTemplate);
    }
}
