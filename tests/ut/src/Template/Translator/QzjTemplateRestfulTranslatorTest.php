<?php
namespace BaseSdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\QzjTemplateRestfulUtils;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class QzjTemplateRestfulTranslatorTest extends TestCase
{
    use QzjTemplateRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new QzjTemplateRestfulTranslator();
        $this->childTranslator = new class extends QzjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $qzjTemplate = \BaseSdk\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);

        $expression['data']['id'] = $qzjTemplate->getId();
        $expression['data']['attributes']['name'] = $qzjTemplate->getName();
        $expression['data']['attributes']['identify'] = $qzjTemplate->getIdentify();
        $expression['data']['attributes']['subjectCategory'] = $qzjTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $qzjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $qzjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $qzjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $qzjTemplate->getInfoCategory();
        $expression['data']['attributes']['description'] = $qzjTemplate->getDescription();
        $expression['data']['attributes']['items'] = $qzjTemplate->getItems();
        $expression['data']['attributes']['category'] = $qzjTemplate->getCategory();
        $expression['data']['attributes']['createTime'] = $qzjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $qzjTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $qzjTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $qzjTemplate->getStatusTime();

        $qzjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\QzjTemplate', $qzjTemplateObject);
        $this->compareArrayAndObject($expression, $qzjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $qzjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\NullQzjTemplate', $qzjTemplate);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
    
    public function testObjectToArray()
    {
        $qzjTemplate = \BaseSdk\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);

        $expression = $this->translator->objectToArray($qzjTemplate);

        $this->compareArrayAndObject($expression, $qzjTemplate);
    }

    public function testArrayToObjectWithIncluded()
    {
        $qzjTranslator = $this->getMockBuilder(QzjTemplateRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getUserGroupRestfulTranslator',
                    ])
                    ->getMock();
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $sourceUnitInfo = ['mockSourceUnit'];

        $sourceUnit = \BaseSdk\UserGroup\Utils\MockFactory::generateUserGroup(1);

        //预言
        $qzjTranslator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $qzjTranslator->expects($this->once())
            ->method('changeArrayFormat')
            ->with($relationships['sourceUnit']['data'])
            ->willReturn($sourceUnitInfo);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        //绑定
        $qzjTranslator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        //揭示
        $qzjTemplate = $qzjTranslator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Template\Model\QzjTemplate', $qzjTemplate);
        $this->assertEquals($sourceUnit, $qzjTemplate->getSourceUnit());
    }
}
