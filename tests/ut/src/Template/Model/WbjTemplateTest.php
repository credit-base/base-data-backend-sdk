<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjTemplateTest extends TestCase
{
    private $stub;

    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $this->stub = new WbjTemplate(
            $this->wbjTemplate->getId(),
            $this->wbjTemplate->getName(),
            $this->wbjTemplate->getIdentify(),
            $this->wbjTemplate->getCategory(),
            $this->wbjTemplate->getSubjectCategory(),
            $this->wbjTemplate->getDimension(),
            $this->wbjTemplate->getExchangeFrequency(),
            $this->wbjTemplate->getInfoClassify(),
            $this->wbjTemplate->getInfoCategory(),
            $this->wbjTemplate->getDescription(),
            $this->wbjTemplate->getItems(),
            $this->wbjTemplate->getSourceUnit(),
            $this->wbjTemplate->getStatus(),
            $this->wbjTemplate->getStatusTime(),
            $this->wbjTemplate->getCreateTime(),
            $this->wbjTemplate->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->wbjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->wbjTemplate);
    }

    public function testGetSourceUnit()
    {
        $this->assertEquals($this->wbjTemplate->getSourceUnit(), $this->stub->getSourceUnit());
    }
}
