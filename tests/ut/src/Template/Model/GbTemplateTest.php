<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbTemplateTest extends TestCase
{
    private $stub;

    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $this->stub = new GbTemplate(
            $this->gbTemplate->getId(),
            $this->gbTemplate->getName(),
            $this->gbTemplate->getIdentify(),
            $this->gbTemplate->getCategory(),
            $this->gbTemplate->getSubjectCategory(),
            $this->gbTemplate->getDimension(),
            $this->gbTemplate->getExchangeFrequency(),
            $this->gbTemplate->getInfoClassify(),
            $this->gbTemplate->getInfoCategory(),
            $this->gbTemplate->getDescription(),
            $this->gbTemplate->getItems(),
            $this->gbTemplate->getStatus(),
            $this->gbTemplate->getStatusTime(),
            $this->gbTemplate->getCreateTime(),
            $this->gbTemplate->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->gbTemplate);
    }
}
