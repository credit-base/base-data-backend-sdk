<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class QzjTemplateTest extends TestCase
{
    private $stub;

    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = \BaseSdk\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);
        $this->stub = new QzjTemplate(
            $this->qzjTemplate->getId(),
            $this->qzjTemplate->getName(),
            $this->qzjTemplate->getIdentify(),
            $this->qzjTemplate->getCategory(),
            $this->qzjTemplate->getSubjectCategory(),
            $this->qzjTemplate->getDimension(),
            $this->qzjTemplate->getExchangeFrequency(),
            $this->qzjTemplate->getInfoClassify(),
            $this->qzjTemplate->getInfoCategory(),
            $this->qzjTemplate->getDescription(),
            $this->qzjTemplate->getItems(),
            $this->qzjTemplate->getSourceUnit(),
            $this->qzjTemplate->getStatus(),
            $this->qzjTemplate->getStatusTime(),
            $this->qzjTemplate->getCreateTime(),
            $this->qzjTemplate->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->qzjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->qzjTemplate);
    }

    public function testGetSourceUnit()
    {
        $this->assertEquals($this->qzjTemplate->getSourceUnit(), $this->stub->getSourceUnit());
    }
}
