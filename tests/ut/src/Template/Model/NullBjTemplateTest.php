<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullBjTemplateTest extends TestCase
{
    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplate = $this->getMockBuilder(NullBjTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->bjTemplate);
    }

    public function testExtendsBjTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\BjTemplate', $this->bjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->bjTemplate);
    }
}
