<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullWbjTemplateTest extends TestCase
{
    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplate = $this->getMockBuilder(NullWbjTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
    }

    public function testExtendsWbjTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\WbjTemplate', $this->wbjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->wbjTemplate);
    }
}
