<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BaseTemplateTest extends TestCase
{
    private $stub;

    private $baseTemplate;

    public function setUp()
    {
        $this->baseTemplate = \BaseSdk\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);
        $this->stub = new BaseTemplate(
            $this->baseTemplate->getId(),
            $this->baseTemplate->getName(),
            $this->baseTemplate->getIdentify(),
            $this->baseTemplate->getCategory(),
            $this->baseTemplate->getSubjectCategory(),
            $this->baseTemplate->getDimension(),
            $this->baseTemplate->getExchangeFrequency(),
            $this->baseTemplate->getInfoClassify(),
            $this->baseTemplate->getInfoCategory(),
            $this->baseTemplate->getDescription(),
            $this->baseTemplate->getItems(),
            $this->baseTemplate->getStatus(),
            $this->baseTemplate->getStatusTime(),
            $this->baseTemplate->getCreateTime(),
            $this->baseTemplate->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->baseTemplate);
    }
}
