<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullBaseTemplateTest extends TestCase
{
    private $baseTemplate;

    public function setUp()
    {
        $this->baseTemplate = $this->getMockBuilder(NullBaseTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->baseTemplate);
    }

    public function testExtendsBaseTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\BaseTemplate', $this->baseTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->baseTemplate);
    }
}
