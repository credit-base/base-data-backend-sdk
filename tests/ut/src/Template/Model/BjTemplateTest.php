<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjTemplateTest extends TestCase
{
    private $stub;

    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $this->stub = new BjTemplate(
            $this->bjTemplate->getId(),
            $this->bjTemplate->getName(),
            $this->bjTemplate->getIdentify(),
            $this->bjTemplate->getCategory(),
            $this->bjTemplate->getSubjectCategory(),
            $this->bjTemplate->getDimension(),
            $this->bjTemplate->getExchangeFrequency(),
            $this->bjTemplate->getInfoClassify(),
            $this->bjTemplate->getInfoCategory(),
            $this->bjTemplate->getDescription(),
            $this->bjTemplate->getItems(),
            $this->bjTemplate->getSourceUnit(),
            $this->bjTemplate->getGbTemplate(),
            $this->bjTemplate->getStatus(),
            $this->bjTemplate->getStatusTime(),
            $this->bjTemplate->getCreateTime(),
            $this->bjTemplate->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->bjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->bjTemplate);
    }

    public function testGetSourceUnit()
    {
        $this->assertEquals($this->bjTemplate->getSourceUnit(), $this->stub->getSourceUnit());
    }

    public function testGetGbTemplate()
    {
        $this->assertEquals($this->bjTemplate->getGbTemplate(), $this->stub->getGbTemplate());
    }
}
