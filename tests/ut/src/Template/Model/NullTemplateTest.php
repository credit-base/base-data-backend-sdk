<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullTemplateTest extends TestCase
{
    private $template;

    public function setUp()
    {
        $this->template = NullTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->template);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\Template', $this->template);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->template);
    }
}
