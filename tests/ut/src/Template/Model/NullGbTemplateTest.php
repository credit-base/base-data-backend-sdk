<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullGbTemplateTest extends TestCase
{
    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplate = $this->getMockBuilder(NullGbTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
    }

    public function testExtendsGbTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\GbTemplate', $this->gbTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbTemplate);
    }
}
