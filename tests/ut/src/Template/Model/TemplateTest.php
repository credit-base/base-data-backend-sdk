<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TemplateTest extends TestCase
{
    private $stub;

    private $template;

    public function setUp()
    {
        $this->template = \BaseSdk\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);
        $this->stub = new MockTemplate(
            $this->template->getId(),
            $this->template->getName(),
            $this->template->getIdentify(),
            $this->template->getCategory(),
            $this->template->getSubjectCategory(),
            $this->template->getDimension(),
            $this->template->getExchangeFrequency(),
            $this->template->getInfoClassify(),
            $this->template->getInfoCategory(),
            $this->template->getDescription(),
            $this->template->getItems(),
            $this->template->getStatus(),
            $this->template->getStatusTime(),
            $this->template->getCreateTime(),
            $this->template->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetId()
    {
        $this->assertEquals($this->template->getId(), $this->stub->getId());
    }

    public function testGetName()
    {
        $this->assertEquals($this->template->getName(), $this->stub->getName());
    }

    public function testGetIdentify()
    {
        $this->assertEquals($this->template->getIdentify(), $this->stub->getIdentify());
    }

    public function testGetCategory()
    {
        $this->assertEquals($this->template->getCategory(), $this->stub->getCategory());
    }

    public function testGetSubjectCategory()
    {
        $this->assertEquals($this->template->getSubjectCategory(), $this->stub->getSubjectCategory());
    }

    public function testGetDimension()
    {
        $this->assertEquals($this->template->getDimension(), $this->stub->getDimension());
    }

    public function testGetExchangeFrequency()
    {
        $this->assertEquals($this->template->getExchangeFrequency(), $this->stub->getExchangeFrequency());
    }

    public function testGetInfoClassify()
    {
        $this->assertEquals($this->template->getInfoClassify(), $this->stub->getInfoClassify());
    }

    public function testGetInfoCategory()
    {
        $this->assertEquals($this->template->getInfoCategory(), $this->stub->getInfoCategory());
    }

    public function testGetDescription()
    {
        $this->assertEquals($this->template->getDescription(), $this->stub->getDescription());
    }

    public function testGetItems()
    {
        $this->assertEquals($this->template->getItems(), $this->stub->getItems());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->template->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->template->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->template->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->template->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
