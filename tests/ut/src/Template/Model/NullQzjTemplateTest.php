<?php
namespace BaseSdk\Template\Model;

use PHPUnit\Framework\TestCase;

class NullQzjTemplateTest extends TestCase
{
    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = $this->getMockBuilder(NullQzjTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
    }

    public function testExtendsQzjTemplate()
    {
        $this->assertInstanceof('BaseSdk\Template\Model\QzjTemplate', $this->qzjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->qzjTemplate);
    }
}
