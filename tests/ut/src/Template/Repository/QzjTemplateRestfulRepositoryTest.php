<?php
namespace BaseSdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\QzjTemplateMockFactory;
use BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use BaseSdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter;

class QzjTemplateRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(QzjTemplateRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIQzjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\QzjTemplateMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(QzjTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $id = 1;
        $qzjTemplate = QzjTemplateMockFactory::generateQzjTemplate($id);
        
        $adapter = $this->prophesize(IQzjTemplateAdapter::class);
        $adapter->add(Argument::exact($qzjTemplate))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($qzjTemplate);
    }

    public function testEdit()
    {
        $id = 1;
        $qzjTemplate = QzjTemplateMockFactory::generateQzjTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IQzjTemplateAdapter::class);
        $adapter->edit(Argument::exact($qzjTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($qzjTemplate, $keys);
    }
}
