<?php
namespace BaseSdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\GbTemplateMockFactory;
use BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseSdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter;

class GbTemplateRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(GbTemplateRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIGbTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\GbTemplateMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(GbTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $id = 1;
        $gbTemplate = GbTemplateMockFactory::generateGbTemplate($id);
        
        $adapter = $this->prophesize(IGbTemplateAdapter::class);
        $adapter->add(Argument::exact($gbTemplate))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($gbTemplate);
    }

    public function testEdit()
    {
        $id = 1;
        $gbTemplate = GbTemplateMockFactory::generateGbTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IGbTemplateAdapter::class);
        $adapter->edit(Argument::exact($gbTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($gbTemplate, $keys);
    }
}
