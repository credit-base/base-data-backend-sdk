<?php
namespace BaseSdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\BaseTemplateMockFactory;
use BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use BaseSdk\Template\Adapter\BaseTemplate\BaseTemplateRestfulAdapter;

class BaseTemplateRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BaseTemplateRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIBaseTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\BaseTemplateRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\BaseTemplateMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(BaseTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testEdit()
    {
        $id = 1;
        $baseTemplate = BaseTemplateMockFactory::generateBaseTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IBaseTemplateAdapter::class);
        $adapter->edit(Argument::exact($baseTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($baseTemplate, $keys);
    }
}
