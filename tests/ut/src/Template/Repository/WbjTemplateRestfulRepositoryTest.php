<?php
namespace BaseSdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\WbjTemplateMockFactory;
use BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseSdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter;

class WbjTemplateRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WbjTemplateRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIWbjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\WbjTemplateMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(WbjTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $id = 1;
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate($id);
        
        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->add(Argument::exact($wbjTemplate))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($wbjTemplate);
    }

    public function testEdit()
    {
        $id = 1;
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->edit(Argument::exact($wbjTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($wbjTemplate, $keys);
    }
}
