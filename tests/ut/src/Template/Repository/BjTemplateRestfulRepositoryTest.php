<?php
namespace BaseSdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Utils\BjTemplateMockFactory;
use BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseSdk\Template\Adapter\BjTemplate\BjTemplateRestfulAdapter;

class BjTemplateRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BjTemplateRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIBjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\BjTemplateRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\BjTemplateMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(BjTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAdd()
    {
        $id = 1;
        $bjTemplate = BjTemplateMockFactory::generateBjTemplate($id);
        
        $adapter = $this->prophesize(IBjTemplateAdapter::class);
        $adapter->add(Argument::exact($bjTemplate))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($bjTemplate);
    }

    public function testEdit()
    {
        $id = 1;
        $bjTemplate = BjTemplateMockFactory::generateBjTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IBjTemplateAdapter::class);
        $adapter->edit(Argument::exact($bjTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($bjTemplate, $keys);
    }
}
