<?php
namespace BaseSdk\Template\Adapter\GbTemplate;

use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Model\GbTemplate;

class GbTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new GbTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new GbTemplate()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new GbTemplate(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\GbTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\GbTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\GbTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
