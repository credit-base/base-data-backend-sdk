<?php
namespace BaseSdk\Template\Adapter\GbTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockGbTemplateRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new GbTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIGbTemplateAdapter()
    {
        $adapter = new GbTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\GbTemplateRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('gbTemplates', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'GB_TEMPLATE_LIST',
                GbTemplateRestfulAdapter::SCENARIOS['GB_TEMPLATE_LIST']
            ],
            [
                'GB_TEMPLATE_FETCH_ONE',
                GbTemplateRestfulAdapter::SCENARIOS['GB_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\NullGbTemplate',
            $this->adapter->getNullObject()
        );
    }

    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockGbTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $data = array('data');

        $gbTranslator = $this->prophesize(IRestfulTranslator::class);
        $gbTranslator->objectToArray($gbTemplate)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($gbTranslator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($gbTemplate);
        }
        
        return $gbTemplate;
    }

    public function testAdd()
    {
        $gbTemplate = $this->initAdd(true);

        $result = $this->adapter->add($gbTemplate);
        $this->assertEquals($gbTemplate->getId(), $result);
    }

    public function testAddFail()
    {
        $object = $this->initAdd(false);

        $result = $this->adapter->add($object);
        $this->assertFalse($result);
    }

    private function initEdit(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockGbTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $keys = array();
        $data = array('data');

        $gbTranslator = $this->prophesize(IRestfulTranslator::class);
        $gbTranslator->objectToArray($gbTemplate, $keys)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($gbTranslator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return [$gbTemplate, $keys];
    }

    public function testEdit()
    {
        list($gbTemplate, $keys) = $this->initEdit(true);

        $result = $this->adapter->edit($gbTemplate, $keys);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        list($gbTemplate, $keys) = $this->initEdit(false);

        $result = $this->adapter->edit($gbTemplate, $keys);
        $this->assertFalse($result);
    }
}
