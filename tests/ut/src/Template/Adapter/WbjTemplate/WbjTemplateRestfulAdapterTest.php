<?php
namespace BaseSdk\Template\Adapter\WbjTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockWbjTemplateRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new WbjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIWbjTemplateAdapter()
    {
        $adapter = new WbjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\WbjTemplateRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('templates', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'WBJ_TEMPLATE_LIST',
                WbjTemplateRestfulAdapter::SCENARIOS['WBJ_TEMPLATE_LIST']
            ],
            [
                'WBJ_TEMPLATE_FETCH_ONE',
                WbjTemplateRestfulAdapter::SCENARIOS['WBJ_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\NullWbjTemplate',
            $this->adapter->getNullObject()
        );
    }

    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockWbjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $data = array('data');

        $wbjTranslator = $this->prophesize(IRestfulTranslator::class);
        $wbjTranslator->objectToArray($wbjTemplate)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($wbjTranslator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($wbjTemplate);
        }
        
        return $wbjTemplate;
    }

    public function testAdd()
    {
        $wbjTemplate = $this->initAdd(true);

        $result = $this->adapter->add($wbjTemplate);
        $this->assertEquals($wbjTemplate->getId(), $result);
    }

    public function testAddFail()
    {
        $object = $this->initAdd(false);

        $result = $this->adapter->add($object);
        $this->assertFalse($result);
    }

    private function initEdit(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockWbjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $keys = array();
        $data = array('data');

        $wbjTranslator = $this->prophesize(IRestfulTranslator::class);
        $wbjTranslator->objectToArray($wbjTemplate, $keys)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($wbjTranslator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return [$wbjTemplate, $keys];
    }

    public function testEdit()
    {
        list($wbjTemplate, $keys) = $this->initEdit(true);

        $result = $this->adapter->edit($wbjTemplate, $keys);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        list($wbjTemplate, $keys) = $this->initEdit(false);

        $result = $this->adapter->edit($wbjTemplate, $keys);
        $this->assertFalse($result);
    }
}
