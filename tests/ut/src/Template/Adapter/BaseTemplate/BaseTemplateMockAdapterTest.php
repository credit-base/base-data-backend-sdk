<?php
namespace BaseSdk\Template\Adapter\BaseTemplate;

use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Model\BaseTemplate;

class BaseTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BaseTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\BaseTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\BaseTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\BaseTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new BaseTemplate(), ['keys']));
    }
}
