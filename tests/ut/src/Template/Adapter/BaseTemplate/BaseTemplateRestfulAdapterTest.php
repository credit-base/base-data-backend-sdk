<?php
namespace BaseSdk\Template\Adapter\BaseTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

class BaseTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockBaseTemplateRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new BaseTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIBaseTemplateAdapter()
    {
        $adapter = new BaseTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\BaseTemplateRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('baseTemplates', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'BASE_TEMPLATE_LIST',
                BaseTemplateRestfulAdapter::SCENARIOS['BASE_TEMPLATE_LIST']
            ],
            [
                'BASE_TEMPLATE_FETCH_ONE',
                BaseTemplateRestfulAdapter::SCENARIOS['BASE_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\NullBaseTemplate',
            $this->adapter->getNullObject()
        );
    }

    private function initEdit(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockBaseTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $keys = array();
        $baseTemplate = \BaseSdk\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);
        $data = array('data');

        $baseTranslator = $this->prophesize(IRestfulTranslator::class);
        $baseTranslator->objectToArray($baseTemplate, $keys)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($baseTranslator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return [$baseTemplate, $keys];
    }

    public function testEdit()
    {
        list($baseTemplate, $keys) = $this->initEdit(true);

        $result = $this->adapter->edit($baseTemplate, $keys);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        list($baseTemplate, $keys) = $this->initEdit(false);

        $result = $this->adapter->edit($baseTemplate, $keys);
        $this->assertFalse($result);
    }
}
