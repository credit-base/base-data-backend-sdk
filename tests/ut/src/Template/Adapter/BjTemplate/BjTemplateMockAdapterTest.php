<?php
namespace BaseSdk\Template\Adapter\BjTemplate;

use PHPUnit\Framework\TestCase;

use BaseSdk\Template\Model\BjTemplate;

class BjTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BjTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new BjTemplate()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new BjTemplate(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\BjTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\BjTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\Template\Model\BjTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
