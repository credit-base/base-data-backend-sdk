<?php
namespace BaseSdk\Template\Adapter\BjTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Template\Model\BjTemplate;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockBjTemplateRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new BjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIBjTemplateAdapter()
    {
        $adapter = new BjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\BjTemplateRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('bjTemplates', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'BJ_TEMPLATE_LIST',
                BjTemplateRestfulAdapter::SCENARIOS['BJ_TEMPLATE_LIST']
            ],
            [
                'BJ_TEMPLATE_FETCH_ONE',
                BjTemplateRestfulAdapter::SCENARIOS['BJ_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\NullBjTemplate',
            $this->adapter->getNullObject()
        );
    }

    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockBjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $data = array('data');

        $bjTranslator = $this->prophesize(IRestfulTranslator::class);
        $bjTranslator->objectToArray($bjTemplate)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($bjTranslator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($bjTemplate);
        }
        
        return $bjTemplate;
    }

    public function testAdd()
    {
        $bjTemplate = $this->initAdd(true);

        $result = $this->adapter->add($bjTemplate);
        $this->assertEquals($bjTemplate->getId(), $result);
    }

    public function testAddFail()
    {
        $bjTemplate = $this->initAdd(false);

        $result = $this->adapter->add($bjTemplate);
        $this->assertFalse($result);
    }

    private function initEdit(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockBjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $keys = array();
        $data = array('data');

        $bjTranslator = $this->prophesize(IRestfulTranslator::class);
        $bjTranslator->objectToArray($bjTemplate, $keys)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($bjTranslator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return [$bjTemplate, $keys];
    }

    public function testEdit()
    {
        list($bjTemplate, $keys) = $this->initEdit(true);

        $result = $this->adapter->edit($bjTemplate, $keys);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        list($bjTemplate, $keys) = $this->initEdit(false);

        $result = $this->adapter->edit($bjTemplate, $keys);
        $this->assertFalse($result);
    }
}
