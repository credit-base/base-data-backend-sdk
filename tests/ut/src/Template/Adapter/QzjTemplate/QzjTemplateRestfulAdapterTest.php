<?php
namespace BaseSdk\Template\Adapter\QzjTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class QzjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockQzjTemplateRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new QzjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIQzjTemplateAdapter()
    {
        $adapter = new QzjTemplateRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\QzjTemplateRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('qzjTemplates', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'QZJ_TEMPLATE_LIST',
                QzjTemplateRestfulAdapter::SCENARIOS['QZJ_TEMPLATE_LIST']
            ],
            [
                'QZJ_TEMPLATE_FETCH_ONE',
                QzjTemplateRestfulAdapter::SCENARIOS['QZJ_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Model\NullQzjTemplate',
            $this->adapter->getNullObject()
        );
    }

    private function initAdd(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockQzjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'post', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $qzjTemplate = \BaseSdk\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);
        $data = array('data');

        $qzjTranslator = $this->prophesize(IRestfulTranslator::class);
        $qzjTranslator->objectToArray($qzjTemplate)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($qzjTranslator->reveal());

        $this->adapter->expects($this->once())->method('post');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject')->willReturn($qzjTemplate);
        }
        
        return $qzjTemplate;
    }

    public function testAdd()
    {
        $qzjTemplate = $this->initAdd(true);

        $result = $this->adapter->add($qzjTemplate);
        $this->assertEquals($qzjTemplate->getId(), $result);
    }

    public function testAddFail()
    {
        $object = $this->initAdd(false);

        $result = $this->adapter->add($object);
        $this->assertFalse($result);
    }

    private function initEdit(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockQzjTemplateRestfulAdapter::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $qzjTemplate = \BaseSdk\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);
        $keys = array();
        $data = array('data');

        $qzjTranslator = $this->prophesize(IRestfulTranslator::class);
        $qzjTranslator->objectToArray($qzjTemplate, $keys)->shouldBeCalledTimes(1)->willReturn($data);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($qzjTranslator->reveal());

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('templates');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return [$qzjTemplate, $keys];
    }

    public function testEdit()
    {
        list($qzjTemplate, $keys) = $this->initEdit(true);

        $result = $this->adapter->edit($qzjTemplate, $keys);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        list($qzjTemplate, $keys) = $this->initEdit(false);

        $result = $this->adapter->edit($qzjTemplate, $keys);
        $this->assertFalse($result);
    }
}
