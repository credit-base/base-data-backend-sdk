<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\ResourceCatalogData\Model\WbjItemsData;

class WbjItemsDataMockFactory
{
    public static function generateWbjItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $data = isset($value['data']) ? $value['data'] : $faker->words(3, false);
        $status = isset($value['status']) ? $value['status'] : 0;
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        return new WbjItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);
    }
}
