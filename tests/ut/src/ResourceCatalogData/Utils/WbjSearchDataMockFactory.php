<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\Template\Model\WbjTemplate;

use BaseSdk\ResourceCatalogData\Model\WbjItemsData;
use BaseSdk\ResourceCatalogData\Model\WbjSearchData;

class WbjSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateWbjSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = self::generateCrew($faker, $value);
        $name = self::generateName($faker, $value);
        $identify = self::generateName($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $expirationDate = self::generateExpirationDate($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $template = self::generateTemplate($faker, $value);
        $task = self::generateTask($faker, $value);
        $itemsData = self::generateItemsData($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();
        $hash = md5(base64_encode(gzcompress(serialize($itemsData->getData()))));

        $wbjSearchData = new WbjSearchData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $template,
            $itemsData,
            $task,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $wbjSearchData;
    }

    private static function generateTemplate($faker, $value) : WbjTemplate
    {
        return isset($value['template']) ?
        $value['template'] :
        \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($faker->randomDigit());
    }

    private static function generateItemsData($faker, $value) : WbjItemsData
    {
        return isset($value['itemsData']) ?
        $value['itemsData'] : WbjItemsDataMockFactory::generateWbjItemsData($faker->randomDigit());
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ? $value['status'] : $faker->randomDigit();
    }
}
