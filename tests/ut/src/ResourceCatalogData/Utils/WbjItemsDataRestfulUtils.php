<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait WbjItemsDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjItemsData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $wbjItemsData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['data'], $wbjItemsData->getData());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $wbjItemsData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $wbjItemsData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $wbjItemsData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $wbjItemsData->getUpdateTime());
    }
}
