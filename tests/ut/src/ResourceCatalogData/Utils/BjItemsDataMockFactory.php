<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\ResourceCatalogData\Model\BjItemsData;

class BjItemsDataMockFactory
{
    public static function generateBjItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $data = isset($value['data']) ? $value['data'] : $faker->words(3, false);
        $status = isset($value['status']) ? $value['status'] : 0;
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        return new BjItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);
    }
}
