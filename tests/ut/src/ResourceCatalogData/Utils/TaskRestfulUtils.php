<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait TaskRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $task
    ) {
        $this->assertEquals($expectedArray['data']['id'], $task->getId());
        $this->assertEquals($expectedArray['data']['attributes']['pid'], $task->getPid());
        $this->assertEquals($expectedArray['data']['attributes']['total'], $task->getTotal());
        $this->assertEquals($expectedArray['data']['attributes']['successNumber'], $task->getSuccessNumber());
        $this->assertEquals($expectedArray['data']['attributes']['failureNumber'], $task->getFailureNumber());
        $this->assertEquals($expectedArray['data']['attributes']['sourceCategory'], $task->getSourceCategory());
        $this->assertEquals($expectedArray['data']['attributes']['sourceTemplate'], $task->getSourceTemplate());
        $this->assertEquals($expectedArray['data']['attributes']['targetCategory'], $task->getTargetCategory());
        $this->assertEquals($expectedArray['data']['attributes']['targetTemplate'], $task->getTargetTemplate());
        $this->assertEquals($expectedArray['data']['attributes']['targetRule'], $task->getTargetRule());
        $this->assertEquals($expectedArray['data']['attributes']['scheduleTask'], $task->getScheduleTask());
        $this->assertEquals($expectedArray['data']['attributes']['errorNumber'], $task->getErrorNumber());
        $this->assertEquals($expectedArray['data']['attributes']['fileName'], $task->getFileName());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $task->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $task->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $task->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $task->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $task->getCrew()->getId()
            );
        }
    }
}
