<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait WbjSearchDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjSearchData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $wbjSearchData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $wbjSearchData->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $wbjSearchData->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $wbjSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $wbjSearchData->getInfoCategory());

        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $wbjSearchData->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $wbjSearchData->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $wbjSearchData->getExpirationDate()
        );
        $this->assertEquals($expectedArray['data']['attributes']['hash'], $wbjSearchData->getHash());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $wbjSearchData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $wbjSearchData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $wbjSearchData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $wbjSearchData->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $wbjSearchData->getCrew()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['type'],
                'templates'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $wbjSearchData->getTemplate()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['type'],
                'wbjItemsData'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['id'],
                $wbjSearchData->getItemsData()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['type'],
                'tasks'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['id'],
                $wbjSearchData->getTask()->getId()
            );
        }
    }
}
