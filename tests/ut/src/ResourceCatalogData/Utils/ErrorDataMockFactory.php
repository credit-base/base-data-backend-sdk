<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Model\BjTemplate;

use BaseSdk\ResourceCatalogData\Model\ErrorData;
use BaseSdk\ResourceCatalogData\Model\ErrorItemsData;

class ErrorDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateErrorData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ErrorData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $identify = self::generateName($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $crew = self::generateCrew($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $expirationDate = self::generateExpirationDate($faker, $value);
        $template = self::generateTemplate($faker, $value);
        $itemsData = self::generateItemsData($faker, $value);
        $category = Template::CATEGORY['BJ'];
        $errorType = self::generateErrorType($faker, $value);
        $errorReason = self::generateErrorReason($faker, $value);
        $task = self::generateTask($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();
        $hash = md5(base64_encode(gzcompress(serialize($itemsData->getData()))));

        $errorData = new ErrorData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $template,
            $itemsData,
            $task,
            $category,
            $errorType,
            $errorReason,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $errorData;
    }

    private static function generateTemplate($faker, $value) : BjTemplate
    {
        return isset($value['template']) ?
        $value['template'] :
        \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate($faker->randomDigit());
    }

    private static function generateItemsData($faker, $value) : ErrorItemsData
    {
        return isset($value['itemsData']) ?
        $value['itemsData'] : ErrorItemsDataMockFactory::generateErrorItemsData($faker->randomDigit());
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ? $value['status'] : $faker->randomDigit();
    }

    protected static function generateErrorType($faker, $value) : int
    {
        return isset($value['errorType']) ? $value['errorType'] : $faker->randomDigit();
    }

    protected static function generateErrorReason($faker, $value) : array
    {
        return isset($value['errorReason']) ? $value['errorReason'] : $faker->words(3, false);
    }
}
