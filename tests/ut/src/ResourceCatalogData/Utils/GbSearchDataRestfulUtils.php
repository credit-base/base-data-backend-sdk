<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait GbSearchDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbSearchData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $gbSearchData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $gbSearchData->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $gbSearchData->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $gbSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $gbSearchData->getInfoCategory());

        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $gbSearchData->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $gbSearchData->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $gbSearchData->getExpirationDate()
        );
        $this->assertEquals($expectedArray['data']['attributes']['hash'], $gbSearchData->getHash());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $gbSearchData->getDescription());
        $this->assertEquals(
            $expectedArray['data']['attributes']['frontEndProcessorStatus'],
            $gbSearchData->getFrontEndProcessorStatus()
        );
        $this->assertEquals($expectedArray['data']['attributes']['status'], $gbSearchData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $gbSearchData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $gbSearchData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $gbSearchData->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $gbSearchData->getCrew()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['type'],
                'gbTemplates'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $gbSearchData->getTemplate()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['type'],
                'gbItemsData'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['id'],
                $gbSearchData->getItemsData()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['type'],
                'tasks'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['id'],
                $gbSearchData->getTask()->getId()
            );
        }
    }
}
