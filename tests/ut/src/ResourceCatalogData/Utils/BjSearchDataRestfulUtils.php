<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait BjSearchDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjSearchData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $bjSearchData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $bjSearchData->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $bjSearchData->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $bjSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $bjSearchData->getInfoCategory());

        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $bjSearchData->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $bjSearchData->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $bjSearchData->getExpirationDate()
        );
        $this->assertEquals($expectedArray['data']['attributes']['hash'], $bjSearchData->getHash());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $bjSearchData->getDescription());
        $this->assertEquals(
            $expectedArray['data']['attributes']['frontEndProcessorStatus'],
            $bjSearchData->getFrontEndProcessorStatus()
        );
        $this->assertEquals($expectedArray['data']['attributes']['status'], $bjSearchData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $bjSearchData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $bjSearchData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $bjSearchData->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $bjSearchData->getCrew()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['type'],
                'bjTemplates'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $bjSearchData->getTemplate()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['type'],
                'bjItemsData'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['id'],
                $bjSearchData->getItemsData()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['type'],
                'tasks'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['id'],
                $bjSearchData->getTask()->getId()
            );
        }
    }
}
