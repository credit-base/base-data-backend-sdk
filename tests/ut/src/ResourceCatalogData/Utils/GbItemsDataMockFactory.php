<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\ResourceCatalogData\Model\GbItemsData;

class GbItemsDataMockFactory
{
    public static function generateGbItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $data = isset($value['data']) ? $value['data'] : $faker->words(3, false);
        $status = isset($value['status']) ? $value['status'] : 0;
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();

        return new GbItemsData($id, $data, $status, $statusTime, $createTime, $updateTime);
    }
}
