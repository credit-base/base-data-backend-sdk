<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\Template\Model\Template;
use BaseSdk\ResourceCatalogData\Model\Task;

use BaseSdk\Crew\Model\Crew;

class TaskMockFactory
{
    public static function generateTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Task {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = self::generateCrew($faker, $value);
        $pid = self::generatePid($faker, $value);
        $total = self::generateTotal($faker, $value);
        $successNumber = self::generateSuccessNumber($faker, $value);
        $failureNumber = self::generateFailureNumber($faker, $value);
        $sourceCategory = self::generateSourceCategory($faker, $value);
        $sourceTemplate = self::generateSourceTemplate($faker, $value);
        $targetCategory = self::generateTargetCategory($faker, $value);
        $targetTemplate = self::generateTargetTemplate($faker, $value);
        $targetRule = self::generateTargetRule($faker, $value);
        $scheduleTask = self::generateScheduleTask($faker, $value);
        $errorNumber = self::generateErrorNumber($faker, $value);
        $fileName = self::generateFileName($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();
        
        $task = new Task(
            $id,
            $crew,
            $pid,
            $total,
            $successNumber,
            $failureNumber,
            $sourceCategory,
            $sourceTemplate,
            $targetCategory,
            $targetTemplate,
            $targetRule,
            $scheduleTask,
            $errorNumber,
            $fileName,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $task;
    }

    protected static function generateCrew($faker, $value) : Crew
    {
        return isset($value['crew']) ?
        $value['crew'] :
        \BaseSdk\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
    }

    protected static function generatePid($faker, $value) : int
    {
        return isset($value['pid']) ? $value['pid'] : $faker->randomDigit();
    }

    protected static function generateTotal($faker, $value) : int
    {
        return isset($value['total']) ? $value['total'] : $faker->randomDigit();
    }

    protected static function generateSuccessNumber($faker, $value) : int
    {
        return isset($value['successNumber']) ? $value['successNumber'] : $faker->randomDigit();
    }

    protected static function generateFailureNumber($faker, $value) : int
    {
        return isset($value['failureNumber']) ? $value['failureNumber'] : $faker->randomDigit();
    }

    protected static function generateSourceCategory($faker, $value) : int
    {
        return isset($value['sourceCategory']) ?
            $value['sourceCategory'] :
            $faker->randomElement(
                Template::CATEGORY
            );
    }

    protected static function generateSourceTemplate($faker, $value) : int
    {
        return isset($value['sourceTemplate']) ? $value['sourceTemplate'] : $faker->randomDigit();
    }
    
    protected static function generateTargetCategory($faker, $value) : int
    {
        return isset($value['targetCategory']) ?
            $value['targetCategory'] :
            $faker->randomElement(
                Template::CATEGORY
            );
    }

    protected static function generateTargetTemplate($faker, $value) : int
    {
        return isset($value['targetTemplate']) ? $value['targetTemplate'] : $faker->randomDigit();
    }

    protected static function generateTargetRule($faker, $value) : int
    {
        return isset($value['targetRule']) ? $value['targetRule'] : $faker->randomDigit();
    }

    protected static function generateScheduleTask($faker, $value) : int
    {
        return isset($value['scheduleTask']) ? $value['scheduleTask'] : $faker->randomDigit();
    }

    protected static function generateErrorNumber($faker, $value) : int
    {
        return isset($value['errorNumber']) ? $value['errorNumber'] : $faker->randomDigit();
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                Task::STATUS
            );
    }

    protected static function generateFileName($faker, $value) : string
    {
        return isset($value['fileName']) ? $value['fileName'] : $faker->name().'xls';
    }
}
