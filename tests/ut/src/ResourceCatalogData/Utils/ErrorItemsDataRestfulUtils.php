<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait ErrorItemsDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $errorItemsData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $errorItemsData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['data'], $errorItemsData->getData());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $errorItemsData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $errorItemsData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $errorItemsData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $errorItemsData->getUpdateTime());
    }
}
