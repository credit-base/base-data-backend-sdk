<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait BjItemsDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjItemsData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $bjItemsData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['data'], $bjItemsData->getData());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $bjItemsData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $bjItemsData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $bjItemsData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $bjItemsData->getUpdateTime());
    }
}
