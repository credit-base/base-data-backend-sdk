<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait GbItemsDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbItemsData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $gbItemsData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['data'], $gbItemsData->getData());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $gbItemsData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $gbItemsData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $gbItemsData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $gbItemsData->getUpdateTime());
    }
}
