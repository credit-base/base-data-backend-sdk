<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\Crew\Model\Crew;

use BaseSdk\ResourceCatalogData\Model\Task;

trait SearchDataMockFactoryTrait
{
    protected static function generateName($faker, $value) : string
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    protected static function generateIdentify($faker, $value) : string
    {
        return isset($value['identify']) ? $value['identify'] : $faker->bothify('##############????');
    }

    protected static function generateInfoClassify($faker, $value) : int
    {
        return isset($value['infoClassify']) ? $value['infoClassify'] : $faker->randomDigit();
    }

    protected static function generateInfoCategory($faker, $value) : int
    {
        return isset($value['infoCategory']) ? $value['infoCategory'] : $faker->randomDigit();
    }

    protected static function generateCrew($faker, $value) : Crew
    {
        return isset($value['crew']) ?
        $value['crew'] :
        \BaseSdk\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
    }

    protected static function generateSubjectCategory($faker, $value) : int
    {
        return isset($value['subjectCategory']) ? $value['subjectCategory'] : $faker->randomDigit();
    }

    protected static function generateDimension($faker, $value) : int
    {
        return isset($value['dimension']) ? $value['dimension'] : $faker->randomDigit();
        ;
    }

    protected static function generateExpirationDate($faker, $value) : int
    {
        return isset($value['expirationDate']) ? $value['expirationDate'] : $faker->unixTime();
    }

    protected static function generateTask($faker, $value) : Task
    {
        return isset($value['task']) ?
        $value['task'] :
        TaskMockFactory::generateTask($faker->randomDigit());
    }
}
