<?php
namespace BaseSdk\ResourceCatalogData\Utils;

trait ErrorDataRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $errorData
    ) {
        $this->assertEquals($expectedArray['data']['id'], $errorData->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $errorData->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $errorData->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $errorData->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $errorData->getInfoCategory());

        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $errorData->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $errorData->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $errorData->getExpirationDate()
        );
        $this->assertEquals($expectedArray['data']['attributes']['hash'], $errorData->getHash());
        $this->assertEquals($expectedArray['data']['attributes']['category'], $errorData->getCategory());
        $this->assertEquals($expectedArray['data']['attributes']['errorType'], $errorData->getErrorType());
        $this->assertEquals($expectedArray['data']['attributes']['errorReason'], $errorData->getErrorReason());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $errorData->getStatus());
        $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $errorData->getStatusTime());
        $this->assertEquals($expectedArray['data']['attributes']['createTime'], $errorData->getCreateTime());
        $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $errorData->getUpdateTime());

        if (isset($expectedArray['data']['relationships'])) {
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['type'],
                'crews'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['crew']['data'][0]['id'],
                $errorData->getCrew()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['type'],
                'bjTemplates'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['template']['data'][0]['id'],
                $errorData->getTemplate()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['type'],
                'errorItemsData'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['itemsData']['data'][0]['id'],
                $errorData->getItemsData()->getId()
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['type'],
                'tasks'
            );
            $this->assertEquals(
                $expectedArray['data']['relationships']['task']['data'][0]['id'],
                $errorData->getTask()->getId()
            );
        }
    }
}
