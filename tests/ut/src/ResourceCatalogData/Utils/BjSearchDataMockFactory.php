<?php
namespace BaseSdk\ResourceCatalogData\Utils;

use BaseSdk\Template\Model\BjTemplate;

use BaseSdk\ResourceCatalogData\Model\BjItemsData;
use BaseSdk\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateBjSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = self::generateCrew($faker, $value);
        $name = self::generateName($faker, $value);
        $identify = self::generateName($faker, $value);
        $infoClassify = self::generateInfoClassify($faker, $value);
        $infoCategory = self::generateInfoCategory($faker, $value);
        $subjectCategory = self::generateSubjectCategory($faker, $value);
        $dimension = self::generateDimension($faker, $value);
        $expirationDate = self::generateExpirationDate($faker, $value);
        $template = self::generateTemplate($faker, $value);
        $itemsData = self::generateItemsData($faker, $value);
        $task = self::generateTask($faker, $value);
        $description = self::generateDescription($faker, $value);
        $frontEndProcessorStatus = self::generateFrontEndProcessorStatus($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();
        $hash = md5(base64_encode(gzcompress(serialize($itemsData->getData()))));

        $bjSearchData = new BjSearchData(
            $id,
            $name,
            $identify,
            $infoClassify,
            $infoCategory,
            $crew,
            $subjectCategory,
            $dimension,
            $expirationDate,
            $hash,
            $template,
            $itemsData,
            $task,
            $description,
            $frontEndProcessorStatus,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $bjSearchData;
    }

    private static function generateTemplate($faker, $value) : BjTemplate
    {
        return isset($value['template']) ?
        $value['template'] :
        \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate($faker->randomDigit());
    }

    private static function generateItemsData($faker, $value) : BjItemsData
    {
        return isset($value['itemsData']) ?
        $value['itemsData'] : BjItemsDataMockFactory::generateBjItemsData($faker->randomDigit());
    }

    protected static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ? $value['status'] : $faker->randomDigit();
    }

    protected static function generateFrontEndProcessorStatus($faker, $value) : int
    {
        return isset($value['frontEndProcessorStatus']) ?
            $value['frontEndProcessorStatus'] :
            $faker->randomElement(
                BjSearchData::FRONT_END_PROCESSOR_STATUS
            );
    }

    protected static function generateDescription($faker, $value) : string
    {
        return isset($value['description']) ? $value['description'] : $faker->word();
    }
}
