<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\WbjSearchDataRestfulUtils;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Translator\WbjTemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataRestfulTranslatorTest extends TestCase
{
    use WbjSearchDataRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new WbjSearchDataRestfulTranslator();
        $this->childTranslator = new class extends WbjSearchDataRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getWbjTemplateRestfulTranslator() : WbjTemplateRestfulTranslator
            {
                return parent::getWbjTemplateRestfulTranslator();
            }

            public function getWbjItemsDataRestfulTranslator() : WbjItemsDataRestfulTranslator
            {
                return parent::getWbjItemsDataRestfulTranslator();
            }

            public function getTaskRestfulTranslator() : TaskRestfulTranslator
            {
                return parent::getTaskRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetWbjTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\WbjTemplateRestfulTranslator',
            $this->childTranslator->getWbjTemplateRestfulTranslator()
        );
    }

    public function testGetWbjItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\WbjItemsDataRestfulTranslator',
            $this->childTranslator->getWbjItemsDataRestfulTranslator()
        );
    }

    public function testGetTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\TaskRestfulTranslator',
            $this->childTranslator->getTaskRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $wbjSearchData = \BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression['data']['id'] = $wbjSearchData->getId();
        $expression['data']['attributes']['name'] = $wbjSearchData->getName();
        $expression['data']['attributes']['identify'] = $wbjSearchData->getIdentify();
        $expression['data']['attributes']['infoClassify'] = $wbjSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $wbjSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $wbjSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $wbjSearchData->getDimension();
        $expression['data']['attributes']['expirationDate'] = $wbjSearchData->getExpirationDate();
        $expression['data']['attributes']['hash'] = $wbjSearchData->getHash();
        $expression['data']['attributes']['createTime'] = $wbjSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $wbjSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $wbjSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $wbjSearchData->getStatusTime();

        $wbjSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjSearchData', $wbjSearchDataObject);
        $this->compareArrayAndObject($expression, $wbjSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $wbjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullWbjSearchData', $wbjSearchData);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testObjectToArray()
    {
        $wbjSearchData = \BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression = $this->translator->objectToArray($wbjSearchData);

        $this->assertEquals(
            [],
            $expression
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'crew'=>['data'=>'mockCrew'],
            'template'=>['data'=>'mockWbjTemplate'],
            'itemsData'=>['data'=>'mockWbjItemsData'],
        ];

        $translator = $this->getMockBuilder(WbjSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getWbjTemplateRestfulTranslator',
             'getCrewRestfulTranslator',
             'getWbjItemsDataRestfulTranslator',
             'getTaskRestfulTranslator'
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $taskInfo = ['mockTask'];
        $wbjTemplateInfo = ['mockWbjTemplate'];
        $wbjItemsDataInfo = ['mockWbjItemsData'];

        $wbjItemsData = \BaseSdk\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(1);
        $wbjTemplate = \BaseSdk\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['template']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['task']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $wbjTemplateInfo, $wbjItemsDataInfo, $taskInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $wbjTemplateRestfulTranslator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $wbjTemplateRestfulTranslator->arrayToObject(Argument::exact($wbjTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplate);

        $wbjItemsDataRestfulTranslator = $this->prophesize(WbjItemsDataRestfulTranslator::class);
        $wbjItemsDataRestfulTranslator->arrayToObject(Argument::exact($wbjItemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjItemsData);

        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);
        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getWbjTemplateRestfulTranslator')
            ->willReturn($wbjTemplateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getWbjItemsDataRestfulTranslator')
            ->willReturn($wbjItemsDataRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTaskRestfulTranslator')
            ->willReturn($taskRestfulTranslator->reveal());

        //揭示
        $wbjSearchData = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjSearchData', $wbjSearchData);
        $this->assertEquals($wbjTemplate, $wbjSearchData->getTemplate());
        $this->assertEquals($wbjItemsData, $wbjSearchData->getItemsData());
        $this->assertEquals($crew, $wbjSearchData->getCrew());
        $this->assertEquals($task, $wbjSearchData->getTask());
    }
}
