<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Model\NullErrorItemsData;
use BaseSdk\ResourceCatalogData\Utils\ErrorItemsDataRestfulUtils;

class ErrorItemsDataRestfulTranslatorTest extends TestCase
{
    use ErrorItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorItemsDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $errorItemsData = \BaseSdk\ResourceCatalogData\Utils\ErrorItemsDataMockFactory::generateErrorItemsData(1);

        $expression['data']['id'] = $errorItemsData->getId();
        $expression['data']['attributes']['data'] = $errorItemsData->getData();

        $expression['data']['attributes']['status'] = $errorItemsData->getStatus();
        $expression['data']['attributes']['createTime'] = $errorItemsData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $errorItemsData->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $errorItemsData->getStatusTime();

        $errorItemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorItemsData', $errorItemsDataObject);
        $this->compareArrayAndObject($expression, $errorItemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $errorItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullErrorItemsData', $errorItemsData);
    }

    public function testObjectToArray()
    {
        $errorItemsData = NullErrorItemsData::getInstance();

        $expression = $this->translator->objectToArray($errorItemsData);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
