<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Model\NullBjItemsData;
use BaseSdk\ResourceCatalogData\Utils\BjItemsDataRestfulUtils;

class BjItemsDataRestfulTranslatorTest extends TestCase
{
    use BjItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjItemsDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $bjItemsData = \BaseSdk\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);

        $expression['data']['id'] = $bjItemsData->getId();
        $expression['data']['attributes']['data'] = $bjItemsData->getData();

        $expression['data']['attributes']['status'] = $bjItemsData->getStatus();
        $expression['data']['attributes']['createTime'] = $bjItemsData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $bjItemsData->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $bjItemsData->getStatusTime();

        $bjItemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjItemsData', $bjItemsDataObject);
        $this->compareArrayAndObject($expression, $bjItemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $bjItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullBjItemsData', $bjItemsData);
    }

    public function testObjectToArray()
    {
        $bjItemsData = NullBjItemsData::getInstance();

        $expression = $this->translator->objectToArray($bjItemsData);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
