<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Model\NullGbItemsData;
use BaseSdk\ResourceCatalogData\Utils\GbItemsDataRestfulUtils;

class GbItemsDataRestfulTranslatorTest extends TestCase
{
    use GbItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbItemsDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $gbItemsData = \BaseSdk\ResourceCatalogData\Utils\GbItemsDataMockFactory::generateGbItemsData(1);

        $expression['data']['id'] = $gbItemsData->getId();
        $expression['data']['attributes']['data'] = $gbItemsData->getData();

        $expression['data']['attributes']['status'] = $gbItemsData->getStatus();
        $expression['data']['attributes']['createTime'] = $gbItemsData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $gbItemsData->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $gbItemsData->getStatusTime();

        $gbItemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbItemsData', $gbItemsDataObject);
        $this->compareArrayAndObject($expression, $gbItemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $gbItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullGbItemsData', $gbItemsData);
    }

    public function testObjectToArray()
    {
        $gbItemsData = NullGbItemsData::getInstance();

        $expression = $this->translator->objectToArray($gbItemsData);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
