<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\TaskRestfulUtils;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

class TaskRestfulTranslatorTest extends TestCase
{
    use TaskRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new TaskRestfulTranslator();
        $this->childTranslator = new class extends TaskRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $expression['data']['id'] = $task->getId();
        $expression['data']['attributes']['pid'] = $task->getPid();
        $expression['data']['attributes']['total'] = $task->getTotal();
        $expression['data']['attributes']['successNumber'] = $task->getSuccessNumber();
        $expression['data']['attributes']['failureNumber'] = $task->getFailureNumber();
        $expression['data']['attributes']['sourceCategory'] = $task->getSourceCategory();
        $expression['data']['attributes']['sourceTemplate'] = $task->getSourceTemplate();
        $expression['data']['attributes']['targetCategory'] = $task->getTargetCategory();
        $expression['data']['attributes']['targetTemplate'] = $task->getTargetTemplate();
        $expression['data']['attributes']['targetRule'] = $task->getTargetRule();
        $expression['data']['attributes']['scheduleTask'] = $task->getScheduleTask();
        $expression['data']['attributes']['errorNumber'] = $task->getErrorNumber();
        $expression['data']['attributes']['fileName'] = $task->getFileName();
        $expression['data']['attributes']['createTime'] = $task->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $task->getUpdateTime();
        $expression['data']['attributes']['status'] = $task->getStatus();
        $expression['data']['attributes']['statusTime'] = $task->getStatusTime();

        $taskObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\Task', $taskObject);
        $this->compareArrayAndObject($expression, $taskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullTask', $task);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testObjectToArray()
    {
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $expression = $this->translator->objectToArray($task);

        $this->assertEquals(
            [],
            $expression
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew']
        ];

        $translator = $this->getMockBuilder(TaskRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->once())
            ->method('changeArrayFormat')
            ->with($relationships['crew']['data'])
            ->willReturn($crewInfo);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $task = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\Task', $task);
        $this->assertEquals($crew, $task->getCrew());
    }
}
