<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\GbSearchDataRestfulUtils;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Translator\GbTemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchDataRestfulTranslatorTest extends TestCase
{
    use GbSearchDataRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new GbSearchDataRestfulTranslator();
        $this->childTranslator = new class extends GbSearchDataRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getGbTemplateRestfulTranslator() : GbTemplateRestfulTranslator
            {
                return parent::getGbTemplateRestfulTranslator();
            }

            public function getGbItemsDataRestfulTranslator() : GbItemsDataRestfulTranslator
            {
                return parent::getGbItemsDataRestfulTranslator();
            }

            public function getTaskRestfulTranslator() : TaskRestfulTranslator
            {
                return parent::getTaskRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetGbTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\GbTemplateRestfulTranslator',
            $this->childTranslator->getGbTemplateRestfulTranslator()
        );
    }

    public function testGetGbItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\GbItemsDataRestfulTranslator',
            $this->childTranslator->getGbItemsDataRestfulTranslator()
        );
    }

    public function testGetTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\TaskRestfulTranslator',
            $this->childTranslator->getTaskRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $gbSearchData = \BaseSdk\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);

        $expression['data']['id'] = $gbSearchData->getId();
        $expression['data']['attributes']['name'] = $gbSearchData->getName();
        $expression['data']['attributes']['identify'] = $gbSearchData->getIdentify();
        $expression['data']['attributes']['infoClassify'] = $gbSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $gbSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $gbSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $gbSearchData->getDimension();
        $expression['data']['attributes']['expirationDate'] = $gbSearchData->getExpirationDate();
        $expression['data']['attributes']['hash'] = $gbSearchData->getHash();
        $expression['data']['attributes']['description'] = $gbSearchData->getDescription();
        $expression['data']['attributes']['frontEndProcessorStatus'] = $gbSearchData->getFrontEndProcessorStatus();
        $expression['data']['attributes']['createTime'] = $gbSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $gbSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $gbSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $gbSearchData->getStatusTime();

        $gbSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbSearchData', $gbSearchDataObject);
        $this->compareArrayAndObject($expression, $gbSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $gbSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullGbSearchData', $gbSearchData);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testObjectToArray()
    {
        $gbSearchData = \BaseSdk\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);

        $expression = $this->translator->objectToArray($gbSearchData);

        $this->assertEquals(
            [],
            $expression
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'crew'=>['data'=>'mockCrew'],
            'template'=>['data'=>'mockGbTemplate'],
            'itemsData'=>['data'=>'mockGbItemsData'],
        ];

        $translator = $this->getMockBuilder(GbSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getGbTemplateRestfulTranslator',
             'getCrewRestfulTranslator',
             'getGbItemsDataRestfulTranslator',
             'getTaskRestfulTranslator'
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $taskInfo = ['mockTask'];
        $gbTemplateInfo = ['mockGbTemplate'];
        $gbItemsDataInfo = ['mockGbItemsData'];

        $gbItemsData = \BaseSdk\ResourceCatalogData\Utils\GbItemsDataMockFactory::generateGbItemsData(1);
        $gbTemplate = \BaseSdk\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['template']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['task']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $gbTemplateInfo, $gbItemsDataInfo, $taskInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $gbTemplateRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);
        $gbTemplateRestfulTranslator->arrayToObject(Argument::exact($gbTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);

        $gbItemsDataRestfulTranslator = $this->prophesize(GbItemsDataRestfulTranslator::class);
        $gbItemsDataRestfulTranslator->arrayToObject(Argument::exact($gbItemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbItemsData);

        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);
        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getGbTemplateRestfulTranslator')
            ->willReturn($gbTemplateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getGbItemsDataRestfulTranslator')
            ->willReturn($gbItemsDataRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTaskRestfulTranslator')
            ->willReturn($taskRestfulTranslator->reveal());

        //揭示
        $gbSearchData = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbSearchData', $gbSearchData);
        $this->assertEquals($gbTemplate, $gbSearchData->getTemplate());
        $this->assertEquals($gbItemsData, $gbSearchData->getItemsData());
        $this->assertEquals($crew, $gbSearchData->getCrew());
        $this->assertEquals($task, $gbSearchData->getTask());
    }
}
