<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\ResourceCatalogData\Utils\ErrorDataRestfulUtils;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Model\Template;
use BaseSdk\Template\Translator\TranslatorFactory;
use BaseSdk\Template\Translator\BjTemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorDataRestfulTranslatorTest extends TestCase
{
    use ErrorDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockErrorDataRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\TranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetErrorTemplateRestfulTranslator()
    {
        $translator = $this->getMockBuilder(MockErrorDataRestfulTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $restfulTranslator = new BjTemplateRestfulTranslator();
        $category = Template::CATEGORY['BJ'];

        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($restfulTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getErrorTemplateRestfulTranslator($category);
        $this->assertInstanceof('Marmot\Interfaces\IRestfulTranslator', $result);
        $this->assertEquals($restfulTranslator, $result);
    }

    public function testGetErrorItemsDataRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\ErrorItemsDataRestfulTranslator',
            $translator->getErrorItemsDataRestfulTranslator()
        );
    }

    public function testGetTaskRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();
        
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\TaskRestfulTranslator',
            $translator->getTaskRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $errorData = \BaseSdk\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);

        $expression['data']['id'] = $errorData->getId();
        $expression['data']['attributes']['name'] = $errorData->getName();
        $expression['data']['attributes']['identify'] = $errorData->getIdentify();
        $expression['data']['attributes']['infoClassify'] = $errorData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $errorData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $errorData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $errorData->getDimension();
        $expression['data']['attributes']['expirationDate'] = $errorData->getExpirationDate();
        $expression['data']['attributes']['hash'] = $errorData->getHash();
        $expression['data']['attributes']['category'] = $errorData->getCategory();
        $expression['data']['attributes']['errorType'] = $errorData->getErrorType();
        $expression['data']['attributes']['errorReason'] = $errorData->getErrorReason();
        $expression['data']['attributes']['createTime'] = $errorData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $errorData->getUpdateTime();
        $expression['data']['attributes']['status'] = $errorData->getStatus();
        $expression['data']['attributes']['statusTime'] = $errorData->getStatusTime();

        $errorDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorData', $errorDataObject);
        $this->compareArrayAndObject($expression, $errorDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $errorData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullErrorData', $errorData);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testObjectToArray()
    {
        $errorData = \BaseSdk\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);

        $expression = $this->translator->objectToArray($errorData);

        $this->assertEquals(
            [],
            $expression
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'crew'=>['data'=>'mockCrew'],
            'template'=>['data'=>'mockBjTemplate'],
            'itemsData'=>['data'=>'mockErrorItemsData'],
        ];

        $translator = $this->getMockBuilder(ErrorDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getErrorTemplateRestfulTranslator',
             'getCrewRestfulTranslator',
             'getErrorItemsDataRestfulTranslator',
             'getTaskRestfulTranslator'
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $taskInfo = ['mockTask'];
        $bjTemplateInfo = ['mockBjTemplate'];
        $errorItemsDataInfo = ['mockErrorItemsData'];

        $errorItemsData = \BaseSdk\ResourceCatalogData\Utils\ErrorItemsDataMockFactory::generateErrorItemsData(1);
        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['template']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['task']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $bjTemplateInfo, $errorItemsDataInfo, $taskInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $templateRestfulTranslator = $this->prophesize(IRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($bjTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplate);

        $itemsDataRestfulTranslator = $this->prophesize(ErrorItemsDataRestfulTranslator::class);
        $itemsDataRestfulTranslator->arrayToObject(Argument::exact($errorItemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorItemsData);

        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);
        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getErrorTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getErrorItemsDataRestfulTranslator')
            ->willReturn($itemsDataRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTaskRestfulTranslator')
            ->willReturn($taskRestfulTranslator->reveal());

        //揭示
        $errorData = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorData', $errorData);
        $this->assertEquals($bjTemplate, $errorData->getTemplate());
        $this->assertEquals($errorItemsData, $errorData->getItemsData());
        $this->assertEquals($crew, $errorData->getCrew());
        $this->assertEquals($task, $errorData->getTask());
    }
}
