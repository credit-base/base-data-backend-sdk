<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\BjSearchDataRestfulUtils;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Translator\BjTemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataRestfulTranslatorTest extends TestCase
{
    use BjSearchDataRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new BjSearchDataRestfulTranslator();
        $this->childTranslator = new class extends BjSearchDataRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getBjTemplateRestfulTranslator() : BjTemplateRestfulTranslator
            {
                return parent::getBjTemplateRestfulTranslator();
            }

            public function getBjItemsDataRestfulTranslator() : BjItemsDataRestfulTranslator
            {
                return parent::getBjItemsDataRestfulTranslator();
            }

            public function getTaskRestfulTranslator() : TaskRestfulTranslator
            {
                return parent::getTaskRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetBjTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Translator\BjTemplateRestfulTranslator',
            $this->childTranslator->getBjTemplateRestfulTranslator()
        );
    }

    public function testGetBjItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\BjItemsDataRestfulTranslator',
            $this->childTranslator->getBjItemsDataRestfulTranslator()
        );
    }

    public function testGetTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\TaskRestfulTranslator',
            $this->childTranslator->getTaskRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $bjSearchData = \BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $expression['data']['id'] = $bjSearchData->getId();
        $expression['data']['attributes']['name'] = $bjSearchData->getName();
        $expression['data']['attributes']['identify'] = $bjSearchData->getIdentify();
        $expression['data']['attributes']['infoClassify'] = $bjSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $bjSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $bjSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $bjSearchData->getDimension();
        $expression['data']['attributes']['expirationDate'] = $bjSearchData->getExpirationDate();
        $expression['data']['attributes']['hash'] = $bjSearchData->getHash();
        $expression['data']['attributes']['description'] = $bjSearchData->getDescription();
        $expression['data']['attributes']['frontEndProcessorStatus'] = $bjSearchData->getFrontEndProcessorStatus();
        $expression['data']['attributes']['createTime'] = $bjSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $bjSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $bjSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $bjSearchData->getStatusTime();

        $bjSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjSearchData', $bjSearchDataObject);
        $this->compareArrayAndObject($expression, $bjSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $bjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullBjSearchData', $bjSearchData);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testObjectToArray()
    {
        $bjSearchData = \BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $expression = $this->translator->objectToArray($bjSearchData);

        $this->assertEquals(
            [],
            $expression
        );
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'crew'=>['data'=>'mockCrew'],
            'template'=>['data'=>'mockBjTemplate'],
            'itemsData'=>['data'=>'mockBjItemsData'],
        ];

        $translator = $this->getMockBuilder(BjSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getBjTemplateRestfulTranslator',
             'getCrewRestfulTranslator',
             'getBjItemsDataRestfulTranslator',
             'getTaskRestfulTranslator'
         ])
        ->getMock();

        $crewInfo = ['mockCrew'];
        $taskInfo = ['mockTask'];
        $bjTemplateInfo = ['mockBjTemplate'];
        $bjItemsDataInfo = ['mockBjItemsData'];

        $bjItemsData = \BaseSdk\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);
        $bjTemplate = \BaseSdk\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);
        $task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['template']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['task']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $bjTemplateInfo, $bjItemsDataInfo, $taskInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $bjTemplateRestfulTranslator = $this->prophesize(BjTemplateRestfulTranslator::class);
        $bjTemplateRestfulTranslator->arrayToObject(Argument::exact($bjTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplate);

        $bjItemsDataRestfulTranslator = $this->prophesize(BjItemsDataRestfulTranslator::class);
        $bjItemsDataRestfulTranslator->arrayToObject(Argument::exact($bjItemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjItemsData);

        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);
        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getBjTemplateRestfulTranslator')
            ->willReturn($bjTemplateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getBjItemsDataRestfulTranslator')
            ->willReturn($bjItemsDataRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTaskRestfulTranslator')
            ->willReturn($taskRestfulTranslator->reveal());

        //揭示
        $bjSearchData = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjSearchData', $bjSearchData);
        $this->assertEquals($bjTemplate, $bjSearchData->getTemplate());
        $this->assertEquals($bjItemsData, $bjSearchData->getItemsData());
        $this->assertEquals($crew, $bjSearchData->getCrew());
        $this->assertEquals($task, $bjSearchData->getTask());
    }
}
