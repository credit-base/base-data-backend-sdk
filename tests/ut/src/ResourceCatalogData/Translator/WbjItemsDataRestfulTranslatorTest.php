<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Model\NullWbjItemsData;
use BaseSdk\ResourceCatalogData\Utils\WbjItemsDataRestfulUtils;

class WbjItemsDataRestfulTranslatorTest extends TestCase
{
    use WbjItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjItemsDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $wbjItemsData = \BaseSdk\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(1);

        $expression['data']['id'] = $wbjItemsData->getId();
        $expression['data']['attributes']['data'] = $wbjItemsData->getData();

        $expression['data']['attributes']['status'] = $wbjItemsData->getStatus();
        $expression['data']['attributes']['createTime'] = $wbjItemsData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $wbjItemsData->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $wbjItemsData->getStatusTime();

        $wbjItemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjItemsData', $wbjItemsDataObject);
        $this->compareArrayAndObject($expression, $wbjItemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $wbjItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\NullWbjItemsData', $wbjItemsData);
    }

    public function testObjectToArray()
    {
        $wbjItemsData = NullWbjItemsData::getInstance();

        $expression = $this->translator->objectToArray($wbjItemsData);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
