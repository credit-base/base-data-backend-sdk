<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjItemsDataTest extends TestCase
{
    private $stub;

    private $bjItemsData;

    public function setUp()
    {
        $this->bjItemsData = \BaseSdk\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);
        $this->stub = new BjItemsData(
            $this->bjItemsData->getId(),
            $this->bjItemsData->getData(),
            $this->bjItemsData->getStatus(),
            $this->bjItemsData->getStatusTime(),
            $this->bjItemsData->getCreateTime(),
            $this->bjItemsData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ItemsData', $this->bjItemsData);
    }
}
