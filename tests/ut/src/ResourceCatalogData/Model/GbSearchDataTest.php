<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchDataTest extends TestCase
{
    private $stub;

    private $gbSearchData;

    public function setUp()
    {
        $this->gbSearchData = \BaseSdk\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);
        $this->stub = new GbSearchData(
            $this->gbSearchData->getId(),
            $this->gbSearchData->getName(),
            $this->gbSearchData->getIdentify(),
            $this->gbSearchData->getInfoClassify(),
            $this->gbSearchData->getInfoCategory(),
            $this->gbSearchData->getCrew(),
            $this->gbSearchData->getSubjectCategory(),
            $this->gbSearchData->getDimension(),
            $this->gbSearchData->getExpirationDate(),
            $this->gbSearchData->getHash(),
            $this->gbSearchData->getTemplate(),
            $this->gbSearchData->getItemsData(),
            $this->gbSearchData->getTask(),
            $this->gbSearchData->getDescription(),
            $this->gbSearchData->getFrontEndProcessorStatus(),
            $this->gbSearchData->getStatus(),
            $this->gbSearchData->getStatusTime(),
            $this->gbSearchData->getCreateTime(),
            $this->gbSearchData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->gbSearchData);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\SearchData', $this->gbSearchData);
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->gbSearchData->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetItemsData()
    {
        $this->assertEquals($this->gbSearchData->getItemsData(), $this->stub->getItemsData());
    }

    public function testGetDescription()
    {
        $this->assertEquals($this->gbSearchData->getDescription(), $this->stub->getDescription());
    }

    public function testGetFrontEndProcessorStatus()
    {
        $this->assertEquals(
            $this->gbSearchData->getFrontEndProcessorStatus(),
            $this->stub->getFrontEndProcessorStatus()
        );
    }
}
