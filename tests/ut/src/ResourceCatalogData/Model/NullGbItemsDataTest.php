<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullGbItemsDataTest extends TestCase
{
    private $gbItemsData;

    public function setUp()
    {
        $this->gbItemsData = $this->getMockBuilder(NullGbItemsData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->gbItemsData);
    }

    public function testExtendsGbItemsData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbItemsData', $this->gbItemsData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbItemsData);
    }
}
