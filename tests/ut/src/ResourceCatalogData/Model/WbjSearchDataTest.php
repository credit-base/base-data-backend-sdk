<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataTest extends TestCase
{
    private $stub;

    private $wbjSearchData;

    public function setUp()
    {
        $this->wbjSearchData = \BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        $this->stub = new WbjSearchData(
            $this->wbjSearchData->getId(),
            $this->wbjSearchData->getName(),
            $this->wbjSearchData->getIdentify(),
            $this->wbjSearchData->getInfoClassify(),
            $this->wbjSearchData->getInfoCategory(),
            $this->wbjSearchData->getCrew(),
            $this->wbjSearchData->getSubjectCategory(),
            $this->wbjSearchData->getDimension(),
            $this->wbjSearchData->getExpirationDate(),
            $this->wbjSearchData->getHash(),
            $this->wbjSearchData->getTemplate(),
            $this->wbjSearchData->getItemsData(),
            $this->wbjSearchData->getTask(),
            $this->wbjSearchData->getStatus(),
            $this->wbjSearchData->getStatusTime(),
            $this->wbjSearchData->getCreateTime(),
            $this->wbjSearchData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->wbjSearchData);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\SearchData', $this->wbjSearchData);
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->wbjSearchData->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetItemsData()
    {
        $this->assertEquals($this->wbjSearchData->getItemsData(), $this->stub->getItemsData());
    }
}
