<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullWbjItemsDataTest extends TestCase
{
    private $wbjItemsData;

    public function setUp()
    {
        $this->wbjItemsData = $this->getMockBuilder(NullWbjItemsData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->wbjItemsData);
    }

    public function testExtendsWbjItemsData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjItemsData', $this->wbjItemsData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->wbjItemsData);
    }
}
