<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullErrorItemsDataTest extends TestCase
{
    private $errorItemsData;

    public function setUp()
    {
        $this->errorItemsData = $this->getMockBuilder(NullErrorItemsData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->errorItemsData);
    }

    public function testExtendsErrorItemsData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorItemsData', $this->errorItemsData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->errorItemsData);
    }
}
