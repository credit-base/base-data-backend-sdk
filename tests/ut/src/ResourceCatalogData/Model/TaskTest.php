<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TaskTest extends TestCase
{
    private $stub;

    private $task;

    public function setUp()
    {
        $this->task = \BaseSdk\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $this->stub = new Task(
            $this->task->getId(),
            $this->task->getCrew(),
            $this->task->getPid(),
            $this->task->getTotal(),
            $this->task->getSuccessNumber(),
            $this->task->getFailureNumber(),
            $this->task->getSourceCategory(),
            $this->task->getSourceTemplate(),
            $this->task->getTargetCategory(),
            $this->task->getTargetTemplate(),
            $this->task->getTargetRule(),
            $this->task->getScheduleTask(),
            $this->task->getErrorNumber(),
            $this->task->getFileName(),
            $this->task->getStatus(),
            $this->task->getStatusTime(),
            $this->task->getCreateTime(),
            $this->task->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->task);
    }

    public function testGetId()
    {
        $this->assertEquals($this->task->getId(), $this->stub->getId());
    }

    public function testGetCrew()
    {
        $this->assertEquals($this->task->getCrew(), $this->stub->getCrew());
    }

    public function testGetPid()
    {
        $this->assertEquals($this->task->getPid(), $this->stub->getPid());
    }

    public function testGetTotal()
    {
        $this->assertEquals($this->task->getTotal(), $this->stub->getTotal());
    }

    public function testGetSuccessNumber()
    {
        $this->assertEquals($this->task->getSuccessNumber(), $this->stub->getSuccessNumber());
    }

    public function testGetFailureNumber()
    {
        $this->assertEquals($this->task->getFailureNumber(), $this->stub->getFailureNumber());
    }

    public function testGetSourceCategory()
    {
        $this->assertEquals($this->task->getSourceCategory(), $this->stub->getSourceCategory());
    }

    public function testGetSourceTemplate()
    {
        $this->assertEquals($this->task->getSourceTemplate(), $this->stub->getSourceTemplate());
    }

    public function testGetTargetCategory()
    {
        $this->assertEquals($this->task->getTargetCategory(), $this->stub->getTargetCategory());
    }

    public function testGetTargetTemplate()
    {
        $this->assertEquals($this->task->getTargetTemplate(), $this->stub->getTargetTemplate());
    }

    public function testGetTargetRule()
    {
        $this->assertEquals($this->task->getTargetRule(), $this->stub->getTargetRule());
    }

    public function testGetScheduleTask()
    {
        $this->assertEquals($this->task->getScheduleTask(), $this->stub->getScheduleTask());
    }

    public function testGetErrorNumber()
    {
        $this->assertEquals($this->task->getErrorNumber(), $this->stub->getErrorNumber());
    }

    public function testGetFileName()
    {
        $this->assertEquals($this->task->getFileName(), $this->stub->getFileName());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->task->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->task->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->task->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->task->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
