<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullTaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = $this->getMockBuilder(NullTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    public function testExtendsTask()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\Task', $this->task);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->task);
    }
}
