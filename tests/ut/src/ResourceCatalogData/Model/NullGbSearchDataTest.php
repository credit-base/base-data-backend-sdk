<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullGbSearchDataTest extends TestCase
{
    private $gbSearchData;

    public function setUp()
    {
        $this->gbSearchData = $this->getMockBuilder(NullGbSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->gbSearchData);
    }

    public function testExtendsGbSearchData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbSearchData', $this->gbSearchData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbSearchData);
    }
}
