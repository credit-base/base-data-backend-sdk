<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbItemsDataTest extends TestCase
{
    private $stub;

    private $gbItemsData;

    public function setUp()
    {
        $this->gbItemsData = \BaseSdk\ResourceCatalogData\Utils\GbItemsDataMockFactory::generateGbItemsData(1);
        $this->stub = new GbItemsData(
            $this->gbItemsData->getId(),
            $this->gbItemsData->getData(),
            $this->gbItemsData->getStatus(),
            $this->gbItemsData->getStatusTime(),
            $this->gbItemsData->getCreateTime(),
            $this->gbItemsData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ItemsData', $this->gbItemsData);
    }
}
