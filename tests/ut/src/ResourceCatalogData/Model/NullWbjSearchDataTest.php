<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullWbjSearchDataTest extends TestCase
{
    private $wbjSearchData;

    public function setUp()
    {
        $this->wbjSearchData = $this->getMockBuilder(NullWbjSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->wbjSearchData);
    }

    public function testExtendsWbjSearchData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjSearchData', $this->wbjSearchData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->wbjSearchData);
    }
}
