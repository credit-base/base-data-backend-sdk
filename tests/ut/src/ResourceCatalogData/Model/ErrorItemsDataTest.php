<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorItemsDataTest extends TestCase
{
    private $stub;

    private $errorItemsData;

    public function setUp()
    {
        $this->errorItemsData = \BaseSdk\ResourceCatalogData\Utils\ErrorItemsDataMockFactory::generateErrorItemsData(1);
        $this->stub = new ErrorItemsData(
            $this->errorItemsData->getId(),
            $this->errorItemsData->getData(),
            $this->errorItemsData->getStatus(),
            $this->errorItemsData->getStatusTime(),
            $this->errorItemsData->getCreateTime(),
            $this->errorItemsData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ItemsData', $this->errorItemsData);
    }
}
