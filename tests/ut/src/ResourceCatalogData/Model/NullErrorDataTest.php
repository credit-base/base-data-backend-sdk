<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullErrorDataTest extends TestCase
{
    private $errorData;

    public function setUp()
    {
        $this->errorData = $this->getMockBuilder(NullErrorData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    public function testExtendsErrorData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorData', $this->errorData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->errorData);
    }
}
