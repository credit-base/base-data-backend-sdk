<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBjSearchDataTest extends TestCase
{
    private $bjSearchData;

    public function setUp()
    {
        $this->bjSearchData = $this->getMockBuilder(NullBjSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->bjSearchData);
    }

    public function testExtendsBjSearchData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjSearchData', $this->bjSearchData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->bjSearchData);
    }
}
