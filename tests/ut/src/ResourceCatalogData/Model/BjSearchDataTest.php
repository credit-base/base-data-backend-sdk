<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataTest extends TestCase
{
    private $stub;

    private $bjSearchData;

    public function setUp()
    {
        $this->bjSearchData = \BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $this->stub = new BjSearchData(
            $this->bjSearchData->getId(),
            $this->bjSearchData->getName(),
            $this->bjSearchData->getIdentify(),
            $this->bjSearchData->getInfoClassify(),
            $this->bjSearchData->getInfoCategory(),
            $this->bjSearchData->getCrew(),
            $this->bjSearchData->getSubjectCategory(),
            $this->bjSearchData->getDimension(),
            $this->bjSearchData->getExpirationDate(),
            $this->bjSearchData->getHash(),
            $this->bjSearchData->getTemplate(),
            $this->bjSearchData->getItemsData(),
            $this->bjSearchData->getTask(),
            $this->bjSearchData->getDescription(),
            $this->bjSearchData->getFrontEndProcessorStatus(),
            $this->bjSearchData->getStatus(),
            $this->bjSearchData->getStatusTime(),
            $this->bjSearchData->getCreateTime(),
            $this->bjSearchData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->bjSearchData);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\SearchData', $this->bjSearchData);
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->bjSearchData->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetItemsData()
    {
        $this->assertEquals($this->bjSearchData->getItemsData(), $this->stub->getItemsData());
    }

    public function testGetDescription()
    {
        $this->assertEquals($this->bjSearchData->getDescription(), $this->stub->getDescription());
    }

    public function testGetFrontEndProcessorStatus()
    {
        $this->assertEquals(
            $this->bjSearchData->getFrontEndProcessorStatus(),
            $this->stub->getFrontEndProcessorStatus()
        );
    }
}
