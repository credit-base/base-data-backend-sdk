<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorDataTest extends TestCase
{
    private $stub;

    private $errorData;

    public function setUp()
    {
        $this->errorData = \BaseSdk\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $this->stub = new ErrorData(
            $this->errorData->getId(),
            $this->errorData->getName(),
            $this->errorData->getIdentify(),
            $this->errorData->getInfoClassify(),
            $this->errorData->getInfoCategory(),
            $this->errorData->getCrew(),
            $this->errorData->getSubjectCategory(),
            $this->errorData->getDimension(),
            $this->errorData->getExpirationDate(),
            $this->errorData->getHash(),
            $this->errorData->getTemplate(),
            $this->errorData->getItemsData(),
            $this->errorData->getTask(),
            $this->errorData->getCategory(),
            $this->errorData->getErrorType(),
            $this->errorData->getErrorReason(),
            $this->errorData->getStatus(),
            $this->errorData->getStatusTime(),
            $this->errorData->getCreateTime(),
            $this->errorData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->errorData);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\SearchData', $this->errorData);
    }

    public function testGetTemplate()
    {
        $this->assertEquals($this->errorData->getTemplate(), $this->stub->getTemplate());
    }

    public function testGetItemsData()
    {
        $this->assertEquals($this->errorData->getItemsData(), $this->stub->getItemsData());
    }

    public function testGetCategory()
    {
        $this->assertEquals($this->errorData->getCategory(), $this->stub->getCategory());
    }

    public function testGetErrorType()
    {
        $this->assertEquals($this->errorData->getErrorType(), $this->stub->getErrorType());
    }

    public function testGetErrorReason()
    {
        $this->assertEquals($this->errorData->getErrorReason(), $this->stub->getErrorReason());
    }
}
