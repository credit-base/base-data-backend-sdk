<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SearchDataTest extends TestCase
{
    private $stub;

    private $searchData;

    public function setUp()
    {
        $this->searchData = \BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $this->stub = new MockSearchData(
            $this->searchData->getId(),
            $this->searchData->getName(),
            $this->searchData->getIdentify(),
            $this->searchData->getInfoClassify(),
            $this->searchData->getInfoCategory(),
            $this->searchData->getCrew(),
            $this->searchData->getSubjectCategory(),
            $this->searchData->getDimension(),
            $this->searchData->getExpirationDate(),
            $this->searchData->getHash(),
            $this->searchData->getTask(),
            $this->searchData->getStatus(),
            $this->searchData->getStatusTime(),
            $this->searchData->getCreateTime(),
            $this->searchData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->searchData);
    }

    public function testGetId()
    {
        $this->assertEquals($this->searchData->getId(), $this->stub->getId());
    }

    public function testGetName()
    {
        $this->assertEquals($this->searchData->getName(), $this->stub->getName());
    }

    public function testGetIdentify()
    {
        $this->assertEquals($this->searchData->getIdentify(), $this->stub->getIdentify());
    }

    public function testGetInfoClassify()
    {
        $this->assertEquals($this->searchData->getInfoClassify(), $this->stub->getInfoClassify());
    }

    public function testGetInfoCategory()
    {
        $this->assertEquals($this->searchData->getInfoCategory(), $this->stub->getInfoCategory());
    }

    public function testGetCrew()
    {
        $this->assertEquals($this->searchData->getCrew(), $this->stub->getCrew());
    }

    public function testGetSubjectCategory()
    {
        $this->assertEquals($this->searchData->getSubjectCategory(), $this->stub->getSubjectCategory());
    }

    public function testGetDimension()
    {
        $this->assertEquals($this->searchData->getDimension(), $this->stub->getDimension());
    }

    public function testGetExpirationDate()
    {
        $this->assertEquals($this->searchData->getExpirationDate(), $this->stub->getExpirationDate());
    }

    public function testGetHash()
    {
        $this->assertEquals($this->searchData->getHash(), $this->stub->getHash());
    }

    public function testGetTask()
    {
        $this->assertEquals($this->searchData->getTask(), $this->stub->getTask());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->searchData->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->searchData->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->searchData->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->searchData->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
