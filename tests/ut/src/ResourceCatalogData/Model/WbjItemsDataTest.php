<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjItemsDataTest extends TestCase
{
    private $stub;

    private $wbjItemsData;

    public function setUp()
    {
        $this->wbjItemsData = \BaseSdk\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(1);
        $this->stub = new WbjItemsData(
            $this->wbjItemsData->getId(),
            $this->wbjItemsData->getData(),
            $this->wbjItemsData->getStatus(),
            $this->wbjItemsData->getStatusTime(),
            $this->wbjItemsData->getCreateTime(),
            $this->wbjItemsData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ItemsData', $this->wbjItemsData);
    }
}
