<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBjItemsDataTest extends TestCase
{
    private $bjItemsData;

    public function setUp()
    {
        $this->bjItemsData = $this->getMockBuilder(NullBjItemsData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->bjItemsData);
    }

    public function testExtendsBjItemsData()
    {
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjItemsData', $this->bjItemsData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->bjItemsData);
    }
}
