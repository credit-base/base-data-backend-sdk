<?php
namespace BaseSdk\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ItemsDataTest extends TestCase
{
    private $stub;

    private $itemsData;

    public function setUp()
    {
        $this->itemsData = \BaseSdk\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);
        $this->stub = new MockItemsData(
            $this->itemsData->getId(),
            $this->itemsData->getData(),
            $this->itemsData->getStatus(),
            $this->itemsData->getStatusTime(),
            $this->itemsData->getCreateTime(),
            $this->itemsData->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetId()
    {
        $this->assertEquals($this->itemsData->getId(), $this->stub->getId());
    }

    public function testGetData()
    {
        $this->assertEquals($this->itemsData->getData(), $this->stub->getData());
    }

    public function testGetStatus()
    {
        $this->assertEquals($this->itemsData->getStatus(), $this->stub->getStatus());
    }

    public function testGetStatusTime()
    {
        $this->assertEquals($this->itemsData->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->itemsData->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->itemsData->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
