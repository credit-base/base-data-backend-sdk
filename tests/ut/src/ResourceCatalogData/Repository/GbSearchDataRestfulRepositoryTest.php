<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\GbSearchDataMockFactory;
use BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataRestfulAdapter;

class GbSearchDataRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(GbSearchDataRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIGbSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(GbSearchDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testConfirm()
    {
        $id = 1;
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData($id);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->confirm(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($gbSearchData);
    }

    public function testDeletes()
    {
        $id = 1;
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData($id);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->deletes(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($gbSearchData);
    }

    public function testDisable()
    {
        $id = 1;
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData($id);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->disable(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($gbSearchData);
    }
}
