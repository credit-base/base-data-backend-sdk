<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory;
use BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter;

class WbjSearchDataRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WbjSearchDataRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIWbjSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(WbjSearchDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testDisable()
    {
        $id = 1;
        $wbjSearchData = WbjSearchDataMockFactory::generateWbjSearchData($id);
        
        $adapter = $this->prophesize(IWbjSearchDataAdapter::class);
        $adapter->disable(Argument::exact($wbjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($wbjSearchData);
    }
}
