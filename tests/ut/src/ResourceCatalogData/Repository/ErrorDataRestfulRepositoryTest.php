<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Adapter\ErrorData\ErrorDataRestfulAdapter;

class ErrorDataRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ErrorDataRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIErrorDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\ErrorDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\ErrorDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(ErrorDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
