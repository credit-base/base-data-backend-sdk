<?php
namespace BaseSdk\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory;
use BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use BaseSdk\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataRestfulAdapter;

class BjSearchDataRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BjSearchDataRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIBjSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(BjSearchDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testConfirm()
    {
        $id = 1;
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData($id);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->confirm(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($bjSearchData);
    }

    public function testDeletes()
    {
        $id = 1;
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData($id);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->deletes(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($bjSearchData);
    }

    public function testDisable()
    {
        $id = 1;
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData($id);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->disable(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($bjSearchData);
    }
}
