<?php
namespace BaseSdk\ResourceCatalogData\Adapter\GbSearchData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockGbSearchDataRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new GbSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIGbSearchDataAdapter()
    {
        $adapter = new GbSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\GbSearchDataRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('gbSearchData', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'GB_SEARCH_DATA_LIST',
                GbSearchDataRestfulAdapter::SCENARIOS['GB_SEARCH_DATA_LIST']
            ],
            [
                'GB_SEARCH_DATA_FETCH_ONE',
                GbSearchDataRestfulAdapter::SCENARIOS['GB_SEARCH_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Model\NullGbSearchData',
            $this->adapter->getNullObject()
        );
    }

    private function initGbSearchData(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockGbSearchDataRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $gbSearchData = \BaseSdk\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('gbSearchData');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $gbSearchData;
    }

    public function testConfirm()
    {
        $gbSearchData = $this->initGbSearchData(true);

        $result = $this->adapter->confirm($gbSearchData);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $gbSearchData = $this->initGbSearchData(false);

        $result = $this->adapter->confirm($gbSearchData);
        $this->assertFalse($result);
    }

    public function testDeletes()
    {
        $gbSearchData = $this->initGbSearchData(true);

        $result = $this->adapter->deletes($gbSearchData);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $gbSearchData = $this->initGbSearchData(false);

        $result = $this->adapter->deletes($gbSearchData);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $gbSearchData = $this->initGbSearchData(true);

        $result = $this->adapter->disable($gbSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $gbSearchData = $this->initGbSearchData(false);

        $result = $this->adapter->disable($gbSearchData);
        $this->assertFalse($result);
    }
}
