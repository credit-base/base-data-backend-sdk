<?php
namespace BaseSdk\ResourceCatalogData\Adapter\WbjSearchData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockWbjSearchDataRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new WbjSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIWbjSearchDataAdapter()
    {
        $adapter = new WbjSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\WbjSearchDataRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('wbjSearchData', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'WBJ_SEARCH_DATA_LIST',
                WbjSearchDataRestfulAdapter::SCENARIOS['WBJ_SEARCH_DATA_LIST']
            ],
            [
                'WBJ_SEARCH_DATA_FETCH_ONE',
                WbjSearchDataRestfulAdapter::SCENARIOS['WBJ_SEARCH_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Model\NullWbjSearchData',
            $this->adapter->getNullObject()
        );
    }

    private function initWbjSearchData(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockWbjSearchDataRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $wbjSearchData = \BaseSdk\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('wbjSearchData');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $wbjSearchData;
    }

    public function testDisable()
    {
        $wbjSearchData = $this->initWbjSearchData(true);

        $result = $this->adapter->disable($wbjSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $wbjSearchData = $this->initWbjSearchData(false);

        $result = $this->adapter->disable($wbjSearchData);
        $this->assertFalse($result);
    }
}
