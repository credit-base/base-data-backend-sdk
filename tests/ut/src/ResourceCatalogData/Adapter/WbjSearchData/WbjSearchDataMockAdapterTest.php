<?php
namespace BaseSdk\ResourceCatalogData\Adapter\WbjSearchData;

use PHPUnit\Framework\TestCase;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData;

class WbjSearchDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new WbjSearchDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testDisable()
    {
        $this->assertTrue($this->adapter->disable(new WbjSearchData()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Model\WbjSearchData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\ResourceCatalogData\Model\WbjSearchData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\ResourceCatalogData\Model\WbjSearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
