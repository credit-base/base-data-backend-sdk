<?php
namespace BaseSdk\ResourceCatalogData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

class ErrorDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockErrorDataRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new ErrorDataRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIErrorDataAdapter()
    {
        $adapter = new ErrorDataRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\ErrorDataRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('errorDatas', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'ERROR_DATA_LIST',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_LIST']
            ],
            [
                'ERROR_DATA_FETCH_ONE',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Model\NullErrorData',
            $this->adapter->getNullObject()
        );
    }
}
