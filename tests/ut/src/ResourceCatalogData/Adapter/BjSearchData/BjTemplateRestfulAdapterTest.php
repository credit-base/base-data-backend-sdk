<?php
namespace BaseSdk\ResourceCatalogData\Adapter\BjSearchData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockBjSearchDataRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new BjSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIBjSearchDataAdapter()
    {
        $adapter = new BjSearchDataRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Translator\BjSearchDataRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('bjSearchData', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'BJ_SEARCH_DATA_LIST',
                BjSearchDataRestfulAdapter::SCENARIOS['BJ_SEARCH_DATA_LIST']
            ],
            [
                'BJ_SEARCH_DATA_FETCH_ONE',
                BjSearchDataRestfulAdapter::SCENARIOS['BJ_SEARCH_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Model\NullBjSearchData',
            $this->adapter->getNullObject()
        );
    }

    private function initBjSearchData(bool $result)
    {
        $this->adapter = $this->getMockBuilder(MockBjSearchDataRestfulAdapter::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $bjSearchData = \BaseSdk\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $this->adapter->expects($this->once())->method('patch');
        $this->adapter->expects($this->once())->method('getResource')->willReturn('bjSearchData');
        $this->adapter->expects($this->once())->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->adapter->expects($this->once())->method('translateToObject');
        }
        
        return $bjSearchData;
    }

    public function testConfirm()
    {
        $bjSearchData = $this->initBjSearchData(true);

        $result = $this->adapter->confirm($bjSearchData);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $bjSearchData = $this->initBjSearchData(false);

        $result = $this->adapter->confirm($bjSearchData);
        $this->assertFalse($result);
    }

    public function testDeletes()
    {
        $bjSearchData = $this->initBjSearchData(true);

        $result = $this->adapter->deletes($bjSearchData);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $bjSearchData = $this->initBjSearchData(false);

        $result = $this->adapter->deletes($bjSearchData);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $bjSearchData = $this->initBjSearchData(true);

        $result = $this->adapter->disable($bjSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $bjSearchData = $this->initBjSearchData(false);

        $result = $this->adapter->disable($bjSearchData);
        $this->assertFalse($result);
    }
}
