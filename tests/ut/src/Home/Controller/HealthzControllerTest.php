<?php
namespace Home\Controller;

use PHPUnit\Framework\TestCase;

class HealthzControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new HealthzController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testHealthz()
    {
        $result = $this->controller->healthz();

        $this->assertTrue($result);
    }
}
