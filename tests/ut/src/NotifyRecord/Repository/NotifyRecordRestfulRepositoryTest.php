<?php
namespace BaseSdk\NotifyRecord\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseSdk\NotifyRecord\Adapter\NotifyRecord\NotifyRecordRestfulAdapter;

class NotifyRecordRestfulRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(NotifyRecordRestfulRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsINotifyRecordAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\NotifyRecordRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\NotifyRecordMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(NotifyRecordRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
