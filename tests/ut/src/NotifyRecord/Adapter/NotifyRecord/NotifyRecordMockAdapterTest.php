<?php
namespace BaseSdk\NotifyRecord\Adapter\NotifyRecord;

use PHPUnit\Framework\TestCase;

class NotifyRecordMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new NotifyRecordMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Model\NotifyRecord',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\NotifyRecord\Model\NotifyRecord',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseSdk\NotifyRecord\Model\NotifyRecord',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
