<?php
namespace BaseSdk\NotifyRecord\Adapter\NotifyRecord;

use PHPUnit\Framework\TestCase;

class NotifyRecordRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockNotifyRecordRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new NotifyRecordRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsINotifyRecordAdapter()
    {
        $adapter = new NotifyRecordRestfulAdapter();

        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('notifyRecords', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'NOTIFY_RECORD_LIST',
                NotifyRecordRestfulAdapter::SCENARIOS['NOTIFY_RECORD_LIST']
            ],
            [
                'NOTIFY_RECORD_FETCH_ONE',
                NotifyRecordRestfulAdapter::SCENARIOS['NOTIFY_RECORD_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Model\NullNotifyRecord',
            $this->adapter->getNullObject()
        );
    }
}
