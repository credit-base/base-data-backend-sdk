<?php
namespace BaseSdk\NotifyRecord\Utils;

trait NotifyRecordRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $notifyRecord
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $notifyRecord->getId());
        }
        
        $this->assertEquals(
            $expectedArray['data']['attributes']['name'],
            $notifyRecord->getName()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['hash'],
            $notifyRecord->getHash()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['status'],
            $notifyRecord->getStatus()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['statusTime'],
            $notifyRecord->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['createTime'],
            $notifyRecord->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['updateTime'],
            $notifyRecord->getUpdateTime()
        );
    }
}
