<?php
namespace BaseSdk\NotifyRecord\Utils;

use BaseSdk\NotifyRecord\Model\NotifyRecord;

class MockFactory
{
    public static function generateNotifyRecord(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : NotifyRecord {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $name = self::generateName($faker, $value);
        $hash = self::generateHash($faker, $value);
        $status = self::generateStatus($faker, $value);
        $statusTime = $faker->unixTime();
        $createTime = $faker->unixTime();
        $updateTime = $faker->unixTime();


        $notifyRecord = new NotifyRecord(
            $id,
            $name,
            $hash,
            $status,
            $statusTime,
            $createTime,
            $updateTime
        );

        return $notifyRecord;
    }

    private static function generateName($faker, $value) : string
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateHash($faker, $value) : string
    {
        return isset($value['hash']) ? $value['hash'] : $faker->md5();
    }

    private static function generateStatus($faker, $value) : int
    {
        return isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                NotifyRecord::STATUS
            );
    }
}
