<?php
namespace BaseSdk\NotifyRecord\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\NotifyRecord\Model\NullNotifyRecord;
use BaseSdk\NotifyRecord\Utils\NotifyRecordRestfulUtils;

class NotifyRecordRestfulTranslatorTest extends TestCase
{
    use NotifyRecordRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NotifyRecordRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $notifyRecord = \BaseSdk\NotifyRecord\Utils\MockFactory::generateNotifyRecord(1);

        $expression['data']['id'] = $notifyRecord->getId();
        $expression['data']['attributes']['name'] = $notifyRecord->getName();
        $expression['data']['attributes']['hash'] = $notifyRecord->getHash();

        $expression['data']['attributes']['createTime'] = $notifyRecord->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $notifyRecord->getUpdateTime();
        $expression['data']['attributes']['status'] = $notifyRecord->getStatus();
        $expression['data']['attributes']['statusTime'] = $notifyRecord->getStatusTime();

        $notifyRecordObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\NotifyRecord\Model\NotifyRecord', $notifyRecordObject);
        $this->compareArrayAndObject($expression, $notifyRecordObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $notifyRecord = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\NotifyRecord\Model\NullNotifyRecord', $notifyRecord);
    }

    public function testObjectToArray()
    {
        $notifyRecord = NullNotifyRecord::getInstance();

        $expression = $this->translator->objectToArray($notifyRecord);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
