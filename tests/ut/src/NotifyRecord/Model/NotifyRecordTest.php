<?php
namespace BaseSdk\NotifyRecord\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NotifyRecordTest extends TestCase
{
    private $stub;

    private $notifyRecord;

    public function setUp()
    {
        $this->notifyRecord = \BaseSdk\NotifyRecord\Utils\MockFactory::generateNotifyRecord(1);
        $this->stub = new NotifyRecord(
            $this->notifyRecord->getId(),
            $this->notifyRecord->getName(),
            $this->notifyRecord->getHash(),
            $this->notifyRecord->getStatus(),
            $this->notifyRecord->getStatusTime(),
            $this->notifyRecord->getCreateTime(),
            $this->notifyRecord->getUpdateTime()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->notifyRecord);
    }

    public function testGetId()
    {
        $this->assertEquals($this->notifyRecord->getId(), $this->stub->getId());
    }

    public function testGetName()
    {
        $this->assertEquals($this->notifyRecord->getName(), $this->stub->getName());
    }

    public function testGetHash()
    {
        $this->assertEquals($this->notifyRecord->getHash(), $this->stub->getHash());
    }
    
    public function testGetStatus()
    {
        $this->assertEquals($this->notifyRecord->getStatus(), $this->stub->getStatus());
    }
    
    public function testGetStatusTime()
    {
        $this->assertEquals($this->notifyRecord->getStatusTime(), $this->stub->getStatusTime());
    }

    public function testGetCreateTime()
    {
        $this->assertEquals($this->notifyRecord->getCreateTime(), $this->stub->getCreateTime());
    }

    public function testGetUpdateTime()
    {
        $this->assertEquals($this->notifyRecord->getUpdateTime(), $this->stub->getUpdateTime());
    }
}
