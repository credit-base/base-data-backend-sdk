<?php
namespace BaseSdk\NotifyRecord\Model;

use PHPUnit\Framework\TestCase;

class NullNotifyRecordTest extends TestCase
{
    private $notifyRecord;

    public function setUp()
    {
        $this->notifyRecord = $this->getMockBuilder(NullNotifyRecord::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->notifyRecord);
    }

    public function testExtendsNotifyRecord()
    {
        $this->assertInstanceof('BaseSdk\NotifyRecord\Model\NotifyRecord', $this->notifyRecord);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->notifyRecord);
    }
}
