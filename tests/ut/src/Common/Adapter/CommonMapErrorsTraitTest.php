<?php
namespace BaseSdk\Common\Adapter;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CommonMapErrorsTraitTest extends TestCase
{
    private $trait;
    
    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockCommonMapErrorsTraitObject::class)
            ->setMethods(
                [
                    'lastErrorId',
                    'lastErrorPointer'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testMapErrors()
    {
        $id = 10;
        $pointer = 'test';

        $this->trait->expects($this->once())->method('lastErrorId')->willReturn($id);
        $this->trait->expects($this->once())->method('lastErrorPointer')->willReturn($pointer);
        $this->trait->mapErrorsPublic();

        $this->assertEquals($id, Core::getLastError()->getId());
        $this->assertEquals($pointer, Core::getLastError()->getSource()['pointer']);
    }
}
