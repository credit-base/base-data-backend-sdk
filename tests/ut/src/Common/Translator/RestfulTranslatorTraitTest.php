<?php
namespace BaseSdk\Common\Translator;

use Marmot\Core;
use Marmot\Common\Model\Object;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class RestfulTranslatorTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockRestfulTranslatorTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    //includedAttributes
    public function testIncludedAttributes()
    {
        $included = array(
            array('type' => 'test', 'id' => 1, 'attributes' => array('test' => 'test')),
            array('type' => 'testTwo', 'id' => 2)
        );
        
        $result = array(
            'test' => array(
                '1' => array('test' => 'test')
            ),
            'testTwo' => array(
                '2' => ''
            )
        );

        $execResult = $this->trait->publicIncludedAttributes($included);

        $this->assertEquals($execResult, $result);
    }

    //includedRelationships
    public function testIncludedRelationships()
    {
        $included = array(
            array('type' => 'test', 'id' => 1, 'relationships' => array('test' => 'test')),
        );
        
        $result = array(
            'test' => array(
                '1' => array('test' => 'test')
            ),
        );

        $execResult = $this->trait->publicIncludedRelationships($included);

        $this->assertEquals($execResult, $result);
    }

    //changeArrayFormat
    public function testChangeArrayFormat()
    {
        $included = array(
            array('type' => 'test', 'id' => 1, 'relationships' => array('test' => 'test')),
        );

        $relationships = array('relationships');

        $relationshipResult['data'] = $relationships;
        $relationshipResult['included'] = $included;

        $execResult = $this->trait->publicChangeArrayFormat($relationships, $included);

        $this->assertEquals($execResult, $relationshipResult);
    }

    protected function object()
    {
        return new class{
            use Object;
        };
    }

    //arrayToObjects-emptyFail
    public function testArrayToObjectsEmptyFail()
    {
        $expression = array();

        $execResult = $this->trait->arrayToObjects($expression);

        $this->assertEquals($execResult, array([], 0));
    }

    //arrayToObjects-dataEmptyFail
    public function testArrayToObjectsFail()
    {
        $trait = $this->getMockBuilder(
            MockRestfulTranslatorTrait::class
        )->setMethods(['translateToObject'])->getMock();

        $expression['data']['id'] = 1;
        $object = $this->object();

        $trait->expects($this->once())->method('translateToObject')->with($expression)->willReturn($object);

        $result = [[1 => $object], 1];

        $execResult = $trait->arrayToObjects($expression);

        $this->assertEquals($execResult, $result);
    }

    //arrayToObjects
    public function testArrayToObjectsSuccess()
    {
        $trait = $this->getMockBuilder(
            MockRestfulTranslatorTrait::class
        )->setMethods(['translateToObject'])->getMock();

        $expression = array(
            'data' => array(array('id' => 1)),
            'included' => array('included'),
            'meta' => array('count' => 1)
        );

        $object = $this->object();

        $trait->expects($this->any())->method('translateToObject')->willReturn($object);

        $result[1] = $object;

        $execResult = $trait->arrayToObjects($expression);

        $this->assertEquals($execResult, [$result, 1]);
    }

    //relationship
    public function testRelationship()
    {
        $trait = $this->getMockBuilder(
            MockRestfulTranslatorTrait::class
        )->setMethods(['includedAttributes', 'includedRelationships'])->getMock();

        $included = array('included');

        $attributes = array(
            'test' => array(
                '1' => array('test' => 'test')
            )
        );

        $includedRelationships = array(
            'test' => array(
                '1' => array('test' => 'test')
            ),
        );

        $relationships = array(
            array('data' => array('type' => 'test', 'id' => 1))
        );

        $trait->expects($this->once())->method('includedAttributes')->with($included)->willReturn($attributes);
        $trait->expects($this->once())->method(
            'includedRelationships'
        )->with($included)->willReturn($includedRelationships);

        $relationshipResult = array(
            array(
                'data' => array(
                    'type' => 'test',
                    'id' => 1,
                    'attributes' => array('test' => 'test'),
                    'relationships' => array('test' => 'test')
                )
            )
        );

        $execResult = $trait->publicRelationship($included, $relationships);

        $this->assertEquals($execResult, $relationshipResult);
    }

    public function testRelationshipMultiple()
    {
        $trait = $this->getMockBuilder(
            MockRestfulTranslatorTrait::class
        )->setMethods(['includedAttributes', 'includedRelationships'])->getMock();

        $included = array('included');

        $attributes = array('test' => array('1' => array('attributes')));

        $includedRelationships = array('test' => array('1' => array('relationships')));

        $relationships = array(array('data' => array(array('type' => 'test', 'id' => 1))));

        $trait->expects($this->once())->method('includedAttributes')->with($included)->willReturn($attributes);
        $trait->expects($this->once())->method(
            'includedRelationships'
        )->with($included)->willReturn($includedRelationships);

        $relationshipResult = array(
            array(
                'data' => array(
                    array(
                        'type' => 'test',
                        'id' => 1,
                        'attributes' => array('attributes'),
                        'relationships' => array('relationships')
                    )
                )
            )
        );

        $execResult = $trait->publicRelationship($included, $relationships);

        $this->assertEquals($execResult, $relationshipResult);
    }
}
