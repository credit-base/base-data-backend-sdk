<?php
namespace BaseSdk\Common\Translator;

use PHPUnit\Framework\TestCase;

class NullRestfulTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new NullRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testExtendsNullTranslator()
    {
        $this->assertInstanceof('Marmot\Basecode\Classes\NullTranslator', $this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceof('Marmot\Interfaces\IRestfulTranslator', $this->translator);
    }

    public function testArrayToObjects()
    {
        $result = $this->translator->arrayToObjects(array());

        $this->assertEquals(array(), $result);
    }
}
