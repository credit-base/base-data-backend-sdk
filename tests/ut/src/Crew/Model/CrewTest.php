<?php
namespace BaseSdk\Crew\Model;

use PHPUnit\Framework\TestCase;

class CrewTest extends TestCase
{
    private $stub;

    private $crew;

    public function setUp()
    {
        $this->crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);
        $this->stub = new Crew(
            $this->crew->getId()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->crew);
    }

    public function testGetId()
    {
        $this->assertEquals($this->crew->getId(), $this->stub->getId());
    }
}
