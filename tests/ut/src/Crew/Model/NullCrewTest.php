<?php
namespace BaseSdk\Crew\Model;

use PHPUnit\Framework\TestCase;

class NullCrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = $this->getMockBuilder(NullCrew::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceof('BaseSdk\Crew\Model\Crew', $this->crew);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->crew);
    }
}
