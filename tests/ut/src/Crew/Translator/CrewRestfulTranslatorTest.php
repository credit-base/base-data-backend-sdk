<?php
namespace BaseSdk\Crew\Translator;

use PHPUnit\Framework\TestCase;

use BaseSdk\Crew\Model\NullCrew;
use BaseSdk\Crew\Utils\CrewRestfulUtils;

class CrewRestfulTranslatorTest extends TestCase
{
    use CrewRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $crew = \BaseSdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression['data']['id'] = $crew->getId();

        $crewObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Crew\Model\Crew', $crewObject);
        $this->compareArrayAndObject($expression, $crewObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $crew = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseSdk\Crew\Model\NullCrew', $crew);
    }

    public function testObjectToArray()
    {
        $crew = NullCrew::getInstance();

        $expression = $this->translator->objectToArray($crew);

        $this->assertEquals(
            [],
            $expression
        );
    }
}
