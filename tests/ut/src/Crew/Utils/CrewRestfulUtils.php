<?php
namespace BaseSdk\Crew\Utils;

trait CrewRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $crew->getId());
        }
    }
}
