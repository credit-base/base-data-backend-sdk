<?php
namespace BaseSdk\Crew\Utils;

use BaseSdk\Crew\Model\Crew;

class MockFactory
{
    public static function generateCrew(
        int $id = 0
    ) : Crew {
        $crew = new Crew($id);
        return $crew;
    }
}
