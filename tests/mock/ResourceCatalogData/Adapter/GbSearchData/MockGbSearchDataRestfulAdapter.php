<?php
namespace BaseSdk\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockGbSearchDataRestfulAdapter extends GbSearchDataRestfulAdapter
{
    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
