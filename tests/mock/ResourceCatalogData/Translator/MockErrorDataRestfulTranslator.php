<?php
namespace BaseSdk\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\Template\Translator\TranslatorFactory;

class MockErrorDataRestfulTranslator extends ErrorDataRestfulTranslator
{
    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getTemplateTranslatorFactory() : TranslatorFactory
    {
        return parent::getTemplateTranslatorFactory();
    }

    public function getErrorTemplateRestfulTranslator(int $category) : IRestfulTranslator
    {
        return parent::getErrorTemplateRestfulTranslator($category);
    }

    public function getErrorItemsDataRestfulTranslator() : ErrorItemsDataRestfulTranslator
    {
        return parent::getErrorItemsDataRestfulTranslator();
    }

    public function getTaskRestfulTranslator() : TaskRestfulTranslator
    {
        return parent::getTaskRestfulTranslator();
    }
}
