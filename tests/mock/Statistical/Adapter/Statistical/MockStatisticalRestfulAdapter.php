<?php
namespace BaseSdk\Statistical\Adapter\Statistical;

use BaseSdk\Statistical\Model\Statistical;

use Marmot\Interfaces\IRestfulTranslator;

class MockStatisticalRestfulAdapter extends StatisticalRestfulAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        unset($filter);
        return new Statistical();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getResource() : string
    {
        return parent::getResource();
    }
}
