<?php
namespace BaseSdk\Common\Adapter;

use Marmot\Interfaces\INull;

class MockFetchAbleRestfulAdapter
{
    use FetchAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    protected function getNullObject() : INull
    {
    }
    
    public function fetchOneActionPublic(int $id)
    {
        return $this->fetchOneAction($id);
    }

    public function fetchListActionPublic(array $ids)
    {
        return $this->fetchListAction($ids);
    }

    public function filterActionPublic(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $offset, $size);
    }
}
