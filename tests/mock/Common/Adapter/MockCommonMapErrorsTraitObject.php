<?php
namespace BaseSdk\Common\Adapter;

class MockCommonMapErrorsTraitObject
{
    use CommonMapErrorsTrait;

    public function lastErrorId() : int
    {
        return 0;
    }

    public function lastErrorPointer()
    {
        return 'test';
    }

    public function mapErrorsPublic() : void
    {
        $this->mapErrors();
    }
}
