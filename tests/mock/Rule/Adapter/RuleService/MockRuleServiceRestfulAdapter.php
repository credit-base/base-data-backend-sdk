<?php
namespace BaseSdk\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockRuleServiceRestfulAdapter extends RuleServiceRestfulAdapter
{
    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
