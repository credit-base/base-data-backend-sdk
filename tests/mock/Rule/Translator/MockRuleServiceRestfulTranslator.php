<?php
namespace BaseSdk\Rule\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\Crew\Translator\CrewRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

use BaseSdk\Template\Translator\TranslatorFactory;

class MockRuleServiceRestfulTranslator extends RuleServiceRestfulTranslator
{
    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getTemplateTranslatorFactory() : TranslatorFactory
    {
        return parent::getTemplateTranslatorFactory();
    }

    public function getTemplateRestfulTranslator(int $category) : IRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator($category);
    }
}
