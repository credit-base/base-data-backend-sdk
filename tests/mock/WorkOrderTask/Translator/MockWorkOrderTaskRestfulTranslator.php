<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class MockWorkOrderTaskRestfulTranslator extends WorkOrderTaskRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getParentTaskRestfulTranslator() : ParentTaskRestfulTranslator
    {
        return parent::getParentTaskRestfulTranslator();
    }

    public function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return parent::getTemplateTranslatorFactory();
    }

    public function getTemplateRestfulTranslator(int $templateType) : IRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator($templateType);
    }
}
