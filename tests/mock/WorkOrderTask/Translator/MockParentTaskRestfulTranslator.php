<?php
namespace BaseSdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseSdk\UserGroup\Translator\UserGroupRestfulTranslator;

class MockParentTaskRestfulTranslator extends ParentTaskRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return parent::getTemplateTranslatorFactory();
    }

    public function getTemplateRestfulTranslator(int $templateType) : IRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator($templateType);
    }

    public function getAssignObjectsArray(array $assignObjects) : array
    {
        return parent::getAssignObjectsArray($assignObjects);
    }
}
