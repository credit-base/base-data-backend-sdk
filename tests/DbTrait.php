<?php
namespace tests;

use Pdo;

use Marmot\Core;

/**
 * @SuppressWarnings(PHPMD)
 */
trait DbTrait
{
    protected function prepare()
    {
        if (!Core::$container->has('connection')) {
            $host = Core::$container->get('database.host');
            $port = Core::$container->get('database.port');
            $user = Core::$container->get('database.user');
            $password = Core::$container->get('database.password');
            $dbname = Core::$container->get('database.dbname');
            $dsn = 'mysql:host='.$host.';port='.$port.';dbname='.$dbname;

            $pdo = new PDO(
                $dsn,
                $user,
                $password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
            );
            $connection = $this->createDefaultDBConnection($pdo, $dbname);
            Core::$container->set('connection', $connection);
        }
    }

    protected function getConnection()
    {
        $this->prepare();
        return Core::$container->get('connection');
    }

    protected function clear(string $tableName)
    {
        Core::$cacheDriver->flushAll();
        $conn = $this->getConnection();
        $pdo = $conn->getConnection();
        $pdo->exec('TRUNCATE TABLE '.$tableName.';');
    }
}
