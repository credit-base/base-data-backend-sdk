<?php
ini_set("display_errors","on");

return [
    //database
    'database.host'     => '172.25.1.11',
    'database.port'     => 5555,
    'database.dbname'   => 'credit_ty',
    'database.user'     => 'credit_ty',
    'database.password' => 'credit_ty',
    'database.tablepre' => 'pcore_',
    'database.charset' => 'utf8mb4',
    //mongo
    'mongo.host' => '',
    'mongo.uriOptions' => [
    ],
    'mongo.driverOptions' => [
    ],
    //cache
    'cache.route.disable' => false,
    //memcached
    'memcached.service'=>[
        ['credit-backend-memcached-0.credit-backend-memcached',11211],
        ['credit-backend-memcached-1.credit-backend-memcached',11211]
    ],

    // 'baseSdk.url' => 'http://api.base.qixinyun.com/',
    // 'sdk.url' => 'http://api.base.qixinyun.com/',
];
