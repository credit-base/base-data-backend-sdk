# 公共业务基础项目后端服务Sdk包

### 目录

* [简介](#abstract)
* [沟通记录](#communicationRecord)
* [接口文档](#api)
* [参考文档](#tutor)
* [版本记录](#version)

---

### <a name="abstract">简介</a>

用于实现 信用平台2.0 公共业务基础项目 后端服务sdk包.

---

### <a name="communicationRecord">沟通记录</a>

根据沟通日期命名.
	
---

### <a name="api">接口文档</a>

* 接口错误返回说明 `error`
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明")
* 员工接口文档 `crew`
	* [员工接口文档](./docs/Api/crewApi.md "员工接口文档")
* 委办局接口文档 `userGroup`
	* [委办局接口文档](./docs/Api/userGroupApi.md "委办局接口文档")
* 科室接口文档 `department`
	* [科室接口文档](./docs/Api/departmentApi.md "科室接口文档")
* 国标目录接口文档 `gbTemplate`
	* [国标目录接口文档](./docs/Api/gbTemplateApi.md "国标目录接口文档")
* 本级目录接口文档 `bjTemplate`
	* [本级目录接口文档](./docs/Api/bjTemplateApi.md "本级目录接口文档")
* 委办局目录接口文档 `wbjTemplate`
	* [委办局目录接口文档](./docs/Api/wbjTemplateApi.md "委办局目录接口文档")
* 基础目录接口文档 `baseTemplate`
	* [基础目录接口文档](./docs/Api/baseTemplateApi.md "基础目录接口文档")
* 工单任务接口文档 `workOrderTask`
	* [工单任务接口文档](./docs/Api/workOrderTaskApi.md "工单任务接口文档")
* 原始资源目录数据接口文档 `ysSearchData`
	* [原始资源目录数据接口文档](./docs/Api/ysSearchDataApi.md "原始资源目录数据接口文档")
* 委办局资源目录数据接口文档 `wbjSearchData`
	* [委办局资源目录数据接口文档](./docs/Api/wbjSearchDataApi.md "委办局资源目录数据接口文档")
* 本级资源目录数据接口文档 `bjSearchData`
	* [本级资源目录数据接口文档](./docs/Api/bjSearchDataApi.md "本级资源目录数据接口文档")
* 国标资源目录数据接口文档 `gbSearchData`
	* [国标资源目录数据接口文档](./docs/Api/gbSearchDataApi.md "国标资源目录数据接口文档")
* 任务接口文档 `task`
	* [任务接口文档](./docs/Api/taskApi.md "任务接口文档")
* 失败数据接口文档 `errorData`
	* [失败数据接口文档](./docs/Api/errorDataApi.md "失败数据接口文档")
* 规则接口文档 `rule`
	* [规则接口文档](./docs/Api/ruleApi.md "规则接口文档")
* 规则审核接口文档 `unAuditedRule`
	* [规则审核接口文档](./docs/Api/unAuditedRuleApi.md "规则审核接口文档")
* 新闻接口文档 `news`
	* [新闻接口文档](./docs/Api/newsApi.md "新闻接口文档")
* 新闻审核接口文档 `unAuditedNews`
	* [新闻审核接口文档](./docs/Api/unAuditedNewsApi.md "新闻审核接口文档")
* 前台用户接口文档 `member`
	* [前台用户接口文档](./docs/Api/memberApi.md "前台用户接口文档")
* 信用刊物接口文档 `journal`
	* [信用刊物接口文档](./docs/Api/journalApi.md "信用刊物接口文档")
* 信用刊物审核接口文档 `unAuditedJournal`
	* [信用刊物审核接口文档](./docs/Api/unAuditedJournalApi.md "信用刊物审核接口文档")
* 信用随手拍接口文档 `creditPhotography`
	* [信用随手拍接口文档](./docs/Api/creditPhotographyApi.md "信用随手拍接口文档")
* 调度任务系统接口文档 `task`
	* [上传记录接口文档](./docs/Api/notifyRecordApi.md "上传记录接口文档")
* 网站定制接口文档 `websiteCustomize`
	* [网站定制接口文档](./docs/Api/websiteCustomizeApi.md "网站定制接口文档")
* 企业管理接口文档 `enterprise`
	* [企业管理接口文档](./docs/Api/enterpriseApi.md "企业管理接口文档")
* 信用投诉接口文档 `complaint`
	* [信用投诉接口文档](./docs/Api/complaintApi.md "信用投诉接口文档")
* 信用投诉审核接口文档 `unAuditedComplaint`
	* [信用投诉审核接口文档](./docs/Api/unAuditedComplaintApi.md "信用投诉审核接口文档")
* 信用表扬接口文档 `praise`
	* [信用表扬接口文档](./docs/Api/praiseApi.md "信用表扬接口文档")
* 信用表扬审核接口文档 `unAuditedPraise`
	* [信用表扬审核接口文档](./docs/Api/unAuditedPraiseApi.md "信用表扬审核接口文档")
* 异议申诉接口文档 `appeal`
	* [异议申诉接口文档](./docs/Api/appealApi.md "异议申诉接口文档")
* 异议申诉审核接口文档 `unAuditedAppeal`
	* [异议申诉审核接口文档](./docs/Api/unAuditedAppealApi.md "异议申诉审核接口文档")
* 问题反馈接口文档 `feedback`
	* [问题反馈接口文档](./docs/Api/feedbackApi.md "问题反馈接口文档")
* 问题反馈审核接口文档 `unAuditedFeedback`
	* [问题反馈审核接口文档](./docs/Api/unAuditedFeedbackApi.md "问题反馈审核接口文档")
* 信用问答接口文档 `qa`
	* [信用问答接口文档](./docs/Api/qaApi.md "信用问答接口文档")
* 信用问答审核接口文档 `unAuditedQA`
	* [信用问答审核接口文档](./docs/Api/unAuditedQAApi.md "信用问答审核接口文档")

---

### <a name="tutor">参考文档</a>

* [jsonapi媒体协议](http://jsonapi.org/ "jsonapi")
* [框架](https://github.com/chloroplast1983/marmot) 

---
